# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150917235114) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: true do |t|
    t.integer  "user_id"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "dynamic_attribs"
    t.string   "paypal_email"
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "carts", id: false, force: true do |t|
    t.integer "id"
    t.integer "user_id"
    t.string  "attachment_content_type"
    t.string  "attachment_url"
    t.string  "orientation_type"
    t.string  "photo_content_type"
    t.integer "photographer_id"
    t.string  "photographer_name"
    t.string  "preview_url"
    t.decimal "price"
    t.string  "video_content_type"
    t.string  "video_thumbnails"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "credit_cards", force: true do |t|
    t.string   "card_id"
    t.string   "valid_until"
    t.string   "state"
    t.string   "payer_id"
    t.integer  "expire_month"
    t.integer  "expire_year"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "links"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "verified"
    t.integer  "payment_method_id"
    t.string   "card_type"
    t.string   "number"
    t.string   "encrypted_address1"
    t.string   "encrypted_address2"
    t.string   "encrypted_city"
    t.string   "encrypted_postal_code"
    t.string   "encrypted_address_state"
    t.boolean  "is_default"
    t.string   "country_code"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "events", force: true do |t|
    t.integer  "location_id"
    t.integer  "user_id"
    t.date     "date"
    t.time     "time_from"
    t.time     "time_to"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "groups_photos", force: true do |t|
    t.string   "name"
    t.integer  "photo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "group_id"
  end

  create_table "help_tooltip_translations", force: true do |t|
    t.integer  "help_tooltip_id", null: false
    t.string   "locale",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "text"
  end

  add_index "help_tooltip_translations", ["help_tooltip_id"], name: "index_help_tooltip_translations_on_help_tooltip_id", using: :btree
  add_index "help_tooltip_translations", ["locale"], name: "index_help_tooltip_translations_on_locale", using: :btree

  create_table "help_tooltips", force: true do |t|
    t.string   "slug"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "image_assets", force: true do |t|
    t.integer  "user_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "licenses", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "attachment_content_type"
    t.string   "attachment_file_name"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "licenses", ["user_id"], name: "index_licenses_on_user_id", using: :btree

  create_table "location_views", force: true do |t|
    t.integer  "location_id"
    t.integer  "user_id"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "location_owner_id"
  end

  create_table "locations", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "local_names", default: [], array: true
    t.string   "address"
    t.integer  "zoom"
  end

  add_index "locations", ["latitude", "longitude"], name: "index_locations_on_latitude_and_longitude", using: :btree
  add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

  create_table "paid_photos", force: true do |t|
    t.integer  "uuid"
    t.integer  "payment_id"
    t.integer  "source_id"
    t.integer  "photographer_id"
    t.boolean  "is_photo"
    t.boolean  "is_video"
    t.decimal  "price"
    t.datetime "date_time"
    t.integer  "license_id"
    t.integer  "width"
    t.integer  "height"
    t.integer  "location_id"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "video_fingerprint"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "photo_fingerprint"
    t.string   "video_meta"
    t.string   "photo_meta"
    t.integer  "buyer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_methods", force: true do |t|
    t.string   "type"
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.text     "preferences"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.string   "method_type"
  end

  create_table "payments", force: true do |t|
    t.integer  "from"
    t.string   "to",                     default: [], array: true
    t.string   "description"
    t.string   "items",                  default: [], array: true
    t.decimal  "discount"
    t.decimal  "price"
    t.decimal  "tax"
    t.boolean  "verified"
    t.datetime "verified_at"
    t.datetime "refunded"
    t.datetime "refunded_at"
    t.string   "pay_key"
    t.string   "sender_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "payment_method_id"
    t.boolean  "photographers_paid"
    t.datetime "photographers_paid_at"
    t.integer  "credit_card_id"
    t.integer  "paypal_account_id"
    t.string   "customer_location_name"
    t.decimal  "customer_latitude"
    t.decimal  "customer_longitude"
  end

  create_table "paypal_accounts", force: true do |t|
    t.integer "payment_method_id"
    t.integer "user_id"
    t.string  "email"
    t.boolean "is_default"
  end

  add_index "paypal_accounts", ["payment_method_id"], name: "index_paypal_accounts_on_payment_method_id", using: :btree

  create_table "photo_views", force: true do |t|
    t.integer  "photo_id"
    t.integer  "photographer_id"
    t.integer  "user_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "view_type"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", force: true do |t|
    t.integer  "category_id"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "exif"
    t.integer  "user_id"
    t.decimal  "price",                   default: 10.0
    t.string   "local_name"
    t.string   "gps_location"
    t.integer  "watermark_id"
    t.integer  "thumbnail_size"
    t.string   "uuid"
    t.string   "attachment_fingerprint"
    t.datetime "date_time"
    t.integer  "license_id"
    t.integer  "width"
    t.integer  "height"
    t.integer  "thumb_width"
    t.integer  "thumb_height"
    t.integer  "location_id"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "video_fingerprint"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "photo_fingerprint"
    t.string   "video_meta"
    t.string   "photo_meta"
    t.binary   "thumb_base64"
    t.boolean  "verified"
    t.datetime "verified_at"
  end

  add_index "photos", ["license_id"], name: "index_photos_on_license_id", using: :btree
  add_index "photos", ["location_id"], name: "index_photos_on_location_id", using: :btree
  add_index "photos", ["user_id"], name: "index_photos_on_user_id", using: :btree
  add_index "photos", ["watermark_id"], name: "index_photos_on_watermark_id", using: :btree

  create_table "photos_tags", force: true do |t|
    t.integer "photo_id"
    t.integer "tag_id"
  end

  create_table "roles", force: true do |t|
    t.string "name"
  end

  create_table "roles_users", id: false, force: true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "server_stats", force: true do |t|
    t.string   "name"
    t.text     "dynamic_attribs"
    t.datetime "date_time"
    t.string   "timestamps"
    t.string   "unit"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "translations", force: true do |t|
    t.string   "locale"
    t.string   "key"
    t.text     "value"
    t.text     "interpolations"
    t.boolean  "is_proc",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                     default: "",    null: false
    t.string   "encrypted_password",        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "guest",                     default: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "telephone"
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "dropbox_session"
    t.integer  "failed_attempts",           default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "location_id"
    t.string   "encrypted_otp_secret"
    t.string   "encrypted_otp_secret_iv"
    t.string   "encrypted_otp_secret_salt"
    t.boolean  "otp_required_for_login"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["location_id"], name: "index_users_on_location_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vendors", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "vendors", ["email"], name: "index_vendors_on_email", unique: true, using: :btree
  add_index "vendors", ["reset_password_token"], name: "index_vendors_on_reset_password_token", unique: true, using: :btree

  create_table "watermarks", force: true do |t|
    t.string   "name"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "display_type"
    t.string   "position"
    t.integer  "image_asset_id"
    t.float    "stretched_size"
    t.float    "positioned_size"
    t.float    "base_image_height"
    t.float    "base_image_width"
    t.float    "tiled_size"
    t.float    "rotation"
    t.float    "opacity"
  end

  add_index "watermarks", ["image_asset_id"], name: "index_watermarks_on_image_asset_id", using: :btree
  add_index "watermarks", ["user_id"], name: "index_watermarks_on_user_id", using: :btree

end
