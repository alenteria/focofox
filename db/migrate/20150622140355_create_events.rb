class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :location_id
      t.integer :user_id
      t.date :date
      t.time :time_from
      t.time :time_to
      t.string :description
      t.timestamps
    end
  end
end
