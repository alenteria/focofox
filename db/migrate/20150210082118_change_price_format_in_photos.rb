class ChangePriceFormatInPhotos < ActiveRecord::Migration
  def change
  	Photo.where(price: '').update_all({price: 0})
  	change_column :photos, :price, 'decimal USING CAST("price" AS decimal)', default: 0.0
  end
end
