class AddBuyerIdToPaidPhotos < ActiveRecord::Migration
  def change
    add_column :paid_photos, :buyer_id, :integer, index: true
  end
end
