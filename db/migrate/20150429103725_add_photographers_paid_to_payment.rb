class AddPhotographersPaidToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :photographers_paid, :boolean
    add_column :payments, :photographers_paid_at, :datetime
  end
end
