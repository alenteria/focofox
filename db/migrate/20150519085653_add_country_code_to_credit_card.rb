class AddCountryCodeToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :country_code, :string
  end
end
