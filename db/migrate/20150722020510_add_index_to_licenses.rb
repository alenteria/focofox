class AddIndexToLicenses < ActiveRecord::Migration
  def change
  	add_index :licenses, :user_id
  end
end
