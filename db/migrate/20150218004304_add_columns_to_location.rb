class AddColumnsToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :city, :string
    add_column :locations, :province, :string
    add_column :locations, :country, :string
    add_column :locations, :latitude, :float
    add_column :locations, :longitude, :float
    add_column :locations, :local_names, :string, array: true, default: '{}'
  end
end
