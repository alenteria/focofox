class AddPaypalToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :paypal_account_id, :integer
  end
end
