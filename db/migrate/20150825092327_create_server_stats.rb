class CreateServerStats < ActiveRecord::Migration
  def change
    create_table :server_stats do |t|
      t.string :name
      t.text :dynamic_attribs
      t.datetime :date_time
      t.string :timestamps
    end
  end
end
