class CreatePayment < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :from
      t.string :to, array: true, default: []
      t.string :description
      t.string :items, array: true, default: []
      t.decimal :discount
      t.decimal :price
      t.decimal :tax
      t.boolean :verified
      t.datetime :verified_at
      t.datetime :refunded
      t.datetime :refunded_at
      t.string :pay_key
      t.string :sender_email
      t.timestamps
    end
  end
end
