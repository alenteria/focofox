class AddDynamicAttribsToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :customer_location_name, :string
    add_column :payments, :customer_latitude, :decimal
    add_column :payments, :customer_longitude, :decimal
  end
end
