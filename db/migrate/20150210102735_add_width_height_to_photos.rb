class AddWidthHeightToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :width, :integer
    add_column :photos, :height, :integer
    add_column :photos, :thumb_width, :integer
    add_column :photos, :thumb_height, :integer
  end
end
