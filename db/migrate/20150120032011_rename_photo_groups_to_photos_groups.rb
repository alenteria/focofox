class RenamePhotoGroupsToPhotosGroups < ActiveRecord::Migration
  def change
  	if !(table_exists? :groups_photos) && table_exists?(:photo_groups) 
  		rename_table :photo_groups, :groups_photos
  	end

  	if !(table_exists? :groups_photos) && table_exists?(:photos_groups) 
  		rename_table :photos_groups, :groups_photos
  	end

  end
end
