class DropExifs < ActiveRecord::Migration
  def change
  	drop_table :exifs if ActiveRecord::Base.connection.table_exists? :exifs
  end
end
