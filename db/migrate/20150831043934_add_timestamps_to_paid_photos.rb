class AddTimestampsToPaidPhotos < ActiveRecord::Migration
  def change
		add_column(:paid_photos, :created_at, :datetime)
		add_column(:paid_photos, :updated_at, :datetime)
  end
end
