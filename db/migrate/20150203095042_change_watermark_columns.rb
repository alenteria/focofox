class ChangeWatermarkColumns < ActiveRecord::Migration
  def change
  	if column_exists? :watermarks, :stretch_margin
  		remove_column :watermarks, :stretch_margin
  		add_column :watermarks, :stretched_size, :float
  	end
  	if column_exists? :watermarks, :fixed_ratio_margin
  		remove_column :watermarks, :fixed_ratio_margin
  	end
  	if column_exists? :watermarks, :size
  		remove_column :watermarks, :size
  		add_column :watermarks, :positioned_size, :float
  	end

  	if column_exists? :watermarks, :image_height
  		remove_column :watermarks, :image_height
  		add_column :watermarks, :base_image_height, :float
  	end
  	if column_exists? :watermarks, :image_width
  		remove_column :watermarks, :image_width
  		add_column :watermarks, :base_image_width, :float
  	end
  	if column_exists? :watermarks, :tile_size
  		remove_column :watermarks, :tile_size
  		add_column :watermarks, :tiled_size, :float
  	end
  	if column_exists? :watermarks, :rotation
  		remove_column :watermarks, :rotation
  		add_column :watermarks, :rotation, :float
  	end
  	if column_exists? :watermarks, :opacity
  		remove_column :watermarks, :opacity
  		add_column :watermarks, :opacity, :float
  	end
  end
end
