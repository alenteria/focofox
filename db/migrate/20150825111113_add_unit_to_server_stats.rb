class AddUnitToServerStats < ActiveRecord::Migration
  def change
    add_column :server_stats, :unit, :string
  end
end
