class CreatePaidPhotos < ActiveRecord::Migration
  def change
    create_table :paid_photos do |t|
      t.integer :uuid
      t.integer :payment_id
      t.integer :source_id
      t.integer :photographer_id
      t.boolean :is_photo
      t.boolean :is_video
      t.decimal :price
      t.datetime :date_time
      t.integer :license_id
      t.integer :width
      t.integer :height
      t.integer :location_id
      t.string :video_file_name
      t.string :video_content_type
      t.integer :video_file_size
      t.datetime :video_updated_at
      t.string :video_fingerprint
      t.string :photo_file_name
      t.string :photo_content_type
      t.integer :photo_file_size
      t.datetime :photo_updated_at
      t.string :photo_fingerprint
      t.string :video_meta
      t.string :photo_meta
    end
  end
end
