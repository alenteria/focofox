class CreateExifs < ActiveRecord::Migration
  def change
    create_table :exifs do |t|
      t.integer :photo_id
      t.string :camera
      t.string :lens
      t.integer :focal_length
      t.string :shutter_speed
      t.string :aperture
      t.string :iso
      t.datetime :taken
      t.timestamps
    end
  end
end
