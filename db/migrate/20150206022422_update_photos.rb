class UpdatePhotos < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :license
  		remove_column :photos, :license
  		add_column :photos, :license_id, :integer
  	end
  end
end
