class AddCardTypeToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :card_type, :string
    if column_exists? :credit_cards, :type
      remove_column :credit_cards, :type
    end

    if column_exists? :credit_cards, :number
      remove_column :credit_cards, :number
      add_column :credit_cards, :number, :string
    end
  end
end
