class AddMethodTypeToPaymentMethod < ActiveRecord::Migration
  def change
    add_column :payment_methods, :method_type, :integer
  end
end
