class AddColumnStretchMarginToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :stretch_margin, :string
    add_column :watermarks, :fixed_ratio_margin, :string
  end
end
