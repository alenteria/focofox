class AddColumnsHwToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :image_height, :integer
    add_column :watermarks, :image_width, :integer
  end
end
