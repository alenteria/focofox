class CreateCart < ActiveRecord::Migration
  def change
    create_table :carts, id: false do |t|
      t.integer :id
      t.integer :user_id
      t.string :attachment_content_type
      t.string :attachment_url
      t.string :orientation_type
      t.string :photo_content_type
      t.integer :photographer_id
      t.string :photographer_name
      t.string :preview_url
      t.decimal :price
      t.string :video_content_type
      t.string :video_thumbnails
    end
  end
end
