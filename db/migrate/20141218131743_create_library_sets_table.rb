class CreateLibrarySetsTable < ActiveRecord::Migration
  def change
    create_table :library_sets do |t|
      t.string :name
      t.string :slug
      t.boolean :private
      t.string :password
      t.timestamps
    end
  end
end
