class CreateHelpTooltips < ActiveRecord::Migration
  def up
    create_table :help_tooltips do |t|
      t.string :slug
      t.text :text

      t.timestamps
    end

    HelpTooltip.create_translation_table! text: :text
  end

  def down
    drop_table :help_tooltips
    HelpTooltip.drop_translation_table!
  end
end
