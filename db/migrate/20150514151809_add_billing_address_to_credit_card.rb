class AddBillingAddressToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :encrypted_address1, :string
    add_column :credit_cards, :encrypted_address2, :string
    add_column :credit_cards, :encrypted_city, :string
    add_column :credit_cards, :encrypted_postal_code, :string
    add_column :credit_cards, :encrypted_address_state, :string
  end
end
