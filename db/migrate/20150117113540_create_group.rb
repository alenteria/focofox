class CreateGroup < ActiveRecord::Migration
  def change
    create_table :photo_groups do |t|
      t.integer :user_id
      t.string :name
      t.integer :photo_id
      t.timestamps
    end
  end
end
