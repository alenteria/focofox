class ChangePhotoExifRelationship < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :exif_id
		  remove_column :photos, :exif_id
		end
  	
  	unless column_exists? :exifs, :photo_id
  		add_column :exifs, :photo_id, :integer
  	end
  end
end
