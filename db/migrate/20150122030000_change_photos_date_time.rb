class ChangePhotosDateTime < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :date
  		remove_column :photos, :date
  	end

  	if column_exists? :photos, :time
  		remove_column :photos, :time
  	end

  	unless column_exists? :photos, :date_time
  		add_column :photos, :date_time, :datetime
  	end
	
  end
end
