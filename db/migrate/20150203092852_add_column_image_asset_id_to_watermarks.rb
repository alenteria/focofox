class AddColumnImageAssetIdToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :image_asset_id, :integer
  end
end
