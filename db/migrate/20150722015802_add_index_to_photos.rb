class AddIndexToPhotos < ActiveRecord::Migration
  def change
  	add_index :photos, :user_id
  	add_index :photos, :watermark_id
  	add_index :photos, :license_id
  	add_index :photos, :location_id
  end
end
