class AddIsDefaultToPaypalAccount < ActiveRecord::Migration
  def change
    add_column :paypal_accounts, :is_default, :boolean
  end
end
