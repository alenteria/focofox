class AddAccountsDynamicAttribute < ActiveRecord::Migration
  def change
  	add_column :accounts, :dynamic_attribs, :text

  	if column_exists? :accounts, :data
  		remove_column :accounts, :data
  	end
  end
end
