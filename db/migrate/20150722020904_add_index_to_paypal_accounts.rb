class AddIndexToPaypalAccounts < ActiveRecord::Migration
  def change
  	add_index :paypal_accounts, :payment_method_id
  end
end
