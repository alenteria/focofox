class AddColumnLocationOwnerIdToLocationViews < ActiveRecord::Migration
  def change
    add_column :location_views, :location_owner_id, :integer
  end
end
