class AddIndexToLocations < ActiveRecord::Migration
  def change
  	add_index :locations, :user_id
  end
end
