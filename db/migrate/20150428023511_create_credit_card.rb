class CreateCreditCard < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :card_id
      t.string :valid_until
      t.string :state
      t.string :payer_id
      t.string :type
      t.integer :number
      t.integer :expire_month
      t.integer :expire_year
      t.string :first_name
      t.string :last_name
      t.text :links
      t.timestamps
    end
  end
end
