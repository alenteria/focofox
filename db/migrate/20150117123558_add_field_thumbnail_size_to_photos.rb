class AddFieldThumbnailSizeToPhotos < ActiveRecord::Migration
  def change
  	unless column_exists? :photos, :thumbnail_size
  		add_column :photos, :thumbnail_size, :integer
  	end
  end
end
