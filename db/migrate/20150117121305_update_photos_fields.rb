class UpdatePhotosFields < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :title
		  remove_column :photos, :title
		end

		if column_exists? :photos, :description
			remove_column :photos, :description
		end

		if column_exists? :photos, :adult_content
			remove_column :photos, :adult_content
		end

		unless column_exists? :photos, :price
			add_column :photos, :price, :string
		end
		unless column_exists? :photos, :location
			add_column :photos, :location, :string
		end
		unless column_exists? :photos, :local_name
			add_column :photos, :local_name, :string
		end
		unless column_exists? :photos, :gps_location
			add_column :photos, :gps_location, :string
		end
		unless column_exists? :photos, :date
			add_column :photos, :date, :date
		end
		unless column_exists? :photos, :time
			add_column :photos, :time, :time
		end
  end
end
