class AddUserIdToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :user_id, :integer
  end
end
