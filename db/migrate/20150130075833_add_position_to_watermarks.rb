class AddPositionToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :position, :string
    add_column :watermarks, :size, :string
  end
end
