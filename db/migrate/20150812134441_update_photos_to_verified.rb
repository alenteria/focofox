class UpdatePhotosToVerified < ActiveRecord::Migration
  def up
  	Photo.where(verified: [nil, false]).update_all(verified: true)
  end
  def down
  end
end
