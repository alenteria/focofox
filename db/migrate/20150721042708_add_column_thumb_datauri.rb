class AddColumnThumbDatauri < ActiveRecord::Migration
  def change
  	add_column :photos, :thumb_base64, :binary
  end
end
