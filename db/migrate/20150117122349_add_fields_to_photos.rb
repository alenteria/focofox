class AddFieldsToPhotos < ActiveRecord::Migration
  def change
  	unless column_exists? :photos, :watermark_id
  		add_column :photos, :watermark_id, :integer
  	end
  end
end
