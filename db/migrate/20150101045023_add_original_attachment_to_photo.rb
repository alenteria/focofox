class AddOriginalAttachmentToPhoto < ActiveRecord::Migration
  def change
  	add_column :photos, :original_attachment_file_name,  :string
  	add_column :photos, :original_attachment_content_type,  :string
  	add_column :photos, :original_attachment_file_size,  :integer
  	add_column :photos, :original_attachment_updated_at,  :datetime
  end
end
