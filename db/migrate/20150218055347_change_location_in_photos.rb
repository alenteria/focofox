class ChangeLocationInPhotos < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :location
  		remove_column :photos, :location
  	end
  	unless column_exists? :photos, :location_id
  		add_column :photos, :location_id, :integer
  	end
  end
end
