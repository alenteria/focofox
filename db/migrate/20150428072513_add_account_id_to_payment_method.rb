class AddAccountIdToPaymentMethod < ActiveRecord::Migration
  def change
    add_column :payment_methods, :account_id, :integer
  end
end
