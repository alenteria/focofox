class UpdateGroupsFields < ActiveRecord::Migration
  def change
    if column_exists? :groups, :timestamps
      remove_column :groups, :timestamps
    end
    add_column(:groups, :created_at, :datetime)
    add_column(:groups, :updated_at, :datetime)
  end
end
