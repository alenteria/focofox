class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :user, index: true
      t.text :data
      t.string :type
      t.timestamps
    end
  end
end
