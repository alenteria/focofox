class CreateLicense < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.string :title
      t.text :description
      t.string :attachment_content_type
      t.string :attachment_file_name
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.timestamps
    end
  end
end
