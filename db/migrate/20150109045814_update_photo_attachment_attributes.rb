class UpdatePhotoAttachmentAttributes < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :original_attachment_file_name
  		remove_column :photos, :original_attachment_file_name
  	end

  	if column_exists? :photos, :original_attachment_content_type  
  		remove_column :photos, :original_attachment_content_type
  	end

  	if column_exists? :photos, :original_attachment_file_size
  		remove_column :photos, :original_attachment_file_size
  	end
		
		if column_exists? :photos, :original_attachment_updated_at  	
  		remove_column :photos, :original_attachment_updated_at
  	end

  end
end
