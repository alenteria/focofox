class AddVerifiedToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :verified, :boolean
    add_column :photos, :verified_at, :datetime
  end
end
