class AddAttachmentFingerprintToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :attachment_fingerprint, :string
  end
end
