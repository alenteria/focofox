class CreatePaypalAccount < ActiveRecord::Migration
  def change
    create_table :paypal_accounts do |t|
      t.integer :payment_method_id
      t.integer :user_id
      t.string :email
    end
  end
end
