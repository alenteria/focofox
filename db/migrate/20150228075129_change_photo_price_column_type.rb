class ChangePhotoPriceColumnType < ActiveRecord::Migration
  def change
  	change_column_default :photos, :price, 10
  end
end
