class AddMetaFieldToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :video_meta, :string
    add_column :photos, :photo_meta, :string
  end
end
