class AddVerifiedToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :verified, :boolean
  end
end
