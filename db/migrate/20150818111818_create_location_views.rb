class CreateLocationViews < ActiveRecord::Migration
  def change
    create_table :location_views do |t|
      t.integer :location_id, index: true
      t.integer :user_id, index: true
      t.integer :count
      t.timestamps
    end
  end
end
