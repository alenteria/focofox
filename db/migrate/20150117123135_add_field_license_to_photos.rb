class AddFieldLicenseToPhotos < ActiveRecord::Migration
  def change
  	unless column_exists? :photos, :license
  		add_column :photos, :license, :string
  	end
  end
end
