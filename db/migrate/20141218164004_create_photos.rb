class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
    	t.string :title
    	t.string :description
    	t.boolean :adult_content
      t.integer :category_id
      t.integer :set_id
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.timestamps
    end
  end
end
