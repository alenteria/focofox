class AddUserIdToLibrarySets < ActiveRecord::Migration
  def change
    add_column :library_sets, :user_id, :integer
  end
end
