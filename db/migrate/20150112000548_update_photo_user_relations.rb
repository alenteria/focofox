class UpdatePhotoUserRelations < ActiveRecord::Migration
  def change
  	if column_exists? :photos, :set_id
  		remove_column :photos, :set_id
  	end

  	unless column_exists? :photos, :user_id
  		add_column :photos, :user_id, :integer
  	end
  end
end
