class ChangePhotoGroupsColummns < ActiveRecord::Migration
  def change
  	if column_exists? :photo_groups, :user_id
  		remove_column :photo_groups, :user_id
  	end

  	unless column_exists? :photo_groups, :group_id
  		add_column :photo_groups, :group_id, :integer
  	end
  end
end
