class CreatePhotoViews < ActiveRecord::Migration
  def change
    create_table :photo_views do |t|
      t.integer :photo_id
      t.integer :photographer_id
      t.integer :user_id
      t.float :latitude
      t.float :longitude
      t.string :view_type
      t.integer :count
      t.timestamps
    end
  end
end
