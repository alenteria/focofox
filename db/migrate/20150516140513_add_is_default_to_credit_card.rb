class AddIsDefaultToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :is_default, :boolean
  end
end
