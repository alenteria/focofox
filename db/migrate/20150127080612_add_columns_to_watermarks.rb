class AddColumnsToWatermarks < ActiveRecord::Migration
  def change
    add_column :watermarks, :display_type, :string
    add_column :watermarks, :tile_size, :string
    add_column :watermarks, :rotation, :string
    add_column :watermarks, :opacity, :string
  end
end
