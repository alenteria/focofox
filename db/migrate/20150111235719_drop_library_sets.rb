class DropLibrarySets < ActiveRecord::Migration
  def change
  	drop_table :library_sets if ActiveRecord::Base.connection.table_exists? :library_sets
  end
end
