class AddIndexToWatermarks < ActiveRecord::Migration
  def change
  	add_index :watermarks, :user_id
  	add_index :watermarks, :image_asset_id
  end
end
