class AddVideoAttachmentToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :video_file_name, :string
    add_column :photos, :video_content_type, :string
    add_column :photos, :video_file_size, :integer
    add_column :photos, :video_updated_at, :datetime
    add_column :photos, :video_fingerprint, :string
  end
end
