class ChangePaymentMethodMethodType < ActiveRecord::Migration
  def change
    if column_exists? :payment_methods, :method_type
      remove_column :payment_methods, :method_type
      add_column :payment_methods, :method_type, :string
    end
  end
end
