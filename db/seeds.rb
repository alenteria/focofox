# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# do not use this file to migrate data -- its purpose is for initializing the database

%w(roles categories).each do |filename|
  constant = filename.singularize.camelize.constantize

  puts "Adding #{filename.gsub(/_/,' ').titleize}:" unless ["test", "cucumber"].include?(Rails.env)
  items = YAML.load_file(Rails.root.join('db/seeds', filename+'.yml'))
  
  items.each do |attributes|
    # if filename == 'watermarks'
    #   image_asset = ImageAsset.create({image: File.open("#{Rails.root}/db/seeds/files/watermark.png")})
    #   attributes['image_asset_id'] = image_asset.id
    # end
    #puts "  ..#{attributes['name']}" if attributes['name']
    constant.delete(attributes['id'])
    constant.create!(attributes)
  end
  puts "  total: #{items.count}" unless ["test", "cucumber"].include?(Rails.env)
end

Rake::Task['license:reset'].invoke
Rake::Task['watermark:reset'].invoke
Rake::Task['create:test_user'].invoke
Rake::Task['photos:reset'].invoke
Rake::Task['clean:previews'].invoke
