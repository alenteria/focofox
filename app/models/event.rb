class Event < ActiveRecord::Base
	belongs_to :location
	
	def location_name
		location.try(:name)
	end
end