class Watermark < ActiveRecord::Base
	belongs_to :user
  belongs_to :image_asset
	has_many :photos

  validates :name, uniqueness: {message: 'duplicate', scope: :user_id}, allow_nil: true
  validate :not_same_with_defaults

  after_save :update_photos

  delegate :image, to: :image_asset
  def photo_url
    return nil if name.downcase == 'none'
  	self.try(:image_asset).try(:image).try(:url) || Watermark.default.image_asset.try(:image).try(:url)
  end

  def photo_path
  	self.try(:image_asset).try(:image).try(:path) || Watermark.default.image_asset.try(:image).try(:path)
  end

  alias_method :image_url, :photo_url
  alias_method :image_path, :photo_path

  def self.default
  	Watermark.where(["user_id IS ?", nil]).last
  end

  def dimensions
    return nil if image.blank?
    img = Magick::Image.read(image.path).first
    [img.columns, img.rows]
  end

  def update_photos
    Thread.new do
      photos.each do |p|
        p.update_preview(true)
      end
      ActiveRecord::Base.connection.close
    end
  end
  private
  def not_same_with_defaults
    if Watermark.where(user_id: nil, name: self.name).first.present?
      errors.add(:name, "duplicate!")
    end
  end
end