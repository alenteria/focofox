class Photographer < Account
  belongs_to :user

  dynamic_attr_accessor :dynamic_attribs, :storage_space
  dynamic_attr_accessor :dynamic_attribs, :thumbnail_size
  dynamic_attr_accessor :dynamic_attribs, :per_page
  dynamic_attr_accessor :dynamic_attribs, :default_watermark_id
  dynamic_attr_accessor :dynamic_attribs, :default_location_id
  dynamic_attr_accessor :dynamic_attribs, :default_license_id
  dynamic_attr_accessor :dynamic_attribs, :default_photo_price
  dynamic_attr_accessor :dynamic_attribs, :is_resize_photos
  dynamic_attr_accessor :dynamic_attribs, :max_width
  dynamic_attr_accessor :dynamic_attribs, :max_height
  dynamic_attr_accessor :dynamic_attribs, :first_add_photos_at
  dynamic_attr_accessor :dynamic_attribs, :first_1hun_sale_at
  dynamic_attr_accessor :dynamic_attribs, :first_1k_sale_at
  dynamic_attr_accessor :dynamic_attribs, :user_ids_selected_my_location

  scope :search, -> (name) {
    self.joins(:user).where('
      lower(users.first_name) like ? OR 
      lower(users.last_name) like ? OR
      lower(users.email) like ? OR
      lower(users.username) like ?
      ',
      "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%" 
    )
  }

  # == Validations =================================================================
  
  # == Attributes ==================================================================

  def default_watermark
    begin
      Watermark.find(default_watermark_id)
    rescue ActiveRecord::RecordNotFound
      Watermark.default
    end
  end
  def default_license
    begin
      License.find(default_license_id)
    rescue ActiveRecord::RecordNotFound
      License.default || user.licenses.first
    end
  end
  def default_location
    begin
      user.locations.find(default_location_id)
    rescue ActiveRecord::RecordNotFound
      user.locations.first
    end
  end

  def locations
    user.locations
  end
end
