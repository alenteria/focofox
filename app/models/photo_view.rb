class PhotoView < ActiveRecord::Base
	# enum view_type: [ :thumb, :preview, :original ]

	belongs_to :viewer, foreign_key: :user_id
	belongs_to :photographer
	belongs_to :photo

	before_save :verify_data

	def self.bulk_thumbs_view(params)
		photo_ids = (params[:photo_ids].try(:split, ',')||[])
		current_user_id = params[:current_user_id]
		photo_ids.each do |i|
			photo_view = PhotoView.find_or_create_by(user_id: current_user_id, photo_id: i, latitude: params[:latitude].try(:to_f), longitude: params[:longitude].try(:to_f), view_type: 'thumb')
			begin
				photo_view.update_column(:count, photo_view.count.to_i+1)
			rescue
			end
		end
	end

	private
		def verify_data
			if self.photographer_id.blank?
				self.photographer_id = Photo.find(self.photo_id).try(:user_id)
			end
			if self.view_type.blank?
				self.view_type = 'preview'
			end

			validity = if photographer_id == user_id
				false
			else
				true
			end
			validity
		end
end