class Admin < Account
  belongs_to :user

  dynamic_attr_accessor :dynamic_attribs, :storage_space
  dynamic_attr_accessor :dynamic_attribs, :thumbnail_size
  dynamic_attr_accessor :dynamic_attribs, :per_page
  dynamic_attr_accessor :dynamic_attribs, :default_watermark_id
  dynamic_attr_accessor :dynamic_attribs, :default_location_id
  dynamic_attr_accessor :dynamic_attribs, :default_license_id
  dynamic_attr_accessor :dynamic_attribs, :default_photo_price
  dynamic_attr_accessor :dynamic_attribs, :is_resize_photos
  dynamic_attr_accessor :dynamic_attribs, :max_width
  dynamic_attr_accessor :dynamic_attribs, :max_height
  
  # == Attributes ==================================================================

  def default_watermark
    begin
      Watermark.find(default_watermark_id)
    rescue ActiveRecord::RecordNotFound
      Watermark.default
    end
  end
  def default_license
    begin
      License.find(default_license_id)
    rescue ActiveRecord::RecordNotFound
      License.default || user.licenses.first
    end
  end
  def default_location
    begin
      user.locations.find(default_location_id)
    rescue ActiveRecord::RecordNotFound
      user.locations.first
    end
  end

  def locations
    user.locations
  end
end