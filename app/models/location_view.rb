class LocationView < ActiveRecord::Base
	belongs_to :user
	belongs_to :location
	belongs_to :owner, class_name: 'User', foreign_key: :location_owner_id

	before_save :verify_data
	private
		def verify_data
			photographer_id = Location.find(location_id).user_id
			validity = if user_id == photographer_id
				false
			else
				self.location_owner_id = photographer_id
				true
			end
			validity
		end

	alias_method :viewer, :user
end