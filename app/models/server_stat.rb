class ServerStat < ActiveRecord::Base
	# enum name: [
	# 		'logins', 
	# 		'new_users', 
	# 		'photos_uploaded', 
	# 		'storage_required', 
	# 		'droplets_provisioned',
	# 		'bandwidth'
	# 	]
	
	include Dynattribs
	dynamic_attr_accessor :dynamic_attribs, :stat
	dynamic_attr_accessor :dynamic_attribs, :rx
	dynamic_attr_accessor :dynamic_attribs, :tx

	def self.fetch_provisioned_droplets
		res = `curl -X GET "https://app.cloud66.com/api/3/stacks/87f75914c9d926828a3aca34ed763525/servers"  -H "Authorization: Bearer 5e97a0680ef1211203bb5fecad135312e675e731788c9c4bc3190dc646e4d058"`
		res = JSON.parse(res)
		count = res.try(:[], 'count').to_i
		while res.try(:[], 'pagination').try(:[], 'next').present?
			res = `curl -X GET "#{res.try(:[], 'pagination').try(:[], 'next')}"  -H "Authorization: Bearer 5e97a0680ef1211203bb5fecad135312e675e731788c9c4bc3190dc646e4d058"`
			res = JSON.parse(res)
			_count = res.try(:[], 'count')
			count += _count.to_i
		end

		ss = ServerStat.find_or_create_by(name: 'droplets_provisioned', date_time: Date.current)
		ss.stat = count
		ss.save
	end

	def self.calc_storage_required
		space = `du -hsm public/system/`.to_s.try(:split, ' ').try(:[], 0)
		ss = ServerStat.find_or_create_by(name: 'storage_required', date_time: Date.current)
		ss.stat = space.to_f
		ss.unit = 'MB'
		ss.save
	end

	def self.calc_bandwidth
		device = 'wlan1'
		xml = `vnstat -i #{device} -d --xml`
		hash = Hash.from_xml(xml.gsub("\n", ""))
		stats = hash.try(:[], "vnstat").try(:[], "interface").try(:[], "traffic").try(:[], "days").try(:[], "day")
		ss = ServerStat.find_or_create_by(name: 'bandwidth', date_time: Date.current)
		ss.rx = stats["rx"]
		ss.tx = stats["tx"]
		ss.unit = 'KiB'
		ss.save
		ss
	end
end