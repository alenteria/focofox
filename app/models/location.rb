class Location < ActiveRecord::Base
  acts_as_mappable  :lat_column_name => :latitude,
                    :lng_column_name => :longitude

  UNIQUENESS_DISTANCE = 0.0621371
  before_save :check_attributes

  belongs_to :user
  has_many	:photos

  validates :name,
    :uniqueness => {
      :message => 'Duplicate location name',
      :scope => :user
    },
    :allow_blank => false

  validate :latlon_validator

  geocoded_by :address
  after_validation :geocode, :if => :address_changed?

  # == Attributes =========================================================

  def force_save=(f)
    @force_save = f
  end

  def force_save?
    @force_save
  end

  def self.default
    Location.where(["user_id IS ?", nil]).first
  end

  private
  def check_attributes
    if self.try(:name).empty?
      self.name = [latitude, longitude].join(',')
    end
  end

  def latlon_validator
    return false if user.blank?
    near_location = user.locations.near([latitude, longitude], UNIQUENESS_DISTANCE).where.not(id: self.id).first
    if near_location.present? && !force_save?
      errors.add :address, "This location is too close with #{near_location.name}"
    end
  end

end
