class CreditCard < ActiveRecord::Base
  belongs_to :payment_method
  ENCRYPTION_KEY = '123alksdfa131321'

  attr_encrypted :address1, :key => ENCRYPTION_KEY
  attr_encrypted :address2, :key => ENCRYPTION_KEY
  attr_encrypted :city, :key => ENCRYPTION_KEY
  attr_encrypted :postal_code, :key => ENCRYPTION_KEY
  attr_encrypted :address_state, :key => ENCRYPTION_KEY

  validates :number,
    uniqueness: {message: 'Duplicate card.', scope: :payment_method_id},
    allow_nil: false 

  before_destroy :delete_paypal_card

  scope :defaults, -> {where(is_default: true).limit(1)}

  include Concerns::PaypalAccessToken
  def expired?
    return false if id.nil? || expire_month.nil? || expire_year.nil?
    Date.parse("#{expire_year}-#{expire_month}-1") <  Date.current
  end

  def paypal_links
    begin
      JSON.parse links
    rescue Exception => e
      []
    end
  end

  def set_cc_attributes(args={})
    args[:payer_id] = "foco-payer-#{SecureRandom.uuid}"
    # args[:number] = 4417119669820331 if Rails.env=='development'||Rails.env=='test'
    store_to_paypal(args)
  end

  def store_to_paypal(args={})
    resp = {}
    token = self.access_token
    http = Curl.post("https://api.#{Rails.application.secrets.paypal_host}/v1/vault/credit-card", args.to_json) do|req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{token}"
    end

    begin
      data = unless http.body.empty?
        JSON.parse(http.body_str)
      else
        {}
      end

      if data.try(:[], 'name') == "VALIDATION_ERROR"
        resp = {status: :fail, message: data['details']}
      elsif data['state'] == 'ok'
        self.card_id = data['id']    
        self.state = data['state']
        self.card_type = data['type']
        self.payer_id = data['payer_id']
        self.number = data['number']    
        self.expire_month = data['expire_month']    
        self.expire_year = data['expire_year']  
        self.first_name = data['first_name']  
        self.last_name = data['last_name']  
        self.valid_until = data['valid_until']  
        self.links = data['links'].to_json
        self.verified = true
        resp = {status: :ok}
      else
        resp = {status: :fail, message: http.status}
      end
    rescue Exception => e
      resp = {status: :fail, message: 'unknow error occured'}
    end
    resp
  end

  def update_paypal_card(args={})
    params = {value: args}
    params[:op] = 'replace'
    params[:path] = '/billing_address'

    resp = {}
    token = self.access_token
    http = Curl.put("https://api.#{Rails.application.secrets.paypal_host}/v1/vault/credit-cards/#{self.card_id}", params) do|req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{token}"
    end

    begin
      data = unless http.body.empty?
        JSON.parse(http.body_str)
      else
        {}
      end
    rescue
    end
  end

  def delete_paypal_card
    token = self.access_token
    http = {}
    http = Curl.delete("https://api.#{Rails.application.secrets.paypal_host}/v1/vault/credit-cards/#{self.card_id}") do|req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{token}"
    end
    http.try(:status)
  end
end
