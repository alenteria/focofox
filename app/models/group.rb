class Group < ActiveRecord::Base
	has_and_belongs_to_many :photos
	belongs_to :user

	validates :name,
		uniqueness: {message: 'Duplicate group name!'}
	
	scope :search, -> (name) do
  	self.where("lower(name) like ?",
      "%#{name.downcase}%"
    )
  end
	# == Attributes =========================================================

	def name
		name = read_attribute(:name)
		begin
			if name.include? 'photos uploaded on'
				name = "#{photos.count} #{name}"
			end
		rescue
		end
		name
	end

	alias_method :members, :photos
end