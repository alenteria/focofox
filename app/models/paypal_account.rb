class PaypalAccount < ActiveRecord::Base
  include Concerns::PaypalAccessToken

  belongs_to :payment_method
  
  validates :email,
    presence: true,
    uniqueness: {
      message: 'Account already added!',
      scope: :payment_method_id,
    }, allow_nil: false,
    :format     => {
      with: /\A.+@.+\..+\z/,
      message: 'The email address you entered is not valid'
    }
  validate :verified_paypal_account

  scope :defaults, -> {where(is_default: true).limit(1)}
  
  def verified_paypal_account
    token = self.access_token
    args = {
      matchCriteria: :NONE,
      emailAddress: self.email
    }

    http = Curl.get("https://svcs.#{Rails.application.secrets.paypal_host}/AdaptiveAccounts/GetVerifiedStatus", args) do|req|
      req.headers['X-PAYPAL-SECURITY-USERID'] = Rails.application.secrets.paypal_api_username
      req.headers['X-PAYPAL-SECURITY-PASSWORD'] = Rails.application.secrets.paypal_api_password
      req.headers['X-PAYPAL-SECURITY-SIGNATURE'] = Rails.application.secrets.paypal_api_signature
      req.headers['X-PAYPAL-REQUEST-DATA-FORMAT'] = 'NV'
      req.headers['X-PAYPAL-RESPONSE-DATA-FORMAT'] = 'JSON'
      req.headers['X-PAYPAL-APPLICATION-ID'] = Rails.application.secrets.paypal_app_id
    end
    
    begin
      resp = JSON.parse(http.body)
      is_verified = resp['accountStatus']=="VERIFIED"

      unless is_verified
        errors.add(:Paypal, 'Not a verified paypal account')
      end
    rescue Exception => e
      errors.add(:Paypal, "Can't verify paypal account.")
    end
  end
end