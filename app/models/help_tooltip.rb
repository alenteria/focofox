class HelpTooltip < ActiveRecord::Base
  translates :text
  globalize_accessors locales: [:en, :es], attributes: [:text]

  def self.get(slug)
    result = self.find_by(slug: slug)
    result
  end
end
