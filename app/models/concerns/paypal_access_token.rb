require 'active_support/concern'

module Concerns::PaypalAccessToken
  extend ActiveSupport::Concern

  def access_token
    args = "grant_type=client_credentials"
    http = Curl.post("https://api.#{Rails.application.secrets.paypal_host}/v1/oauth2/token", args) do|req|
      req.http_auth_types = :basic
      req.username = Rails.application.secrets.paypal_client_id
      req.password = Rails.application.secrets.paypal_client_secret
      req.headers['Accept'] = 'application/json'
      req.headers['Accept-Language'] = 'en_US'
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    end

    begin
      data = JSON.parse(http.body_str)
      return data['access_token']
    rescue Exception => e
      nil
    end

  end
end