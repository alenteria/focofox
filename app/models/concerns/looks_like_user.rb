require 'active_support/concern'

module Concerns::LooksLikeUser
  extend ActiveSupport::Concern

  INTERFACE = [:email, :first_name, :last_name, :username, :display_name, :full_name, :name, :telephone, :avatar]

  included do
    delegate *INTERFACE, to: :user, allow_nil: true
  end

end
