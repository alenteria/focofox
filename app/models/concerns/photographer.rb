require 'active_support/concern'

module Concerns::Photographer
  extend ActiveSupport::Concern

  INTERFACE = [
	:storage_space,
	:default_watermark_id,
  :default_watermark,
	:default_location_id,
  :default_location,
  :default_license_id,
  :default_license,
	:default_photo_price,
  :thumbnail_size,
  :per_page,
  :paypal_email,
  :payment_methods,
  :is_resize_photos, 
  :max_width, 
  :max_height,
  :first_add_photos_at,
  :first_1hun_sale_at,
  :first_1k_sale_at,
  ]

  included do
    delegate *INTERFACE, to: :photographer_account, allow_nil: true
  end
end
