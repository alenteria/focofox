require 'active_support/concern'

module Concerns::Account
  extend ActiveSupport::Concern

  INTERFACE = [
  :paypal_email,
  :payment_methods,
  :facebook,
  :fb_access_token,
  :registered_at,
  ]

  included do
    delegate *INTERFACE, to: :account, allow_nil: true
  end
end
