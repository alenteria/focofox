require 'active_support/concern'

module Concerns::PaperclipVideoData
  extend ActiveSupport::Concern

  def parse_video_thumbnails(options = {}, attachment = nil)
    return {} if !self.video? || video_thumbnails.length > 1
    thumbs_path = "public/system/videos/.#{uuid}/thumbnails"
    file = self.attachment

    begin
      duration = Paperclip.run('avconv', "-i #{file.path} 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//").split(":").map{|i| i.to_f}

      duration = (duration[0]*60*60) + (duration[1]*60) + duration[2]
      # Extracting iframe count
      cmd = "-i #{file.path} -vcodec copy -f rawvideo -y /dev/null 2>&1 | tr ^M '\n' | awk '/^frame=/ {print $2}'|tail -n 1"
      number_of_frames = 10
      offsets = duration / ((number_of_frames + 1) * 10)
      
      frame_rate = (number_of_frames + 1) / duration

      # Extracting iframe thumbnails
      system 'mkdir', '-p', thumbs_path

      cmd = "-i #{file.path} -f image2 -ss #{offsets} -r #{frame_rate} -vframes #{number_of_frames} #{thumbs_path}/thumb-%05d.png" 
      Paperclip.run('avconv', cmd)

      video_thumbnails.each do |_thumb|
        if _thumb !='/video-no-thumbs.png'
          path = "#{Rails.root}/public/system/#{_thumb}"
          thumb = Magick::Image.read(path).first
          thumb.resize_to_fit!(300).write(path)
        end 
      end
    rescue Exception => e
      puts Exception
    end
  end

  def video_thumbnails
    return [] unless self.video?
    
    thumbs_path = "public/system/videos/.#{uuid}/thumbnails"
    thumbs = []
    begin
      Dir["#{Rails.root}/#{thumbs_path}/**"].each do |path|
        thumbs.push "/system/videos/.#{uuid}/thumbnails/#{path.split('/').last}"
      end
    rescue Exception => e
    end

    thumbs.push '/video-no-thumbs.png' if thumbs.blank?
    thumbs
  end

  def parse_video_resolution
    exif = MiniExiftool.new self.attachment.path
    self.width = exif.image_width
    self.height = exif.image_height
    self.save
  end
end