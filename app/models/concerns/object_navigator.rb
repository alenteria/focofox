require 'active_support/concern'

module Concerns::ObjectNavigator
  extend ActiveSupport::Concern
  
  def next
    self.class.where("id > ?", id).first
  end

  def previous
    self.class.where("id < ?", id).last
  end
end