class Category < ActiveRecord::Base
	
	def self.for_select
		Category.all.map{|c| {id: c.id, name: c.name}}
	end
end