class Account < ActiveRecord::Base
  include Dynattribs
  include Concerns::DropboxApp

  dynamic_attr_accessor :dynamic_attribs, :fb_access_token
  dynamic_attr_accessor :dynamic_attribs, :dropbox_access_token
  dynamic_attr_accessor :dynamic_attribs, :first_find_photos_at
  dynamic_attr_accessor :dynamic_attribs, :first_purchase_at
  dynamic_attr_accessor :dynamic_attribs, :fifth_purhase_at

  belongs_to :user
  has_many :payment_methods

  # ================= VALIDATIONS =======================
  validates :user, presence: true

  # ================= INTERFACE =========================
  include Concerns::LooksLikeUser

  # == Attributes ==================================================================

  def facebook
    begin
      fb = FbGraph::User.me(fb_access_token)
      fb = fb.fetch if has_valid_fb_token?
      fb
    rescue Exception => e
      nil
    end
  end

  def fb_albums
    facebook.albums
  end

  def has_valid_fb_token?
    args = {
      # input_token: fb_access_token,
      access_token: fb_access_token,
    }
    begin
      http = Curl.get("https://graph.facebook.com/me?access_token", args)

      res = JSON.parse(http.body)
      res.try(:[], 'verified')  
    rescue Exception => e
      false
    end

  end

  def has_valid_dropbox_token?
    authorize_url = dropbox_authorize_url
  end

  def registered_at
    created_at
  end
end
