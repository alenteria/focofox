class ImageAsset < ActiveRecord::Base
	has_many :watermarks
	belongs_to :user
	
	has_attached_file :image, :styles => {:original=>"500x500>", :medium => "300x300>", :thumb => '50X50'},
    :url => "/system/image_assets/:id_:style_:filename"

  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates_attachment_presence :image
  
  def image_url
  	image.url
  end

  class << self
  	def defaults
	  	self.where(["user_id IS ?", nil])
	  end
  end
end