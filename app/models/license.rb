class License < ActiveRecord::Base
	belongs_to :user

	scope :search, -> (name) {
  	self.where('
      lower(title) like ?
      ',
      "%#{name.downcase}%"
    )
  }
	# == Attributes =========================================================

	
	has_attached_file :attachment,
    :url => "/licenses/:id/:filename"
  validates_attachment_content_type :attachment, :content_type => [
		  	"image/jpg",
		  	"image/jpeg",
		  	"image/png",
		  	"application/pdf",
		  	"application/vnd.ms-excel",     
		    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				"application/msword", 
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
				"text/plain"
			]
	def attachment_url
		attachment.url
	end
	def attachment_name
		attachment_file_name
	end
	
	def self.default
		License.where(["user_id IS ?", nil]).first
	end
	def self.defaults
		License.where(["user_id IS ?", nil])
	end
end