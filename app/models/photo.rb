require 'open-uri'

class Photo < ActiveRecord::Base
  MAX_PHOTO_PREVIEW_SIZE = 1600

  belongs_to :user
  belongs_to  :category
  belongs_to :watermark
  belongs_to :license
  belongs_to :location
  has_and_belongs_to_many :groups
  has_many :photo_views

  attr_accessor :skip_photo_fingerprint_validation, :skip_video_fingerprint_validation, :session_names

  has_attached_file :photo, 
    :url => "/system/photos/.:uuid/:id_:style_:filename",
    :default_url => "/missing.png"

  has_attached_file :video, 
    :processors=> [ :transcoder ],
    :url => "/system/videos/.:uuid/:id_:style_:filename",
    :default_url => "/missing.png"
  
  VIDEO_FORMATS = [ 'video/x-ms-wmv', 'application/x-mp4', 'video/mpeg', 'video/avi', 'video/quicktime', 'video/x-la-asf', 'video/x-ms-asf', 'video/x-msvideo','video/mov', 'video/x-sgi-movie', 'video/x-flv', 'flv-application/octet-stream','application/octet-stream', 'video/3gp', 'video/3gpp', 'video/3gpp2', 'video/3gpp-tt', 'video/BMPEG', 'video/BT656', 'video/CelB', 'video/DV', 'video/H261', 'video/H263', 'video/H263-1998', 'video/H263-2000', 'video/H264', 'video/JPEG', 'video/MJ2', 'video/MP1S', 'video/MP2P', 'video/MP2T', 'video/mp4', 'video/MP4V-ES', 'video/MPV', 'video/mpeg4', 'video/mpeg4-generic', 'video/nv', 'video/parityfec', 'video/pointer', 'video/raw', 'video/rtx', 'video/ogg', 'video/flv', 'video/mkv','video/divx', 'video/wmv', 'video/webm', 'video/ogv' ]
  PHOTO_FORMATS = ['image/jpg', 'image/png', 'image/gif', 'image/jpeg']

  # ================== VALIDATIONS =================================================
  validates_attachment_content_type :photo, :content_type => PHOTO_FORMATS
  validates_attachment_content_type :video, :content_type => VIDEO_FORMATS
  validates :video_fingerprint, uniqueness: {message: 'duplicate'}, allow_nil: true, unless: :skip_video_fingerprint_validation
  validates :photo_fingerprint, uniqueness: {message: 'duplicate'}, allow_nil: true, unless: :skip_photo_fingerprint_validation
  validates :price, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100000 }

  # validate :photographer_account_check
  # validate :storage_space_check

  # == CALLBACKS====================================================================
  before_create  :generate_uuid
  after_create :add_to_session
  after_create :set_first_upload_date
  after_photo_post_process :process_photo_attachment, on: :create
  after_video_post_process :process_video_attachment, on: :create
  before_destroy :delete_folders

  process_in_background :photo
  process_in_background :video
  # == Attributes ==================================================================
  serialize :exif
  serialize :gps_location

  # ==INTERFACE=====================================================================
  include Concerns::PaperclipVideoData
  include Concerns::ObjectNavigator
  
  def orientation_type
    return nil if width.blank? || height.blank?

    if width > height
      :'landscape'
    else
      :'portrait'
    end
  end

  def file_name
    photo_file_name || video_file_name
  end

  def exif
    JSON.parse self[:exif] if self[:exif].present?
  end

  def attachment
    _attachment = if video.content_type.present?
      video
    else
      photo
    end
    
    _attachment
  end

  def url
    attachment.url
  end

  def attachment_content_type
    attachment.content_type
  end

  def attachment_url(size=:thumb)
    return attachment.url if self.video?

    attachment.url(size)
  end

  def attachment_absolute_url
    ApplicationController.helpers.asset_url(attachment.url)
  end

  def thumbnail_size
    # user.try(:thumbnail_size)
  end

  def photographer
    user
  end

  def photographer_name
    user.try(:display_name)||''
  end

  def location_name
    location.try(:name) || ""
  end

  def group_name
    groups.first.try(:name) || ""
  end

  def license_name
    license.try(:title)||''
  end
  
  def formatted_date_time
    if self[:date_time].present?
      self[:date_time].strftime("%m/%d/%Y %I:%M %p")
    else
      ''
    end
  end

  def video?
    Photo.video_formats
    .include?(attachment.content_type)
  end

  def image?
    photo_content_type =~ %r{^(image|(x-)?application)/(bmp|gif|jpeg|jpg|pjpeg|png|x-png)$}
  end
  
  def dimensions
    if self.video?
      c =  Paperclip.run('avconv', "-i #{video.path} 2>&1 | perl -lane 'print $1 if /(\\d+x\\d+)/'")
      d = c.split('x')
      if d.length>2
        d = c.squish.split(' ').last.split('x').map(&:to_i)
      else
        d = d.map(&:to_i)
      end
      d
    else
      image = Magick::Image.read(self.attachment.path(:original)).first
      [image.columns, image.rows]
    end
  end

  def generate_preview(force=false)
    if self.video?
      generate_video_preview(force)
    else
      generate_photo_preview(force)
    end
  end

  def preview_url
    if self.video?
      video_preview_url
    else
      photo_preview_url
    end
  end

  def self.video_formats
    VIDEO_FORMATS
  end

  def self.photo_formats
    PHOTO_FORMATS
  end
  
  alias_method :update_preview, :generate_preview

  def self.bulk_delete(ids=[], user_id)
    photos = ids.present? ? Photo.where(id: ids) : User.find_by_id(user_id).try(:photos)
    ActiveRecord::Base.transaction do
      (photos||[]).each do |photo|
        photo.delete
      end
    end
  end

  def delete_folders
    require 'fileutils'
    _type = video? ? 'videos' : 'photos'
    FileUtils.rm_rf("#{Rails.root}/public/system/#{_type}/.#{uuid}")
    FileUtils.rm_rf("#{Rails.root}/public/system/.tmp/.#{uuid}")
  end

   def photos_custom_size
    size = if user.is_resize_photos && user.max_width && user.max_height
      [user.max_width, user.max_height]
    else
      ''
    end
  end

  private
    def generate_uuid
      self.uuid = SecureRandom.uuid
    end

    def photographer_account_check
      unless user.try(:verified_photographer?)
        errors.add(:photographer_account, "You are not a verified photographer")
      end
    end

    def storage_space_check
      file_size = attachment.try(:size)||0
      if user.try(:verified_photographer?)
        if user.remaining_storage_space.to_f - file_size.to_f <= 0
          errors.add(:attachment_file_size, "Maximum storage space was reached")
        end
      end
    end
    def ratio_calculator(option)
      ratio = ratio(option[:x1].to_f, option[:y1].to_f)
      begin
        num = option[:x1].to_f/ratio
        den = option[:y1].to_f/ratio
        option[:x2] = (option[:y2].to_f*num) / den
      rescue
        option[:x1]
      end
    end
    def ratio(a, b)
      (b==0) ? a : ratio(b, a%b)
    end

    def video_preview_url
      path = "/system/.tmp/.#{uuid}/#{id}_preview_#{video_file_name}"
    end

    def generate_video_preview(force)
      path = "/system/.tmp/.#{uuid}/#{id}_preview_#{video_file_name}"
      output_path = "#{Rails.root}/public#{path}"
      
      return nil if !video?
      return path if File.exist?(output_path) && !force
      
      FileUtils.rm_rf(output_path)
      system 'mkdir', '-p', "#{Rails.root}/public/system/.tmp/.#{uuid}/"
      
      begin
        dw = watermark || user.try(:default_watermark)
        watermark_temp_path = "#{Rails.root}/public/system/.tmp/.#{uuid}/.#{dw.id}_#{dw.image_asset.image_file_name}"

        case dw.display_type
        when 'stretched'
          position = 'main_w/2-overlay_w/2:main_h/2-overlay_h/2'
          w = (dw.stretched_size * dimensions[0]) / 100
          h = (dw.stretched_size * dimensions[1]) / 100
          dw_src = Magick::Image.read(dw.image.try(:path)).first
          dw_src.resize!(w, h)
          dw_src.background_color = 'Transparent'
          if dw.rotation.present?
            dw_src.rotate! dw.rotation.to_f
          end

          if dw.opacity.present?
            white_canvas = Magick::Image.new(dw_src.columns, dw_src.rows) { self.background_color = "Transparent" }
            white_canvas.alpha(Magick::ActivateAlphaChannel)
            white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

            # Important: DstIn composite operation (white canvas + watermark)
            dw_src = dw_src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
            dw_src.alpha(Magick::ActivateAlphaChannel)
          end

        when 'tiled'
          dw_src = Magick::Image.read(dw.image.try(:path)).first
          tiled_size = ratio_calculator({x1: dw.tiled_size, y1: dw.base_image_width, y2: dimensions[0]})
          dw_src.resize_to_fit!(tiled_size.to_f)
          
          dw_src.alpha(Magick::ActivateAlphaChannel)
          if dw.opacity.present?
            white_canvas = Magick::Image.new(dw_src.columns, dw_src.rows) { self.background_color = "Transparent" }
            white_canvas.alpha(Magick::ActivateAlphaChannel)
            white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

            # Important: DstIn composite operation (white canvas + watermark)
            dw_src = dw_src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
            dw_src.alpha(Magick::ActivateAlphaChannel)

          end
          dw_src.background_color = 'Transparent'
          if dw.rotation.present?
            dw_src.rotate! dw.rotation.to_f
          end

          blank = Magick::Image.new(dimensions[0], dimensions[1])
          blank.composite_tiled!(dw_src, Magick::OverCompositeOp)
          blank.composite(dw_src, Magick::ForgetGravity, Magick::OverCompositeOp)
          dw_src = blank      

        when 'positioned'
          position = case dw.position
          when 'top left'
            '10:10'
          when 'top right'
            'main_w-overlay_w-10:10'
          when 'top center'
            '(main_w-overlay_w)/2:10'
          when 'bottom left'
            '10:main_h-overlay_h-10'
          when 'bottom right'
            'main_w-overlay_w-10:main_h-overlay_h-10'
          when 'bottom center'
            '(main_w-overlay_w)/2:main_h-overlay_h-10'
          when 'center center'
            'main_w/2-overlay_w/2:main_h/2-overlay_h/2'
          end

          positioned_size = (dw.positioned_size * dimensions[0]) / 100

          dw_src = Magick::Image.read(dw.image.try(:path)).first
          dw_src.resize_to_fit!(positioned_size)

          dw_src.alpha(Magick::ActivateAlphaChannel)

          if dw.opacity.present?
            white_canvas = Magick::Image.new(dw_src.columns, dw_src.rows) { self.background_color = "Transparent" }
            white_canvas.alpha(Magick::ActivateAlphaChannel)
            white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

            # Important: DstIn composite operation (white canvas + watermark)
            dw_src = dw_src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
            dw_src.alpha(Magick::ActivateAlphaChannel)
          end
          if dw.rotation.present?
            dw_src.rotate! dw.rotation.to_f
          end

          dw_src.background_color = 'Transparent'
        end

        # dw_src.format = 'gif'
        dw_src.write(watermark_temp_path)

        command = " -y -i #{video.path} -acodec aac -strict experimental -vf 'movie=#{watermark_temp_path} [wm];[in][wm] overlay=#{position} [out]' -c:v libx264 #{output_path}"
        Paperclip.run('avconv', command)
        path
      rescue
        FileUtils.cp(video.path, output_path) if File.exist?(video.path)
        path
      end
    end

    def photo_preview_url
      "/system/.tmp/.#{uuid}/#{self.attachment.original_filename}"
    end

    def generate_photo_preview(force)
      dw = watermark || user.try(:default_watermark)
      path = "/system/.tmp/.#{uuid}/#{self.attachment.original_filename}"
      abs_path = "#{Rails.root}/public/#{path}"
      system 'mkdir', '-p', "#{Rails.root}/public/system/.tmp/.#{uuid}"
      if dw.present? && dw.image_asset.present?
        return path if File.exist?(abs_path) && !force
        begin
          dst = Magick::Image.read(self.attachment.path(:original)).first
          orig_watermark_path = dw.photo_path
          result = case dw.display_type
            when 'stretched'
              w = (dw.stretched_size * dst.columns) / 100
              h = (dw.stretched_size * dst.rows) / 100
              src = Magick::Image.read(orig_watermark_path).first
              src.resize!(w, h)
              if dw.rotation.present?
                src.background_color = 'none'
                src.rotate! dw.rotation.to_f
              end

              if dw.opacity.present?
                white_canvas = Magick::Image.new(src.columns, src.rows) { self.background_color = "none" }
                white_canvas.alpha(Magick::ActivateAlphaChannel)
                white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

                # Important: DstIn composite operation (white canvas + watermark)
                src = src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
                src.alpha(Magick::ActivateAlphaChannel)
              end

              dst.composite(src, Magick::CenterGravity, Magick::OverCompositeOp)      
            when 'tiled'
              src = Magick::Image.read(orig_watermark_path).first
              tiled_size = ratio_calculator({x1: dw.tiled_size, y1: dw.base_image_width, y2: dst.columns})
              src.resize_to_fit!(tiled_size.to_f)
              
              src.alpha(Magick::ActivateAlphaChannel)
              if dw.opacity.present?
                white_canvas = Magick::Image.new(src.columns, src.rows) { self.background_color = "none" }
                white_canvas.alpha(Magick::ActivateAlphaChannel)
                white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

                # Important: DstIn composite operation (white canvas + watermark)
                src = src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
                src.alpha(Magick::ActivateAlphaChannel)
              end
              if dw.rotation.present?
                src.background_color = 'none'
                src.rotate! dw.rotation.to_f
              end
              dst.composite_tiled!(src, Magick::OverCompositeOp)

              dst.composite(src, Magick::ForgetGravity, Magick::OverCompositeOp)
            when 'maximized'
              fixed_ratio_margin = ratio_calculator({x1: dw.fixed_ratio_margin, y1: dw.base_image_width, y2: dst.columns})
              w = dst.columns-(fixed_ratio_margin.to_f * 2)
              h = dst.rows-(fixed_ratio_margin.to_f * 2)
              src = Magick::Image.read(orig_watermark_path).first
              src.resize_to_fit!(w, h)
              if dw.rotation.present?
                src.background_color = 'none'
                src.rotate! dw.rotation.to_f
              end
              if dw.opacity.present?
                white_canvas = Magick::Image.new(src.columns, src.rows) { self.background_color = "none" }
                white_canvas.alpha(Magick::ActivateAlphaChannel)
                white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

                # Important: DstIn composite operation (white canvas + watermark)
                src = src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
                src.alpha(Magick::ActivateAlphaChannel)
              end
              dst.composite(src, Magick::CenterGravity, Magick::OverCompositeOp)
            when 'positioned'
              positioned_size = (dw.positioned_size * dst.columns) / 100

              src = Magick::Image.read(orig_watermark_path).first
              src.resize_to_fit!(positioned_size)
              
              src.alpha(Magick::ActivateAlphaChannel) 
              if dw.opacity.present?
                white_canvas = Magick::Image.new(src.columns, src.rows) { self.background_color = "none" }
                white_canvas.alpha(Magick::ActivateAlphaChannel)
                white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * dw.opacity.to_f)

                # Important: DstIn composite operation (white canvas + watermark)
                src = src.composite(white_canvas, Magick::NorthWestGravity, 0, 0, Magick::DstInCompositeOp)
                src.alpha(Magick::ActivateAlphaChannel)
              end
              if dw.rotation.present?
                src.background_color = 'none'
                src.rotate! dw.rotation.to_f
              end
              case dw.position
              when 'top left'
                gravity = Magick::NorthWestGravity
              when 'top center'
                gravity = Magick::NorthGravity
              when 'top right'
                gravity = Magick::NorthEastGravity
              when 'center center'
                gravity = Magick::CenterGravity
              when 'center left'
                gravity = Magick::WestGravity
              when 'center right'
                gravity = Magick::EastGravity
              when 'bottom left'
                gravity = Magick::SouthWestGravity
              when 'bottom center'
                gravity = Magick::SouthGravity
              when 'bottom right'
                gravity = Magick::SouthEastGravity
              end
              dst.composite(src, gravity, Magick::OverCompositeOp)
            end 
          
          result.resize_to_fit!(MAX_PHOTO_PREVIEW_SIZE)
          result.write(abs_path)
          path
        rescue Exception => e
          photo.url
        end
      else
        FileUtils.cp(photo.path, abs_path) if File.exist?(photo.path)
        path
      end
    end

    def set_first_upload_date
      _ps = user.photographer_account
      return false if _ps.blank?
      return false unless _ps.first_add_photos_at.blank?
      _ps.first_add_photos_at = DateTime.current
      _ps.save
    end

    def add_to_session
      sn = self.session_names ||["photos uploaded on #{DateTime.current.strftime("%m/%d/%Y %I:%M %p")}"]
      sn.each do |g|
        group = Group.find_or_create_by(name: g.strip, user_id: self.user_id)
        self.groups << group
      end

    end

    def process_photo_attachment
      return false if video? || photo.blank?

      thumb_path = self.attachment.path(:thumb)
      original = self.attachment.path

      original_magick_image = Magick::Image.read(photo.path(:original)).first
         
      # auto_orient_image
      original_magick_image.auto_orient.write(photo.path(:original)) 

      # generate_thumb
      thumb_magick_image = original_magick_image.resize_to_fit(250, 250)
      thumb_magick_image.write(thumb_path) do 
        self.quality = 50
      end

      # generate_thumb_base64
      # blob = thumb_magick_image.to_blob
      # base64 = Base64.encode64(blob)
      # self.thumb_base64 = ['data:', photo.content_type, "\;", 'base64,', base64].join('')  

      # post_process_photo
      unless original.nil?
        geometry = Paperclip::Geometry.from_file(original)
        self.width = geometry.width.to_i
        self.height = geometry.height.to_i
      end

      unless thumb_path.nil? || (self.thumb_width.present? && self.thumb_height.present?)
        geometry = Paperclip::Geometry.from_file(thumb_path)
        self.thumb_width = geometry.width.to_i
        self.thumb_height = geometry.height.to_i
      end


      # generate_preview
      generate_preview(true)

      # verify_photo
      self.verified = true
      self.verified_at = DateTime.current
      self.save(validate: false)
    end
    def process_video_attachment
      return false if image? || video.blank?
      # parse_thumbnails
      parse_video_thumbnails()

      # parse_resolution
      parse_video_resolution()

      # generate_preview
      generate_preview(true)

      # verify_photo
      self.verified = true
      self.verified_at = DateTime.current
      self.save(validate: false)
    end

  # == ASYNCHRONOUS CALLBACKS ======================================================
  handle_asynchronously :process_photo_attachment
  handle_asynchronously :process_video_attachment
  handle_asynchronously :delete_folders
  handle_asynchronously :set_first_upload_date
end

