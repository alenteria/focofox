class PaidPhoto < ActiveRecord::Base
  belongs_to :payment
  belongs_to :license
  belongs_to :location
  belongs_to :photographer, class_name: 'User', foreign_key: 'photographer_id'
  belongs_to :source, class_name: 'Photo', foreign_key: 'source_id'
  belongs_to :buyer, class_name: 'User', foreign_key: 'buyer_id'

  has_attached_file :photo, 
    :styles => {:thumb => '300x300'},
    :url => "/system/paid_photos/:id_:style_:filename",
    :default_url => "/assets/Content-Removed.png",
    :convert_options => { :all => '-auto-orient' }
  
  has_attached_file :video, 
    :processors=> [ :transcoder ],
    :url => "/system/paid_videos/:id_:style_:filename",
    :default_url => "/assets/Content-Removed.png"

  VIDEO_FORMATS = [ 'video/x-ms-wmv', 'application/x-mp4', 'video/mpeg', 'video/avi', 'video/quicktime', 'video/x-la-asf', 'video/x-ms-asf', 'video/x-msvideo','video/mov', 'video/x-sgi-movie', 'video/x-flv', 'flv-application/octet-stream','application/octet-stream', 'video/3gp', 'video/3gpp', 'video/3gpp2', 'video/3gpp-tt', 'video/BMPEG', 'video/BT656', 'video/CelB', 'video/DV', 'video/H261', 'video/H263', 'video/H263-1998', 'video/H263-2000', 'video/H264', 'video/JPEG', 'video/MJ2', 'video/MP1S', 'video/MP2P', 'video/MP2T', 'video/mp4', 'video/MP4V-ES', 'video/MPV', 'video/mpeg4', 'video/mpeg4-generic', 'video/nv', 'video/parityfec', 'video/pointer', 'video/raw', 'video/rtx', 'video/ogg', 'video/flv', 'video/mkv','video/divx', 'video/wmv', 'video/webm', 'video/ogv' ]
  PHOTO_FORMATS = ['image/jpg', 'image/png', 'image/gif', 'image/jpeg']

  validates_attachment_content_type :photo, :content_type => PHOTO_FORMATS
  validates_attachment_content_type :video, :content_type => VIDEO_FORMATS

  after_create :copy_thumbnails
  after_create :set_stats_data
  
  scope :verifieds, -> {
    self.joins(:payment).where('
      payments.verified = ?
      ',
      true
    )
  }

  def self.bulk_delete(ids=[], user_id=nil)
    photos = ids.present? ? PaidPhoto.where(id: ids) : User.find_by_id(user_id).try(:paid_photos)
    ActiveRecord::Base.transaction do
      (photos||[]).each do |photo|
        photo.delete
      end
    end
  end

  def video?
    is_video?
  end

  def photo?
    is_photo?    
  end

  def group_name
    photo.try(:group_name)
  end

  def location_name
    location.try(:name)
  end

  def license_name
    license.try(:title)
  end

  def photographer_name
    photographer.display_name
  end

  def orientation_type
    return nil if width.blank? || height.blank?

    if width > height
      :'landscape'
    else
      :'portrait'
    end
  end

  def formatted_date_time
    if self[:date_time].present?
      self[:date_time].strftime("%m/%d/%Y %I:%M %p")
    else
      ''
    end
  end

  def file_name
    photo_file_name || video_file_name
  end
  
  def attachment
    _attachment = if video.content_type.present?
      video
    else
      photo
    end
    
    _attachment
  end

  def url
    attachment.url
  end

  def attachment_content_type
    attachment.content_type
  end

  def attachment_url(size=:thumb)
    return attachment.url if self.video?

    attachment.url(size)
  end

  def attachment_absolute_url
    ApplicationController.helpers.asset_url(attachment.url)
  end

  def video_thumbnails
    return nil unless self.video?
    source.try(:video_thumbnails)
  end

  private
  def copy_thumbnails
  end

  def set_stats_data
    month_sale = photographer.sold_photos.where("created_at > ?", 1.month.ago).sum(:price) + price
    _pa = photographer.photographer_account
    if month_sale >= 100 && _pa.try(:first_1hun_sale_at).try(:blank?)
      _pa.first_1hun_sale_at = DateTime.current
    elsif month_sale >= 1000 && _pa.try(:first_1k_sale_at).try(:blank?)
      _pa.first_1k_sale_at = DateTime.current
    end
    _pa.save if _pa.try(:changed?)
  end

  handle_asynchronously :set_stats_data
  handle_asynchronously :copy_thumbnails
end