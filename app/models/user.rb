require 'RMagick'
class User < ActiveRecord::Base
  devise :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :lockable, :timeoutable, :two_factor_authenticatable,
          :otp_secret_encryption_key => Rails.application.secrets.otp_secret_encryption_key

	TEMP_EMAIL_PREFIX = 'unknown@me'
  TEMP_EMAIL_REGEX = /\Aunknown@me/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_and_belongs_to_many :roles
  has_many :identities
  has_many :photos
  has_many :photo_groups, class_name: :Group
  has_many :watermarks
  has_one :account, class_name: :Account
  has_one :photographer_account, class_name: :Photographer
  has_one :admin_account, class_name: :Admin
  has_many :locations
  has_many :image_assets
  has_many :licenses
  has_many :payments, foreign_key: :from
  has_many :payment_methods, through: :account
  has_many :events
  has_one :location
  has_many :photo_views, foreign_key: 'photographer_id'
  has_many :viewed_photos, class_name: 'PhotoView', foreign_key: 'user_id'
  has_many :viewed_locations, class_name: 'LocationView', foreign_key: 'user_id'
  has_many :locations_viewed, class_name: 'LocationView', foreign_key: 'location_owner_id'
  has_many :sold_photos, class_name: 'PaidPhoto', foreign_key: :photographer_id
  has_many :paid_photos, foreign_key: 'buyer_id'

  has_attached_file :avatar,
    :url => "/system/avatars/:id_:style_:filename",
    :default_url => "/missing.png",
    :styles => { :medium => "300x300#", :thumb => "100x100#" }
  
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates_attachment :avatar,
    :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png"] },
    :size => { :in => 0..5.megabytes }

  devise :registerable, :confirmable,
          :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
          :omniauth_providers => [:twitter, :linkedin, :facebook, :google_oauth2]
  
  attr_accessor :x1, :y1, :x2, :y2, :w, :h

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validates :username,
    :uniqueness => {
      :message => 'already been taken',
      :case_sensitive => false
      },
    :allow_blank => true

  validates :email,
    :presence   => {:message => 'Please enter an email address'},
    :uniqueness => {:message => 'This email has already been taken'},
    :format     => {
      with: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/i,
      message: 'The email address you entered is not valid'
    }

  validates_strength_of :password, :level => :weak, on: :create

  # ========== Interfaces ======================================================
  include Concerns::Account
  include Concerns::Photographer
  include Concerns::Admin

  # ========== SCOPE ===========================================================
  scope :search, -> (name) {
    self.where('
      lower(first_name) like ? OR 
      lower(last_name) like ? OR
      lower(email) like ? OR
      lower(username) like ?
      ',
      "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%" 
    )
  }

  scope :photographers, ->{
    self.joins(:account).where('accounts.type IN (?)', ['Photographer', 'Admin']).uniq
  }

  scope :customers, ->{
    self.joins(:account).where("accounts.type != 'Photographer' OR accounts.type IS NULL").uniq
  }

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if email = conditions.delete(:email)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => email.downcase }]).first
    else
      where(conditions).first
    end
  end

  def full_name
    [first_name, last_name].compact.join(' ')
  end

  def display_name
    (username||'').empty? ? ((first_name||'').empty? ? email : first_name) : username 
  end

  def facebook
    account.try(:facebook) || FbGraph::User.me(nil)
  end

  def image_asset_lists
    begin
      self.image_assets + ImageAsset.defaults
    rescue
      self.image_assets
    end
    .compact
  end

  def watermark_lists
    begin
      self.watermarks + Watermark.where(user_id: nil)
    rescue
      self.watermarks
    end
  end

  def license_lists
    begin
      self.licenses + License.defaults
    rescue
      self.licenses
    end
  end

  def sets_for_select
    library_sets.map{|ls| {id: ls.id, name: ls.name}}
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)
    
    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    email = if auth.info.email
        auth.info.email
      elsif auth.extra && auth.extra.raw_info && auth.extra.raw_info.email
        auth.extra.raw_info.email
      elsif auth.email
        auth.email
      end

    first_name = if auth.info.first_name
        auth.info.first_name
      elsif auth.info.firstName
        auth.info.firstName
      elsif auth.extra && auth.extra.raw_info && auth.extra.raw_info.first_name
        auth.extra.raw_info.first_name
      elsif auth.extra && auth.extra.raw_info && auth.extra.raw_info.firstName
        auth.extra.raw_info.firstName
      end

    last_name = if auth.info.last_name
        auth.info.last_name
      elsif auth.info.lastName
        auth.info.lastName
      elsif auth.extra && auth.extra.raw_info && auth.extra.raw_info.last_name
        auth.extra.raw_info.last_name
      elsif auth.extra && auth.extra.raw_info && auth.extra.raw_info.lastName
        auth.extra.raw_info.lastName
      end

    user = signed_in_resource ? signed_in_resource : identity.user
    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      #email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)

      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}#{auth.uid}#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.confirmed_at = Date.current
        user.unconfirmed_email = nil
        user.first_name = first_name
        user.last_name = last_name

        user.skip_confirmation! if user.respond_to?(:skip_confirmation)
        user.save!
      end
    else
      user.confirmed_at = Date.current
      user.email = email if email
      user.unconfirmed_email = nil
      user.first_name = first_name if first_name
      user.last_name = last_name if last_name
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def role?(role)
    return !!self.roles.find_by_name(role.to_s.camelize)
  end

  def logged_in_with_social_media?
    identity = identities.first
    if identity
      ["facebook", "twitter", "linkedin", "google_oauth2"].include?(identities.first.provider)
    else
      false
    end
  end

  def crop_avatar(att)
    # Should we crop?
    Image.resize(self.avatar.path(:original), self.avatar.path(:original), att[:orig_w], att[:orig_h])
    
    scaled_img = Magick::ImageList.new(self.avatar.path)
    orig_img = Magick::ImageList.new(self.avatar.path(:original))

    scale = orig_img.columns.to_f / scaled_img.columns
    args = [ att[:x1], att[:y1], att[:w], att[:h] ]
    args = args.collect { |a| a.to_i * scale }

    orig_img.crop!(*args)
    orig_img.write(self.avatar.path(:original))

    self.avatar.reprocess!
    self.save
  end

  def remaining_storage_space(unit=:B)
    space = unless !verified_photographer?
      photographer_account.storage_space.to_f - photos.sum(:attachment_file_size).to_f
    else
      0
    end

    if unit == :GB
      space = space/1000000000
      space = '%.2f' %space
    end
    "#{space} #{unit}"
  end

  def verified_photographer?
    admin? || photographer_account.present?
  end
  alias_method :photographer?, :verified_photographer?

  def admin?
    admin_account.present?
  end

  def after_database_authentication
    return nil if self.admin?
    ss = ServerStat.find_or_create_by(name: 'logins', date_time: Date.current)
    ss.stat = ss.try(:stat).to_f + 1
    ss.save
  end
end
