class Payment < ActiveRecord::Base
  belongs_to :buyer, class_name: :'User', foreign_key: :'from'
  belongs_to :payment_method
  belongs_to :credit_card
  belongs_to :paypal_account
  has_many :paid_photos,  dependent: :destroy
  has_many :locations, through: :paid_photos

  include Concerns::PaypalAccessToken

  before_save :set_data
  after_create :set_photos

  scope :active, -> {where(verified: true)}
  scope :verifieds, -> {where(verified: true)}

  def photographers
    User.where(id: to).uniq
  end

  def paid_at
    verified_at.to_s(:classical_date) if verified_at.present?
  end

  def photos_by_location
    paid_photos.group_by{|p|p.location_name}
  end

  def total
    paid_photos.sum(:price).to_f
  end

  def source_photos
    Photo.where(id: items).uniq
  end

  def photos_count
    paid_photos.try(:count)
  end

  def setup(type='paypal')
    if type=='paypal'
      setup_paypal_payment
    else
      setup_credit_card_payment
    end
  end

  def setup_paypal_payment
    args = {
      USER: Rails.application.secrets.paypal_api_username,
      PWD: Rails.application.secrets.paypal_api_password,
      SIGNATURE: Rails.application.secrets.paypal_api_signature,
      METHOD: :SetExpressCheckout,
      VERSION: 93,
      RETURNURL: "http://#{Rails.application.secrets.app_host}/checkout/success/?id=#{id}",
      CANCELURL: "http://#{Rails.application.secrets.app_host}/checkout/cancelled/?id=#{id}",
      PAYMENTREQUEST_0_PAYMENTACTION: :SALE,
      PAYMENTREQUEST_0_CURRENCYCODE: :USD,
      PAYMENTREQUEST_0_AMT: source_photos.sum(:price).to_f,
      PAYMENTREQUEST_0_SELLERPAYPALACCOUNTID: Rails.application.secrets.merchant_email,
      PAYMENTREQUEST_0_PAYMENTREQUESTID: "payment_request_#{id}",
    }
    
    args[:EMAIL] = paypal_account.email

    source_photos.each_with_index do |p, i|
      args[:"L_PAYMENTREQUEST_0_NAME#{i}"] = "#{p.file_name} - #{[p.width, p.height].join('x')}"
      args[:"L_PAYMENTREQUEST_0_AMT#{i}"] = p.price.to_f
    end

    http = Curl.post("https://api-3t.#{Rails.application.secrets.paypal_host}/nvp", args) do|req|
    end

    begin
      res = unless http.body.empty?
        CGI::parse http.body_str
      else
        {}
      end

      if res['TOKEN'].present? && res["ACK"].try(:first) == 'Success'
        Rails.cache.write(:token, res['TOKEN'].first)
        @redirect_to = "https://www.#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?cmd=_express-checkout&token=#{res['TOKEN'].first}"
      else
        res = (res||{})['L_LONGMESSAGE0'].try(:first)
        errors.add('payment_error', res)
      end
    rescue Exception => e
      errors.add('payment_error', e)
    end
    self
  end

  def setup_credit_card_payment
    return nil if credit_card.blank?
    token = self.access_token
    items = []
    source_photos.each_with_index do |p, i|
      items[i] = {
        quantity: 1,
        price: "%.2f" % p.price.to_f,
        currency: :USD,
        name: p.file_name,
      }
    end
    args = {
            "intent"=> "sale",
            "payer"=> {
              "payment_method"=> "credit_card",
              "funding_instruments"=> [
                {
                  "credit_card_token"=> {
                    "credit_card_id"=> credit_card.card_id,
                    "payer_id"=> credit_card.payer_id
                  }
                }
              ]
            },
            "transactions"=> [
              {
                "amount"=> {
                  "total"=> "%.2f" % price.to_f,
                  "currency"=> "USD"
                },
                "item_list"=>{
                  "items"=> items
                },
              }
            ]
          }
    http = Curl.post("https://api.#{Rails.application.secrets.paypal_host}/v1/payments/payment", args.to_json) do|req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['Authorization'] = "Bearer #{token}"
    end
    
    res = unless http.body.empty?
      JSON.parse http.body
    else
      {}
    end

    if res["state"]=="approved"
      self.update_attributes({verified: true, verified_at: Date.current})
      set_purchase_stats
      true
    else
      details = res['details'].try(:first).try(:[], 'issue')
      errors.add('payment_error', details||res['message'])
      false
    end
  end

  def parse_paypal_purchase_details
    args = {
      USER: Rails.application.secrets.paypal_api_username,
      PWD: Rails.application.secrets.paypal_api_password,
      SIGNATURE: Rails.application.secrets.paypal_api_signature,
      METHOD: :GetExpressCheckoutDetails,
      VERSION: 93,
      TOKEN: Rails.cache.read(:token),
    }

    http = Curl.post("https://api-3t.#{Rails.application.secrets.paypal_host}/nvp", args) do|req|
    end

    begin
      res = unless http.body.empty?
        CGI::parse http.body
      else
        {}  
      end
      if res['TOKEN'].present? && res["ACK"].try(:first) == 'Success'
        payer_id=res["PAYERID"].first
        do_express_checkout(payer_id)
      end
      res
    rescue Exception => e
      e
    end
  end

  def parse_credit_card_purchase_details
  end

  def do_express_checkout(payer_id)
    args = {
      USER: Rails.application.secrets.paypal_api_username,
      PWD: Rails.application.secrets.paypal_api_password,
      SIGNATURE: Rails.application.secrets.paypal_api_signature,
      METHOD: :DoExpressCheckoutPayment,
      VERSION: 93,
      TOKEN: Rails.cache.read(:token),
      PAYERID: payer_id,
      PAYMENTREQUEST_0_PAYMENTACTION: :SALE,
      PAYMENTREQUEST_0_CURRENCYCODE: :USD,
      PAYMENTREQUEST_0_AMT: source_photos.sum(:price).to_f,
    }

    http = Curl.post("https://api-3t.#{Rails.application.secrets.paypal_host}/nvp", args) do|req|
    end

    res = unless http.body.empty?
      CGI::parse http.body
    else
      {}
    end

    if res["PAYMENTINFO_0_PAYMENTSTATUS"].first == 'Completed'&&res["PAYMENTINFO_0_ACK"].first=='Success'
      self.update_attributes({verified: true, verified_at: Date.current})
    end
    self
  end

  def destribute_payment_to_photographers
    return false unless verified? || photographers_paid?
    args = {
      USER: Rails.application.secrets.paypal_api_username,
      PWD: Rails.application.secrets.paypal_api_password,
      SIGNATURE: Rails.application.secrets.paypal_api_signature,
      METHOD: :MassPay,
      VERSION: 90,
      RECEIVERTYPE: :EmailAddress,
      CURRENCYCODE: :USD,
    }
    photographers.each_with_index do |p, i|
      args[:"L_EMAIL#{i}"] = p.paypal_email
      args[:"L_AMT0#{i}"] = calculate_price(p)
    end

    http = Curl.post("https://api-3t.#{Rails.application.secrets.paypal_host}/nvp", args) do|req|
    end

    res = unless http.body.empty?
      CGI::parse(http.body)
    else
      {}
    end
    if res["ACK"].first == "Success"
      self.update_attributes({photographers_paid: true, photographers_paid_at: Date.current})
    end
  end

  def redirect_to
    @redirect_to
  end

  alias_method :merchants, :photographers
  alias_attribute :photos, :paid_photos

  private
  def set_purchase_stats
    payments_count = buyer.payments.verifieds.count
    acc = buyer.account

    if payments_count == 1 && acc.first_purchase_at.blank?
      acc.first_purchase_at = DateTime.current      
    end

    if payments_count == 5 && acc.fifth_purchase_at.blank?
      acc.fifth_purchase_at = DateTime.current
    end

    acc.save if acc.changed?
  end

  def calculate_price(photographer)
    p = source_photos.sum(:price, conditions: {user_id: photographer.id}).to_f
    p-(p*Rails.application.secrets.tax)
  end

  def set_data
    self.price = source_photos.sum(:price).to_f
    self.tax = Rails.application.secrets.tax * self.price.to_f
  end

  def set_photos
    source_photos.each do |p|
      params = {
        buyer_id: self.from,
        source_id: p.id, 
        photographer_id: p.user_id, 
        price: p.price, 
        date_time: p.date_time, 
        license_id: p.license_id, 
        width: p.width, 
        height: p.height, 
        location_id: p.location_id, 
        is_video: p.video?, 
        is_photo: !p.video?,
        uuid: p.uuid,
      }

      pp = PaidPhoto.find_or_create_by(params)
      if p.video?
        pp.video = p.video
      else
        pp.photo = p.photo
      end
      pp.save
      self.paid_photos << pp
    end
  end
end
