class PaymentMethod < ActiveRecord::Base
  METHOD_TYPES = [:send, :receive]

  belongs_to :payment
  has_many :credit_cards
  has_many :paypal_accounts

  validates :method_type,
    :inclusion => {:in => METHOD_TYPES}
end