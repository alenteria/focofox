angular.module 'regApp'
.directive 'input', [
  '$parse'
  ($parse) ->
    {
      restrict: 'E'
      require: '?ngModel'
      link: (scope, element, attrs) ->
        if attrs.value && attrs.type=='text'
          try
            $parse(attrs.ngModel).assign scope, attrs.value
          catch e
          
        return

    }
]
