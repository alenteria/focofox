angular.module('regApp').controller('registrationController', [
  '$scope'
  'toastr'
  ($scope, toastr) ->
    #
    if (window.flash_success||"").length > 0
      toastr.success(window.flash_success, 'Success!') 

    if (window.flash_notice||"").length > 0
      toastr.warning(window.flash_notice, 'Notice!') 

    if (window.flash_alert||"").length > 0
      toastr.error(window.flash_alert, 'Alert!') 

    if (window.flash_errors||"").length > 0
      toastr.error(window.flash_errors, 'Error!')

    if window.validation_errors
      _.forEach window.validation_errors, (ve, key)->
        if (ve||[]).length > 0
          toastr.error(ve[0], key)
            


    default_avatar_preview = '/missing.png'
    $scope.avatar_preview = default_avatar_preview
    $scope.reg_photographer_warning = 'All fields are required before you can register as a photographer'
    
    (document.getElementById('browse-photos')||{}).onchange = (e)->
      $scope.original_avatar = e.target.files[0]

      if ($scope.original_avatar||{}).size > 5000000
        $scope.avatar_too_large = true
      else
        $scope.avatar_too_large = false

      unless ($scope.original_avatar.type||[]).indexOf('image') >= 0
        $scope.avatar_invalid = true
      else
        $scope.avatar_invalid = false

      loadingImage = loadImage(e.target.files[0], ((img) ->
        unless img.type == "error"
          $('.avatar-preview').html img
          $scope.avatar_preview = img
        else
          $scope.avatar_preview = default_avatar_preview
          $('.avatar-preview').html('<img src="'+default_avatar_preview+'"/>')
          document.getElementById('browse-photos').value = ''
        return
      ), {maxWidth: 200,maxHeight: 200, crop: true, canvas: true})
      
      if !loadingImage
        $scope.avatar_preview = default_avatar_preview
        $('.avatar-preview').html('<img src="'+default_avatar_preview+'"/>')
        document.getElementById('browse-photos').value = ''

      $scope.$apply()
    return
]).directive 'passwordVerify', ->
  {
    require: 'ngModel'
    scope:
      passwordVerify: '='
      ngModel: '='
      validate: '='
    link: (scope, element, attrs, ctrl) ->
      scope.$watch 'ngModel', ->
        window.fox = ctrl
        if scope.ngModel != scope.passwordVerify
          scope.validate.$setValidity 'passwordVerify', false
          undefined
        else
          scope.validate.$setValidity 'passwordVerify', true
          scope.ngModel
        return
      return

  }
.directive 'uniqueEmail',[
  '$http'
  ($http)->
    {
      require: 'ngModel'
      link: (scope, element, attrs, ctrl) ->
        scope.$watch 'email', (val)->
          $http.post('/api/users/verify_email', {email: val}).success (res)->
            if res
              ctrl.$setValidity 'uniqueEmail', false
            else
              ctrl.$setValidity 'uniqueEmail', true
    }
]
.directive 'uniqueUsername',[
  '$http'
  ($http)->
    {
      require: 'ngModel'
      link: (scope, element, attrs, ctrl) ->
        scope.$watch 'username', (val)->
          $http.post('/api/users/verify_username', {username: val}).success (res)->
            if res
              ctrl.$setValidity 'uniqueUsername', false
            else
              ctrl.$setValidity 'uniqueUsername', true
    }
]
.directive 'passwordStrength', [
  '$http'
  ($http)->
    {
      scope: false
      link: (scope, element, attrs, ctrl)->
        scope.$watch 'password', (p)->
          $http.post('/api/users/verify_password', {username: scope.username, email: scope.email, password: scope.password}).success (res)->
            # ctrl.$setValidity 'valid', res.valid
            switch res.status
              when 'weak'
                scope.password_level = 10
              when 'good'
                scope.password_level = 50
              when 'strong'
                scope.password_level = 100
              when 'invalid'
                res.status = 'weak'
                scope.password_level = 0
                
            scope.password_status = res.status
    }
]