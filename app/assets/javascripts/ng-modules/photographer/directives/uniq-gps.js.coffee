angular.module('Photographer')
.directive 'uniqueGps', [
  '$http'
  ($http)->
    {
      require: 'ngModel'
      link: (scope, element, attrs, ctrl) ->
        scope.$watch 'location.gps', (val)->
          $http.post('/api/locations/verify_gps', scope.location, {notify: false, ignoreLoadingBar: true}).success (res)->
            if res.verdict
              scope.nearby_locations = res.nearby
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true
        scope.$watch 'location.gps.latitude', (val)->
          $http.post('/api/locations/verify_gps', scope.location, {notify: false, ignoreLoadingBar: true}).success (res)->
            if res.verdict
              scope.nearby_locations = res.nearby
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true

        scope.$watch 'location.gps.longitude', (val)->
          $http.post('/api/locations/verify_gps', scope.location, {notify: false, ignoreLoadingBar: true}).success (res)->
            if res.verdict
              scope.nearby_locations = res.nearby
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true
    }
]