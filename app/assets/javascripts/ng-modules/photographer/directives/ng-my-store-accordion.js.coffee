angular.module('Photographer').directive 'ngMyStoreAccordion', [
  'localStorageService'
  'toastr'
  (localStorageService, toastr)->
    return{
      restrict: 'AE'
      scope: false
      link: ($scope, el, attrs)->
        $scope.$watch '$state', (s)->
          $('#accordion').accordion 
            active: true
            collapsible: true
            heightStyle: 'content'
            activate: (event, ui)->
              oldHeader = ui.oldHeader
              newHeader = ui.newHeader
              if oldHeader.length > 0 && newHeader.length <=0
                oldIndex = parseInt(oldHeader.attr('data-index'))
                index = if oldIndex == $('#accordion h3').length then 1 else oldIndex + 1
                $('h3[data-index="'+index+'"]').click()
            setTimeout ->
              $('#accordion h3.active').click()
            , 200
          
        $(document).ready ->
          $('#accordion h3').click (e)->
            url = $(this).attr('href')
            if url
              url = url.trim().replace('#','')
            
            $scope.$location.path(url)

            $scope.$apply() if !$scope.$$phase
        return  
    }
]
