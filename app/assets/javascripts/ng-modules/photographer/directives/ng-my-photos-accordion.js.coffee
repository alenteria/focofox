angular.module('Photographer').directive 'ngMyPhotosAccordion', [
  'localStorageService'
  'toastr'
  (localStorageService, toastr)->
    return{
      restrict: 'AE'
      scope: false
      link: ($scope, el, attrs)->
        $scope.$watch '$state', (s)->
          $('#accordion').accordion 
            active: true
            collapsible: true
            heightStyle: 'content'
            activate: (event, ui)->
              oldHeader = ui.oldHeader
              newHeader = ui.newHeader
              if oldHeader.length > 0 && newHeader.length <=0
                oldIndex = parseInt(oldHeader.attr('data-index'))
                index = if oldIndex == $('#accordion h3').length then 1 else oldIndex + 1
                $('h3[data-index="'+index+'"]').click()
            setTimeout ->
              $('#accordion h3.active').click()
            , 200
          
        $(document).ready ->
          $('#accordion h3').click (e)->
            url = $(this).attr('href')
            if url
              url = url.trim().replace('#','')
            return false if (url.indexOf('/photos') >=0 && $('.photos-list').is(':visible'))
            
            child_scope = angular.element('.photos-list').scope()||angular.element('.add-photos-list').scope()

            if $scope.$state.includes('add_photos') && child_scope.queue.length > 0 && url.indexOf('/photos') >=0 #&& $scope.add_photos_first_load
              # $scope.add_photos_first_load = false
              if !$scope.upload_started
                if !confirm('Photos not uploaded will be discarded! Do you wish to proceed?')
                  setTimeout ->
                    $('#accordion').accordion({active: 0})
                  ,100
                  e.preventDefault()
                  return false
              else if $scope.upload_started
                conf = confirm('Photos not uploaded will be discarded! Do you wish to proceed?')
                if !conf
                  setTimeout ->
                    $('#accordion').accordion({active: 0})
                  ,100
                  e.preventDefault()
                  return false

            $scope.$location.path(url)

            $scope.$apply() if !$scope.$$phase
        return  
    }
]
