angular.module 'upload_progress', [
  'LocalStorageModule'
  'angular-loading-bar'
  'toastr'
  'templates'
  'ui.bootstrap'
  'Photographer'
]

.config [
  "$compileProvider"
  "$sceDelegateProvider"
  'localStorageServiceProvider'
  '$httpProvider'
  'toastrConfig'
  (
    $compileProvider,
    $sceDelegateProvider,
    localStorageServiceProvider
    $httpProvider
    toastrConfig
  ) ->
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|blob):/)
    $sceDelegateProvider.resourceUrlWhitelist(['self', /.*/])
    localStorageServiceProvider.setPrefix('focofox_'+user_id)
    angular.extend(toastrConfig, {
      allowHtml: true,
      autoDismiss: true,
      maxOpened: 4
      preventOpenDuplicates: true
      positionClass: 'toast-top-left'
      newestOnTop: true
    })
]
.controller 'main_controller',[
  '$scope'
  '$modal'
  ($scope, $modal)->
    $scope.showDialog = ()->
      $scope.modalInstance = $modal.open
        keyboard: false
        windowClass: 'upload-progress'
        template: ' <div class="modal-header">
                      <h3 class="modal-title text-center">Add Photos Progress</h3>
                    </div>
                    <div class="modal-body">
                      <!--p class="">
                        <b>Filename:</b> {{file.name}}
                      </p>
                      <p class="">
                        <b>Datetime:</b> {{file.date_time}}
                      </p>
                      <p class="">
                        <b>File size:</b> {{file.size | formatFileSize}}
                      </p>
                      <p class="">
                        <b>Price:</b> ${{file.price}}
                      </p>
                      <p class="">
                        <b>Location:</b> {{file.location.name}}
                      </p>
                      <p class="">
                        <b>License:</b> {{getLicenseName()}}
                      </p -->
                      <div class="row" ng-repeat="file in uploading_files">
                        <div class="col-md-2 no-padding" style="overflow: hidden;max-height:23px;padding-right:8px !important;">
                          <p style="overflow: hidden;">{{file.name}}</p>
                        </div>
                        <div class="upload-progress-bar col-md-10" style="padding:0">
                          <div class="col-md-12 progress progress-striped active" style="margin:0;padding:0">
                            <div class="progress-bar progress-bar-success" data-ng-style="{width: file.file_progress + \'%\'}"></div>
                            <span style="float: right;">{{file.file_progress}}%</span>
                          </div>
                          <div class="progress-extended">&#160;</div>
                        </div>
                      </div>
                      <br/>
                      <div class="row">
                        <p ng-show="parent_scope.queue_count-(parent_scope.completedUploadIndex+uploading_files.length)">plus {{parent_scope.queue_count-(parent_scope.completedUploadIndex+uploading_files.length)}} more...</p>
                      </div>
                      <div class="row">
                        <div class="col-md-2" style="padding:0">
                          Total Progress
                        </div>
                        <div class="upload-progress-bar col-md-10" data-ng-class="{in: parent_scope.active()}" style="padding:0">
                          <div class="col-md-12 progress progress-striped active" style="margin:0;padding:0">
                            <div class="progress-bar progress-bar-success" data-ng-style="{width: overall_progress + \'%\'}"></div>
                            <span style="float: right;">{{overall_progress}}%</span>
                          </div>
                          <div class="progress-extended">&#160;</div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-8 text-left" style="margin-bottom:22px;padding:0;">
                          ( {{parent_scope.completedUploadIndex}} uploaded/{{parent_scope.queue_count}} total)
                        </div>
                      </div>
                      <center>
                        <button class="btn btn-warning" ng-click="cancel();">Cancel Remaining Uploads</button>
                      </center>
                    </div>'
        controller: [
          '$scope'
          '$modalInstance'
          'parent_scope'
          ($scope, $modalInstance, parent_scope)->
            $scope.parent_scope = parent_scope
            $scope.getLicenseName = ->
              _l = ''
              try
                $scope.file.license = _.findWhere(parent_scope.licenses, {id: $scope.file.license_id})
                _l = $scope.file.license.title
              catch e
                # ...

              _l

            $scope.cancel = ()->
              $scope.uploading_files = []
              try
                parent_scope.deleteAll()
                parent_scope.queue = []
                ($modalInstance||{dismiss: ->}).dismiss()
              catch e
                parent_scope.deleteAll()
                parent_scope.queue = []
                ($modalInstance||{dismiss: ->}).dismiss()

              parent_scope.$apply() if !parent_scope.$$phase
        ]
        scope: $scope
        backdrop: 'static'
        size: 'md'
        resolve:
          parent_scope: ->
            $scope.parent_scope

      $scope.modalInstance.result.then ()->

      return

    $scope.closeDialog = ->
      ($scope.modalInstance||{dismiss: ->}).dismiss()
]

angular.bootstrap(document.getElementById("upload_progress"),['upload_progress'])
