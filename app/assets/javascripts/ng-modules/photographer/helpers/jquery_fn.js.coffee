thumb_interval = `null`
i = 0
$(document).on 'mouseover', '.photos-list div[data-content-type^=video]', ()->
  $scope = angular.element('.photos-list').scope()
  index = $(@).data('index')
  photo = $scope.photos[index]
  clearInterval thumb_interval
  thumb_interval = setInterval((->
    unless photo.video_thumbnails[i]
      i = 0
    # preload
    img = new Image()
    img.onerror = ->
      i = 0
      clearInterval thumb_interval

    img.onload = ->
      $('.img-'+index).attr('src', photo.video_thumbnails[i]) if ((photo.video_thumbnails[i]||"").match(/\.(jpeg|jpg|gif|png)$/) != null)
    
    img.src = photo.video_thumbnails[i]

    i +=1
    return
  ), 1000)
  return

$(document).on 'mouseout', '.photos-list div[data-content-type^=video]', ()->
  $scope = angular.element('.photos-list').scope()
  i = 0
  clearInterval thumb_interval
  thumb_interval = `null`
  return

leftKeyCode = 37
rightKeyCode = 39
deleteKeyCode = 46
aKeyCode = 65
escKeyCode = 27
$(document).on 'keydown', '.photos-list.thumbnails', (e)->
  $scope = angular.element('.photos-list').scope()
  if e.keyCode == leftKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.photos, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.photos[0]

    index = $scope.photos.indexOf(lastSelected)
    if $scope.photos[index-1]
      index = index-1
    else if  $scope.photos[$scope.photos.length-1]
      index = $scope.photos.length-1

    $scope.selectedPhotos = [$scope.photos[index]]
    $scope.selectedPhotoIds = [$scope.photos[index].id]

  else if e.keyCode == rightKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.photos, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.photos[0]

    index = $scope.photos.indexOf(lastSelected)
    if $scope.photos[index+1]
      index = index+1     
    else if  $scope.photos[0]
      index = 0

    $scope.selectedPhotos = [$scope.photos[index]]
    $scope.selectedPhotoIds = [$scope.photos[index].id]
  else if e.keyCode == deleteKeyCode
    e.preventDefault()
    $scope.deleteSelected()
  else if e.keyCode == aKeyCode && ctrlDown
    e.preventDefault()
    $scope.selectAllPhotos = !$scope.selectAllPhotos
  else if e.keyCode == escKeyCode
    e.preventDefault()
    $scope.selectAllPhotos = true
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []
    $scope.$apply()
    $scope.selectAllPhotos = false

  $scope.$apply()
    # catch
      # 

$(document).on 'mousedown', '.photos-list.thumbnails', (e)->
  $scope = angular.element('.photos-list').scope()
  if $(e.target).hasClass('thumbnails')
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []
    $scope.$apply()

$(document).on 'mousedown', '.photos-list .thumbnail', (e)->
  $scope = angular.element('.photos-list').scope()
  if $(e.target).hasClass('fa-search-plus')
    return false

  element = $(this)
  index = element.data('index')
  photo = $scope.photos[index]
  if shiftDown
    if !$scope.selectedPhotos[0]
      $scope.last_selected_index = index
      $scope.selectedPhotos[0] = photo
      return false

    $scope.last_selected_index = $scope.last_selected_index||index    

    i1 = Math.min($scope.last_selected_index, index)
    i2 = Math.max($scope.last_selected_index, index)

    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []
    $('.thumbnail.ui-selected').removeClass('ui-selected')
    i = i1
    while i <= i2
      photo = $scope.photos[i]
      if photo
        $('.thumbnail[data-id="'+photo.id+'"]').addClass('ui-selected')
        $scope.selectedPhotos.push photo
        $scope.selectedPhotoIds.push photo.id
      i++
    $scope.$apply()
  else if ctrlDown || true #Single click same as with ctrl - issue #108
    photos = angular.copy $scope.selectedPhotos
    ids = $scope.selectedPhotoIds || []
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []
    _s = $(element).data('id') 
    if !_.includes(ids, _s)
      $scope.last_selected_index = index
      photos.push photo
      ids.push photo.id
      $('.thumbnail[data-id="'+photo.id+'"]').addClass('ui-selected')
    else
      $scope.last_selected_index = `null`
      photo = _.findWhere photos, {id: photo.id}
      photos = _.without photos, photo
      ids = _.without ids, photo.id
      $('.thumbnail[data-id="'+photo.id+'"]').removeClass('ui-selected')

    $scope.selectedPhotos = photos
    $scope.selectedPhotoIds = ids

    $scope.$apply()

  else
    if !element.hasClass('ui-selected')
      $scope.last_selected_index = index
      $scope.selectedPhotos = [photo]
      $scope.selectedPhotoIds = [photo.id]
      $scope.$apply()
    else
      $scope.last_selected_index = `null`
      $scope.selectedPhotos = []
      $scope.selectedPhotoIds = []
    
    $scope.$apply()


$(document).on 'mousedown', '.add-photos .thumbnails', (e)->
  $scope = angular.element('.add-photos').scope()
  if $(e.target).hasClass('thumbnails') && !shiftDown && !ctrlDown
    $scope.selectedPhotos = []
    $('.ui-selected').removeClass('ui-selected')
    $scope.$apply() if(!$scope.$$phase)

$(document).on 'mousedown', '.add-photos .thumbnail', (e)->
  $scope = angular.element('.add-photos').scope()
  if $(e.target).hasClass('fa-search-plus')
    return false
  element = $(this)
  index = element.data('index')
  photo = $scope.queue[index]
  if shiftDown
    unless !!$scope.selectedPhotos[0]
      $scope.last_selected_index = index
      element.addClass 'ui-selected'
      $scope.selectedPhotos[0] = photo
      setter($scope.selectedPhotos)
      return false
    else
      
      $scope.last_selected_index = $scope.last_selected_index||index

      i1 = Math.min($scope.last_selected_index, index)
      i2 = Math.max($scope.last_selected_index, index)


      $('.ui-selected').removeClass('ui-selected')
      $scope.selectedPhotos = []

      i = i1
      while i <= i2
        $('[data-index="'+i+'"]').addClass('ui-selected')
        photo = $scope.queue[i]
        if photo
          $scope.selectedPhotos.push photo
        i++
  else if ctrlDown || true #Single click same as with ctrl - issue #108
    if !element.hasClass('ui-selected')
      $scope.last_selected_index = index
      element.addClass 'ui-selected'
      a = $scope.selectedPhotos
      $scope.selectedPhotos = []
      $scope.$apply() if(!$scope.$$phase)
      a.push photo
      $scope.selectedPhotos = a
    else
      $scope.last_selected_index = `null`
      element.removeClass 'ui-selected'
      $scope.selectedPhotos = _.without $scope.selectedPhotos, photo

  else
    if !element.hasClass('ui-selected')
      $scope.last_selected_index = index
      $('.ui-selected').removeClass('ui-selected')
      element.addClass 'ui-selected'
      $scope.selectedPhotos = [photo]
    else
      $scope.last_selected_index = `null`
      $('.ui-selected').removeClass('ui-selected')
      $scope.selectedPhotos = []

  $scope.photos = $scope.selectedPhotos
  $scope.$digest() if(!$scope.$$phase)

$(document).on 'click', 'div', (e)->
  $(e.target).hasClass('fa-info-circle')
  unless _.includes(["select-multiple", "select-one", 'text', 'range', 'number', 'select'], ((e.originalEvent||{}).target||{}).type) || $(e.target).hasClass('fa-info-circle')|| $(e.target).hasClass('show-info')
    $('.thumbnails').focusWithoutScrolling()

leftKeyCode = 37
rightKeyCode = 39
deleteKeyCode = 46
aKeyCode = 65
escKeyCode = 27
$(document).on 'keydown', '.add-photos .thumbnails', (e)->
  $scope = angular.element('.add-photos').scope()
  if e.keyCode == leftKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.queue, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.queue[0]

    index = $scope.queue.indexOf(lastSelected)
    if $scope.queue[index-1]
      index = index-1
    else if  $scope.queue[$scope.queue.length-1]
      index = $scope.queue.length-1

    $('.ui-selected').removeClass('ui-selected')
    $('[data-index="'+index+'"]').addClass('ui-selected')
    $scope.selectedPhotos = [$scope.queue[index]]

  else if e.keyCode == rightKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.queue, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.queue[0]

    index = $scope.queue.indexOf(lastSelected)
    if $scope.queue[index+1]
      index = index+1     
    else if  $scope.queue[0]
      index = 0

    $('.ui-selected').removeClass('ui-selected')
    $('[data-index="'+index+'"]').addClass('ui-selected')
    $scope.selectedPhotos = [$scope.queue[index]]
  else if e.keyCode == deleteKeyCode
    e.preventDefault()
    $scope.deleteSelected()
  else if e.keyCode == aKeyCode && ctrlDown
    e.preventDefault()
    $scope.selectAllPhotos = true
  else if e.keyCode == escKeyCode
    e.preventDefault()
    $scope.selectAllPhotos = true
    $scope.selectedPhotos = []
    $scope.$apply() if(!$scope.$$phase)
    $scope.selectAllPhotos = false

  $scope.$apply() if(!$scope.$$phase)

$(document).on 'click', '.ui-datepicker-close', ()->
 $('.datetime').dblclick()
