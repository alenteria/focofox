$(window).bind 'beforeunload', ()->
  $scope = angular.element('.add-photos-list').scope()
  if $('.upload-progress').is(':visible') || (($scope||{}).queue||[]).length > 0
    return 'Upload in progress, this will be interupted!'
  