angular.module('Photographer')
.controller 'NewEventModalInstanceCtrl', [
	'$scope'
	'$http'
	'$modal'
	'$cookies'
	'localStorageService'
	'$modalInstance'
	($scope, $http, $modal, $cookies, localStorageService, $modalInstance) ->
		$scope.event = {
			location: `null`
			date: ''
			time_from: ''
			time_to: ''
			description: ''
		}
		$scope.save = ()->
			$http.post('/api/events', $scope.event).success (res)->
				$modalInstance.close(res.data)
				
			.error (res)->
				$modalInstance.close()

		$scope.cancel = ()->
			$modalInstance.dismiss 'cancel'

]