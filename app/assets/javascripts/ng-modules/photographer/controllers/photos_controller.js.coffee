url = '/api/photos'
angular.module('Photographer')
.controller 'PhotosCtrl',[
  '$rootScope'
  '$scope'
  '$http'
  '$modal'
  '$cookieStore'
  '$cookies'
  'localStorageService'
  'cfpLoadingBar'
  '$stateParams'
  '$location'
  '$timeout'
  ($rootScope, $scope, $http, $modal, $cookieStore, $cookies, localStorageService, cfpLoadingBar, $stateParams, $location, $timeout) ->
    $http.get('/api/photographer_preferences/default_configs', {ignoreLoadingBar: true}).success (res)->
      $scope.default_photo_price=res.default_photo_price||10
      $scope.licenses=res.license_lists
      $scope.license_lists=$scope.licenses
      $scope.default_license=res.default_license||{}
      $scope.default_license_id=$scope.default_license.id||0
      $scope.locations=res.locations||[]
      $scope.locations_list=$scope.locations||[]
      $scope.default_location=res.default_location||{}
      $scope.default_location_id= $scope.default_location.id
      $scope.photos_count=res.photos_count
      $scope.watermarks=res.watermark_lists
      $scope.watermark_lists=$scope.watermarks
      $scope.default_watermark=res.default_watermark||{}
      $scope.default_watermark_id=$scope.default_watermark.id
      $scope.groups=res.groups
      $scope.groups_list=$scope.groups

      window.navigator.geolocation.getCurrentPosition (position) ->
        latitude = position.coords.latitude
        longitude = position.coords.longitude
        $scope.default_gps_latitude = latitude
        $scope.default_gps_longitude = longitude

        $scope.gps = {latitude: latitude, longitude: longitude}
        
        unless $scope.default_location_id || ($scope.default_location||{}).id
          getAddressNameFromLatLong latitude, longitude, (name)->
            default_location = {latitude: latitude, longitude: longitude, id: 'undefined', name: name}
            exist = _.filter $scope.locations, (loc)->
              parseInt(loc.latitude) == parseInt(latitude) && parseInt(loc.longitude) == parseInt(longitude)

            unless exist[0]
              default_location['notify'] = false
              $http.post('/api/locations', default_location, {ignoreLoadingBar: true}).success (res)->
                if `res.status=="ok"`
                  $scope.default_location = res.data
                  $scope.default_location_id = res.data.id
                  ($scope.locations_list||[]).push($scope.default_location)
                  ($scope.locations||[]).push($scope.default_location)
                  $scope.$apply() if(!$scope.$$phase)
            else
              $scope.default_location_id = exist[0].id
          return
          
      $scope.page_ready = true



    $scope.updateFilterParams = ->
      localStorageService.set('filter', $scope.filter)
      $scope.selectAllPhotos = 0
      params = angular.copy $scope.filter
      params.prices = JSON.stringify params.prices if params.prices
      params.date_times = JSON.stringify params.date_times if params.date_times
      params.locations = JSON.stringify params.locations if params.locations
      params.groups = JSON.stringify params.groups if params.groups
      params.licenses = JSON.stringify params.licenses if params.licenses
      $scope.filter_params = params

    $scope._filter = {}
    cached_filter = (localStorageService.get('filter')||{})
    $scope.filter = {
      prices: cached_filter.prices||[]
      date_times: cached_filter.date_times||[]
      locations: cached_filter.locations||[]
      groups: cached_filter.groups||[]
      licenses: cached_filter.licenses||[]
      sort_by: $scope.sort_by
      media_type: cached_filter.media_type
    }
    $scope.filter_params = {} 
    $scope.updateFilterParams()
    
    $scope.result = {total: 0}
    $scope.thumbnail_size = localStorageService.get('thumbnail_size') || 80
    $scope.groups = []
    $scope.photos_url = url
    $scope.photos = []
    $scope.locations_filter = {}
    $scope.groups_filter = {}
    $scope.licenses_filter = {}
    $scope.is_bulk_update = true

    $scope.allowed_filter = ['date_time','locations.name', 'groups.created_at', 'price']
    $scope.selectedPhotos = localStorageService.get('selectedPhotos')||[]
    $scope.selectedPhotoIds = _.pluck $scope.selectedPhotos||[], 'id'
    $scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200,300,400,500]
    $scope.perPage = parseInt(localStorageService.get('perPage')) || 50
    sort_by = localStorageService.get('sort_by')||'date_time'
    if _.includes($scope.allowed_filter, sort_by)
      $scope.sort_by = sort_by || 'date_time'
    else
      $scope.sort_by = 'date_time'

    $scope.w = {
      width: $(window).width()
      height: $(window).height()
    }
    
    $scope.$on 'pagination:loadStart', (event, status, config)->
      # console.log config

    $scope.$on 'pagination:loadPage', (event, status, config)->
      $scope.is_photos_loaded = true
      $scope.result = event.targetScope.collection||{}
      $scope.photos = ($scope.result||{}).photos||[]

      _ids = angular.copy($scope.selectedPhotoIds)
      $scope.selectedPhotoIds = []
      $timeout ->
        $scope.selectedPhotoIds = _ids
      , 10

    $scope.$watch 'sort_by', (sb)->
      if sb && _.includes($scope.allowed_filter, sb)
        localStorageService.set('sort_by', sb)
        $scope.filter.sort_by = sb
        $scope.updateFilterParams()

    $scope.$watch 'thumbnail_size', (ts)->
      $scope.thumbnail_size = parseInt(ts)
      localStorageService.set('thumbnail_size', ts)
    
    $(document).on 'change', 'select[ng-model="$parent.perPage"]', ()->
      val = $scope.perPagePresets[$(@).val()]
      $scope.updatePerPage(val) if val  
      
    $scope.updatePerPage = (p)->
      localStorageService.set('perPage', parseInt(p))
      if p
        angular.forEach $scope.selectedPhotos, (sp)->
          setTimeout ()->
            $('[data-id="'+sp.id+'"]').addClass('ui-selected')
          ,
            2000
            
    $scope.$watch 'result.photos', (photos)->
      $scope.photos = photos ||[]
      angular.forEach $scope.photos, (p, i)->
        p.selected = true if _.findWhere($scope.selectedPhotos, {id: p.id})
        if (p.video_thumbnails||[]).length>0
          angular.forEach p.video_thumbnails, (t)->
            img = new Image()
            img.src = t
        
        unless p.width && p.height
          loadImage p.attachment_url, ((img) ->
            p.width = img.width
            p.height = img.height

            $scope.$digest()
            return
          )



      # $scope.ignore_filter_warning = $cookies.ignore_filter_warning
      # if photos && photos.length<=0 && $scope.new_filter_type && $scope.ignore_filter_warning!='true' && !$('#filter-warning').is(':visible')
      #   modalInstance = $modal.open
      #     templateUrl: 'ng-modules/photographer/views/shared/_filter_warning.html'
      #     size: 'sm'
      #     controller: 'filterWarningModalInstanceCtrl'
      #     resolve:
      #       ignored: $scope.ignore_filter_warning

      #   modalInstance.result.then (action)->
      #     if action == 'cancel'
      #       obj = $scope.filter[$scope.new_filter_type]
      #       obj.splice(obj.length-1, 1)
      #       $scope.updateFilterParams()
      #     $scope.new_filter_type = ''

    $scope.pickDateFilter = (option)->
      if option.modal
        modalInstance = $modal.open
          template: ' <form><div class="modal-header">
                        <h3 class="modal-title">Datetime Filter</h3>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <div class="input-group-addon">
                          From
                          </div>
                          <input class="datetime form-control" ng-model="datetime_filter_from" type="text" />
                          <div class="input-group-addon">
                          To
                          </div>
                          <input class="datetime form-control" ng-model="datetime_filter_to" type="text"/>
                        </div>

                      </div>
                      <div class="modal-footer">
                      <button class="btn btn-primary" ng-click="add()">Add</button>
                      <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
                      </div></form>'
          controller: 'dateTimeFilterDialogModalInstnceCtrl'
          backdrop: 'static'
          size: 'md'

        modalInstance.result.then (filter)->
          if filter.from && filter.to
            $scope.datetime_filter_from = filter.from
            $scope.datetime_filter_to = filter.to
            $scope.addDateTimeFilter()
      else
        $scope.open_date_filter_form = !$scope.open_date_filter_form

    $scope.cancelDateTimeFilterForm = ->
      $scope.open_date_filter_form = false

    $scope.pickLocationsFilter = ->
      $scope.open_date_filter_form = false

      modalInstance = $modal.open
        templateUrl: 'ng-modules/common/views/locations-filter-dialog.html'
        controller: 'locationsFilterDialogModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        resolve:
          photos: ->
            $scope.photos
          locations: ->
            angular.forEach $scope.filter.locations, (l)->
              _l = _.findWhere($scope.locations, {id: l.id})
              _l.selected = true
            angular.copy($scope.locations)

      modalInstance.result.then (locations)->
        angular.forEach locations, (l)->
          exist = _.findWhere($scope.filter.locations, {id: l.id})
          if l && !!!exist
            ($scope.filter.locations||[]).push l
            $scope.updateFilterParams()

    $scope.updateMediaTypeFilter = (mt)->
      $scope.filter.media_type = mt
      $scope.updateFilterParams(false)

    $scope.removeMediaTypeFilter = ->
      $scope.filter.media_type = `null`
      $scope.updateFilterParams(false)

    $scope.selectAll = ->
      if $scope.selectedPhotos.length != $scope.photos.length
        $scope.selectedPhotos = $scope.photos
        $scope.selectedPhotoIds = $scope.result.all_photo_ids
        $('.thumbnail').addClass('ui-selected')
      else
        $scope.selectedPhotos = []
        $('.ui-selected').removeClass('ui-selected')

      $scope.$apply() if !$scope.$$phase

    $scope.$watch 'selectAllPhotos', (val)->
      if val
        $scope.selectedPhotos = $scope.photos
        $scope.selectedPhotoIds = $scope.result.all_photo_ids
        $('.thumbnail').addClass('ui-selected')
      else
        $scope.selectedPhotos = []
        $scope.selectedPhotoIds = []
        $('.ui-selected').removeClass('ui-selected')
    
    $scope.refresh = ->
      $scope.filter_params._update = !$scope.filter_params._update

    $scope.refresh2 = (event)->
      e = $(event.currentTarget)
      if e.attr('aria-selected')=='false' && $scope.settings_updated
        $scope.refresh()
        $scope.settings_updated = false

    $scope.resetFilter = ->
      $scope.filter =
        prices: []
        date_times: []
        locations: []
        groups: []
        licenses: []

    $scope.deleteSelected = ()->
      $http.post(url+'/bulk_delete', {photos: $scope.selectedPhotoIds}).success (res)->
        $scope.selectedPhotos = []
        $scope.selectedPhotoIds = []
        $scope.refresh()

    $scope.addPriceFilter = (from, to)->
      $scope.price_filter_from = $scope.price_filter_from || from
      $scope.price_filter_to = $scope.price_filter_to || to
      if !!$scope.price_filter_from && !!$scope.price_filter_to
        $scope.new_filter_type = 'prices'
        ($scope.filter.prices||[]).push {from: $scope.price_filter_from, to: $scope.price_filter_to}
        $scope.price_filter_from = ''
        $scope.price_filter_to = ''
        $scope.updateFilterParams()

    $scope.$watch '_filter.datetime_filter_from', (dff)->
      return false unless dff
      return false if $scope._filter.datetime_filter_to && (new Date(dff)) < (new Date($scope._filter.datetime_filter_to))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope._filter.datetime_filter_to = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    # $scope.$watch '_filter.datetime_filter_to', (dft)->
    #   return false unless dft
    #   return false if $scope._filter.datetime_filter_from && (new Date(dft)) > (new Date($scope._filter.datetime_filter_from))

    #   dt = new Date(dft)
    #   dt = dt.minusHours(1)
    #   $scope._filter.datetime_filter_from = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.addDateTimeFilter = (datetime_filter_from, datetime_filter_to)->
      $scope.datetime_filter_from = $scope._filter.datetime_filter_from || datetime_filter_from
      $scope.datetime_filter_to = $scope._filter.datetime_filter_to || datetime_filter_to

      return false unless $scope.datetime_filter_from && $scope.datetime_filter_to
      $scope.new_filter_type = 'date_times'
      ($scope.filter.date_times||[]).push {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
      $scope.datetime_filter_from = `null`
      $scope.datetime_filter_to = `null`
      $scope.updateFilterParams()
      $scope.open_date_filter_form = false

    $scope.addLocationsFilter = ()->
      $scope.new_filter_type = 'locations'
      ($scope.filter.locations||[]).push $scope.locations_filter.selected
      $scope.locations = _.without  $scope.locations, $scope.locations_filter.selected
      $scope.locations_filter = {}
      $scope.updateFilterParams()
    
    $scope.addGroupsFilter = ()->
      $scope.new_filter_type = 'groups'
      ($scope.filter.groups||[]).push $scope.groups_filter.selected
      $scope.groups = _.without $scope.groups, $scope.groups_filter.selected
      $scope.groups_filter = {}
      $scope.updateFilterParams()

    $scope.addLicensesFilter = ()->
      $scope.new_filter_type = 'licenses'
      ($scope.filter.licenses||[]).push $scope.licenses_filter.selected
      $scope.licenses = _.without $scope.licenses, $scope.licenses_filter.selected
      $scope.licenses_filter = {}
      $scope.updateFilterParams()

    $scope.removePriceFilter = ($index)->
      $scope.new_filter_type = ''
      $scope.filter.prices.splice($index, 1)
      $scope.updateFilterParams()

    $scope.removeDateTimeFilter = ($index)->
      $scope.new_filter_type=''
      $scope.filter.date_times.splice($index, 1)
      $scope.updateFilterParams()

    $scope.removeLocationFilter = ($index)->
      $scope.new_filter_type=''
      location = $scope.filter.locations[$index]
      location = _.findWhere($scope.locations, {id: location.id})
      location.selected = false

      $scope.filter.locations.splice($index, 1)
      $scope.updateFilterParams()

    $scope.removeGroupFilter = ($index)->
      $scope.new_filter_type =''
      ($scope.groups||[]).push $scope.filter.groups[$index]
      $scope.filter.groups.splice($index, 1)
      $scope.updateFilterParams()
    
    $scope.removeLicenseFilter = ($index)->
      $scope.new_filter_type=''
      ($scope.licenses||[]).push $scope.filter.licenses[$index]
      $scope.filter.licenses.splice($index, 1)
      $scope.updateFilterParams()
        
    $scope.updateData = (key, val, options)->
      switch key
        when 'price'
          val = if val then val else 0
          $scope.setting.price = val

      $scope.settings_updated = true
      if $scope.selectedPhotos.length > 0

        angular.forEach $scope.selectedPhotos, (photo)->
          orig_photo = _.findWhere $scope.photos, {id: photo.id}
          orig_photo[key] = val if orig_photo
          photo[key] = val

          if key=='date_time'
            orig_photo['formatted_date_time'] = val if orig_photo
            photo['formatted_date_time'] = val

          
          unless $scope.is_bulk_update
            $http.put(url+'/'+photo.id, photo).success (res)->

        if $scope.is_bulk_update
          $scope.bulkUpdate($scope.selectedPhotos, key, val)

    $scope.bulkUpdate = (photos, key, val)->
      $scope.settings_updated = true
      $http.put(url+'/bulk_update', {key: key, val: val, photos: _.pluck(photos, 'id'), notify: false}).success (res)->
        switch res.key
          when 'location_id'
            key = 'location'
          when 'watermark_id'
            timestamp = new Date().getTime()
            angular.forEach photos, (p)->
              preview_url = p.preview_url.split('?')[0]
              p.preview_url = preview_url+'?'+timestamp
              # src = p.attachment_url.split('?')[0]
              # p.attachment_url = src+'?'+timestamp

          else
            key = res.key
        
        # excluded_keys = ['price'] #don't show messages on specific settings i.e. 'price'
        # return false if _.includes excluded_keys, key

        InputElement = $('[ng-model="setting.'+key+'"]')
        $('.notify-'+key).remove()

        if res.status=='ok'
          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
        else
          InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save  <b>'+res.error+'</b></small>')
          InputElement.addClass('border-danger')
          $('.notify-'+key).fadeIn(1000)

        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success').removeClass('border-danger')
        , 2000

      .error (res, statusCode)->
        InputElement = $('[ng-model="setting.'+key+'"]')
        switch key
          when 'location_id'
            key = 'location'
          else
            key = key

        InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save <b>'+res.error+'</b></small>')
        InputElement.addClass('border-danger')
        $('.notify-'+key).fadeIn(1000)

        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success').removeClass('border-danger')
        , 2000

    $scope.$watch 'selectedPhotos', (sp)->
      sp = sp || []
      attrs = ['price', 'license_id', 'location', 'date_time', 'tags', 'watermark_id']

      $.each attrs, (i, attr)->
        vals = _.pluck(sp, attr)
        isCommon = vals.allValuesSame()
        if (vals||[]).length > 0
          switch attr
            when 'location'
              default_val = $scope.default_location
              vals = _.pluck(sp, 'location_id')
              isCommon = vals.allValuesSame()
              vals[0] = _.findWhere($scope.locations_list, {id: (sp[0]||{}).location_id})
            when 'price'
              default_val = $scope.default_photo_price
            when 'tags'
              default_val = []
            when 'watermark_id'
              default_val = $scope.default_watermark_id
            when 'license_id'
              default_val = $scope.default_license_id
            when 'date_time'
              vals = _.pluck(sp, 'formatted_date_time')
              isCommon = vals.allValuesSame()
            else
              default_val = `undefined`
      
          value = (if isCommon then vals[0] else `null`)
          $scope.setting = $scope.setting||{}
          $scope.setting[attr] = value
          $scope.$digest() if !$scope.$$phase

      if `sp.length == ($scope.photos||[]).length`
        $scope.selectAllPhotos = 1
        
      if sp.length <= 0
        $scope.setting = {}
        $scope.$apply() if !$scope.$$phase

    $scope.newGroup = ()->
      modalInstance = $modal.open
        template: ' <form><div class="modal-header">
                      <h3 class="modal-title">New Series</h3>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label>Name:</label>
                        <input class="form-control" type="text" name="group_name" ng-model="group_name">
                      </div>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-primary" ng-click="ok()">OK</button>
                    <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
                    </div></form>'
        controller: 'NewGroupModalInstanceCtrl'
        size: 'sm'
        resolve:
          selectedPhotos: ()->
            $scope.selectedPhotos      

      modalInstance.result.then (group)->
        $scope.activeGroup = group
        ($scope.groups||[]).push group

        angular.forEach $scope.selectedPhotos, (photo)->
          file = _.findWhere $scope.photos, {'$$hashKey': photo['$$hashKey']}
          if file
            if file['groups'] 
              (file['groups']||[]).push group.name
            else
              file['groups'] = [group.name]

      , ()->
        # $log.info('Modal dismissed at: ' + new Date())

    $scope.ungroup = ()->
      $scope.groups = _.without($scope.groups, $scope.activeGroup)
      $scope.activeGroup = `undefined`
      return

    $scope.$watch 'page', (p)->
      $scope.selectAllPhotos = 0
      angular.forEach $scope.selectedPhotos, (sp)->
        setTimeout ()->
          $('[data-id="'+sp.id+'"]').addClass('ui-selected')
        ,
          2000
      return

    $scope.preview = (p)->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/common/views/preview-photo.html'
        controller: 'PreviewPhotoModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        windowClass: 'preview-photo'
        resolve:
          photo: ()->
            p
          photos: ()->
            $scope.photos
          w: ()->
            {
              width: $(window).width()
              height: $(window).height()
            }
          type: ()->
            "my-photos"
            
          photo_info_url: ->
            '/api/photos'

      return false

    $scope.newLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/locations/new.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          notify_success: ->
            false

          location: ()->
            `null`

          parent: ()->
            $scope

          gps: ()->
            default_data = 
              latitude: $scope.default_gps_latitude
              longitude: $scope.default_gps_longitude

            if $scope.selectedPhotos.length > 1
              location_ids = []
              try
                location_ids = _.pluck($scope.selectedPhotos, 'location_id')

              try
                common = location_ids.allValuesSame()
              catch
              # 
              commonData = {}
              commonLocation = _.findWhere $scope.locations, {id: location_ids[0]}
              try
                commonData = 
                  latitude: commonLocation.latitude
                  longitude: commonLocation.longitude 

              gps = if !!common then commonData else default_data
              return gps

            else if $scope.selectedPhotos.length == 1
              try
                gps = {
                  latitude: $scope.selectedPhotos[0].location.latitude
                  longitude: $scope.selectedPhotos[0].location.longitude
                }
              catch e
                # ...
              unless !!gps
                gps = default_data
                
              return gps

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          ($scope.locations_list||[]).push(res.data)
          $scope.locations = $scope.locations_list
          $scope.setting.location = _.findWhere($scope.locations_list, {id: res.data.id})
          $scope.updateData('location', $scope.setting.location)
          $scope.updateData('location_id', $scope.setting.location.id)
        
        key = 'location'
        InputElement = $('[ng-model="setting.'+key+'"]')
        $('.notify-'+key).remove()
        InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
        InputElement.addClass('border-success')
        $('.notify-'+key).fadeIn(1000)
        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success')
        , 2000

    $scope.editLocation = ()->
      return false unless $scope.setting.location
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/locations/edit.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          notify_success: ->
            false
            
          location: ()->
            angular.copy($scope.setting.location) ||{}

          parent: ()->
            $scope

          gps: ()->
            {latitude: $scope.default_gps_latitude, longitude: $scope.default_gps_longitude}
            
      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = (_.findWhere $scope.locations, {id: res.data.id}) ||  (_.findWhere $scope.locations, {name: $scope.setting.location.name})
          if existing
            index = $scope.locations.indexOf existing
            if `res.action=='delete'`
              $scope.locations.splice(index, 1)
              $scope.setting.location = $scope.locations[0].id
              return false
            $scope.locations[index] = angular.copy(res.data)
            $scope.setting.location = $scope.locations[index]
            
        key = 'location'
        InputElement = $('[ng-model="setting.'+key+'"]')
        $('.notify-'+key).remove()
        InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
        InputElement.addClass('border-success')
        $('.notify-'+key).fadeIn(1000)
        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success')
        , 2000

    $scope.watermarkEditPreviewLabel = ()->
      default_watermark = _.findWhere $scope.watermarks, {id: $scope.setting.watermark_id}
      if default_watermark && !!default_watermark.user_id then 'Edit' else 'Preview' 
    
    $scope.newWatermark = ()->
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_new.html'
        controller: 'watermarkModalInstanceCtrl'
        resolve:
          notify_success: ->
            false
            
          watermark: ()->
            {
              display_type: 'tiled'
              tiled_size: 200
              position: 'center center'
              positioned_size: 50
              rotation: 0
              stretched_size: 50
              opacity: 1
            }

      modalInstance.result.then (watermark)->
        ($scope.watermarks||[]).push watermark
        try
          $scope.setting.watermark_id = $scope.watermarks[$scope.watermarks.length-1]['id']
        catch
          # 
        $scope.updateData('watermark_id', $scope.setting.watermark_id)

        key = 'watermark_id'
        InputElement = $('[ng-model="setting.'+key+'"]')
        $('.notify-'+key).remove()
        InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
        InputElement.addClass('border-success')
        $('.notify-'+key).fadeIn(1000)
        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success')
        , 2000

    $scope.editWatermark = ()->
      watermark = _.findWhere $scope.watermarks, {id: $scope.setting.watermark_id}
      if !!!watermark.watermark_image && !!watermark.photo_url
        loadImage(
          watermark.photo_url,
          (img)->
              watermark.watermark_image = {previewUrl: img.src}
              modalInstance = $modal.open
                size: 'lg'
                templateUrl: "ng-modules/photographer/views/preferences/watermarks/_edit.html"
                controller: 'watermarkModalInstanceCtrl'
                resolve:
                  notify_success: ->
                    false
                  watermark: ()->
                    watermark

              modalInstance.result.then (_watermark)->
                if `_watermark.action=='delete'`
                  $scope.watermarks.splice($scope.watermarks.indexOf(_watermark), 1)
                  $scope.setting.watermark = $scope.watermarks[0]
                  $scope.setting.watermark_id = $scope.setting.watermark.id
                else
                  $scope.setting.watermark = _watermark
                  if _watermark.type == 'create'
                    _watermark.type = null
                    ($scope.watermarks||[]).push _watermark
                    try
                      $scope.setting.watermark_id = $scope.watermarks[$scope.watermarks.length-1]['id']
                    catch
                      # 
                    $scope.updateData('watermark_id', $scope.setting.watermark_id)

                key = 'watermark_id'
                InputElement = $('[ng-model="setting.'+key+'"]')
                $('.notify-'+key).remove()
                InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
                InputElement.addClass('border-success')
                $('.notify-'+key).fadeIn(1000)
                setTimeout ->
                  $('.notify-'+key).fadeOut 100, ()-> @remove()
                  InputElement.removeClass('border-success')
                , 2000
              return
          ,
          {maxWidth: 900, noRevoke: true, canvas: false}
        )

        return
      else if !!!watermark.photo_url
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: "ng-modules/photographer/views/preferences/watermarks/_edit.html"
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            watermark: ()->
              watermark
            notify_success: ->
              false

        modalInstance.result.then (_watermark)->
          if `_watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf(_watermark), 1)
            $scope.setting.watermark = $scope.watermarks[0]
            $scope.setting.watermark_id = $scope.setting.watermark.id
          else
            $scope.setting.watermark = _watermark
            if _watermark.type == 'create'
              _watermark.type = null
              ($scope.watermarks||[]).push _watermark
              try
                $scope.setting.watermark_id = $scope.watermarks[$scope.watermarks.length-1]['id']
              catch
                # 
              $scope.updateData('watermark_id', $scope.setting.watermark_id)
          key = 'watermark_id'
          InputElement = $('[ng-model="setting.'+key+'"]')
          $('.notify-'+key).remove()
          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success')
          , 2000
        return
      else
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: "ng-modules/photographer/views/preferences/watermarks/_edit.html"
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            watermark: ()->
              watermark
            notify_success: ->
              false

        modalInstance.result.then (_watermark)->
          if `_watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf(_watermark), 1)
            $scope.setting.watermark = $scope.watermarks[0]
            $scope.setting.watermark_id = $scope.setting.watermark.id
          else
            $scope.setting.watermark = _watermark
            if _watermark.type == 'create'
              _watermark.type = null
              ($scope.watermarks||[]).push _watermark
              try
                $scope.setting.watermark_id = $scope.watermarks[$scope.watermarks.length-1]['id']
              catch
                # 
              $scope.updateData('watermark_id', $scope.setting.watermark_id)
          key = 'watermark_id'
          InputElement = $('[ng-model="setting.'+key+'"]')
          $('.notify-'+key).remove()
          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success')
          , 2000
        return
      false
    $scope.sessionGroupSearch = (query) ->
      params = {q: query, sensor: false}
      reqStr = '/api/groups/search'

      $http.get(reqStr, {params: params}, {ignoreLoadingBar: true}).then ((response) ->
        if response.data.length > 0
          data = response.data||[{}]

        data = _.uniq data, (g)->
          g.id
        data = _.filter data, (g)->
          parseInt(g.name[0]) >= 1

        $scope.groups = $scope.groups_list = data

        return
      ), ->
        console.log 'ERROR!!!'
        return
      return
    $scope.licenseSearch = (query) ->
      params = {q: query, sensor: false}
      reqStr = '/api/licenses/search'

      $http.get(reqStr, {params: params}, {ignoreLoadingBar: true}).then ((response) ->
        if response.data.length > 0
          data = response.data

        $scope.licenses = $scope.licenses_list = data

        prev_query = query
        return
      ), ->
        console.log 'ERROR!!!'
        return
      return

    $scope.$watch 'filterSelect', (f)->
      switch f
        when 'none'
          $scope.selectedPhotoIds = []
        when 'visible'
          $scope.selectedPhotoIds = _.pluck($scope.photos, 'id')
        when 'all'
          $scope.selectedPhotoIds = $scope.result.all_photo_ids

    $scope.$watch 'selectedPhotoIds', (ids)->
      $scope.selectedPhotos = []
      $('.ui-selected').removeClass('ui-selected')
      angular.forEach ids, (id)->
        $scope.selectedPhotos.push _.findWhere $scope.photos, {id: id}
        $('[data-id="'+id+'"]').addClass('ui-selected')

    return
]
