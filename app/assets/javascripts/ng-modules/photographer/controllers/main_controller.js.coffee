angular.module('Photographer')
.controller 'MainCtrl',[
  '$rootScope'
  '$scope'
  '$state'
  '$location'
  'Auth'
  'toastr'
  ($rootScope, $scope, $state, $location, Auth, toastr)->
    $scope.$state = $state
    $scope.$location = $location

    $rootScope.$on '$stateChangeSuccess', (ev, to, toParams, from, fromParams)->
      if $scope.$state.includes('store.sales_history')
        $scope.paginator_label = 'Sales'
      else if $scope.$state.includes('store.event_manager')
        $scope.paginator_label = 'Events'

      $scope.$previousState = from
      $scope.$previousState.params = fromParams

    $rootScope.$on '$stateChangeStart', (ev, to, toParams, from, fromParams)->
      child_scope = angular.element('.add-photos-list').scope()

      if from.url=='/add-photos' && to.url.indexOf('/photos/')<0 && child_scope.queue.length > 0
        if !child_scope.upload_started
          if !confirm('Photos not uploaded will be discarded! Do you wish to proceed?')
            ev.preventDefault()
            return false
        else if child_scope.upload_started
          conf = confirm('Photos not uploaded will be discarded! Do you wish to proceed?')
          if child_scope.queue.length > 0 && child_scope.queue_count != child_scope.completedUploadIndex && conf
            #upload_scope = angular.element('#upload_progress [ng-controller]').scope()
            #upload_scope.child_scope = child_scope
            #upload_scope.file = upload_scope.file||child_scope.queue[0]
            #upload_scope.showDialog() unless $('.upload-progress').is(':visible')
          else if !conf
            ev.preventDefault()
            return false


    return
]