angular.module('Photographer')
.controller 'StoreCtrl', [
  '$scope'
  '$http'
  '$modal'
  '$cookies'
  '$cookieStore'
  'localStorageService'
  '$state'
  ($scope, $http, $modal, $cookies, $cookieStore, localStorageService, $state) ->
    $scope.data = {}
    $http.get('/api/photographer_preferences/default_configs', {ignoreLoadingBar: true}).success (res)->
      $scope.default_photo_price=res.default_photo_price||10
      $scope.licenses=res.license_lists
      $scope.license_lists=$scope.licenses
      $scope.default_license=res.default_license||{}
      $scope.default_license_id=$scope.default_license.id||0
      $scope.locations=res.locations||[]
      $scope.locations_list=$scope.locations
      $scope.default_location=res.default_location||{}
      $scope.default_location_id= $scope.default_location.id
      $scope.photos_count=res.photos_count
      $scope.watermarks=res.watermark_lists
      $scope.watermark_lists=$scope.watermarks
      $scope.default_watermark=res.default_watermark||{}
      $scope.default_watermark_id=$scope.default_watermark.id
      $scope.groups=[]
      $scope.groups_list=$scope.groups
      
    $scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200,300,400,500]
    $scope.perPage = parseInt($cookies.perPage) || 50
    $scope.sales_url = '/api/sales/history'
    $scope.events_url = '/api/events'

    window.navigator.geolocation.getCurrentPosition (position) ->
      $scope.default_gps_latitude = position.coords.latitude
      $scope.default_gps_longitude = position.coords.longitude

    cached_filter = (localStorageService.get('filter')||{})
    $scope.filter = {
      date_times: cached_filter.date_times||[]
      locations: cached_filter.locations||[]
      sort: cached_filter.sort||[]
      sort_type: cached_filter.sort_type||[]
    }
    $scope.filter_params = {} 
    setTimeout ->
      $scope.updateFilterParams()
    , 300

    $scope.updatePerPage = (p)->
      $cookieStore.put('perPage', parseInt(p))

    $scope.addDateTimeFilter = (datetime_filter_from, datetime_filter_to)->
      $scope.datetime_filter_from = $scope.datetime_filter_from || datetime_filter_from
      $scope.datetime_filter_to = $scope.datetime_filter_to || datetime_filter_to

      return false unless $scope.datetime_filter_from && $scope.datetime_filter_to
      $scope.new_filter_type = 'date_times'
      $scope.filter.date_times.push {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
      $scope.datetime_filter_from = `null`
      $scope.datetime_filter_to = `null`
      $scope.updateFilterParams()
      $scope.open_date_filter_form = false

    $scope.refresh = ->
      $scope.filter_params._update = !$scope.filter_params._update

    $scope.updateFilterParams = ->
      localStorageService.set('filter', $scope.filter)
      params = angular.copy $scope.filter
      params.date_times = JSON.stringify params.date_times if params.date_times
      params.locations = JSON.stringify params.locations if params.locations
      params.sort = params.sort
      params.sort_type = params.sort_type
      $scope.filter_params = params

    $scope.pickDateFilter = (option)->
      if option.modal
        modalInstance = $modal.open
          template: ' <form><div class="modal-header">
                        <h3 class="modal-title">Datetime Filter</h3>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <div class="input-group-addon">
                          From
                          </div>
                          <input class="datetime form-control" ng-model="datetime_filter_from" type="text" />
                          <div class="input-group-addon">
                          To
                          </div>
                          <input class="datetime form-control" ng-model="datetime_filter_to" type="text"/>
                        </div>

                      </div>
                      <div class="modal-footer">
                      <button class="btn btn-primary" ng-click="add()">Add</button>
                      <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
                      </div></form>'
          controller: 'dateTimeFilterDialogModalInstnceCtrl'
          backdrop: 'static'
          size: 'md'

        modalInstance.result.then (filter)->
          if filter.from && filter.to
            $scope.datetime_filter_from = filter.from
            $scope.datetime_filter_to = filter.to
            $scope.addDateTimeFilter()
      else
        $scope.open_date_filter_form = !$scope.open_date_filter_form

    $scope.cancelDateTimeFilterForm = ->
      $scope.open_date_filter_form = false

    $scope.pickLocationsFilter = ->
      $scope.open_date_filter_form = false

      modalInstance = $modal.open
        templateUrl: 'ng-modules/common/views/locations-filter-dialog.html'
        controller: 'locationsFilterDialogModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        resolve:
          photos: ->
            $scope.photos
          locations: ->
            locations = _.map $scope.locations, (l)->
              l.selected = !!(l.selected)
              l
            locations           

      modalInstance.result.then (locations)->
        angular.forEach locations, (l)->
          exist = _.findWhere($scope.filter.locations, {id: l.id})
          if l && !!!exist
            $scope.filter.locations.push l
            $scope.updateFilterParams()

    $scope.removeDateTimeFilter = ($index)->
      $scope.new_filter_type=''
      $scope.filter.date_times.splice($index, 1)
      $scope.updateFilterParams()

    $scope.removeLocationFilter = ($index)->
      $scope.new_filter_type=''
      location = $scope.filter.locations[$index]
      location = _.findWhere($scope.locations, {id: location.id}) || {}
      location.selected = false

      $scope.filter.locations.splice($index, 1)
      $scope.updateFilterParams()

    $scope.viewSalesDetails = (sale)->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/photographer/views/store/_sale-details.html'
        controller: [
          '$scope'
          '$modalInstance'
          'sale'
          ($scope, $modalInstance, sale)->
            $scope.sale = sale
            $scope.cancel = ->
              $modalInstance.dismiss('cancel')
            $scope.preview = (p)->
              modalInstance = $modal.open
                templateUrl: 'ng-modules/common/views/preview-photo.html'
                controller: 'PreviewPhotoModalInstanceCtrl'
                size: 'lg'
                backdrop: 'static'
                windowClass: 'preview-photo'
                resolve:
                  photo: ()->
                    p
                  photos: ()->
                    sale.photos

                  w: ()->
                    {
                      width: $(window).width()
                      height: $(window).height()
                    }
                  type: ()->
                    "my-photos"
                    
                  photo_info_url: ->
                    '/api/paid_photos'

              return false

            $scope.total_price = 0
            angular.forEach sale.photos, (p)->
              $scope.total_price += parseFloat(p.price)

        ]

        size: 'md'
        backdrop: 'static'
        resolve:
          sale: ->
            sale        

      modalInstance.result.then (event)->

    $scope.newEvent = ()->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/photographer/views/store/_new_event.html'
        controller: 'NewEventModalInstanceCtrl'
        size: 'md'
        scope: $scope
        backdrop: 'static'
        # resolve:        

      modalInstance.result.then (ev)->
        if ev
          $scope.data.events.push ev
      return

    $scope.listEvents = ()->

    $scope.newLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/locations/new.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          location: ()->
            {}
          gps: ()->
            default_data = 
              latitude: $scope.default_gps_latitude
              longitude: $scope.default_gps_longitude

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          $scope.locations_list.push(res.data)
          $scope.locations.push(res.data)
          $scope.location = $scope.locations[$scope.locations.length-1]
          $scope.updateData('location', $scope.location)
          $scope.updateData('location_id', $scope.location.id)
          
    $scope.editLocation = (location)->
      location = _.findWhere($scope.locations, {id: location.id})
      return false unless location
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/locations/edit.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          location: ()->
            angular.copy(location) ||{}
          gps: ()->
            {latitude: $scope.default_gps_latitude, longitude: $scope.default_gps_longitude}
            
      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = (_.findWhere $scope.locations, {id: res.data.id}) ||  (_.findWhere $scope.locations, {name: $scope.location.name})
          if existing
            index = $scope.locations.indexOf existing
            if `res.action=='delete'`
              $scope.locations.splice(index, 1)
              $scope.location = $scope.locations[0].id
              return false
            $scope.locations[index] = angular.copy(res.data)
            $scope.location = $scope.locations[index]

    $scope.$watch 'data.sales', (s)->
      $scope.sales = s
      $scope.data.total_sales = 0
      $scope.data.total_earnings = 0
      _.each s, (_s)->
        $scope.data.total_sales += _s.photos_count
        $scope.data.total_earnings += _s.total_price
      
    $scope.sort = (sort)->
      $scope.data.sorted = true
      $scope.filter.sort = sort

      if sort != $scope.filter.sort
        $scope.filter.sort_type = 'asc'
      else
        $scope.filter.sort_type = if $scope.filter.sort_type=='asc' then 'desc' else 'asc'

      $scope.data.page = (angular.element('.paginate-anything').scope()||{}).page+1
      $scope.updateFilterParams()
      setTimeout ->
        if $scope.data.sorted && (angular.element('.paginate-anything').scope()||{}).page+1 != $scope.data.page
          $($('ul.pagination li')[parseInt($scope.data.page)]).children('a').click()
        $scope.data.sorted = false
      , 0.5
]