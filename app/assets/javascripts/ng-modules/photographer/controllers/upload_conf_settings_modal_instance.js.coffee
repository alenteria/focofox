watermark_url = '/api/watermarks'
location_url = '/api/locations'
license_url = '/api/licenses'
angular.module('Photographer')
.controller 'uploadConfirmationSettingsModalInstanceCtrl',[
  '$scope'
  "$modal"
  "$modalInstance"
  "locations"
  "licenses"
  "watermarks"
  "default_photo_price"
  "default_license"
  "default_location"
  "default_watermark"
  "is_resize_photos"
  "max_width"
  "max_height"
  "$timeout"
  "$http"
  ($scope, $modal, $modalInstance, locations, licenses, watermarks, default_photo_price, default_license, default_location, default_watermark, is_resize_photos, max_height, max_width, $timeout, $http)->
    $scope.override_attrs = []
    $scope.locations = locations
    $scope.licenses = licenses
    $scope.watermarks = watermarks
    $scope.default_photo_price = default_photo_price
    $scope.default_license = default_license
    $scope.default_license_id = default_license.id
    $scope.default_location = default_location
    $scope.default_location_id = default_location.id
    $scope.default_watermark = default_watermark
    $scope.default_watermark_id = default_watermark.id
    $scope.is_resize_photos = is_resize_photos
    $scope.max_width = max_width
    $scope.max_height = max_height

    today = new Date()
    date = $.datepicker.formatDate('mm/dd/yy', today)
    time = formatAMPM(today)
    $scope.default_date_time = date+' '+time

    $scope.watermarkEditPreviewLabel = ()->
      default_watermark = $scope.default_watermark
      if default_watermark && !!default_watermark.user_id then 'Edit' else 'Preview' 
    $scope.editWatermark = ()->
      $scope.watermark = $scope.default_watermark
      if !!!$scope.watermark.watermark_image && !!$scope.watermark.photo_url
        loadImage(
          $scope.watermark.photo_url,
          (img)->
              $scope.watermark.watermark_image = {previewUrl: img.src}
              modalInstance = $modal.open
                size: 'lg'
                templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
                controller: 'watermarkModalInstanceCtrl'
                resolve:
                  notify_success: ->
                    false
                  watermark: ()->
                    $scope.watermark

              modalInstance.result.then (watermark)->
                if `watermark.action=='delete'`
                  $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
                  $scope.default_watermark = $scope.watermarks[0]
                  $scope.default_watermark_id = $scope.default_watermark.id
                else
                  $scope.watermark = watermark
                  if watermark.type == 'create'
                    $scope.watermarks.push watermark
                    try
                      $scope.default_watermark = watermark
                      $scope.default_watermark_id = $scope.default_watermark.id
                      $scope.mark_default_watermark(watermark.id)
                    catch

              return
          ,
          {maxWidth: 900, noRevoke: true, canvas: false}
        )

        return
      else if !!!$scope.watermark.photo_url
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            notify_success: ->
              false
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0]
            $scope.default_watermark_id = $scope.default_watermark.id
          else
            $scope.watermark = watermark
            if watermark.type == 'create'
              $scope.watermarks.push watermark
              try
                $scope.default_watermark = watermark
                $scope.default_watermark_id = $scope.default_watermark.id
                $scope.mark_default_watermark(watermark.id)
              catch
        return
      else
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            notify_success: ->
              false
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0]
            $scope.default_watermark_id = $scope.default_watermark.id
          else
            $scope.watermark = watermark
            if watermark.type == 'create'
              $scope.watermarks.push watermark
              try
                $scope.default_watermark = watermark
                $scope.default_watermark_id = $scope.default_watermark.id
                $scope.mark_default_watermark(watermark.id)
              catch
        return
      false

    $scope.newWatermark = ()->
      $scope.watermark = 
        display_type: 'tiled'
        tiled_size: 200
        position: 'center center'
        positioned_size: 50
        rotation: 0
        stretched_size: 50
        opacity: 1

      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_new.html'
        controller: 'watermarkModalInstanceCtrl'
        resolve:
          notify_success: ->
            false
          watermark: ()->
            $scope.watermark

      modalInstance.result.then (watermark)->
        $scope.watermarks.push watermark
        try
          $scope.default_watermark = watermark
          $scope.default_watermark_id = $scope.default_watermark.id
          $scope.mark_default_watermark(watermark.id)
        catch
          # 

    $scope.mark_default_watermark = (dw)->
      if dw
        $scope.override_attrs.push 'default_watermark' if _.indexOf($scope.override_attrs, 'default_watermark') < 0
        id = if typeof(dw) is 'object' then dw.id else dw
        $scope.default_watermark = _.findWhere($scope.watermarks, {id: id})
        $http.post(watermark_url+"/"+dw+'/mark_default_watermark/', {id: id}, {notify_success: false}).success (res)->

    $scope.mark_default_location = (dl)->
      if dl
        $scope.override_attrs.push 'default_location' if _.indexOf($scope.override_attrs, 'default_location') < 0
        id = if typeof(dl) is 'object' then dl.id else dl
        $scope.default_location = _.findWhere($scope.locations, {id: id})
        $http.post(location_url+"/"+dl+'/mark_default_location/', {id: id}, {notify_success: false}).success (res)->
      
    $scope.setting = {location_id: $scope.default_location_id, location: {}}
    $scope.updateData = (key, val)->
      $scope.default_location_id = if (val||{}).id then val.id else val
      $scope.mark_default_location($scope.default_location_id)

    $scope.newLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        backdrop: 'static'
        keyboard: false
        templateUrl: 'ng-modules/photographer/views/preferences/locations/new.html'
        controller: 'locationModalInstanceCtrl'
        windowClass: 'location-preferences-dialog'
        resolve:
          notify_success: ->
            false
          location: ()->
            `null`

          parent: ()->
            $scope

          gps: ()->
            angular.copy($scope.gps)

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = _.findWhere $scope.locations, {id: res.data.id}
          unless existing
            $scope.locations.push(res.data)
            $scope.default_location = $scope.locations[$scope.locations.length-1]
            $scope.default_location_id = $scope.default_location.id
            $scope.mark_default_location($scope.default_location_id)

    $scope.editLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        backdrop: 'static'
        keyboard: false
        templateUrl: 'ng-modules/photographer/views/preferences/locations/edit.html'
        controller: 'locationModalInstanceCtrl'
        windowClass: 'location-preferences-dialog'
        resolve:
          notify_success: ->
            false
          location: ()->
            angular.copy _.findWhere $scope.locations, {id: $scope.default_location_id}

          parent: ()->
            $scope

          gps: ()->
            $scope.gps
            
      modalInstance.result.then (res)->        
        if `res.status=="ok"`
          if $scope.default_location=='undefined'
            d = _.findWhere $scope.locations, {id: 'undefined'}
            d.id = res.data.id
            $scope.default_location = d
            $scope.default_location_id = d.id

          existing = _.findWhere $scope.locations, {id: res.data.id}

          if existing
            index = $scope.locations.indexOf existing
            if `res.action=='delete'`
              $scope.locations.splice(index, 1)
              $scope.default_location = $scope.locations[0]
              $scope.default_location_id = $scope.default_location.id
              return false
            $scope.locations[index] = res.data
        return
    $scope.update_default_photo_price = (p)->
      $scope.default_photo_price = parseInt(p)  
      if $scope.default_photo_price
        $scope.override_attrs.push 'default_photo_price' if _.indexOf($scope.override_attrs, 'default_photo_price') < 0
        $http.put('/api/photographer_preferences/update_default_photo_price', {default_photo_price: $scope.default_photo_price}, {notify_success: false, ignoreLoadingBar: true}).success (res)->
          
    $scope.newLicense = ()->
      $scope.license = {}
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/licenses/_new.html'
        controller: 'licenseModalInstanceCtrl'
        resolve:
          license: ()->
            $scope.license

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = _.findWhere $scope.licenses, {id: res.data.id}
          unless existing
            $scope.licenses.push(res.data)
            $scope.default_license = $scope.licenses[$scope.licenses.length-1]
            $scope.default_license_id = $scope.default_license.id

    $scope.editPreviewLicenseLabel = ()->
      default_license = $scope.default_license
      if default_license && !!default_license.user_id then 'Edit' else 'Preview'

    $scope.editLicense = ()->
      $scope.license = angular.copy $scope.default_license
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/licenses/_edit.html'
        controller: 'licenseModalInstanceCtrl'
        resolve:
          license: ()->
            $scope.license

      modalInstance.result.then (license)->
        if `license.action=='delete'`
          $scope.licenses = $scope.licenses.splice($scope.licenses.indexOf($scope.license), 1)
          $scope.default_license = $scope.licenses[0]
          $scope.default_license_id = $scope.licenses[0].id
        else          
          existing = _.findWhere $scope.licenses, {id: license.data.id}
          if existing
            index = $scope.licenses.indexOf existing
            $scope.licenses[index] = license.data

    $scope.mark_default_license = (dl)->
      if dl
        $scope.override_attrs.push 'default_license' if _.indexOf($scope.override_attrs, 'default_license') < 0
        id = if typeof(dl) is 'object' then dl.id else dl
        $scope.default_license = _.findWhere($scope.licenses, {id: id})
        $http.put(license_url+"/"+dl+'/mark_default/', {id: dl}, {notify_success: false}).success (res)->

    $scope.update_default_datetime = (dt)->
      if dt
        $scope.override_attrs.push 'default_date_time' if _.indexOf($scope.override_attrs, 'default_date_time') < 0

    $scope.cancel = ->
      args = {
        default_photo_price: $scope.default_photo_price
        default_license: $scope.default_license
        default_location: $scope.default_location
        default_watermark: $scope.default_watermark
        is_resize_photos: $scope.is_resize_photos
        max_width: $scope.max_width
        max_height: $scope.max_height
      }
      $modalInstance.dismiss(args)

    $scope.uploadNow = ->
      args = {
        default_photo_price: $scope.default_photo_price
        default_license: $scope.default_license
        default_location: $scope.default_location
        default_watermark: $scope.default_watermark
        is_resize_photos: $scope.is_resize_photos
        max_width: $scope.max_width
        max_height: $scope.max_height
        override_attrs: $scope.override_attrs
      }      
      
      $modalInstance.close(args)     

    initializingResizePhotos = true
    $scope.$watch 'is_resize_photos', (rp)->
      if initializingResizePhotos
        $timeout ->
          initializingResizePhotos = false
        ,1000
        return false

      return false unless $scope.max_width && $scope.max_height

      $http.post('/api/photographer_preferences/disable_photo_resizing', {value: rp}, {notify_success: false, ignoreLoadingBar: true}).success (res)->

    initializingSizes = true
    $scope.$watchGroup ['max_width', 'max_height'], (val, oldVal)->
      if initializingSizes
        $timeout ->
          initializingSizes = false
        , 1000
        return false

      if val[0] && val[1] && $scope.is_resize_photos
        $scope.max_width = parseInt(val[0])
        $scope.max_height = parseInt(val[1])
        $http.put('/api/photographer_preferences/update_photo_resizing',{max_width: $scope.max_width, max_height: $scope.max_height}, {notify_success: false, ignoreLoadingBar: true}).success (res)->

]