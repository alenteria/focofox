TYPES = ['system-wide-sales', 'my-sales']
angular.module('Photographer')
.controller 'StatCtrl',[
  '$rootScope'
  '$scope'
  '$http'
  '$modal'
  '$cookieStore'
  '$cookies'
  'localStorageService'
  'cfpLoadingBar'
  '$stateParams'
  '$location'
  '$timeout'
  'ChartJs'
  ($rootScope, $scope, $http, $modal, $cookieStore, $cookies, localStorageService, cfpLoadingBar, $stateParams, $location, $timeout, ChartJs) ->
    $scope.subpage = $scope.$state.params.type
    price_demand_percentage_calc = (purchases, type)->
      $scope.data = [[]]
      $scope.labels = ['0%', '25%', '50%', '75%', '100%']
      $scope.purchases = purchases
      max_price = parseInt _.max(_.pluck(purchases, 'price'))
      br = parseInt((max_price/5)/2)

      c0 = (_.filter purchases, ((o)-> o.price > parseInt(0.1*max_price)-br && o.price < (parseInt(0.1*max_price)+br))) || []
      c1 = (_.filter purchases, ((o)-> o.price > parseInt(0.25*max_price)-br && o.price < (parseInt(0.25*max_price)+br))) || []
      c2 = (_.filter purchases, ((o)-> o.price > parseInt(0.50*max_price)-br && o.price < (parseInt(0.50*max_price)+br))) || []
      c3 = (_.filter purchases, ((o)-> o.price > parseInt(0.75*max_price)-br && o.price < (parseInt(0.75*max_price)+br))) || []
      c4 = (_.filter purchases, ((o)-> o.price > parseInt(max_price)-br && o.price < (parseInt(max_price)+br)))
      
      price_count = [
        {count: c0.length, price: _.reduce(_.pluck(c0, 'price'), (a, b)->parseInt(a)+parseInt(b))/c0.length}
        {count: c1.length, price: _.reduce(_.pluck(c1, 'price'), (a, b)->parseInt(a)+parseInt(b))/c1.length}
        {count: c2.length, price: _.reduce(_.pluck(c2, 'price'), (a, b)->parseInt(a)+parseInt(b))/c2.length}
        {count: c3.length, price: _.reduce(_.pluck(c3, 'price'), (a, b)->parseInt(a)+parseInt(b))/c3.length}
        {count: c4.length, price: _.reduce(_.pluck(c4, 'price'), (a, b)->parseInt(a)+parseInt(b))/c4.length}
      ]
      $scope.price_count = price_count
      
      total_count = purchases.length
      $scope.total_count = total_count
      br2 = parseInt((total_count/5)/2)
      # 25%
      i0 = parseInt(0.0*total_count)
      i1 = parseInt(0.25*total_count)
      i2 = parseInt(0.50*total_count)
      i3 = parseInt(0.75*total_count)
      i4 = parseInt(1*total_count)

      $scope.p1 = (_.filter price_count, ((o)-> o.count > i0-br2 && o.count < (i0+br2)))
      $scope.p1 = parseInt _.reduce(_.pluck($scope.p1, 'price'), (a,b)->parseInt(a)+parseInt(b))/$scope.p1.length || 0
      $scope.p25 = (_.filter price_count, ((o)-> o.count > (i0+i1)/2 && o.count < (i1+i2)/2))
      $scope.p25 = parseInt _.reduce(_.pluck($scope.p25, 'price'), (a,b)->parseInt(a)+parseInt(b))/$scope.p25.length || 0
      $scope.p50 = (_.filter price_count, ((o)-> o.count > (i1+i2)/2 && o.count < (i2+i3)/2))
      $scope.p50 = parseInt _.reduce(_.pluck($scope.p50, 'price'), (a,b)->parseInt(a)+parseInt(b))/$scope.p50.length || 0
      $scope.p75 = (_.filter price_count, ((o)-> o.count > (i2+i3)/2 && o.count < (i3+i4)/2))
      $scope.p75 = parseInt _.reduce(_.pluck($scope.p75, 'price'), (a,b)->parseInt(a)+parseInt(b))/$scope.p75.length || 0
      $scope.p100 = (_.filter price_count, ((o)-> o.count > (i3+i4)/2 && o.count < total_count))
      $scope.p100 = parseInt _.reduce(_.pluck($scope.p100, 'price'), (a,b)->parseInt(a)+parseInt(b))/$scope.p100.length || 0

      $scope.data = [[$scope.p1, $scope.p25, $scope.p50, $scope.p75, $scope.p100]]

      $scope.maxPoint = 0
      max = 0
      angular.forEach $scope.labels, (l, i)->
        m = parseInt(l)*$scope.data[0][i]
        if m > max
          max = m
          $scope.maxPoint = i                    

      $scope.options = {lineAtIndex: $scope.maxPoint}

    fetch_photos_viewed_vs_purchased_data = (options)->
      options = options||{}
      options.system_wide = options.system_wide||''
      $http.get('/api/statistics/photos_viewed_vs_purchased?system_wide='+options.system_wide).success (stat)->
        $scope.labels = _.sortBy(stat.dates)
        $scope.series = ['Photos Previewed', 'Photos Purchased']
        $scope.data = [[], []]
        angular.forEach $scope.labels, (dt)->
          _d = stat.photos_previewed[dt] || []
          _d2 = stat.photos_purchased[dt] || []
          $scope.data[0].push(_d.length)
          $scope.data[1].push(_d2.length)
    fetch_location_views_stats = ()->
      $http.get('/api/location_views/my_stats').success (stats)->
        $scope.labels = _.keys(stats)
        $scope.data = [[]]
        angular.forEach $scope.labels, (l)->
          $scope.data[0].push stats[l].length

    switch $scope.$state.params.type
      when ("")
        $http.get('/api/sales/fetch_sales_stat').success (purchases)->
          price_demand_percentage_calc(purchases)
      
      when "my-sales"
        $http.get('/api/sales/fetch_sales_stat').success (purchases)->
          price_demand_percentage_calc(purchases)

      when "system-wide-sales"
        $http.get('/api/sales/fetch_sales_stat?scope=system').success (purchases)->
          price_demand_percentage_calc(purchases)
        
      when 'photos-viewed'
        fetch_photos_viewed_vs_purchased_data()
      when 'photos-viewed-system-wide'
        fetch_photos_viewed_vs_purchased_data({system_wide: true})

      when 'clients-select-mylocation'
        fetch_location_views_stats()
      else
        # $location.path('/statistics/my-sales')
    
    return
]