url = '/api/photos'
angular.module('Photographer')
.controller "PhotoUploadController", [
  "$scope"
  "$http"
  "$modal"
  # "$filter"
  # "$window"
  "$timeout"
  "cfpLoadingBar"
  '$cookies'
  '$cookieStore'
  'toastr'
  ($scope, $http, $modal, $timeout, cfpLoadingBar, $cookies, $cookieStore, toastr) ->
    upload_scope = angular.element('#upload_progress [ng-controller]').scope()
    $http.get('/api/photographer_preferences/default_configs', {ignoreLoadingBar: true}).success (res)->
      $scope.default_photo_price=res.default_photo_price||10
      $scope.licenses=res.license_lists
      $scope.license_lists=$scope.licenses
      $scope.default_license=res.default_license||{}
      $scope.default_license_id=$scope.default_license.id||0
      $scope.locations=res.locations||[]
      $scope.locations_list=$scope.locations
      $scope.default_location=res.default_location||{}
      $scope.default_location_id= $scope.default_location.id
      $scope.photos_count=res.photos_count
      $scope.watermarks=res.watermark_lists
      $scope.watermark_lists=$scope.watermarks
      $scope.default_watermark=res.default_watermark||{}
      $scope.default_watermark_id=$scope.default_watermark.id
      $scope.groups=[]
      $scope.groups_list=$scope.groups
      $scope.is_resize_photos = res.is_resize_photos
      $scope.max_width = res.max_width
      $scope.max_height = res.max_height

      #if $scope.is_resize_photos && $scope.max_width && $scope.max_height
        #$scope.option('dataType', 'json')
        #$scope.option('disableImageResize', /Android(?!.*Chrome)|Opera/.test(window.navigator && navigator.userAgent))
        #$scope.option('imageMaxWidth', $scope.max_width)
        #$scope.option('imageMaxHeight', $scope.max_height)
      #else
      #  $scope.option('disableImageResize', true)

      window.navigator.geolocation.getCurrentPosition (position) ->
        latitude = position.coords.latitude
        longitude = position.coords.longitude
        $scope.default_gps_latitude = latitude
        $scope.default_gps_longitude = longitude

        $scope.gps = {latitude: latitude, longitude: longitude}
        unless ($scope.default_location||{}).id
          getAddressNameFromLatLong latitude, longitude, (name)->
            default_location = {latitude: latitude, longitude: longitude, id: 'undefined', name: name}
            exist = _.filter $scope.locations, (loc)->
              parseInt(loc.latitude) == parseInt(latitude) && parseInt(loc.longitude) == parseInt(longitude)

            unless exist[0]
              default_location['notify'] = false
              $http.post('/api/locations', default_location, {ignoreLoadingBar: true}).success (res)->
                if `res.status=="ok"`
                  $scope.default_location = res.data
                  ($scope.locations||[]).push($scope.default_location)
                  $scope.$parent.locations = $scope.locations
                  $scope.$parent.locations_list = $scope.locations
                  $scope.$digest() if !$scope.$$phase
            else
              $scope.default_location = exist[0]

          return
      $scope.ready2Upload = true
    
    ctx = document.createElement("canvas")
    ctx.width = 80
    ctx.height = 53
    img = new Image()
    img.src = "/image-preview-not-found.png"
    img.onload = ->
      context = ctx.getContext("2d")
      try
        context.drawImage img, 20, 20
      catch e
        #
      $scope.ctx = ctx

    $scope.thumbnail_size = parseInt($cookies.thumbnail_size) || 200
    $scope.$watch 'thumbnail_size', (ts)->
      $cookieStore.put('thumbnail_size', parseInt(ts))
      ts = parseInt(ts)
      $('#custom-styles').text("
          .preview img, .preview canvas{
            max-width: "+(ts)+"px !important;
            max-height: "+(ts*0.86)+"px !important;
          }
        ")
      angular.forEach $scope.queue, (photo)->
        photo.thumbnail_size = ts

    $scope.default_group = 'photos uploaded on '+(new Date).getFullYear()+'/'+((new Date).getMonth()+1)+'/'+(new Date).getDate()+' at '+formatAMPM(new Date)
    $scope.groups = []
    $scope.activeGroup = `undefined`
    $scope.completedUploadIndex = 0
    $scope.initialized = false
    $scope._q_photos = []

    setter = (sp, queueAdded)->
      sp = sp || []
      sp = sp.clean(`undefined`)
      if (sp||[]).length > 0
        $scope._q_photos = sp
        $scope.photos_type = if !!queueAdded then 'queue' else 'selected'

        unless $scope.expandGroup
          $scope.activeGroup = `undefined`

        attrs = ['price', 'license_id', 'location', 'date_time','watermark_id']
        $.each attrs, (i, attr)->
          vals = _.pluck(sp, attr)
          isCommon = vals.allValuesSame()
          switch attr
            when 'license_id'
              default_val = $scope.default_license_id
            when 'price'
              default_val = $scope.default_photo_price
            when 'tags'
              default_val = []
            when 'watermark_id'
              default_val = $scope.default_watermark_id

            when 'location'
              default_val = $scope.default_location
              try
                isCommon = (_.pluck(vals, 'id')).allValuesSame()
              catch

            when 'date_time'
              today = new Date()
              date = $.datepicker.formatDate('mm/dd/yy', today)
              time = formatAMPM(today)
              default_val = date+' '+time
            else
              default_val = ''

          unless !!vals[0]
            vals[0] = default_val

          if ($scope._q_photos||[]).length > 0
            default_val = `null`

          $scope.queueAdded = !!queueAdded
          $scope[attr] = (if isCommon then vals[0] else default_val)

        unless (sp||[]).length <= 0 || $scope.expandGroup
          keepGoing = true
          angular.forEach $scope.groups, (g)->
            if keepGoing
              members = g.members
              c = 0
              angular.forEach sp, (m)->
                angular.forEach members, (p)->
                  if m['$$hashKey'] == p['$$hashKey']
                    c +=1
              if `c == (sp||[]).length` && `members.length == (sp||[]).length`
                $scope.activeGroup = g
                keepGoing = false
                return

        if $scope.expandGroup && $scope.activeGroup && (sp||[]).length > 0
          $scope.activeGroup.members = sp

        try
          unless `(sp||[]).length == $scope.queue.length`
            $scope.selectAllPhotos = 0
        catch
        #
      return

    $scope.reparseEmptyThumbnails = ->
      cfpLoadingBar.start()
      photos_with_no_thumbnails = _.filter($scope.queue, (p)->
          !!!p.preview||!!!p.exif
        )
      t = ''

      fn = (photos, i)->
        loadImage(photos[i], (img)->
          photos[i].preview = img
          $scope.$digest() if(!$scope.$$phase)
          clearTimeout(t)
          i += 1
          if photos[i] && _.findWhere($scope.queue, {'$$hashKey': photos[i]['$$hashKey']})
            t = $timeout ->
              fn(photos, i)
            , 500
        ,{canvas: true}
        )

      fn(photos_with_no_thumbnails, 0) if photos_with_no_thumbnails.length > 0
      cfpLoadingBar.complete()
      false

    $scope.parseExifData = (photo, i)->
      i = i||_.indexOf($scope.queue, photo)

      unless !!!photo || photo.exifLoaded
        photo.license_id = $scope.default_license_id
        photo.price = $scope.default_photo_price
        photo.watermark_id = $scope.default_watermark_id

        photo.exifLoaded = true

        if(!$scope.$$phase)
          $scope.$apply()

        loadImage.parseMetaData photo, ((data, i) ->

          exif = if data.exif then data.exif.getAll() else {}
          photo.exif = exif
          width = whereKeyLike(exif, 'width') || whereKeyLike(exif, 'xdimension')
          height = whereKeyLike(exif, 'height') || whereKeyLike(exif, 'ydimension')
          photo.orientation_type = if photo.width > photo.height then 'landscape' else 'portrait'

          latitude = if (whereKeyLike(exif, 'latitude')+"").match(/^[0-9.,]+$/)!=`null` then whereKeyLike(exif, 'latitude') else exif['GPSLatitude']
          longitude = if (whereKeyLike(exif, 'longitude')+"").match(/^[0-9.,]+$/)!=`null` then whereKeyLike(exif, 'longitude') else exif['GPSLongitude']

          if latitude && longitude && !!!photo.location
            _latitude = latitude.split(',')
            latitude = (_latitude[0]*1) + (_latitude[1]/60) + (_latitude[2]/3600)
            _longitude = longitude.split(',')
            longitude = (_longitude[0]*1) + (_longitude[1]/60) + (_longitude[2]/3600)

            exist = _.filter $scope.locations, (loc)->
              parseInt(loc.latitude) == parseInt(latitude) && parseInt(loc.longitude) == parseInt(longitude)

            if exist[0]
              photo.location = exist[0]
            else
              params = {
                latitude: latitude
                longitude: longitude
              }

              ($scope.locations||[]).push(params)

              photo.location = params

              params['notify'] = false
              getAddressNameFromLatLong latitude, longitude, (name)->
                params['name'] = name
                $http.post('/api/locations', params, {ignoreLoadingBar: true}).success (res)->
                  if `res.status=="ok"`
                    $scope.locations[$scope.locations.length-1] = res.data
                    photo.location = res.data
                    photo.location_id = (res.data||{}).id
                    $scope.$digest() if !$scope.$$phase

          else
            photo.location = $scope.default_location

          date_time = whereKeyLike(exif, 'DateTime')
          photo.date_time_original = date_time

          date = if date_time then date_time.split(':').clean() else []
          if date.length > 3
            date[2] = date[2].split(' ')[0]
            date = date[1]+'/'+date[2]+'/'+date[0]
            time = date_time.substring(11).replace /[0-9]{1,2}(:[0-9]{2}){2}/, (time) ->
              hms = time.split(':')
              delete hms[2]

              h = +hms[0]
              suffix = if h < 12 then ' AM' else ' PM'
              hms[0] = h % 12 or 12
              hms.clean().join(':') + suffix

            photo.date_time = date+' '+time

          if !!!photo.date_time
            today = new Date()
            date = $.datepicker.formatDate('mm/dd/yy', today)
            time = formatAMPM(today)
            photo.date_time = date+' '+time
        )
      if(!$scope.$$phase)
        $scope.$apply()
      return

    $scope.reparseEmptyExifData = ->
      photos_with_no_exifs_loaded = _.filter($scope.queue, (p)->
          !!!p.exifLoaded||!!!p.exif
        )
      angular.forEach photos_with_no_exifs_loaded, (p)->
        i = _.indexOf($scope.queue, p)
        if $scope.queue[i]
          $scope.parseExifData(p, i)

    updateData = (key, val)->
      if (!!!val || (typeof(val)=='object' && val.length<=0)) && ($scope._q_photos||[]).length > 0
        return false

      angular.forEach $scope._q_photos, (photo)->
        if $scope.photos_type == 'selected'
          file = _.findWhere $scope.queue, {'$$hashKey': photo['$$hashKey']}
          file = file||{}
          photo = photo||{}
          file[key] = val
          photo[key] = val
        else
          photo = photo || {}
          photo[key] = val

    $scope.options =
      disableExifThumbnail: true
      previewMinWidth: 300
      previewMinHeight: 300
      previewMaxWidth: 300
      previewMaxHeight: 300
      sequentialUploads: false
      #limitConcurrentUploads: 3
      url: url
      recalculateProgress: false
      imageOrientation: true
      error: (res, data)->
        if _.includes([401, 201, 422], parseInt(res.status))
          toastr.warning('Please <a class="loggedin-popup" style="color:green;" href="/users/sign_in">sign in here.</a>', 'Session Expired!') unless $('.loggedin-popup').is(':visible')

      done: (res, data)->
        is_error = false
        $scope.groups = []
        $scope.activeGroup = false

        file = _.findWhere $scope.queue, {'$$hashKey': data.result.hash_key}
        try
          if `data.result.status.toLowerCase() == 'ok'`
            $scope.queue = _.without($scope.queue, file)
            $scope.photos_count = $scope.photos_count+1

          else if file
            is_error = true
            file['errors'] = data.result.errors || 'Errored'
            # unless $('.add-photos-list').is(':visible')
            #   toastr.error(file.name, 'Upload Failed!<br><b>'+data.result.errors+'</b>')
        catch err
          console.error err

        $scope.completedUploadIndex +=1

        #_f = _.findWhere upload_scope.uploading_files, {'$$hashKey': file['$$hashKey']}
        #upload_scope.uploading_files = _.without(upload_scope.uploading_files, _f)

        #if upload_scope.overall_progress >=100
        #  upload_scope.closeDialog()
        #  upload_scope.parent_scope = `null`
        #  upload_scope.uploading_files = []
        #  $scope.$parent.upload_started = false
        #  upload_scope.overall_progress = 0
        #upload_scope.$apply() if !upload_scope.$$phase


        return
      change: (e, data)->
        $scope.$parent.upload_started = false
        $timeout ->
          $scope.queue_count = $scope.queue.length
        , 1000

      progressInterval: 1

      progress: (res, data)->
        #upload_scope.uploading_files = upload_scope.uploading_files||[]
        file = (data.files||[])[0]
        #file.file_progress = Math.floor((data.loaded/data.total)*100) if file
        #exists = _.findWhere upload_scope.uploading_files, {'$$hashKey': file['$$hashKey']}

        #unless exists
        #  upload_scope.uploading_files.push file
        #else
        #  exists.file_progress = file.file_progress

        #upload_scope.$apply() if !upload_scope.$$phase
        q = _.findWhere $scope.queue, {'$$hashKey': file['$$hashKey']}
        q.progress = Math.floor((data.loaded/data.total)*100) if q
        $scope.$apply() if !$scope.$$phase

      progressall: (res, data)->
        #upload_scope.overall_progress = Math.floor((data.loaded/data.total)*100)
        #upload_scope.parent_scope = $scope
        #upload_scope.$apply() if !upload_scope.$$phase

      always: (res, data)->
        #file = (data.files||[])[0]
        #return false unless file

        #upload_scope.parent_scope = $scope

        # overall progress
        #upload_scope.file = file||upload_scope.file
        #if data.result && data.result.status
        #  is_error = false
        #  unless `data.result.status.toLowerCase() == 'ok'`
        #    upload_scope.is_error = is_error
        #    upload_scope.error_message = data.result.errors || 'Errored'

        #if $scope.queue_count == $scope.completedUploadIndex || $scope.queue.length <= 0 || upload_scope.overall_progress >=100
        #  upload_scope.closeDialog()
        #  upload_scope.parent_scope = `null`
        #  upload_scope.file = `null`
        #  toastr.success('<p class="upload-success">Upload process was done. All errors were ignored and skipped.</p>', 'Upload done!') if !$('.add-photos-list').is(':visible') && !$('.upload-success').is(':visible')

        #upload_scope.$apply() if !upload_scope.$$phase

    $scope.initCustomFn = (index, file)->

      img = $('[data-index="'+(index-1)+'"] img')
      canvas = $('[data-index="'+(index-1)+'"] canvas')

      if (index == $scope.queue.length-1) && !$scope.initialized && ((img.length > 0 || canvas.length > 0) || $scope.queue.length ==1)
        console.log 'init custom fn ...'
        $scope.initialized = true

        # Initialize drag and drop
        rangeSelecting = (event, ui)->
          start = $(".ui-selecting", this).first();
          end = $(".ui-selecting", this).last();
          selecting = false;

        $('#selectable').selectable
            filter: '.thumbnail',
            autoRefresh: true
            distance: 1
            selecting: rangeSelecting,
            unselecting: rangeSelecting
            stop: ()->
              selectedPhotos = []
              $scope.selectedPhotos = []
              $.each $('.ui-selected'), (l, el)->
                i = $(el).data('index')
                if $scope.queue[i]
                  photo = $scope.queue[i]
                  existing = _.findWhere($scope.selectedPhotos, {'$$hashKey': photo['$$hashKey']})
                  if existing
                    $scope.selectedPhotos = _.without $scope.selectedPhotos, existing
                    $(el).removeClass('ui-selected')
                  else
                    (selectedPhotos||[]).push photo

              $scope.selectedPhotos = selectedPhotos
              if(!$scope.$$phase)
                $scope.$apply()

        $scope.$watch 'license_id', (val)->
          updateData('license_id', val)

        $scope.$watch 'price', (val)->
          updateData('price', val)

        $scope.$watch 'local_name', (val)->
          updateData('local_name', val)

        $scope.$watch 'location', (val)->
          updateData 'location', val

        $scope.$watch 'date_time', (val)->
          updateData 'date_time', val

        $scope.tags_changed = ()->
          updateData 'tags', $scope.tags

        $scope.$watch 'watermark_id', (val)->
          updateData 'watermark_id', val

        $scope.$watch 'selectAllPhotos', (val)->
          if val
            $scope.selectedPhotos = $scope.queue
            $('.thumbnail').addClass('ui-selected')
          else
            $scope.selectedPhotos = []
            $('.ui-selected').removeClass('ui-selected')

        $scope.$watch 'selectedPhotos', (sp)->

          $scope._q_photos = sp
          $scope.photos_type = 'selected'
          setter(sp)

          if (sp||[]).length <= 0
            $scope.date_time = ''
            $scope.price = ''
            $scope.license_id = ''
            $scope.location = ''
            $scope.watermark_id = ''

        $scope.newGroup = ()->
          modalInstance = $modal.open
            template: ' <form><div class="modal-header">
                          <h3 class="modal-title">New Group</h3>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                            <label>Name:</label>
                            <input class="form-control" type="text" name="group_name" ng-model="group_name">
                          </div>
                        </div>
                        <div class="modal-footer">
                        <button class="btn btn-primary" ng-click="ok()">OK</button>
                        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
                        </div></form>'
            controller: 'NewGroupModalInstanceCtrl'
            size: 'sm'
            resolve:
              selectedPhotos: ()->
                $scope.selectedPhotos

          modalInstance.result.then (group)->
            $scope.activeGroup = group
            ($scope.groups||[]).push group

            angular.forEach $scope.selectedPhotos, (photo)->
              file = _.findWhere $scope.queue, {'$$hashKey': photo['$$hashKey']}
              if file
                if file['groups']
                  (file['groups']||[]).push group.name
                else
                  file['groups'] = [group.name]

          , ()->
            # $log.info('Modal dismissed at: ' + new Date())

        $scope.ungroup = ()->
          $scope.groups = _.without($scope.groups, $scope.activeGroup)
          $scope.activeGroup = `undefined`
          return

        $scope.loadTags = (query)->
          $http.get('/api/tags/search?q=' + query)

        $scope.deleteSelected = ()->
          cfpLoadingBar.start()
          if $scope.selectedPhotos.length == $scope.queue.length
            $scope.cancel()
            $scope.queue = []
            cfpLoadingBar.complete()
          else
            angular.forEach $scope.selectedPhotos, (p, i)->
              file = _.findWhere $scope.queue, {'$$hashKey': p['$$hashKey']}
              (file||{$cancel: ->}).$cancel()
              $scope.queue = _.without($scope.queue, file)
            $scope.selectedPhotos = []
            cfpLoadingBar.complete()

          return

        $scope.deleteAll = ()->
          $scope.cancel()
          $scope.queue = []
          upload_scope.uploading_files = []
          upload_scope.$apply() if !upload_scope.$$phase

    $scope.newLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        backdrop: 'static'
        templateUrl: 'new-location.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          location: ()->
            {}
          gps: ()->
            default_data =
              latitude: $scope.default_gps_latitude
              longitude: $scope.default_gps_longitude

            if $scope.selectedPhotos.length > 1
              locations = []
              try
                locations = _.pluck($scope.selectedPhotos, 'location')
                locations = _.pluck(locations, 'id')
              catch
                #

              try
                common = locations.clean().allValuesSame()
              catch
              #

              try
                commonData =
                  latitude: locations[0].latitude
                  longitude: locations[0].longitude
              catch
                #

              gps = if !!common then commonData else default_data
              return gps

            else if $scope.selectedPhotos.length == 1
              try
                gps = {
                  latitude: $scope.selectedPhotos[0].location.latitude
                  longitude: $scope.selectedPhotos[0].location.longitude
                }
              catch e
                # ...
              unless !!gps
                gps = default_data

              return gps
      modalInstance.result.then (res)->
        if `res.status=="ok"`
          newLocation = res.data
          ($scope.locations||[]).push(newLocation)
          $scope.location = $scope.locations[$scope.locations.length-1]


    $scope.editLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'edit-location.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          location: ()->
            angular.copy $scope.location
          gps: ()->
            {latitude: $scope.default_gps_latitude, longitude: $scope.default_gps_longitude}

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = (_.findWhere $scope.locations, {id: res.data.id}) ||  (_.findWhere $scope.locations, {name: $scope.location.name})
          if existing
            index = $scope.locations.indexOf existing
            if `res.action=='delete'`
              $scope.locations.splice(index, 1)
              $scope.location = $scope.locations[0]
              return false
            $scope.locations[index] = angular.copy(res.data)
            $scope.location = $scope.locations[index]

    $scope.preview = (p)->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/common/views/preview-photo.html'
        controller: 'PreviewPhotoModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        windowClass: 'preview-photo'
        resolve:
          photo: ()->
            p
          photos: ()->
            $scope.queue
          w: ()->
            {width: $(window).width(), height: $(window).height()}
          type: ()->
            "add-photos"
          photo_info_url: ()->

      return false

    $scope.watermarkEditPreviewLabel = ()->
      default_watermark = _.findWhere $scope.watermarks, {id: $scope.watermark_id}
      if default_watermark && !!default_watermark.user_id then 'Edit' else 'Preview'

    $scope.newWatermark = ()->
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'new-watermark.html'
        controller: 'watermarkModalInstanceCtrl'
        resolve:
          watermark: ()->
            {
              display_type: 'tiled'
              tiled_size: 200
              position: 'center center'
              positioned_size: 50
              rotation: 0
              stretched_size: 50
              opacity: 1
            }

      modalInstance.result.then (watermark)->
        ($scope.watermarks||[]).push watermark
        try
          $scope.watermark_id = $scope.watermarks[$scope.watermarks.length-1]['id']
        catch
          #

    $scope.editWatermark = ()->
      $scope.watermark = _.findWhere $scope.watermarks, {id: $scope.watermark_id}
      if !!!$scope.watermark.watermark_image && !!$scope.watermark.photo_url
        loadImage(
          $scope.watermark.photo_url,
          (img)->
              $scope.watermark.watermark_image = {previewUrl: img.src}
              modalInstance = $modal.open
                size: 'lg'
                templateUrl: 'edit-watermark.html'
                controller: 'watermarkModalInstanceCtrl'
                resolve:
                  watermark: ()->
                    $scope.watermark

              modalInstance.result.then (watermark)->
                if `watermark.action=='delete'`
                  $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
                  $scope.watermark_id = _.findWhere($scope.watermarks, {name: 'none'}).id
                else
                  $scope.watermark = watermark
                  $scope.watermark_id = watermark.id
              return
          ,
          {maxWidth: 900, noRevoke: true, canvas: false}
        )

        return
      else if !!!$scope.watermark.photo_url
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'edit-watermark.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0].id
          else
            $scope.watermark = watermark
        return
      else
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'edit-watermark.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0].id
          else
            $scope.watermark = watermark
        return
      false

    $scope.showProgressBar = ->
      cfpLoadingBar.start()
      cfpLoadingBar.inc()

    $scope.hideProgressBar = ->
      cfpLoadingBar.inc()
      cfpLoadingBar.complete()

    $scope.upload = ()->
      modalInstance = $modal.open
        size: 'md'
        backdrop: 'static'
        keyboard: false
        resolve:
          default_photo_price: -> $scope.default_photo_price   
          default_license: -> $scope.default_license
          default_location: -> $scope.default_location
          default_watermark: -> $scope.default_watermark
          is_resize_photos: -> $scope.is_resize_photos
          max_width: -> $scope.max_width
          max_height: -> $scope.max_width
          licenses: -> $scope.licenses
          watermarks: -> $scope.watermarks
          locations: -> $scope.locations

        templateUrl: 'ng-modules/photographer/views/photos/_popup_confirmation_settings.html'
        controller: 'uploadConfirmationSettingsModalInstanceCtrl'

      modalInstance.result.then (args)->
        $scope.default_photo_price = args.default_photo_price   
        $scope.default_license = args.default_license
        $scope.default_location = args.default_location
        $scope.default_watermark = args.default_watermark
        $scope.is_resize_photos = args.is_resize_photos
        $scope.max_width = args.max_width
        $scope.max_width = args.max_width

        if $scope.is_resize_photos && $scope.max_width && $scope.max_height
          $scope.option('dataType', 'json')
          $scope.option('disableImageResize', /Android(?!.*Chrome)|Opera/.test(window.navigator && navigator.userAgent))
          $scope.option('imageMaxWidth', $scope.max_width)
          $scope.option('imageMaxHeight', $scope.max_height)
        else
          $scope.option('disableImageResize', true)

        $scope.completedUploadIndex = 0
        unless $scope.selectedPhotos.length>0&&$scope.selectedPhotos.length != $scope.queue.length
          #upload_scope.file = upload_scope.file||$scope.queue[0]
          #upload_scope.parent_scope = $scope

          #upload_scope.showDialog() unless $('.upload-progress').is(':visible')
          #upload_scope.$apply() if !upload_scope.$$phase

          $scope.submit()
          $scope.$parent.upload_started = true
        else
          modalInstance = $modal.open
            size: 'sm'
            scope: $scope
            backdrop: 'static'
            templateUrl: 'ng-modules/common/views/upload_confirmation.html'
            controller: 'uploadConfirmationModalInstanceCtrl'
      ,(args)->
        $scope.default_photo_price = args.default_photo_price   
        $scope.default_license = args.default_license
        $scope.default_location = args.default_location
        $scope.default_watermark = args.default_watermark
        $scope.is_resize_photos = args.is_resize_photos
        $scope.max_width = args.max_width
        $scope.max_height = args.max_height
        $scope.override_attrs = args.override_attrs

        if $scope.is_resize_photos && $scope.max_width && $scope.max_height
          $scope.option('dataType', 'json')
          $scope.option('disableImageResize', /Android(?!.*Chrome)|Opera/.test(window.navigator && navigator.userAgent))
          $scope.option('imageMaxWidth', $scope.max_width)
          $scope.option('imageMaxHeight', $scope.max_height)
        else
          $scope.option('disableImageResize', true)

    $scope.onAddFiles = ->
      $scope.showProgressBar()
      $scope.ready2Upload = false


    $scope.onAddFilesDone = ->
      $scope.hideProgressBar()
      $scope.ready2Upload = true
      $scope.reparseEmptyThumbnails()
      # $scope.reparseEmptyExifData()

      # sort photos
      $timeout ->
        _queue = _.sortBy $scope.queue, (q)->
          q.date_time_original
        $scope.queue = _queue.reverse()
      ,100

    $scope.triggerSelectFiles = ->
      $('.upload[type="file"]').click()
      false

    $(document).bind 'fileuploadsubmit', '#fileupload', (e, data)->
      data.process()
      try
        location_id = (data.files[0].location||{}).id
        location = JSON.stringify(data.files[0].location)
      catch e
        console.log e
      try
        tags = JSON.stringify(data.files[0].tags)
      catch e
        console.log e

      groups = `data.files[0].groups || []`
      default_group = 'photos uploaded on '+(new Date).getFullYear()+'/'+((new Date).getMonth()+1)+'/'+(new Date).getDate()+' at '+formatAMPM(new Date)
      (groups||[]).push (default_group)
      data.formData =
        price: (if _.indexOf($scope.override_attrs, 'default_photo_price')>= 0 then $scope.default_photo_price else data.files[0].price)
        license_id: (if _.indexOf($scope.override_attrs, 'default_license')>= 0 then $scope.default_license.id else data.files[0].license_id)
        location_id: (if _.indexOf($scope.override_attrs, 'default_location')>= 0 then $scope.default_location.id else location_id)
        location: (if _.indexOf($scope.override_attrs, 'default_location')>= 0 then JSON.stringify($scope.default_location||{}) else location)
        date_time: (if _.indexOf($scope.override_attrs, 'default_date_time')>= 0 then $scope.default_date_time else data.files[0].date_time)
        watermark_id: (if _.indexOf($scope.override_attrs, 'default_watermark')>= 0 then $scope.default_watermark.id else data.files[0].watermark_id)
        thumb_width: $scope.thumbnail_size
        thumb_height: $scope.thumbnail_size
        thumbnail_size: $scope.thumbnail_size
        groups: groups
        hash_key: (data.files[0]['$$hashKey'] || '')

    return
]
