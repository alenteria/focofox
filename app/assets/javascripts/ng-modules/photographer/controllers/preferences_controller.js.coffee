watermark_url = '/api/watermarks'
location_url = '/api/locations'
license_url = '/api/licenses'
angular.module('Photographer')
.controller 'PreferencesCtrl', [
  '$scope'
  '$http'
  '$modal'
  '$timeout'
  ($scope, $http, $modal, $timeout) ->
    # load defaults
    $http.get('/api/photographer_preferences/default_configs', {ignoreLoadingBar: true}).success (res)->
      $scope.default_photo_price=res.default_photo_price||10
      $scope.licenses=res.license_lists
      $scope.license_lists=$scope.licenses
      $scope.default_license=res.default_license||{}
      $scope.default_license=$scope.default_license.id||0
      $scope.locations=res.locations||[]
      $scope.locations_list=$scope.locations
      $scope.default_location=res.default_location||{}
      $scope.default_location_id= $scope.default_location.id
      $scope.photos_count=res.photos_count
      $scope.watermarks=res.watermark_lists
      $scope.watermark_lists=$scope.watermarks
      $scope.default_watermark=res.default_watermark||{}
      $scope.default_watermark=$scope.default_watermark.id
      $scope.groups=[]
      $scope.groups_list=$scope.groups
      $scope.is_resize_photos = res.is_resize_photos
      $scope.max_width = res.max_width
      $scope.max_height = res.max_height
    
      window.navigator.geolocation.getCurrentPosition ((position) ->
        latitude = position.coords.latitude
        longitude = position.coords.longitude
        $scope.gps = {latitude: latitude, longitude: longitude}
        default_location_valid = _.findWhere $scope.locations, {id: $scope.default_location.id}
        unless default_location_valid
          getAddressNameFromLatLong latitude, longitude, (name)->
            existing = _.filter $scope.locations, (loc)->
              parseInt(loc.latitude) == parseInt(latitude) && parseInt(loc.longitude) == parseInt(longitude)

            $scope.default_location = existing[0] if existing[0]
            unless !!existing[0]
              default_location = {latitude: latitude, longitude: longitude, id: 'undefined', name: name}
              default_location['notify'] = false
              $http.post('/api/locations', default_location, {ignoreLoadingBar: true}).success (res)->
                if `res.status=="ok"`
                  existing = _.findWhere $scope.locations, {id: res.data.id}
                  unless existing
                    $scope.locations.push(res.data)
                  
                  $scope.default_location = $scope.locations[$scope.locations.length-1]

            $scope.$apply() if !$scope.$$phase
          return
        return
      ), (error) ->
        console.error error
        return
    $scope.watermarkEditPreviewLabel = ()->
      default_watermark = _.findWhere $scope.watermarks, {id: $scope.default_watermark}
      if default_watermark && !!default_watermark.user_id then 'Edit' else 'Preview' 
    $scope.editWatermark = ()->
      $scope.watermark = _.findWhere $scope.watermarks, {id: $scope.default_watermark}
      if !!!$scope.watermark.watermark_image && !!$scope.watermark.photo_url
        loadImage(
          $scope.watermark.photo_url,
          (img)->
              $scope.watermark.watermark_image = {previewUrl: img.src}
              modalInstance = $modal.open
                size: 'lg'
                templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
                controller: 'watermarkModalInstanceCtrl'
                resolve:
                  notify_success: ->
                    false
                  watermark: ()->
                    $scope.watermark

              modalInstance.result.then (watermark)->
                if `watermark.action=='delete'`
                  $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
                  $scope.default_watermark = $scope.watermarks[0].id
                else
                  $scope.watermark = watermark
                  if watermark.type == 'create'
                    watermark.type = null
                    $scope.watermarks.push watermark
                    try
                      $scope.default_watermark = $scope.watermarks[$scope.watermarks.length-1]['id']
                      $scope.mark_default_watermark($scope.default_watermark)
                    catch

                key = 'default_watermark'
                InputElement = $('[ng-model="'+key+'"]')
                $('.notify-'+key).remove()

                InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
                InputElement.addClass('border-success')
                $('.notify-'+key).fadeIn(1000)

                setTimeout ->
                  $('.notify-'+key).fadeOut 100, ()-> @remove()
                  InputElement.removeClass('border-success').removeClass('border-danger')
                , 2000
              return
          ,
          {maxWidth: 900, noRevoke: true, canvas: false}
        )

        return
      else if !!!$scope.watermark.photo_url
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            notify_success: ->
              false
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0].id
          else
            $scope.watermark = watermark
            if watermark.type == 'create'
              watermark.type = null
              $scope.watermarks.push watermark
              try
                $scope.default_watermark = $scope.watermarks[$scope.watermarks.length-1]['id']
                $scope.mark_default_watermark($scope.default_watermark)
              catch
          
          key = 'default_watermark'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000
        return
      else
        modalInstance = $modal.open
          size: 'lg'
          templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_edit.html'
          controller: 'watermarkModalInstanceCtrl'
          resolve:
            notify_success: ->
              false
            watermark: ()->
              $scope.watermark

        modalInstance.result.then (watermark)->
          if `watermark.action=='delete'`
            $scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
            $scope.default_watermark = $scope.watermarks[0].id
          else
            $scope.watermark = watermark
            if watermark.type == 'create'
              watermark.type = null
              $scope.watermarks.push watermark
              try
                $scope.default_watermark = $scope.watermarks[$scope.watermarks.length-1]['id']
                $scope.mark_default_watermark($scope.default_watermark)
              catch
          
          key = 'default_watermark'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000
        return
      false

    $scope.newWatermark = ()->
      $scope.watermark = 
        display_type: 'tiled'
        tiled_size: 200
        position: 'center center'
        positioned_size: 50
        rotation: 0
        stretched_size: 50
        opacity: 1

      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_new.html'
        controller: 'watermarkModalInstanceCtrl'
        resolve:
          notify_success: ->
            false
          watermark: ()->
            $scope.watermark

      modalInstance.result.then (watermark)->
        $scope.watermarks.push watermark
        try
          $scope.default_watermark = $scope.watermarks[$scope.watermarks.length-1]['id']
          $scope.mark_default_watermark($scope.default_watermark)
        catch
          # 

    $scope.mark_default_watermark = (dw)->
      if dw
        id = if typeof(dw) is 'object' then dw.id else dw
        $http.post(watermark_url+"/"+dw+'/mark_default_watermark/', {id: id}, {notify_success: false}).success (res)->
          key = 'default_watermark'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          if res.status=='ok'
            InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
            InputElement.addClass('border-success')
            $('.notify-'+key).fadeIn(1000)
          else
            InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
            InputElement.addClass('border-danger')
            $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000

    $scope.mark_default_location = (dl)->
      if dl
        id = if typeof(dl) is 'object' then dl.id else dl
        $http.post(location_url+"/"+dl+'/mark_default_location/', {id: id}, {notify_success: false}).success (res)->
          key = 'default_location_id'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          if res.status=='ok'
            InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
            InputElement.addClass('border-success')
            $('.notify-'+key).fadeIn(1000)
          else
            InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
            InputElement.addClass('border-danger')
            $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000
      
    $scope.setting = {location_id: $scope.default_location_id, location: {}}
    $scope.updateData = (key, val)->
      $scope.default_location_id = if (val||{}).id then val.id else val
      $scope.mark_default_location($scope.default_location_id)

    $scope.newLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        backdrop: 'static'
        keyboard: false
        templateUrl: 'ng-modules/photographer/views/preferences/locations/new.html'
        controller: 'locationModalInstanceCtrl'
        windowClass: 'location-preferences-dialog'
        resolve:
          notify_success: ->
            false
          location: ()->
            `null`

          parent: ()->
            $scope

          gps: ()->
            angular.copy($scope.gps)

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = _.findWhere $scope.locations, {id: res.data.id}
          unless existing
            $scope.locations.push(res.data)
            $scope.default_location = $scope.locations[$scope.locations.length-1]
            $scope.default_location_id = $scope.default_location.id
            $scope.mark_default_location($scope.default_location_id)

    $scope.editLocation = ()->
      modalInstance = $modal.open
        size: 'lg'
        backdrop: 'static'
        keyboard: false
        templateUrl: 'ng-modules/photographer/views/preferences/locations/edit.html'
        controller: 'locationModalInstanceCtrl'
        windowClass: 'location-preferences-dialog'
        resolve:
          notify_success: ->
            false
          location: ()->
            angular.copy _.findWhere $scope.locations, {id: $scope.default_location_id}

          parent: ()->
            $scope

          gps: ()->
            $scope.gps
            
      modalInstance.result.then (res)->
        key = 'default_location_id'
        InputElement = $('[ng-model="'+key+'"]')
        
        $('.notify-'+key).remove()

        if res.status=='ok'
          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
        else
          InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
          InputElement.addClass('border-danger')
          $('.notify-'+key).fadeIn(1000)

        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success').removeClass('border-danger')
        , 2000
        
        if `res.status=="ok"`
          if $scope.default_location=='undefined'
            d = _.findWhere $scope.locations, {id: 'undefined'}
            d.id = res.data.id
            $scope.default_location = d
            $scope.default_location_id = d.id

          existing = _.findWhere $scope.locations, {id: res.data.id}

          if existing
            index = $scope.locations.indexOf existing
            if `res.action=='delete'`
              $scope.locations.splice(index, 1)
              $scope.default_location = $scope.locations[0]
              $scope.default_location_id = $scope.default_location.id
              return false
            $scope.locations[index] = res.data
        return
    $scope.update_default_photo_price = (p)->
      $scope.default_photo_price = parseInt(p)  
      if $scope.default_photo_price
        $http.put('/api/photographer_preferences/update_default_photo_price', {default_photo_price: $scope.default_photo_price}, {notify_success: false, ignoreLoadingBar: true}).success (res)->
          key = 'default_photo_price'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          if res.status=='ok'
            InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
            InputElement.addClass('border-success')
            $('.notify-'+key).fadeIn(1000)
          else
            InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save <b>'+res.error+'</b></small>')
            InputElement.addClass('border-danger')
            $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000


    $scope.newLicense = ()->
      $scope.license = {}
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/licenses/_new.html'
        controller: 'licenseModalInstanceCtrl'
        resolve:
          license: ()->
            $scope.license

      modalInstance.result.then (res)->
        if `res.status=="ok"`
          existing = _.findWhere $scope.licenses, {id: res.data.id}
          unless existing
            $scope.licenses.push(res.data)
            $scope.default_license = $scope.licenses[$scope.licenses.length-1]

    $scope.editPreviewLicenseLabel = ()->
      default_license = _.findWhere $scope.licenses, {id: $scope.default_license}
      if default_license && !!default_license.user_id then 'Edit' else 'Preview'

    $scope.editLicense = ()->
      $scope.license = angular.copy _.findWhere $scope.licenses, {id: $scope.default_license}
      modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/photographer/views/preferences/licenses/_edit.html'
        controller: 'licenseModalInstanceCtrl'
        resolve:
          license: ()->
            $scope.license

      modalInstance.result.then (license)->
        if `license.action=='delete'`
          $scope.licenses = $scope.licenses.splice($scope.licenses.indexOf($scope.license), 1)
          $scope.default_license = $scope.licenses[0].id
        else          
          existing = _.findWhere $scope.licenses, {id: license.data.id}
          if existing
            index = $scope.licenses.indexOf existing
            $scope.licenses[index] = license.data

    $scope.mark_default_license = (dl)->
      if dl
        id = if typeof(dl) is 'object' then dl.id else dl
        $http.put(license_url+"/"+dl+'/mark_default/', {id: dl}, {notify_success: false}).success (res)->
          key = 'default_license'
          InputElement = $('[ng-model="'+key+'"]')
          $('.notify-'+key).remove()

          if res.status=='ok'
            InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
            InputElement.addClass('border-success')
            $('.notify-'+key).fadeIn(1000)
          else
            InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
            InputElement.addClass('border-danger')
            $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000
    
    initializingResizePhotos = true
    $scope.$watch 'is_resize_photos', (rp)->
      if initializingResizePhotos
        $timeout ->
          initializingResizePhotos = false
        ,1000
        return false

      return false unless $scope.max_width && $scope.max_height

      $http.post('/api/photographer_preferences/disable_photo_resizing', {value: rp}, {notify_success: false, ignoreLoadingBar: true}).success (res)->
        key = 'resize-photos'
        InputElement = $('.'+key)
        $('.notify-'+key).remove()

        if res.status=='ok'
          InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
          InputElement.addClass('border-success')
          $('.notify-'+key).fadeIn(1000)
        else
          InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
          InputElement.addClass('border-danger')
          $('.notify-'+key).fadeIn(1000)

        setTimeout ->
          $('.notify-'+key).fadeOut 100, ()-> @remove()
          InputElement.removeClass('border-success').removeClass('border-danger')
        , 2000

    initializingSizes = true
    $scope.$watchGroup ['max_width', 'max_height'], (val, oldVal)->
      if initializingSizes
        $timeout ->
          initializingSizes = false
        , 1000
        return false

      if val[0] && val[1] && $scope.is_resize_photos
        $scope.max_width = parseInt(val[0])
        $scope.max_height = parseInt(val[1])
        $http.put('/api/photographer_preferences/update_photo_resizing',{max_width: $scope.max_width, max_height: $scope.max_height}, {notify_success: false, ignoreLoadingBar: true}).success (res)->
          key = 'resize-photos'
          InputElement = $('.'+key)
          $('.notify-'+key).remove()

          if res.status=='ok'
            InputElement.parent().append('<small class="notify-'+key+'" style="color: green;">&nbsp;<i class="fa fa-check">&nbsp;</i>saved</small>')
            InputElement.addClass('border-success')
            $('.notify-'+key).fadeIn(1000)
          else
            InputElement.parent().append('<small class="notify-'+key+'" style="color: #9B1D1D;">&nbsp;<i class="fa fa-times">&nbsp;</i>fail to save</small>')
            InputElement.addClass('border-danger')
            $('.notify-'+key).fadeIn(1000)

          setTimeout ->
            $('.notify-'+key).fadeOut 100, ()-> @remove()
            InputElement.removeClass('border-success').removeClass('border-danger')
          , 2000

    return
]