'use strict'
viewPath = 'ng-modules/photographer/views'
angular.module('Photographer', [
  'ngCookies'
  'blueimp.fileupload'
  "ui.bootstrap"
  'ngTagsInput'
  'ngResource'
  'ngSanitize'
  'ngMap'
  'ngAutocomplete'
  'common'
  'templates'
  'ui.select'
  'bgf.paginateAnything'
  'ui.router'
  'tableSort'
  'chart.js'
])
.config [
  "$httpProvider"
  "fileUploadProvider"
  "$stateProvider"
  "$urlRouterProvider"
  ($httpProvider, fileUploadProvider, $stateProvider, $urlRouterProvider) ->
    delete $httpProvider.defaults.headers.common["X-Requested-With"]
    angular.extend fileUploadProvider.defaults,
        previewCanvas: true
        loadImageMaxFileSize: 100000000000000
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4|ogg|mpeg|avi|flv|mkv|divx|wmv|webm|3gp|mov|ogv)$/i

    $urlRouterProvider.otherwise("/photos/index")

    $stateProvider
    .state('photos', {
      url: "/photos/:subpage",
      templateUrl: viewPath+'/photos/index.html'
      controller: 'PhotosCtrl'
    })
    .state('add_photos', {
      url: "/add-photos",
      templateUrl: viewPath+'/photos/add.html'
    })
    .state('store', {
      url: "/store",
      templateUrl: viewPath+'/store/index.html'
      controller: 'StoreCtrl'
    })
    .state('store.sales_history', {
      url: '/sales-history',
      templateUrl: viewPath+'/store/sales-history.html'
    })
    .state('store.event_manager',{
      url: '/event-manager'
      templateUrl: viewPath+'/store/event-manager.html'
    })
    .state('store.statistics',{
      url: '/statistics/:type'
      controller: 'StatCtrl'
      templateUrl: viewPath+'/store/statistics.html'
    })
    .state('preferences', {
      url: "/preferences",
      templateUrl: viewPath+'/preferences/index.html'
      controller: 'PreferencesCtrl'
    })
]
