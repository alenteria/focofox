'use strict'
angular.module 'paymentMethodsApp'
.controller 'mainController', [
  '$scope'
  '$http'
  '$locale'
  'cfpLoadingBar'
  'toastr'
  ($scope, $http, $locale, cfpLoadingBar, toastr)->
    $scope.country_codes = country_codes
    $scope.currentYear = new Date().getFullYear()
    $scope.currentMonth = new Date().getMonth() + 1
    $scope.months = $locale.DATETIME_FORMATS.MONTH

    #init default country code
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition (position) ->
        $.getJSON 'http://ws.geonames.org/countryCode?username=focofox', {
          lat: position.coords.latitude
          lng: position.coords.longitude
          type: 'JSON'
        }, (result) ->
          $scope.default_country_code = result.countryCode
          find = _.findWhere($scope.country_codes, {value: result.countryCode})
          
          $scope.country_codes.unshift(find)
          if $scope.action_new
            $scope.addNew()
            $scope.$digest() if !$scope.$$phase
            setTimeout ()->
              $('html, body').animate({
                  scrollTop: $('[name="ccFirstName"]').offset().top
              }, 100)
              $("[name='ccFirstName']").focus()
            ,100

          return
        return

    $scope.removeSpaces = (obj)->
      obj = obj.replace(/\s/g, '') if obj

    $scope.initCardFormatter = (e)->
      $('[name="number"]').payment('formatCardNumber')
      false

    $scope.initExpiryFormatter = (e)->
      $('[name="card_expiry"]').payment('formatCardExpiry')
      false

    $scope.expiryChanged =  (expiry)->
      return false unless expiry

      card = _.findWhere($scope.send_method_cards, {id: 0})
      expiry = expiry.split(' / ')
      card.expire_month = expiry[0]
      year = expiry[1]||""
      year = (new Date()).getFullYear().toString().substr(0,2)+year if year.length ==2
      card.expire_year = year

    $scope.is_invalidExpiry = (card)->
      !$.payment.validateCardExpiry(card.expire_month, card.expire_year)

    $scope.is_invalid = (form, id)->
      if id
        (form.ccCvc.$touched && form.ccCvc.$invalid)||(form.ccFirstName.$touched && form.ccFirstName.$invalid)||(form.ccLastName.$touched && form.ccLastName.$invalid) || (form.ccNumber.$touched && form.ccNumber.$invalid)||(form.ccExpMonth.$touched && form.ccExpMonth.$invalid)||(form.ccExpYear.$touched && form.ccExpYear.$invalid)
      else
        form.$invalid

    $scope.editCard = (card)->
      card.is_edit = true

    $scope.addCard = (form, card, event)->
      event.preventDefault()
      card.number = card.number.replace(/\s/g, '')
      card.expire_year = (card.expire_year||"").trim()
      card.expire_month = (card.expire_month||"").trim()
      $http.post('/api/credit_cards/', card, {notify_success: false}).success((res)->
        if res.status == 'ok'
          index = _.indexOf($scope.send_method_cards, card)
          $scope.send_method_cards[index] = res.card
        
      ).error (res)->

      return false

    $scope.saveCard = (card)->
      card.is_edit = false
      $http.put('/api/credit_cards/'+card.id, card, {notify_success: false}).success((res)->
      ).error (res)->
    $scope.deleteCard = (card)->
      if card.is_default
        toastr.warning('You cannot delete a card marked as default.', 'Warning!') 
        return false

      return false unless confirm('Are you sure you want to delete this card?')

      $http.delete('/api/credit_cards/'+card.id, {notify_success: false}).success((res)->
        i = _.indexOf($scope.send_method_cards, card)
        $scope.send_method_cards.splice(i, 1)
        switch res.type
          when 'PaypalAccount'
            angular.forEach $scope.send_method_cards, (c)->
              c.is_default = false
            angular.forEach $scope.send_method_paypal_accounts, (p)->
              if p.id==res.data.id
                p.is_default = true
              else
                p.is_default = false
          when 'CreditCard'
            angular.forEach $scope.send_method_cards, (c)->
              if c.id==res.data.id
                c.is_default = true
              else
                c.is_default = false
            angular.forEach $scope.send_method_paypal_accounts, (p)->
              p.is_default = false
      ).error (res)->

    $scope.addNew = ->
      cfpLoadingBar.start()
      cfpLoadingBar.inc()
      unless _.findWhere($scope.send_method_cards, {id: 0})
        $scope.send_method_cards.push {id: 0, is_edit: true}
      setTimeout ()->
        $('#open-card-0').collapse('show')
        $("[name='ccFirstName']").focus()
        cfpLoadingBar.complete()
      , 100

    $scope.cancelNewCard = (card)->
      $scope.send_method_cards = _.without($scope.send_method_cards, card)

    $scope.markDefaultCard = (card)->
      $http.put('/api/credit_cards/'+card.id+'/mark_default', {}, {notify_success: false}).success (res)->
        angular.forEach $scope.send_method_cards, (c)->
          c.is_default = true if c.id == card.id
          c.is_default = false unless c.id==card.id
        angular.forEach $scope.send_method_paypal_accounts, (c)->
          c.is_default = false

    $scope.addNewPaypalAccountForSend = (email, e)->
      e.preventDefault()
      params = {
        email: email
        method_type: 'send'
      }
      $http.post('/api/paypal_accounts/', params, {notify_success: false}).success (res)->
        if res.status=='ok'
          $scope.send_method_paypal_accounts.push res.data
          $scope.new_paypal_email = ''

    $scope.removePaypalAccountForSend = (paypal)->
      if paypal.is_default
        toastr.warning('You cannot delete a paypal account marked as default.', 'Warning!') 
        return false

      return false unless confirm('Are you sure you want to delete this account?')
      $http.delete('/api/paypal_accounts/'+paypal.id+'/?method_type=send', {notify_success: false}).success (res)->
        if res.status == 'ok'
          switch res.type
            when 'PaypalAccount'
              angular.forEach $scope.send_method_cards, (c)->
                c.is_default = false
              angular.forEach $scope.send_method_paypal_accounts, (p)->
                if p.id==res.data.id
                  p.is_default = true
                else
                  p.is_default = false
            when 'CreditCard'
              angular.forEach $scope.send_method_cards, (c)->
                if c.id==res.data.id
                  c.is_default = true
                else
                  c.is_default = false
              angular.forEach $scope.send_method_paypal_accounts, (p)->
                p.is_default = false

          $scope.send_method_paypal_accounts = _.without($scope.send_method_paypal_accounts, paypal)
    
    $scope.markDefaultPaypalAccountForSend = (paypal)->
      $http.post('/api/paypal_accounts/'+paypal.id+'/mark_default', {method_type: 'send'}, {notify_success: false}).success (res)->
        angular.forEach $scope.send_method_paypal_accounts, (p)->
          p.is_default = true if p.id == paypal.id
          p.is_default = false unless p.id==paypal.id

        angular.forEach $scope.send_method_cards, (p)->
          p.is_default = false

    $scope.addNewPaypalAccountForReceive = (email, e)->
      e.preventDefault()
      params = {
        email: email
        method_type: 'receive'
      }
      $http.post('/api/paypal_accounts/', params, {notify_success: false}).success (res)->
        if res.status=='ok'
          $scope.receive_method_paypal_accounts.push res.data
          $scope.new_paypal_email = ''

    $scope.removePaypalAccountForReceive = (paypal)->
      if paypal.is_default
        toastr.warning('You cannot delete a paypal account marked as default.', 'Warning!') 
        return false

      return false unless confirm('Are you sure you want to delete this account?')
      $http.delete('/api/paypal_accounts/'+paypal.id+'/?method_type=receive', {notify_success: false}).success (res)->
        if res.status == 'ok'
          $scope.receive_method_paypal_accounts = _.without($scope.receive_method_paypal_accounts, paypal)
          angular.forEach $scope.receive_method_paypal_accounts, (p)->
            if p.id==res.data.id
              p.is_default = true
            else
              p.is_default = false

    $scope.markDefaultPaypalAccountForReceive = (paypal)->
      $http.post('/api/paypal_accounts/'+paypal.id+'/mark_default', {method_type: 'receive'}, {notify_success: false}).success (res)->
        angular.forEach $scope.receive_method_paypal_accounts, (p)->
          p.is_default = true if p.id == paypal.id
          p.is_default = false unless p.id==paypal.id
]