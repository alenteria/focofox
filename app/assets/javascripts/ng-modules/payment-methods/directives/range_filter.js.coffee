angular.module('paymentMethodsApp')
.filter 'range', ->

  filter = (arr, lower, upper) ->
    i = lower
    while i <= upper
      arr.push i
      i++
    arr

  filter
