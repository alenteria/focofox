angular.module('paymentMethodsApp').directive 'creditCardType', ->
  directive = 
    require: 'ngModel'
    link: (scope, elm, attrs, ctrl) ->
      ctrl.$parsers.unshift (value) ->
        scope.send_method_cards[attrs.index].card_type = if /^5[1-5]/.test(value) then 'mastercard' else if /^4/.test(value) then 'visa' else if /^3[47]/.test(value) then 'amex' else if /^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value) then 'discover' else undefined
        ctrl.$setValidity 'invalid', !!scope.send_method_cards[attrs.index].card_type
        value
      return
  directive