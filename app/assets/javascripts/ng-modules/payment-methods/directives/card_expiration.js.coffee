angular.module('paymentMethodsApp')
.directive 'cardExpiration', ->
  directive = 
    require: 'ngModel'
    link: (scope, elm, attrs, ctrl) ->
      index = attrs.index
      scope.$watch '[send_method_cards['+index+'].expire_month,send_method_cards['+index+'].expire_year]', ((value) ->        
        ctrl.$setValidity 'invalid', true
        # scope.send_method_cards[index].expire_year = (scope.send_method_cards[index].expire_year||"").trim()
        # scope.send_method_cards[index].expire_month = (scope.send_method_cards[index].expire_month||"").trim()

        if parseInt(scope.send_method_cards[index].expire_year) == scope.currentYear&& parseInt(scope.send_method_cards[index].expire_month) <= scope.currentMonth
          ctrl.$setValidity 'invalid', false
        value
      ), true
      return
  directive