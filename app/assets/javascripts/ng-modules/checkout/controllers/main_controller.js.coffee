'use strict'
angular.module 'checkoutApp'
.controller 'mainController', [
  '$scope'
  '$http'
  '$locale'
  'toastr'
  ($scope, $http, $locale, toastr)->
    window.navigator.geolocation.getCurrentPosition (position) ->
      $scope.latitude = position.coords.latitude
      $scope.longitude = position.coords.longitude

      getAddressNameFromLatLong $scope.latitude, $scope.longitude, (name, result)->
        $scope.location_name = name.split(',')[1]

    $scope.goCheckout = (e)->
      unless $scope.payment_method
        toastr.error "Please <a href='"+$scope.payment_methods_url+"&new=1' style='color:#41e5c9;'>Add payment method here</a>.", 'No Payment Method Selected!'
        e.preventDefault()
        return false
    $scope.goTo = (url)->
      window.location.assign(url)
]