//= require lib/ng-tags-input.min
//= require lib/ngAutocomplete
//= require lib/jquery.ui.widget
//= require lib/jquery.row-grid.min
//= require lib/load-image.all.min
//= require lib/canvas-to-blob.min
//= require lib/jquery.iframe-transport
//= require lib/jquery-ui-timepicker-addon
//= require lib/ui-select/select.min
//= require lib/paginate-anything
//= require lib/angular-route.min
//= require lib/map-infobox
//= require lib/html2canvas.min
//= require Chart.js/Chart
//= require angular-chart.js/dist/angular-chart
//= require_tree .