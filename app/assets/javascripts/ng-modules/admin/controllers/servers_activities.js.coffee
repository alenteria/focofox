angular.module('Admin')
.controller 'ServerActivitiesCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	'$http'
	($rootScope, $scope, toastr, localStorageService, $http)->
		
		$scope.labels = {}
		$scope.stats = {}
		$scope.data = {}
		$scope.series = {}
		$scope.query = (type, filter)->
			$http.post('/admin/api/server_stats', {type: type, filter: filter}).success (stats)->
				$scope.stats[type] = stats

				$scope.labels[type] = _.keys(stats).sort()

				$scope.data[type] = [[], []]
				
				if type=='bandwidth'
					$scope.series[type] = ["Transmit", "Received"]
				
				angular.forEach $scope.labels[type], (d)->
					s = stats[d]
					if type == 'bandwidth'
						sum = _.reduce _.pluck(s, 'tx'), (m, n)-> parseFloat(m)+parseFloat(n)
						sum2 = _.reduce _.pluck(s, 'rx'), (m, n)-> parseFloat(m)+parseFloat(n)
						$scope.data[type][0].push sum
						$scope.data[type][1].push sum2
					else
						sum = _.reduce _.pluck(s, 'stat'), (m, n)-> parseFloat(m)+parseFloat(n)
						$scope.data[type][0].push sum
					
				if stats.length <=0
					$scope.data[type] = [ 
					  { 
					    "key" : "A key" , 
					    "values" : [[]]
					  }
					]


		$scope.logins_date_filter = localStorageService.get('logins_date_filter') || {}
		$scope.new_users_date_filter = localStorageService.get('new_users_date_filter') || {}
		$scope.photos_uploaded_date_filter = localStorageService.get('photos_uploaded_date_filter') || {}
		$scope.storage_required_date_filter = localStorageService.get('storage_required_date_filter') || {}
		$scope.droplets_provisioned_date_filter = localStorageService.get('droplets_provisioned_date_filter') || {}
		$scope.bandwidth_date_filter = localStorageService.get('bandwidth_date_filter') || {}

		$scope.first_load = true

		$scope.$watchGroup [
			'logins_date_filter', 
			'new_users_date_filter', 
			'photos_uploaded_date_filter', 
			'storage_required_date_filter', 
			'droplets_provisioned_date_filter',
			'bandwidth_date_filter'
			]
			, (types)->
				angular.forEach types, (filter, i)->
					return false if !(filter.from && filter.to) && !$scope.first_load
					type = ''
					switch i
						when 0
							type = 'logins'
						when 1
							type = 'new_users'
						when 2
							type = 'photos_uploaded'
						when 3
							type = 'storage_required'
						when 4
							type = 'droplets_provisioned'
						when 5
							type = 'bandwidth'
					
					$scope.query(type, filter)

				$scope.first_load = false
 
		$scope.removeDateTimeFilter = (type)->
			localStorageService.set(type, {})
			$scope[type] = {}

			q_type = ''
			switch type
				when 'logins_date_filter'
					q_type = 'logins'
				when 'new_users_date_filter'
					q_type = 'new_users'
				when 'photos_uploaded_date_filter'
					q_type = 'photos_uploaded'
				when 'storage_required_date_filter'
					q_type = 'storage_required'
				when 'droplets_provisioned_date_filter'
					q_type = 'droplets_provisioned'
				when 'bandwidth_date_filter'
					q_type = 'bandwidth'
					
			$scope.query(q_type)

		$scope.addDateTimeFilter = (type, form)->
			$scope[type] = $scope[type] || {}
			$scope[type] = {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
			localStorageService.set(type, $scope[type])
			
			$scope[form] = false
			$scope.datetime_filter_from = null
			$scope.datetime_filter_to = null

		$scope.$watch 'datetime_filter_from', (dff)->
      return false unless dff
      return false if $scope.datetime_filter_to && (new Date(dff)) < (new Date($scope.datetime_filter_to))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope.datetime_filter_to = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.$watch 'datetime_filter_to', (dft)->
      return false unless dft
      return false if $scope.datetime_filter_from && (new Date(dft)) > (new Date($scope.datetime_filter_from))

      dt = new Date(dft)
      dt = dt.minusHours(1)
      $scope.datetime_filter_from = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

]
