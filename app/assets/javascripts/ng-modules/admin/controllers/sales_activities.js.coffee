angular.module('Admin')
.controller 'SalesActivitiesCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	'$http'
	($rootScope, $scope, toastr, localStorageService, $http)->
		$scope.query = (params)->
			$http.post('/admin/api/sales', params).success (sales)->
				$scope.labels = _.keys(sales).sort()
				$scope.data = [[]]
				
				angular.forEach $scope.labels, (d)->
					s = sales[d]
					sum = _.reduce _.pluck(s, 'price'), (m, n)-> parseFloat(m)+parseFloat(n)
					$scope.data[0].push sum
				
				if sales.length <=0
					$scope.data = [ 
					  { 
					    "key" : "A key" , 
					    "values" : [[]]
					  }
					]

		$scope.query2 = (params)->
			$http.post('/admin/api/sales', params).success (sales)->
				$scope.labels2 = _.keys(sales).sort()
				$scope.data2 = [[]]

				angular.forEach $scope.labels2, (d)->
					s = sales[d]
					sum2 = 0
					angular.forEach _.pluck(s, 'items'), (item)->
						sum2 += item.length
					$scope.data2[0].push sum2
				
				if sales.length <=0
					$scope.data2 = [ 
					  { 
					    "key" : "A key" , 
					    "values" : [[]]
					  }
					]

		$scope.stat_dollar_filter = localStorageService.get('stat_dollar_filter')
		$scope.stat_photos_count_filter = localStorageService.get('stat_photos_count_filter')
		
		$scope.query($scope.stat_dollar_filter)
		$scope.query2($scope.stat_photos_count_filter)

		$scope.addDateTimeFilter = ->
			$scope.stat_dollar_filter = {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
			localStorageService.set('stat_dollar_filter', $scope.stat_dollar_filter)
			$scope.query($scope.stat_dollar_filter)
			$scope.showDateTimeFilterForm = false
			$scope.datetime_filter_from = ''
			$scope.datetime_filter_to = ''

		$scope.removeDateTimeFilter = ->
			localStorageService.set('stat_dollar_filter', {})
			$scope.stat_dollar_filter = {}
			$scope.query()

		$scope.addDateTimeFilter2 = ->
			$scope.stat_photos_count_filter = {from: $scope.datetime_filter_from2, to: $scope.datetime_filter_to2}
			localStorageService.set('stat_photos_count_filter', $scope.stat_photos_count_filter)
			$scope.query2($scope.stat_photos_count_filter)
			$scope.showDateTimeFilterForm2 = false
			$scope.datetime_filter_from2 = ''
			$scope.datetime_filter_to2 = ''

		$scope.removeDateTimeFilter2 = ->
			localStorageService.set('stat_photos_count_filter', {})
			$scope.stat_photos_count_filter = {}
			$scope.query2()

		$scope.$watch 'datetime_filter_from', (dff)->
      return false unless dff
      return false if $scope.datetime_filter_to && (new Date(dff)) < (new Date($scope.datetime_filter_to))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope.datetime_filter_to = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.$watch 'datetime_filter_to', (dft)->
      return false unless dft
      return false if $scope.datetime_filter_from && (new Date(dft)) > (new Date($scope.datetime_filter_from))

      dt = new Date(dft)
      dt = dt.minusHours(1)
      $scope.datetime_filter_from = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)


    $scope.$watch 'datetime_filter_from2', (dff)->
      return false unless dff
      return false if $scope.datetime_filter_to2 && (new Date(dff)) < (new Date($scope.datetime_filter_to2))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope.datetime_filter_to2 = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.$watch 'datetime_filter_to2', (dft)->
      return false unless dft
      return false if $scope.datetime_filter_from2 && (new Date(dft)) > (new Date($scope.datetime_filter_from2))

      dt = new Date(dft)
      dt = dt.minusHours(1)
      $scope.datetime_filter_from2 = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)
]
