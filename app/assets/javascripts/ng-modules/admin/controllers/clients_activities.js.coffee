angular.module('Admin')
.controller 'ClientActivitiesCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	'$modal'
	($rootScope, $scope, toastr, localStorageService, $modal)->
		$scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200,300,400,500]
		$scope.perPage = parseInt(localStorageService.get('perPage')) || 50
		$scope.clients_statistics_url = '/admin/api/clients'
		$scope.filter = localStorageService.get('activities_filter')||{}
		$scope.distance = $scope.filter.distance
		$scope.filterq = {}

		window.navigator.geolocation.getCurrentPosition (position) ->
      $scope.default_gps_latitude = position.coords.latitude
      $scope.default_gps_longitude = position.coords.longitude

		$scope.$watch 'datetime_filter_from', (dff)->
      return false unless dff
      return false if $scope.datetime_filter_to && (new Date(dff)) < (new Date($scope.datetime_filter_to))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope.datetime_filter_to = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.$watch 'datetime_filter_to', (dft)->
      return false unless dft
      return false if $scope.datetime_filter_from && (new Date(dft)) > (new Date($scope.datetime_filter_from))

      dt = new Date(dft)
      dt = dt.minusHours(1)
      $scope.datetime_filter_from = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.addDateTimeFilter = ->
    	$scope.filter.date_time = {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
    	$scope.showDateTimeFilterForm = false
    	$scope.datetime_filter_from = ''
    	$scope.datetime_filter_to = ''

    $scope.removeDateTimeFilter = ->
    	$scope.filter.date_time = {}

    $scope.$watchGroup ['filter.date_time.from', 'filter.date_time.to', 'filter.location', 'filter.distance'], (f, t, l)->
    	localStorageService.set('activities_filter', $scope.filter)
    	$scope.filterq.location = JSON.stringify($scope.filter.location)
    	$scope.filterq.date_time = JSON.stringify($scope.filter.date_time)
    	$scope.filterq.distance = $scope.distance

    $scope.pickLocation = ->
    	modalInstance = $modal.open
        size: 'lg'
        templateUrl: 'ng-modules/admin/views/statistics/clients_activity/location_picker.html'
        controller: 'locationModalInstanceCtrl'
        resolve:
          notify_success: ->
            false

          location: ()->
            `null`

          parent: ()->
            $scope

          gps: ()->
            default_data = 
              latitude: $scope.default_gps_latitude
              longitude: $scope.default_gps_longitude

      modalInstance.result.then (loc)->
      	$scope.filter.location = {latitude: (loc.gps||{}).latitude, longitude: (loc.gps||{}).longitude, name: loc.name}
      	$scope.distance = $scope.distance||500
	    	$scope.filter.distance = $scope.distance

    $scope.removeLocationFilter = ->
    	$scope.filter.location = {}
    	$scope.distance = 0
    	$scope.filter.distance = $scope.distance
]
