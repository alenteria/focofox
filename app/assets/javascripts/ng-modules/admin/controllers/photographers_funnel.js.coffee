angular.module('Admin')
.controller 'PhotographerFunnelCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	'$http'
	($rootScope, $scope, toastr, localStorageService, $http)->
		$scope.query = (params)->
			$http.post('/admin/api/photographers/funnel_report', params).success (result)->
				if $scope.photographer_funnel_filter.from and $scope.photographer_funnel_filter.to
					labels = _.filter result.keys||[], (k) ->
						kdate = new Date(k)
						kdate >= new Date($scope.photographer_funnel_filter.from) and kdate <= new Date($scope.photographer_funnel_filter.to)
				else
					labels = result.keys||[]

				$scope.labels = _.sortBy labels
				$scope.series = ['Register', 'Add Photos', 'Sell $100/m', 'Sell $1000/m']
				$scope.data = [[], [], [], [], ]
				
				angular.forEach $scope.labels, (d)->
					#registrations
					$scope.data[0].push (result.registrations[d]||[]).length
					$scope.data[1].push (result.first_add_photos[d]||[]).length
					$scope.data[2].push (result.first_1hun_sales[d]||[]).length
					$scope.data[3].push (result.first_1k_sales[d]||[]).length
					

				if (result.keys||[]).length <=0
					$scope.data = [ 
					  { 
					    "key" : "A key" , 
					    "values" : [[]]
					  }
					]

		$scope.photographer_funnel_filter = localStorageService.get('photographer_funnel_filter')||{}
		
		$scope.query($scope.photographer_funnel_filter)

		$scope.addDateTimeFilter = ->
			$scope.photographer_funnel_filter = {from: $scope.datetime_filter_from, to: $scope.datetime_filter_to}
			localStorageService.set('photographer_funnel_filter', $scope.photographer_funnel_filter)
			$scope.query($scope.photographer_funnel_filter)
			$scope.showDateTimeFilterForm = false
			$scope.datetime_filter_from = ''
			$scope.datetime_filter_to = ''

		$scope.removeDateTimeFilter = ->
			localStorageService.set('photographer_funnel_filter', {})
			$scope.photographer_funnel_filter = {}
			$scope.query() 

		$scope.$watch 'datetime_filter_from', (dff)->
      return false unless dff
      return false if $scope.datetime_filter_to && (new Date(dff)) < (new Date($scope.datetime_filter_to))

      dt = new Date(dff)
      dt = dt.addHours(1)
      $scope.datetime_filter_to = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

    $scope.$watch 'datetime_filter_to', (dft)->
      return false unless dft
      return false if $scope.datetime_filter_from && (new Date(dft)) > (new Date($scope.datetime_filter_from))

      dt = new Date(dft)
      dt = dt.minusHours(1)
      $scope.datetime_filter_from = (dt.getMonth()+1)+'/'+dt.getDate()+'/'+dt.getFullYear()+' '+formatAMPM(dt)

]
