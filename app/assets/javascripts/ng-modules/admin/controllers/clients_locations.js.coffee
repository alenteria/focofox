angular.module('Admin')
.controller 'ClientLocationsCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	'$state'
	'$http'
	($rootScope, $scope, toastr, localStorageService, $state, $http)->
		$scope.photographer_name = $state.params.photographer_name
		$http.get('/admin/api/photographers/'+$state.params.photographer_id+'/locations').success (data)->
			$scope.locations = data
]