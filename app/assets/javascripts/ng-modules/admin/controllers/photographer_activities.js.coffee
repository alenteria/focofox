angular.module('Admin')
.controller 'PhotographerActivitiesCtrl',[
	'$rootScope'
	'$scope'
	'toastr'
	'localStorageService'
	($rootScope, $scope, toastr, localStorageService)->
		$scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200,300,400,500]
		$scope.perPage = parseInt(localStorageService.get('perPage')) || 50
		$scope.photographers_statistics_url = '/admin/api/photographers'

]
