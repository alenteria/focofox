'use strict'
viewPath = 'ng-modules/admin/views'
angular.module('Admin', [
  'ngCookies'
  "ui.bootstrap"
  'ngTagsInput'
  'ngResource'
  'ngSanitize'
  'ngAutocomplete'
  'common'
  'templates'
  'ui.select'
  'bgf.paginateAnything'
  'ui.router'
  'tableSort'
  'chart.js'
])
.config [
  "$httpProvider"
  "$stateProvider"
  "$urlRouterProvider"
  ($httpProvider, $stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise("/statistics/photographers_activity")

    $stateProvider
    .state('photographers_activity', {
      url: '/statistics/photographers_activity', 
      templateUrl: viewPath+'/statistics/photographers_activity.html'
      controller: 'PhotographerActivitiesCtrl'
    })
    .state('photographers_activity_locations', {
      url: '/statistics/photographers_activity/:photographer_id/:photographer_name/locations', 
      templateUrl: viewPath+'/statistics/photographers_activity/locations.html'
      controller: 'PhotographerLocationsCtrl'
    })
    .state('clients_activity', {
      url: '/statistics/clients_activity', 
      templateUrl: viewPath+'/statistics/clients_activity.html'
      controller: 'ClientActivitiesCtrl'
    })
    .state('sales_activity', {
      url: '/statistics/sales_activity', 
      templateUrl: viewPath+'/statistics/sales_activity.html'
      controller: 'SalesActivitiesCtrl'
    })
    .state('photographers_funnel', {
      url: '/statistics/photographers_funnel_report', 
      templateUrl: viewPath+'/statistics/photographers_funnel_report.html'
      controller: 'PhotographerFunnelCtrl'
    })
    .state('clients_funnel', {
      url: '/statistics/clients_funnel_report', 
      templateUrl: viewPath+'/statistics/clients_funnel_report.html'
      controller: 'ClientFunnelCtrl'
    })
    .state('servers_activity', {
      url: '/statistics/servers_activity', 
      templateUrl: viewPath+'/statistics/servers_activity.html'
      controller: 'ServerActivitiesCtrl'
    })
]