watermark_url = '/api/watermarks'
location_url = '/api/locations'
license_url = '/api/licenses'
angular.module 'preferencesApp'
.controller "MainController",	[
	'$scope'
	'$http'
	'$modal'
	($scope, $http, $modal) ->
		window.navigator.geolocation.getCurrentPosition ((position) ->
			latitude = position.coords.latitude
			longitude = position.coords.longitude
			$scope.gps = {latitude: latitude, longitude: longitude}
			$scope.locations = $scope.locations || []
			default_location_valid = _.findWhere $scope.locations, {id: $scope.default_location}
			unless default_location_valid
				getAddressNameFromLatLong latitude, longitude, (name)->
					default_location = {latitude: latitude, longitude: longitude, id: 'undefined', name: name}
					$scope.default_location = default_location.id
					existing = _.findWhere $scope.locations, {latitude: latitude, longitude: longitude}

					unless !!existing
						$http.post('/api/locations', default_location).success (res)->
							if `res.status=="ok"`
								existing = _.findWhere $scope.locations, {id: res.data.id}
								unless existing
									$scope.locations.push(res.data)
									$scope.default_location = $scope.locations[$scope.locations.length-1]['id']

					$scope.$digest()
				return
			return
		), (error) ->
			console.error error
			return
		$scope.watermarks = []
		$scope.locations = []
		$scope.watermarkEditPreviewLabel = ()->
			default_watermark = _.findWhere $scope.watermarks, {id: $scope.default_watermark}
			if default_watermark && !!default_watermark.user_id then 'Edit' else 'Preview' 
		$scope.editWatermark = ()->
			$scope.watermark = _.findWhere $scope.watermarks, {id: $scope.default_watermark}
			if !!!$scope.watermark.watermark_image && !!$scope.watermark.photo_url
				loadImage(
		      $scope.watermark.photo_url,
		      (img)->
		      		$scope.watermark.watermark_image = {previewUrl: img.src}
		      		modalInstance = $modal.open
		      			size: 'lg'
		      			templateUrl: 'edit-watermark.html'
		      			controller: 'watermarkModalInstanceCtrl'
		      			resolve:
		      				watermark: ()->
		      					$scope.watermark

		      		modalInstance.result.then (watermark)->
		      			if `watermark.action=='delete'`
		      				$scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
		      				$scope.default_watermark = $scope.watermarks[0].id
		      			else
		      				$scope.watermark = watermark
		      		return
		      ,
		      {maxWidth: 900, noRevoke: true, canvas: false}
			  )

				return
			else if !!!$scope.watermark.photo_url
				modalInstance = $modal.open
	  			size: 'lg'
	  			templateUrl: 'edit-watermark.html'
	  			controller: 'watermarkModalInstanceCtrl'
	  			resolve:
	  				watermark: ()->
	  					$scope.watermark

	  		modalInstance.result.then (watermark)->
	  			if `watermark.action=='delete'`
	  				$scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
	  				$scope.default_watermark = $scope.watermarks[0].id
	  			else
	  				$scope.watermark = watermark
	  		return
			else
				modalInstance = $modal.open
					size: 'lg'
					templateUrl: 'edit-watermark.html'
					controller: 'watermarkModalInstanceCtrl'
					resolve:
						watermark: ()->
							$scope.watermark

				modalInstance.result.then (watermark)->
					if `watermark.action=='delete'`
	  				$scope.watermarks.splice($scope.watermarks.indexOf($scope.watermark), 1)
	  				$scope.default_watermark = $scope.watermarks[0].id
	  			else
						$scope.watermark = watermark
		  	return
		  false

		$scope.newWatermark = ()->
			$scope.watermark = 
				display_type: 'tiled'
				tiled_size: 200
				position: 'center center'
				positioned_size: 50
				rotation: 0
				stretched_size: 50
				opacity: 1

			modalInstance = $modal.open
				size: 'lg'
				templateUrl: 'new-watermark.html'
				controller: 'watermarkModalInstanceCtrl'
				resolve:
					watermark: ()->
						$scope.watermark

			modalInstance.result.then (watermark)->
				$scope.watermarks.push watermark
				try
					$scope.default_watermark = $scope.watermarks[$scope.watermarks.length-1]['id']
					$scope.mark_default_watermark($scope.default_watermark)
				catch
					# 

		$scope.mark_default_watermark = (dw)->
			if dw
				$http.post(watermark_url+"/"+dw+'/mark_default_watermark/', {id: dw}).success ()->

		$scope.mark_default_location = (dl)->
			if dl
				$http.post(location_url+"/"+dl+'/mark_default_location/', {id: dl}).success ()->
			
		$scope.newLocation = ()->
			modalInstance = $modal.open
				size: 'lg'
				backdrop: 'static'
				keyboard: false
				templateUrl: 'new-location.html'
				controller: 'locationModalInstanceCtrl'
				resolve:
					location: ()->
						$scope.location
					gps: ()->
						$scope.gps

			modalInstance.result.then (res)->
				if `res.status=="ok"`
					existing = _.findWhere $scope.locations, {id: res.data.id}
					unless existing
						$scope.locations.push(res.data)
						$scope.default_location = $scope.locations[$scope.locations.length-1]['id']
						$scope.mark_default_location($scope.default_location)

		$scope.editLocation = ()->
			modalInstance = $modal.open
				size: 'lg'
				backdrop: 'static'
				keyboard: false
				templateUrl: 'edit-location.html'
				controller: 'locationModalInstanceCtrl'
				resolve:
					location: ()->
						angular.copy _.findWhere $scope.locations, {id: $scope.default_location}
					gps: ()->
						$scope.gps
						
			modalInstance.result.then (res)->
				if `res.status=="ok"`
					if $scope.default_location=='undefined'
						d = _.findWhere $scope.locations, {id: 'undefined'}
						d.id = res.data.id
						$scope.default_location = d.id

					existing = _.findWhere $scope.locations, {id: res.data.id}

					if existing
						index = $scope.locations.indexOf existing
						if `res.action=='delete'`
							$scope.locations.splice(index, 1)
							$scope.default_location = $scope.locations[0].id
							return false
						$scope.locations[index] = res.data
				return
		$scope.$watch 'default_photo_price', (p)->
			if p
				$http.put('/api/photographer_preferences/update_default_photo_price', {default_photo_price: p})

		$scope.newLicense = ()->
			$scope.license = {}
			modalInstance = $modal.open
				size: 'lg'
				templateUrl: 'new-license.html'
				controller: 'licenseModalInstanceCtrl'
				resolve:
					license: ()->
						$scope.license

			modalInstance.result.then (res)->
				if `res.status=="ok"`
					existing = _.findWhere $scope.licenses, {id: res.data.id}
					unless existing
						$scope.licenses.push(res.data)
						$scope.default_license = $scope.licenses[$scope.licenses.length-1]

		$scope.editPreviewLicenseLabel = ()->
			default_license = _.findWhere $scope.licenses, {id: $scope.default_license}
			if default_license && !!default_license.user_id then 'Edit' else 'Preview'

		$scope.editLicense = ()->
			$scope.license = angular.copy _.findWhere $scope.licenses, {id: $scope.default_license}
			modalInstance = $modal.open
				size: 'lg'
				templateUrl: 'edit-license.html'
				controller: 'licenseModalInstanceCtrl'
				resolve:
					license: ()->
						$scope.license

			modalInstance.result.then (license)->
				if `license.action=='delete'`
					$scope.licenses = $scope.licenses.splice($scope.licenses.indexOf($scope.license), 1)
					$scope.default_license = $scope.licenses[0].id
				else					
					existing = _.findWhere $scope.licenses, {id: license.data.id}
					if existing
						index = $scope.licenses.indexOf existing
						$scope.licenses[index] = license.data

		$scope.mark_default_license = (dl)->
			if dl
				$http.put(license_url+"/"+dl+'/mark_default/', {id: dl}).success ()->
		return
]