'use strict'
angular.module('preferencesApp', [
	"ui.bootstrap"
	'ngTagsInput'
	'ngResource'
	'ngSanitize'
	'ngMap'
	'ngAutocomplete'
  'common'
])
.config [
  "$httpProvider"
  ($httpProvider) ->

]

$(document).on "dragenter", "#watermarked-image-preview", (e) ->
  e.stopPropagation()
  e.preventDefault()
  $(this).css "border", "2px dotted #0B85A1"
  return

$(document).on "dragover", "#watermarked-image-preview", (e) ->
  e.stopPropagation()
  e.preventDefault()
  return

$(document).on "drop", "#watermarked-image-preview", (e) ->
  $(this).css "border", "none"
  e.preventDefault()
  $scope = angular.element('#preferences').scope()
  file = e.originalEvent.dataTransfer.files[0]
  loadImage(
      file,
      (img)->
      	$scope.$apply ()->
      		$scope.watermark.watermark_image = {file: file, previewUrl: img.src}
      ,
      {maxWidth: 900, noRevoke: true, canvas: false}
  )
  return

