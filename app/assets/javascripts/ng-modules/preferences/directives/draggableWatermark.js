angular.module('preferencesApp').directive('draggableWatermark', function($parse) {
    return {
    	scope: false,
    	link: function(scope, element, attrs) {
        $(element).draggable({stop: function(event, ui){
        	scope.watermark.position = {x: ui.position.left, y: ui.position.top}
     		  if(!scope.$$phase) scope.$apply()
          return
				 }
				});
    }
  }
}
);