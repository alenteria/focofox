angular.module('preferencesApp')
.directive 'uniqueGps', [
  '$http'
  ($http)->
    {
      require: 'ngModel'
      link: (scope, element, attrs, ctrl) ->
        scope.$watch 'location.gps', (val)->
          $http.post('/api/locations/verify_gps', scope.location).success (res)->
            if res
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true
        scope.$watch 'location.gps.latitude', (val)->
          $http.post('/api/locations/verify_gps', scope.location).success (res)->
            if res
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true

        scope.$watch 'location.gps.longitude', (val)->
          $http.post('/api/locations/verify_gps', scope.location).success (res)->
            if res
              ctrl.$setValidity 'uniqueGps', false
            else
              ctrl.$setValidity 'uniqueGps', true
    }
]