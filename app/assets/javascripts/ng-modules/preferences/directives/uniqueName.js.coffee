angular.module('preferencesApp').directive 'uniqueName',[
  '$http'
  ($http)->
    {
      require: 'ngModel'
      link: (scope, element, attrs, ctrl) ->
        obj = (attrs.object||'location')
        scope.$watch obj+'.name', (val)->
          ctrl.$setValidity 'submittable', 'no'
          $http.post('/api/'+obj+'s/verify_name', scope[obj], {ignoreLoadingBar: true}).success (res)->
            ctrl.$setValidity 'submittable', 'yes'
            if res
              ctrl.$setValidity 'uniqueName', false
            else
              ctrl.$setValidity 'uniqueName', true
    }
]