angular.module('purchasesApp')
.factory 'Payment', ($resource)->
  $resource('/api/payments/:id')
