'use strict'
viewPath = 'ng-modules/purchases/views'
angular.module('purchasesApp',
  [
    'ui.bootstrap'
    'templates'
    'ngResource'
    'ui.router'
    'bgf.paginateAnything'
    'common'
  ]
)
.config [
  "$httpProvider"
  "$stateProvider"
  "$urlRouterProvider"
  ($httpProvider, $stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise("/")

    $stateProvider
    .state('payments', {
      url: "/",
      templateUrl: viewPath+'/index.html'
      controller: 'mainController'
    })
    .state('payment', {
      url: "/:id",
      templateUrl: viewPath+'/payment-details.html'
      controller: 'paymentDetailsController'
    })
]