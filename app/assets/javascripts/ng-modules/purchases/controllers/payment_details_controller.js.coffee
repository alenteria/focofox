angular.module 'purchasesApp'
.controller 'paymentDetailsController', [
  '$scope'
  'Payment'
  '$stateParams'
  '$modal'
  '$cookieStore'
  '$cookies'
  'localStorageService'
  ($scope, Payment, $stateParams, $modal, $cookieStore, $cookies, localStorageService)->
    $scope.thumbnail_size = parseInt($cookies.thumbnail_size) || 200
    $scope.perPage = parseInt($cookies.perPage) || 50
    $scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200]
    sort_by = localStorageService.get('sort_by')||'date_time'
    if $.inArray(sort_by, ['created_at'])
      $scope.sort_by = 'date_time'
    else
      $scope.sort_by = sort_by
    
    $scope.filterq = {sort_by: $scope.sort_by}

    $scope.$watch 'thumbnail_size', (ts)->
      $cookieStore.put('thumbnail_size', parseInt(ts))
    
    $scope.$watch 'perPage', (p)->
      $cookieStore.put('perPage', parseInt(p))

    $scope.$watch 'sort_by', (sb)->
      if sb
        localStorageService.set('sort_by', sb)
        $scope.filterq.sort_by = sb

    $scope.payment = Payment.get({id: $stateParams.id})
    $scope.photos_url = 'api/payments/'+$stateParams.id+'/photos'

    $scope.preview = (p, photos)->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/common/views/preview-photo.html'
        controller: 'PreviewPhotoModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        scope: $scope
        resolve:
          photo: ()->
            p
          photos: ()->
            photos||$scope.photos
          w: ()->
            {
            width: $(window).width()
            height: $(window).height()
            }
          type: ()->
            "my-photos"
]