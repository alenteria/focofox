login_url = '/api/users/verify_login_credentials'
angular.module('loginApp')
.controller 'main_controller', [
	'$scope'
	'$http'
	'toastr'
	($scope, $http, toastr)->
		$scope.isOtp = false
		if (window.flash_success||"").length > 0
		  toastr.success(window.flash_success, 'Success!') 

		if (window.flash_notice||"").length > 0
		  toastr.warning(window.flash_notice, 'Notice!') 

		if (window.flash_alert||"").length > 0
		  toastr.error(window.flash_alert, 'Alert!') 

		if (window.flash_errors||"").length > 0
		  toastr.error(window.flash_errors, 'Error!') 

		if window.validation_errors
      _.forEach window.validation_errors, (ve, key)->
        if (ve||[]).length > 0
          toastr.error(ve[0], key)

		$scope.login_failed = false
		$scope.verifyCredentials = ($event)->
			$scope.login_failed_message = `null`
			$event.preventDefault()
			$http.post(login_url, {email: $scope.email, password: $scope.password}).success (data)->
				if data.valid
					$scope.login_success = true
					$('form').submit()
				else
					$scope.login_failed = true
					$scope.login_failed_message = data.message || 'Invalid email or password.'			
				return
			return
		
		$scope.removeMessage = ()->
			$scope.login_failed = false

		$scope.photographerSignupPopover = {
			templateUrl: 'ng-modules/login/views/signup_info_template.html'
		}

		$scope.detect2factorAuth = ->
			$http.post('/api/users/verify_otp', email: $scope.email).success (data)->
				$scope.isOtp = data.is_otp
]