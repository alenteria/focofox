$(document).on 'mousedown', '.my-photos.selectable .thumbnails', (e)->
  $scope = angular.element('.thumbnails').scope()

  if $(e.target).hasClass('thumbnails')
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []

    $('.ui-selected').removeClass('ui-selected')
    $scope.$apply() if(!$scope.$$phase)

$(document).on 'click', '.my-photos.selectable .thumbnail', (e)->
  $scope = angular.element('.thumbnails').scope()
  if $(e.target).hasClass('fa-search-plus') || $(e.target).hasClass('fa-navicon')|| $(e.target).hasClass('details')
    return false

  element = $(this)
  index = element.data('index')
  photo = $scope.photos[index]
  if shiftDown
    if !$scope.selectedPhotos[0]
      $scope.last_selected_index = index
      element.addClass 'ui-selected'
      $scope.selectedPhotos[0] = photo
      $scope.selectedPhotoIds[0] = photo.id
      return false

    $scope.last_selected_index = $scope.last_selected_index||index

    i1 = Math.min($scope.last_selected_index, index)
    i2 = Math.max($scope.last_selected_index, index)

    $('.ui-selected').removeClass('ui-selected')
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []

    i = i1
    while i <= i2
      $('[data-index="'+i+'"]').addClass('ui-selected')
      photo = $scope.photos[i]
      if photo
        $scope.selectedPhotos.push photo
      i++
  else if ctrlDown || true #Single click same as with ctrl - issue #108
    photos = angular.copy $scope.selectedPhotos
    photoIds = angular.copy $scope.selectedPhotoIds
    if !element.hasClass('ui-selected')
      $scope.last_selected_index = index
      element.addClass 'ui-selected'
      photos.push photo
      photoIds.push photo.id
    else
      $scope.last_selected_index = `null`
      element.removeClass 'ui-selected'
      photo = _.findWhere photos, {id: photo.id}
      photos = _.without photos, photo
      photoIds = _.without photoIds, photo.id

    $scope.selectedPhotos = photos
    $scope.selectedPhotoIds = photoIds

  else
    if !element.hasClass('ui-selected')
      $scope.last_selected_index = index
      $('.ui-selected').removeClass('ui-selected')
      element.addClass 'ui-selected'
      $scope.selectedPhotos = [photo]
    else
      $scope.last_selected_index = `null`
      $('.ui-selected').removeClass('ui-selected')
      $scope.selectedPhotos = []
  $scope.$apply() if(!$scope.$$phase)

$(document).on 'click', 'div', (e)->
  unless _.includes(["select-multiple", "select-one", 'text', 'range'], ((e.originalEvent||{}).target||{}).type) || $(e.target).hasClass('fa-info-circle')|| $(e.target).hasClass('show-info')
    $('.thumbnails').focusWithoutScrolling()
