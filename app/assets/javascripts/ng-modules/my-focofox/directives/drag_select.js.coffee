'use strict'
angular.module('myFocofoxApp').directive 'dragSelectable', ()->
  return{
    restrict: 'AE'
    scope: false
    link: ($scope, el, attrs)->
      selectAbleClass = eval(attrs.options)

      rangeSelecting = (event, ui)->
        start = $(".ui-selecting", this).first();
        end = $(".ui-selecting", this).last();
        selecting = false;

      $('#selectable').selectable
        filter: '.'+selectAbleClass,
        distance: 1
        autoRefresh: true
        selecting: rangeSelecting, 
        unselecting: rangeSelecting
        stop: ()->
          $scope.last_selected_index = `null`
          unless $scope.expandGroup
            $scope.selectedPhotos = []
            $scope.selectedPhotoIds = []

          $.each $('.ui-selected'), (l, el)->
            i = $(el).data('index')
            if $scope.photos[i]
              photo = $scope.photos[i]
              existing = _.findWhere($scope.selectedPhotos, {'$$hashKey': photo['$$hashKey']})
              if existing and !ctrlDown
                index = $scope.selectedPhotos.indexOf existing
                $scope.selectedPhotos = _.without($scope.selectedPhotos, $scope.photos[index])
                $scope.selectedPhotoIds = _.without($scope.selectedPhotoIds, $scope.photos[index].id)
                $(el).removeClass('ui-selected')
              else
                $scope.selectedPhotos.push photo
                $scope.selectedPhotoIds.push photo.id
                return
          $scope.$apply() if(!$scope.$$phase)
          return
  }

