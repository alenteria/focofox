'use strict'
escKeyCode = 27
$(document).on 'keydown','.thumbnails', (e)->
  $scope = angular.element('#photos-list').scope()
  if e.keyCode == escKeyCode
    e.preventDefault()
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []
    $('.thumbnail').removeClass('ui-selected')
    $scope.$apply() if(!$scope.$$phase)
    
  