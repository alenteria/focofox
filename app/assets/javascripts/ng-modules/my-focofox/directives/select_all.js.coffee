'use strict'
aKeyCode = 65
$(document).on 'keydown', '.thumbnails', (e)->
  $scope = angular.element('#photos-list').scope()
  if e.keyCode == aKeyCode && ctrlDown
    e.preventDefault()
    $scope.selectAll()
    $scope.$apply() if(!$scope.$$phase)
  