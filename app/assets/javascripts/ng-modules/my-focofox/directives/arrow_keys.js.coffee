'use strict'
leftKeyCode = 37
rightKeyCode = 39
$(document).on 'keydown', '.thumbnails', (e)->
  $scope = angular.element('#photos-list').scope()
  # try
  if e.keyCode == leftKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.photos, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.photos[0]

    index = $scope.photos.indexOf(lastSelected)
    if $scope.photos[index-1]
      index = index-1
    else if  $scope.photos[$scope.photos.length-1]
      index = $scope.photos.length-1

    $('.ui-selected').removeClass('ui-selected')
    $('[data-index="'+index+'"]').addClass('ui-selected')
    $scope.selectedPhotos = [$scope.photos[index]]

  else if e.keyCode == rightKeyCode
    e.preventDefault()
    index = if $scope.selectedPhotos.length > 0 then ($scope.selectedPhotos.length-1) else 0
    lastSelected = $scope.selectedPhotos[index]
    if lastSelected
      lastSelected = _.findWhere $scope.photos, {'$$hashKey': lastSelected['$$hashKey']} 
    else
      lastSelected = $scope.photos[0]

    index = $scope.photos.indexOf(lastSelected)
    if $scope.photos[index+1]
      index = index+1     
    else if  $scope.photos[0]
      index = 0

    $('.ui-selected').removeClass('ui-selected')
    $('[data-index="'+index+'"]').addClass('ui-selected')
    $scope.selectedPhotos = [$scope.photos[index]]

  $scope.$apply() if(!$scope.$$phase)
  # catch
    # 
