'use strict'
viewPath = 'ng-modules/my-focofox/views'
angular.module('myFocofoxApp',
  [
    'ui.bootstrap'
    'templates'
    'ngResource'
    'ui.router'
    'bgf.paginateAnything'
    'common'
  ]
)
.config [
  "$httpProvider"
  "$stateProvider"
  "$urlRouterProvider"
  ($httpProvider, $stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise("/my-photos")

    $stateProvider
    .state('my_photos', {
      url: "/my-photos",
      templateUrl: viewPath+'/my-photos.html'
      controller: 'MyPHotosCtrl'
    })
    .state('purchase_history', {
      url: "/purchase-history",
      templateUrl: viewPath+'/purchase-history.html'
      controller: 'PurchaseHistoryCtrl'
    })
    .state('settings', {
      url: "/settings",
      templateUrl: viewPath+'/settings.html'
      controller: 'SettingsCtrl'
    })
]