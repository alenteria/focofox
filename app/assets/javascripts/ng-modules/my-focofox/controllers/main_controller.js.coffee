angular.module 'myFocofoxApp'
.directive 'selectPlugin', [ ->
  {
    restrict: 'A'
    link: (scope, elem, attr) ->
      runSelect2 = ->
        if angular.element(elem).hasClass 'remove-empty-option'
          angular.element(elem).find('option').first().remove()
        angular.element(elem).addClass('my-focofox-select').select2()

      setTimeout runSelect2, 0
  }
]
.controller 'MainCtrl', [
  '$scope'
  '$modal'
  ($scope, $modal)->
    $scope.tabData   = [
      {
        heading: 'My Photos',
        route:   'my_photos'
        controller: 'MyPHotosCtrl'
      },
      {
        heading: 'Purchase History',
        route:   'purchase_history',
      },
      {
        heading: 'Settings',
        route:   'settings',
      }
    ]

    $scope.preview = (p, photos)->
      modalInstance = $modal.open
        templateUrl: 'preview-photo.html'
        controller: 'PreviewPhotoModalInstanceCtrl'
        size: 'lg'
        backdrop: 'static'
        windowClass: 'preview-photo'
        scope: $scope
        resolve:
          photo: ()->
            p
          photos: ()->
            photos||$scope.photos
          w: ()->
            {
            width: $(window).width()
            height: $(window).height()
            }
          type: ()->
            "my-photos"
          photo_info_url: ()->
            '/api/paid_photos'
      return false
]
