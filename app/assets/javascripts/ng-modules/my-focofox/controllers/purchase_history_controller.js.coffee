angular.module 'myFocofoxApp'
.controller 'PurchaseHistoryCtrl', [
  '$scope'
  'Payment'
  '$cookies'
  'localStorageService'
  ($scope, Payment, $cookies, localStorageService)->
    $scope.allowed_filter = ['date_time','location_name', 'photographer_name', 'price']
    $scope.thumbnail_size = parseInt($cookies.thumbnail_size) || 200
    $scope.perPage = parseInt($cookies.perPage) || 50
    $scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200]
    $scope.photos_url = '/api/payments/0/photos'
    $scope.photos = []
    $scope.filterq = {}
    $scope.payments = Payment.query (data)->
      if $scope.payments[0]
        $scope.payments[0].active = true
        $scope.photos_url = '/api/payments/'+$scope.payments[0].id+'/photos'

        $scope.payments_total_price = 0

        for payment in $scope.payments
          $scope.payments_total_price += parseFloat(payment.price)

    sort_by = localStorageService.get('sort_by')
    if _.includes($scope.allowed_filter, sort_by)
      $scope.sort_by = sort_by
    else
      $scope.sort_by = 'date_time'

    $scope.$on 'pagination:loadPage', (event, status, config) ->
      $scope.page_loaded = true
      return
      
    $scope.$watch 'sort_by', (sb)->
      if sb && $scope.filterq && _.includes($scope.allowed_filter, sb) 
        _temp = angular.copy $scope.filterq
        _temp['sort_by'] = sb

        $scope.filterq = []
        $scope.filterq = _temp
        localStorageService.set('sort_by', sb)
        false

    $scope.togglePurchaseList = () ->
      $scope.purchase_list = !$scope.purchase_list;

    $scope.select = (payment)->
      angular.forEach $scope.payments, (p)->
        if p.id == payment.id
          p.active = true
        else
          p.active = false

      $scope.photos_url = '/api/payments/'+payment.id+'/photos'
      $scope.filterq.payment_id = payment.id
    
    $scope.preview = (p, photos)->
      $scope.$parent.preview(p, photos)

    thumb_interval = `null`
    i = 0
    $(document).on 'mouseover', 'div[data-content-type^=video]', ()->
      index = $(@).data('index')
      photo = $scope.photos[index]||$scope.cart[index]
      
      thumb_interval = setInterval((->
        unless photo.video_thumbnails[i]
          i = 0
        # preload
        img = new Image()
        img.onerror = ->
          i = 0
          clearInterval thumb_interval
          
        img.onload = ->
          $('.img-'+index).attr('src', photo.video_thumbnails[i]) if ((photo.video_thumbnails[i]||"").match(/\.(jpeg|jpg|gif|png)$/) != null)
        
        img.src = photo.video_thumbnails[i]

        i +=1
        return
      ), 1000)
      return
    $(document).on 'mouseout', 'div[data-content-type^=video]', ()->
      i = 0
      clearInterval thumb_interval
      thumb_interval = `null`
]
