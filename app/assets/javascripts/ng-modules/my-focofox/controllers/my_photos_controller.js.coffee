angular.module 'myFocofoxApp'
.controller 'MyPHotosCtrl',[
  '$scope'
  '$http'
  '$modal'
  '$timeout'
  '$cookieStore'
  'localStorageService'
  '$cookies'
  'toastr'
  ($scope, $http, $modal, $timeout, $cookieStore, localStorageService, $cookies, toastr)->
    $scope.allowed_filter = ['date_time','location_name', 'photographer_name', 'price', 'order_date']
    $scope.default_download_type = localStorageService.get('download_type')
    $scope.fbAlbumId = localStorageService.get('fbAlbumId')
    $scope.fbUploadType = localStorageService.get('fbUploadType')
    $scope.dropboxUploadType = localStorageService.get('dropboxUploadType')
    $scope.thumbnail_size = parseInt($cookies.thumbnail_size) || 200
    $scope.perPage = parseInt(localStorageService.get('perPage')) || 50
    $scope.perPagePresets = [10,20,30,40,50,60,70,80,90,100,200]
    $scope.photos_url = '/api/photos/my_photos'
    $scope.photos = []
    $scope.filterq = {}
    $scope.selectedPhotos = []
    $scope.selectedPhotoIds = []

    sort_by = localStorageService.get('sort_by')

    if _.includes($scope.allowed_filter, sort_by)
      $scope.sort_by = sort_by || 'date_time'
    else
      $scope.sort_by = 'date_time'

    $scope.$on 'pagination:loadPage', (event, status, config)->
      $scope.photos_loaded = true
      $scope.result = event.targetScope.collection||{}
      $scope.photos = ($scope.result||{}).photos||[]

      _ids = angular.copy($scope.selectedPhotoIds)
      $scope.selectedPhotoIds = []
      $timeout ->
        $scope.selectedPhotoIds = _ids
      , 10

    $scope.$watch 'sort_by', (sb)->
      if sb && $scope.filterq && _.includes($scope.allowed_filter, sb)
        _temp = angular.copy $scope.filterq
        _temp['sort_by'] = sb

        $scope.filterq = []
        $scope.filterq = _temp
        localStorageService.set('sort_by', sb)
        false

    $(document).on 'change', 'select[ng-model="$parent.perPage"]', ()->
      val = $scope.perPagePresets[$(@).val()]
      $scope.updatePerPage(val) if val      

    $scope.updatePerPage = (p)->
      localStorageService.set('perPage', parseInt(p))

    $scope.$watch 'uploadToFb', (ufb)->
      if ufb && !$scope.hasFbToken
        $scope.uploadToFb = false
        $scope.getFbToken()

    $scope.$watch 'uploadToDropbox', (udp)->
      if udp && !$scope.hasDropboxToken
        $scope.uploadToDropbox = false
        $scope.getDropboxToken()

    $scope.$watch 'download_type', (dt)->
      if dt
        $scope.downloadToPc = true
        localStorageService.set('download_type', dt)

    $scope.$watch 'downloadToPc', (dtp)->
      if dtp
        $scope.download_type = $scope.default_download_type||'compressed'
      else
        $scope.download_type = ''

    $scope.$watch 'fbAlbumId', (fbaid)->
      $scope.fbAlbumId = `null` unless _.findWhere($scope.albums, {identifier: fbaid})
      localStorageService.set('fbAlbumId', fbaid)

    $scope.$watch 'fbUploadType', (fbt)->
      localStorageService.set('fbUploadType', fbt)

    $scope.$watch 'dropboxUploadType', (dut)->
      localStorageService.set('dropboxUploadType', dut)

    $scope.selectAll = ->
      $scope.selectedPhotoIds = _.pluck($scope.photos, 'id')
      $('.thumbnail').addClass('ui-selected')
      false

    $scope.clearSelection = ->
      $scope.selectedPhotos = []
      $scope.selectedPhotoIds = []
      $('.thumbnail').removeClass('ui-selected')
      false

    $scope.deleteSelected = ()->
      $http.post('/api/photos/bulk_delete_paid_photos', {photos: $scope.selectedPhotoIds}).success (res)->
        $scope.selectedPhotos = []
        $scope.selectedPhotoIds = []
        $scope.filterq = !$scope.filterq

    $scope.getFbToken = ->
      FB.login ((response) ->
        if response.status == 'connected'
          $http.post('/api/account/fb_albums', response.authResponse, {}).success (albums)->
            $scope.albums = albums
            $scope.hasFbToken = true
            $scope.uploadToFb = true
            $scope.$apply()

        return
      ), scope: 'email,user_photos,user_videos,publish_actions'

      return

    $scope.getDropboxToken = ->
      window.open($scope.dropbox_authorize_url,'Authenticate Dropbox','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0') if $scope.dropbox_authorize_url
    
    window.dropbox_callback = (folders)->
      $scope.dropbox_folders = folders
      $scope.uploadToDropbox = true
      $scope.hasDropboxToken = true
      $scope.$apply() if(!$scope.$$phase)

    $scope.$watch 'thumbnail_size', (ts)->
      $cookieStore.put('thumbnail_size', parseInt(ts))

    $scope.details = (photo)->
      modalInstance = $modal.open
        templateUrl: 'ng-modules/user-photos/views/details.html'
        controller: 'photoDetailsCtrl'
        size: 'lg'
        resolve:
          photo: ()->
            photo
          photos: ()->
            photos||$scope.photos
          w: ()->
            {
            width: $(window).width()
            height: $(window).height()
            }
          type: ()->
            "my-photos"
      return false

    $scope.valid = ->
      return false unless $scope.uploadToFb || $scope.downloadToPc || $scope.uploadToDropbox
      return false if $scope.uploadToFb && !!!$scope.fbUploadType
      return false if $scope.uploadToFb && $scope.fbUploadType=='album' && !!!$scope.fbAlbumId
      return false if $scope.uploadToFb && $scope.fbUploadType=='new_album' && !!!$scope.fbNewAlbum

      return false if $scope.uploadToDropbox && !!!$scope.dropboxUploadType
      return false if $scope.uploadToDropbox && $scope.dropboxUploadType=='folder' && !!!$scope.dropbox_folder
      return false if $scope.uploadToDropbox && $scope.dropboxUploadType=='new_folder' && !!!$scope.new_dropbox_folder
      return true

    $scope.upload = ->
      if ($scope.selectedPhotos||[]).length <=0
        toastr.warning("Click on photos to select them. Ctrl+click or drag select to choose multiple photos.", "No Photos selected") if $('.toast.toast-warning').length <=0
        return false

      params = {
        ids: _.pluck($scope.selectedPhotos, 'id')
      }

      if $scope.uploadToFb
        params['fb_album_id'] = $scope.fbAlbumId
        params['fb_album_name'] = $scope.fbNewAlbum
        params['is_new_album'] = $scope.fbUploadType=='new_album'
        params['fb_privacy'] = $scope.fb_privacy

        $http.post('/api/photos/share_to_fb', params).success (res)->
          localStorageService.set('fbAlbumId', res.new_fb_album_id) if res.new_fb_album_id
      
      if $scope.downloadToPc
        window.location.assign '/download/?photos='+JSON.stringify(params.ids)

      if $scope.uploadToDropbox
        params['path'] = $scope.dropbox_folder || $scope.new_dropbox_folder
        $http.post('/api/photos/upload_to_dropbox', params).success (res)->
          localStorageService.set('dropbox_folder', params['path']) if params['path']

      false

    $scope.preview = (p, photos)->
      $scope.$parent.preview(p, photos)

    $scope.previewFullSizedPhoto = (photo)->
      newWindow=window.open(photo.url, (photo.photo_file_name||photo.video_file_name),'toolbar=1,statusbar=0,menubar=1,type=fullWindow,fullscreen="yes",scrollbars=1,width='+photo.width+',height='+photo.height)
      newWindow.focus() if (window.focus)
      return false

    thumb_interval = `null`
    i = 0
    $(document).on 'mouseover', 'div[data-content-type^=video]', ()->
      index = $(@).data('index')
      photo = $scope.photos[index]||$scope.cart[index]
      
      thumb_interval = setInterval((->
        unless photo.video_thumbnails[i]
          i = 0
        # preload
        img = new Image()
        img.onerror = ->
          i = 0
          clearInterval thumb_interval
          
        img.onload = ->
          $('.img-'+index).attr('src', photo.video_thumbnails[i]) if ((photo.video_thumbnails[i]||"").match(/\.(jpeg|jpg|gif|png)$/) != null)
        
        img.src = photo.video_thumbnails[i]

        i +=1
        return
      ), 1000)
      return
    $(document).on 'mouseout', 'div[data-content-type^=video]', ()->
      i = 0
      clearInterval thumb_interval
      thumb_interval = `null`

    $scope.$watch 'filterSelect', (f)->
      switch f
        when 'none'
          $scope.selectedPhotoIds = []
        when 'visible'
          $scope.selectedPhotoIds = _.pluck($scope.photos, 'id')
        when 'all'
          $scope.selectedPhotoIds = $scope.result.all_photo_ids

    $scope.$watch 'selectedPhotoIds', (ids)->
      $scope.selectedPhotos = []
      $('.ui-selected').removeClass('ui-selected')
      angular.forEach ids, (id)->
        $scope.selectedPhotos.push _.findWhere $scope.photos, {id: id}
        $('[data-id="'+id+'"]').addClass('ui-selected')

]