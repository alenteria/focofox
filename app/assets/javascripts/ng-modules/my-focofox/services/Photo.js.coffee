angular.module 'myFocofoxApp'
.factory 'Photo', [
  '$resource'
  ($resource)-> 
    $resource('/api/photos/')
]