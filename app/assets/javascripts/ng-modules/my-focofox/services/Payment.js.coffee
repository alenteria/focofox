angular.module 'myFocofoxApp'
.factory 'Payment', [
  '$resource'
  ($resource)-> 
    $resource('/api/payments/')
]