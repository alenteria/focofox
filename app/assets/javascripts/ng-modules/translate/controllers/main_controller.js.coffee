'use strict'
angular.module 'translateApp'
.controller 'mainController', [
  '$scope'
  '$http'
  '$modal'
  ($scope, $http, $modal)->
    $scope.edit = (key,i)->
      $http.get('/api/translations?key='+key).success (res)->
        modalInstance = $modal.open
          templateUrl: "ng-modules/translate/views/index.html"
          size: 'lg'
          controller: 'formCtrl'
          resolve:
            data: ->
              res

        modalInstance.result.then (data)->
          $('[data-content-key="'+key+'"]').text(data.content)
      return

    return
]