'use strict'
angular.module 'translateApp'
.controller 'formCtrl', [
  '$scope'
  '$http'
  '$modal'
  '$modalInstance'
  'data'
  ($scope, $http, $modal, $modalInstance, data)->
    $scope.data = data

    $scope.cancel = ->
      $modalInstance.dismiss('cancel')

    $scope.save = ->
      $http.post('/api/translations/', data).success (res)->
        $modalInstance.close data
]