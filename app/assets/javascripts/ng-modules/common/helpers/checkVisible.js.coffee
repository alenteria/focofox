window.isVisible = (elm, evalType) ->
  evalType = evalType or 'visible'
  vpH = $(window).height()
  st = $(window).scrollTop()
  y = $(elm).offset().top
  elementHeight = $(elm).height()
  if evalType == 'visible'
    return y < vpH + st and y > st - elementHeight
  if evalType == 'above'
    return y < vpH + st
  return