$(document).ready(function() {
  $.ajaxSetup({ cache: true });
  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
    FB.init({
      appId      : $('[data-fb-app-id]').data('fb-app-id'),
      appSecret      : $('[data-fb-app-secret]').data('fb-app-secret'),
    })    
    $('#loginbutton,#feedbutton').removeAttr('disabled');
    FB.getLoginStatus(function(data){
    	// console.log(data);
    });
  });
});