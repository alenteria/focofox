window.formatAMPM = (date) ->
  hours = date.getHours()
  minutes = date.getMinutes()
  ampm = if hours >= 12 then 'pm' else 'am'
  hours = hours % 12
  hours = if hours then hours else 12
  # the hour '0' should be '12'
  minutes = if minutes < 10 then '0' + minutes else minutes
  strTime = hours + ':' + minutes + ' ' + ampm
  strTime

$(document).ready ()->
  try
    $('input.time').timepicker({
      timeFormat: "hh:mm tt"
      onSelect: (e, t) ->
        if !t.clicks
          t.clicks = 0
        if ++t.clicks == 2
          $('[data-handler="hide"]').click()
          t.inline = false
          t.clicks = 0
        setTimeout (->
          t.clicks = 0
          return
        ), 500
    })

    $('input.date').datetimepicker({
      dateFormat: "m/d/y"
      timeFormat: ""
      onSelect: (e, t) ->
        if !t.clicks
          t.clicks = 0
        if ++t.clicks == 2
          $('[data-handler="hide"]').click()
          t.inline = false
          t.clicks = 0
        setTimeout (->
          t.clicks = 0
          return
        ), 500
    })
    $('input.datetime').datetimepicker({
      dateFormat: "m/d/yy"
      separator: ' '
      timeFormat: "h:mm tt"
      onSelect: (e, t) ->
        if !t.clicks
          t.clicks = 0
        if ++t.clicks == 2
          $('[data-handler="hide"]').click()
          t.inline = false
          t.clicks = 0
        setTimeout (->
          t.clicks = 0
          return
        ), 500
    })
  catch
    # 
$(document).on 'mouseover', 'input.datetime', ->
  $(this).datetimepicker({
    dateFormat: "m/d/yy"
    separator: ' '
    timeFormat: "h:mm tt"
    onSelect: (e, t) ->
      if !t.clicks
        t.clicks = 0
      if ++t.clicks == 2
        $('[data-handler="hide"]').click()
        t.inline = false
        t.clicks = 0
      setTimeout (->
        t.clicks = 0
        return
      ), 500
  })

$(document).on 'mouseover', 'input.time', ->
  $(this).timepicker({
    timeFormat: "hh:mm tt"
    onSelect: (e, t) ->
      if !t.clicks
        t.clicks = 0
      if ++t.clicks == 2
        $('[data-handler="hide"]').click()
        t.inline = false
        t.clicks = 0
      setTimeout (->
        t.clicks = 0
        return
      ), 500
  })

$(document).on 'mouseover', 'input.date', ->
  $(this).datetimepicker({
    dateFormat: "m/d/y"
    timeFormat: ""
    onSelect: (e, t) ->
      if !t.clicks
        t.clicks = 0
      if ++t.clicks == 2
        $('[data-handler="hide"]').click()
        t.inline = false
        t.clicks = 0
      setTimeout (->
        t.clicks = 0
        return
      ), 500
  })
