angular.module('common')
.controller 'uploadConfirmationModalInstanceCtrl',[
  '$scope'
  "$modalInstance"
  ($scope, $modalInstance)->
    upload_scope = angular.element('#upload_progress [ng-controller]').scope()
    upload_scope.file = upload_scope.file||$scope.queue[0]
    upload_scope.parent_scope = $scope
    
    $scope.uploadAll = ->
      $scope.submit()
      $scope.$parent.upload_started = true
      $modalInstance.dismiss('cancel')

      #upload_scope.showDialog() unless $('.upload-progress').is(':visible')
      #upload_scope.$apply() if !upload_scope.$$phase

    $scope.uploadSelected = ->
      $scope.submit($scope.selectedPhotos)
      $scope.$parent.upload_started = true
      $modalInstance.dismiss('cancel')

      #upload_scope.showDialog() unless $('.upload-progress').is(':visible')
      #upload_scope.$apply() if !upload_scope.$$phase

    $scope.cancel = ->
      $modalInstance.dismiss('cancel')
]