url = '/api/watermarks'
angular.module 'common'
.controller 'chooseWatermarkModalInstanceCtrl',[
	'$scope'
	'$modalInstance'
	'watermark'
	'$http'
	'$modal'
	'cfpLoadingBar'
	($scope, $modalInstance, watermark, $http, $modal, cfpLoadingBar)->
		$scope.watermark = watermark
		$(document).on 'change', '[name="watermark_image"]', (e)->
			e.preventDefault()
			cfpLoadingBar.start()
			# $scope = angular.element('#preferences').scope()
			file = e.target.files[0]
			loadImage(
		      file,
		      (img)->
		      	cfpLoadingBar.complete()
		      	$scope.$apply ()->
		      		$scope.watermark.watermark_image = {file: file, previewUrl: img.src}
		      		$modalInstance.dismiss('cancel')
		      	return
		      ,
		      {maxWidth: 900, noRevoke: true, canvas: false}
		  );
			return

		$scope.chooseFromImages = ()->
			$modal.open
				size: 'lg'
				templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_images.html'
				controller: 'watermarkImagesModalInstanceCtrl'
				resolve:
					watermark: ()->
						$scope.watermark
			$modalInstance.close			
			$modalInstance.dismiss('cancel')
		$scope.cancel = ()->
			$modalInstance.dismiss('cancel')
]