url = '/api/locations'
angular.module 'common'
.controller 'locationModalInstanceCtrl',[
  '$rootScope'
  '$scope'
  '$modalInstance'
  'location'
  'gps'
  '$http'
  '$timeout'
  'localStorageService'
  'notify_success'
  'parent'
  ($rootScope, $scope, $modalInstance, location, gps, $http, $timeout, localStorageService, notify_success, parent)->
    $scope.locations = parent.locations||[]

    $scope.map_settings = localStorageService.get('map_settings')
  
    custom_styles_default = $('#custom-styles').text()
    $('#custom-styles').text(custom_styles_default+'#map{height:'+($(window).height()*0.50)+'px !important;}')

    $scope.location = location || {}

    if location && location.latitude && location.longitude
      $scope.location.gps = {latitude: location.latitude, longitude: location.longitude}
    else if ($scope.map_settings||{}).center
      $scope.location.gps = {latitude: $scope.map_settings.center.lat, longitude: $scope.map_settings.center.lng}
    else if gps
      $scope.location.gps = gps
    else
      window.navigator.geolocation.getCurrentPosition ((position) ->
        latitude = position.coords.latitude
        longitude = position.coords.longitude
        $scope.location.gps = {latitude: latitude, longitude: longitude}
      )

    $scope.useExistingLocation = (existing)->
      _location = _.findWhere($scope.locations, {id: existing.id})
      parent.setting.location = _location
      parent.updateData('location', parent.setting.location)
      parent.updateData('location_id', parent.setting.location.id)
      $scope.cancel()

    $scope.ok = (type)->
      switch type
        when 'create'
          $http.post(url, $scope.location, {notify_success: notify_success||false}).success (res)->
            if res.status == 'ok'
              $('#custom-styles').text(custom_styles_default)
              $modalInstance.close res
        when 'update'
          $http.put(url+'/'+$scope.location.id, $scope.location, {notify_success: notify_success||false}).success (res)->
            if res.status == 'ok'
              $('#custom-styles').text(custom_styles_default)
              $modalInstance.close res
    $scope.pick = ->
      $modalInstance.close $scope.location

    $scope.cancel = ()->
      $('#custom-styles').text(custom_styles_default)
      $modalInstance.dismiss('cancel')
    
    $scope.getLocationInfo = (lat, lon, callback) ->
      latlng = new (google.maps.LatLng)(lat, lon)
      $scope.geocoder.geocode { 'latLng': latlng }, (results, status) ->
        if status == google.maps.GeocoderStatus.OK
          for i of results
            `i = i`
            if results[i].types && results[i].types.indexOf('political') != -1
              if $scope.map.zoom <= 6
                if results[i].types.indexOf('country') != -1
                  callback results[i]
                  break
              else
                callback results[i]
                break
        return
      return

    $scope.$watchCollection '[center, zoom]', (settings)->
      if settings[0] && settings[1]
        $scope.map_settings = {}
        _center = {lat: settings[0].lat(), lng: settings[0].lng()}
        $scope.map_settings.zoom = settings[1]
        $scope.map_settings.center = _center#new google.maps.LatLng(parseFloat(settings[0].A), parseFloat(settings[0].F))
        localStorageService.set('map_settings', $scope.map_settings)

    $scope.initMap = ()->
      console.log 'initializing map ...'
      $scope.title = null
      $scope.lat = ($scope.location.gps || {}).latitude
      $scope.lon = ($scope.location.gps || {}).longitude
      $scope.country_selection = false
      $scope.zoomChangeRemarkerTimeout = null
      $scope.positionChangeRemarkerTimeout = null
      mapElement = document.getElementById('map')
      unless mapElement
        e = document.createElement('div')
        e.id = 'map'
        $('#location-form').append(e)
        mapElement = e

      $scope.map = new (google.maps.Map)(mapElement,
        zoom: ($scope.location||{}).zoom || ($scope.map_settings||{}).zoom || 9
        center: (new (google.maps.LatLng)($scope.lat, $scope.lon))
        mapTypeId: google.maps.MapTypeId.TERRAIN
        disableDefaultUI: true
        zoomControl: true
        zoomControlOptions: style: google.maps.ZoomControlStyle.SMALL)

      $scope.overlay = new google.maps.OverlayView()
      $scope.overlay.draw = ()->
      $scope.overlay.setMap($scope.map)

      $scope.center_marker = null
      $scope.geocoder = new (google.maps.Geocoder)
      $scope.infoBox = new InfoBox(
        disableAutoPan: false
        pixelOffset: new (google.maps.Size)(-125, 25)
        zIndex: null
        boxStyle: width: '200px'
        infoBoxClearance: new (google.maps.Size)(1, 1)
        isHidden: true
        pane: 'floatPane'
        enableEventPropagation: false)

      $scope.createMarker = (data) ->
        if $scope.marker
          $scope.cleanupMarker()
        $scope.marker = new (google.maps.Marker)(
          map: $scope.map
          icon: '/map-marker.png'
          position: new (google.maps.LatLng)(data.lat, data.lon)
          title: data.title
          draggable: true
          animation: google.maps.Animation.DROP)
        
        $timeout (->
          if typeof data['title'] != 'undefined'
            $scope.showInfoBox data
          else
            $scope.getLocationInfo data.lat, data.lon, (info) ->
              $scope.showInfoBox info
              return
          return
        ), 500
        
        google.maps.event.addListener $scope.map, 'dragend', (e)->
          $scope.center = $scope.map.getCenter()
          $scope.zoom = $scope.map.getZoom()
          
        google.maps.event.addListener $scope.marker, 'dragend', (data)->
          lat = $scope.marker.getPosition().lat()
          long = $scope.marker.getPosition().lng()
          $scope.location.gps = {latitude: lat, longitude: long}
        
          $scope.center = $scope.map.getCenter()
          $scope.zoom = $scope.map.getZoom()

        return

      $scope.showInfoBox = (info) ->
        $scope.infoBox.setContent '<a href="#" class="selectCityButton">' + info.formatted_address + '</a>'
        $scope.infoBox.open $scope.map, $scope.marker
        $scope.infoBox.show()
        return

      $scope.cleanupMarker = ->
        $scope.marker.setMap null
        $scope.infoBox.hide()
        return

      google.maps.event.addListener $scope.map, 'zoom_changed', ->
        $scope.center = $scope.map.getCenter()
        $scope.zoom = $scope.map.getZoom()
        $scope.country_selection = $scope.map.zoom <= 6
        $scope.location.zoom = $scope.map.getZoom()
        return

      $scope.autocomplete = new google.maps.places.Autocomplete((document.getElementById('search_field')),{ types: ['geocode'] })
       
      google.maps.event.addListener $scope.autocomplete, 'place_changed', ->
        place = $scope.autocomplete.getPlace()
        if place.geometry
          $scope.country_selection = place.types.indexOf('country') != -1
          $scope.map.panTo place.geometry.location
          $scope.map.setZoom if $scope.country_selection then 5 else 10
          $scope.location.info = place
          $scope.location.gps = {latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng()}
          $scope.createMarker
            title: place.title
            lat: place.geometry.location.lat()
            lon: place.geometry.location.lng()

          $scope.center = $scope.map.getCenter()
          $scope.zoom = $scope.map.getZoom()
        return
     
      # Initial point
      $scope.createMarker
        lat: $scope.lat
        lon: $scope.lon

      $scope.$watch 'location.gps.latitude', ()->
        
        $scope.getLocationInfo $scope.location.gps.latitude, $scope.location.gps.longitude, (res)->
          $scope.location.info = res
          $scope.showInfoBox($scope.location.info)
          $scope.$digest()

        $scope.createMarker
          lat: $scope.location.gps.latitude
          lon: $scope.location.gps.longitude

      $scope.$watch 'location.gps.longitude', ()->
        $scope.getLocationInfo $scope.location.gps.latitude, $scope.location.gps.longitude, (res)->
          $scope.location.info = res
          $scope.showInfoBox($scope.location.info)
          $scope.$digest()

        $scope.createMarker
          lat: $scope.location.gps.latitude
          lon: $scope.location.gps.longitude

      $scope.$watch 'location.info', (data)->
        setTimeout ()->
          google.maps.event.trigger($scope.map, 'resize')
          $scope.map.setZoom( $scope.map.getZoom()-1 )
          $scope.map.setZoom( $scope.map.getZoom()+1 )
        , 100

        $scope.location.zoom = $scope.map.getZoom()
        if data
          $scope.location.city = ''
          $scope.location.province = ''
          
          try
            province = _.filter data.address_components, (d) ->
              a = _.contains(d.types, 'administrative_area_level_2')
              a
            $scope.location.province = province[0].long_name
          catch e
            # ...

          try
            city = _.filter data.address_components, (d) ->
              a = _.contains(d.types, 'administrative_area_level_1')
              a
            $scope.location.city = city[0].long_name
          catch e
            # ...
          
          try
            city = _.filter data.address_components, (d) ->
              a = _.contains(d.types, 'locality')
              a
            $scope.location.city = city[0].long_name
          catch e
            # ...

          try
            country = _.filter data.address_components, (d) ->
              a = _.contains(d.types, 'country')
              a
            $scope.location.country = country[0].long_name
          catch
            #

          if !!!$scope.location.id
            $scope.location.name = data.formatted_address
    
    $scope.delete= ()->
      if confirm('Are you sure you want proceed on this action?')
        $http.delete(url+'/'+$scope.location.id, {notify_success: notify_success||false}).success (res)->
          $modalInstance.close res

    $(document).on 'mouseenter', '#draggable-marker', (e) ->
      item = $(this)
      #check if the item is already draggable
      item.draggable
        helper: 'clone'
        stop: (e, ui)->
          mOffset=$($scope.map.getDiv()).offset()
          point=new google.maps.Point(
            ui.offset.left-mOffset.left+(ui.helper.width()/2),
            ui.offset.top-mOffset.top+(ui.helper.height())
          )
          ll=$scope.overlay.getProjection().fromContainerPixelToLatLng(point)
          $scope.marker.setPosition(ll)
          lat = $scope.marker.getPosition().lat()
          long = $scope.marker.getPosition().lng()
          $scope.location.gps = {latitude: lat, longitude: long}
          $scope.$digest()
      return
    return
]
