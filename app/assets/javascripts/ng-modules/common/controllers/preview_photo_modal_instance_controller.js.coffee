'use strict'
angular.module 'common'
.controller 'PreviewPhotoModalInstanceCtrl', [
  'Auth'
  "$rootScope"
  "$scope"
  "$modal"
  "$modalInstance"
  "photo"
  "photos"
  "w"
  "type"
  'cfpLoadingBar'
  '$sce'
  '$http'
  'photo_info_url'
  '$timeout'
  (Auth, $rootScope, $scope, $modal, $modalInstance, photo, photos, w, type, cfpLoadingBar, $sce, $http, photo_info_url, $timeout)->      
    
    default_custom_styles = $('#custom-styles').text()
    $scope.type = type
    $scope.w = w
    $scope.previewHeight = parseInt(w.height-208)
    $scope.modalHeight = $scope.w.height
    $scope.photos = photos

    $scope.photo = photo
    $scope.content_type = if !!photo.type then photo.type else (photo.photo_content_type||photo.video_content_type)

    $scope.loadAdditionalInfo = ->
      photo_info_url = photo_info_url || '/api/photos'
      $http.get(photo_info_url+'/'+photo.id+'/photo_info').success (obj)->
        $scope.photo.location_name = obj.location_name
        $scope.photo.photographer_name = obj.photographer_name
        $scope.photo.group_name = obj.group_name
        $scope.photo.license_name = obj.license_name

    $scope.currentIndex = _.indexOf $scope.photos, $scope.photo 
    
    preload_preview_next_photos = (i)->
      if $scope.photos[i-1]
        img1 = new Image()
        photo = $scope.photos[i-1] 
        photo.preview_url = (photo.url||photo.preview_url)
        img1.src = photo.preview_url

      if $scope.photos[i+1]  
        img2 = new Image()
        photo = $scope.photos[i+1] 
        photo.preview_url = (photo.url||photo.preview_url)
        img2.src = photo.preview_url

    preload_preview_next_videos = (i)->


    $scope.activate = (e)->
      $timeout ()->
        $('#preview').focus().click().css({'max-width': '100%'})
      ,
        100

      false

    $scope.zoomIn = ->
      $scope.previewHeight +=20
      $scope.modalHeight  +=20
      $scope.is_zoomout = false

    $scope.zoomOut = ($event)->
      $event.preventDefault()
      if $scope.modalHeight > $scope.w.height  
        $scope.previewHeight -=20
        $scope.modalHeight  -=20
        $scope.is_zoomout = true
      false

    $scope.$watch 'previewHeight', (ph)->
      min_height = if $scope.photo.height < ph then 0 else ph
      styles = default_custom_styles+'.modal-content{
                max-height: '+$scope.modalHeight+'px !important;
              }
              #preview{
                min-height: '+min_height+'px !important;
                max-height: '+ph+'px !important;
                cursor:'+(if $scope.is_zoomout then '-webkit-zoom-out;' else '-webkit-zoom-in;')+'
              }'
      if type != 'my-photos'
        styles += '#preview{
                  height: '+ph+'px;
                }'

      $('#custom-styles').text(styles)

    $scope.$watch 'photo', (p)->
      i = _.indexOf($scope.photos, p)
      $scope.content_type = if !!p.type then p.type else (p.photo_content_type||p.video_content_type)
      unless ($scope.content_type||[]).indexOf('image') >= 0
        p.is_video = true
        p.is_paused = false
        p.is_play = true
      else
        p.is_photo = true

      $scope.loadAdditionalInfo() if type=='my-photos'
      
      pv_params = {
        user_id: user_id
        photographer_id: p.user_id
        photo_id: p.id
        latitude: (window.current_user_location||{}).latitude
        longitude: (window.current_user_location||{}).longitude
      }
      
      if p.preview_url && parseInt(user_id) != p.user_id
        pv_params['view_type'] = 'preview'
        $http.post('/api/photo_views/add', pv_params).success (data)->
      else if p.url && parseInt(user_id) != p.user_id
        pv_params['view_type'] = 'original'
        $http.post('/api/photo_views/add', pv_params).success (data)->

      if type=='my-photos' && p.is_photo
        cfpLoadingBar.start()
        $scope.photo.previewFullPhotoUrl = (p.url||p.preview_url)
        p.preview_url = (p.url||p.preview_url)
        img = new Image()
        img.src = p.preview_url
        img.onload = ()->
          cfpLoadingBar.complete()
        img.onerror = ()->
          cfpLoadingBar.complete()

        preload_preview_next_photos(i)

      else if type == 'my-photos' && p.is_video
        p.preview_url = (p.url||p.preview_url)
        cfpLoadingBar.start()

        cfpLoadingBar.complete()

        preload_preview_next_videos(i)
      else 
        if p.is_photo
          cfpLoadingBar.start()
          loadImage.parseMetaData p, (data)->
            orientation = if data.exif then data.exif.get('Orientation')
            loadImage p, ((img, e) ->
              p.previewFullPhotoUrl = img
              $('#preview').html('')
              $('#preview').append(img)
              p.width = img.width
              p.height = img.height
              p.orientation_type = if img.width>img.height then 'landscape' else 'portrait'
              cfpLoadingBar.complete()
            ),
              orientation: orientation
              canvas: true
              noRevoke: true
              previewMaxHeight: $scope.previewHeight

        else if p.is_video
          $('#preview').html("<video src='"+(p.preview||{}).src+"' controls></video>")
          cfpLoadingBar.complete()
                
        if(!$scope.$$phase)
          $scope.$digest()
    $scope.play = ->
      $('video#preview:first')[0].play()
      $scope.photo.is_play = true
      $scope.photo.is_paused = false

    $scope.pause = ->
      $('video#preview:first')[0].pause()
      $scope.photo.is_play = false
      $scope.photo.is_paused = true

    $scope.fullscreen = ->
      elem = $('video#preview')[0]
      if elem.requestFullscreen
        elem.requestFullscreen()
      else if elem.msRequestFullscreen
        elem.msRequestFullscreen()
      else if elem.mozRequestFullScreen
        elem.mozRequestFullScreen()
      else if elem.webkitRequestFullscreen
        elem.webkitRequestFullscreen()
    $scope.next = ()->
      $scope.direction = 'left'
      $scope.currentIndex = $scope.currentIndex+1
      $scope.photo = $scope.photos[$scope.currentIndex]
      unless $scope.photo
        $scope.currentIndex = 0
        $scope.photo = $scope.photos[$scope.currentIndex]

    $scope.prev = ()->
      $scope.direction = 'right'
      $scope.currentIndex = $scope.currentIndex-1
      $scope.photo = $scope.photos[$scope.currentIndex]
      unless $scope.photo
      	$scope.currentIndex = $scope.photos.length-1
      	$scope.photo = $scope.photos[$scope.currentIndex]

    $scope.previewFullSizedPhoto = (photo)->
      if typeof(photo.previewFullPhotoUrl)=='string'
        newwindow=window.open(photo.previewFullPhotoUrl, (photo.photo_file_name||photo.video_file_name),'toolbar=1,statusbar=0,menubar=1,type=fullWindow,fullscreen="yes",scrollbars=1,width='+photo.width+',height='+photo.height)
        newwindow.focus() if (window.focus)
      else
        previewWindow = window.open('/empty', (photo.photo_file_name||photo.video_file_name),'toolbar=1,statusbar=0,menubar=1,type=fullWindow,fullscreen="yes",scrollbars=1,width='+photo.width+',height='+photo.height)
        previewWindow.onload = ()->
          $(previewWindow.document.getElementsByTagName('body')).html photo.previewFullPhotoUrl
          $(previewWindow.document.getElementsByTagName('title')).text (photo.photo_file_name||photo.video_file_name)
          preview = $(previewWindow.document.getElementsByTagName('canvas'))
          $(preview).attr('oncontextmenu', 'return false')
          $(preview).mousedown (event) ->
            event.preventDefault()
            switch event.which
              when 1
                # 'Left Mouse button pressed.'
                $(previewWindow.document.getElementsByTagName('html')).css('cursor', 'zoom-in')
                zoom = parseFloat(preview.css('zoom'))
                preview.css('zoom', zoom+0.1)
              when 2
                # 'Middle Mouse button pressed.'
                $(previewWindow.document.getElementsByTagName('html')).css('cursor', 'zoom-in')
                zoom = parseFloat(preview.css('zoom'))
                preview.css('zoom', zoom+0.1)
              when 3
                # 'Right Mouse button pressed.'
                $(previewWindow.document.getElementsByTagName('html')).css('cursor', 'zoom-out')
                zoom = parseFloat(preview.css('zoom'))
                preview.css('zoom', zoom-0.1)
                return false     
            false

      return false
    $scope.goto = (i)->
    	$scope.currentIndex = i
    	$scope.photo = $scope.photos[i]

    $scope.cancel = ()->
      $('#custom-styles').text(default_custom_styles)
      $modalInstance.dismiss('cancel')

    $scope.removeFromCart = (photo)->
      $scope.$parent.removeFromCart(photo)
      $modalInstance.dismiss('cancel')

    leftKeyCode = 37
    rightKeyCode = 39
    escKeyCode = 27
    $(document).on 'keydown', (e)->
      # try
      if e.keyCode == leftKeyCode
        $scope.prev()
      else if e.keyCode == rightKeyCode
        $scope.next()
      else if e.keyCode == escKeyCode
        $scope.cancel()

      $scope.$apply() if(!$scope.$$phase)
      
    buyWarningDialog = (photo)->
      modalInstance = $modal.open
        template: '<div class="modal-body">
                      <p>
                        <i class="fa fa-warning">&nbsp;</i>
                        You must purchase this photo to add it to your social media account.
                        <span ng-if="!is_inCart(photo)">Add this photo to your shopping cart?</span>
                      </p>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-primary" ng-if="!is_inCart(photo)" ng-click="buy(photo)"><i class="fa fa-cart-plus">&nbsp;</i>Buy</button>
                    <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
                    </div>'
        controller: 'buyPhotoWarningModalInstanceCtrl'
        scope: $scope
        size: 'md'

    $scope.shareFB = (photo)->
      unless parseInt(photo.price)<=0 #|| is_purchased
        buyWarningDialog(photo)
      else 
        caption = ''
        angular.forEach photo.tags, (t)->
          caption += t.name+' | '
        FB.ui
          method: 'feed'
          name: photo.attachment_file_name
          link: photo.attachment_absolute_url
          picture: photo.attachment_absolute_url
          source: ''
          caption: caption
          description: ''
          message: ''
    $scope.pinIt = (photo, e)->
      unless parseInt(photo.price)<=0 #|| is_purchased
        buyWarningDialog(photo)
      else
        window.open($(e.currentTarget).data('url'))

    return
]


