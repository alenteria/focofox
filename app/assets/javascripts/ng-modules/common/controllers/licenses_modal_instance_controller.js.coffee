angular.module('common')
.controller 'licenseModalInstanceCtrl',[
  '$scope'
  'license'
  '$http'
  '$modalInstance'
  ($scope, license, $http, $modalInstance)->
    $scope.license = license
    $(document).on 'change', '[name="attachment"]', (e)->
      e.preventDefault()
      $scope = angular.element('#preferences').scope()
      $scope.attachment = e.target.files[0]
      if !$scope.$$phase
        $scope.$apply()
      return

    $scope.ok = (type)->
      attr = ['title','description', 'attachment']
      switch type
        when 'create'
          fd = new FormData()
          if !!$scope.attachment
            fd.append 'attachment', $scope.attachment

          for property of $scope.license
            if !!$scope.license[property] && $.inArray(property, attr) >=0
              fd.append property, $scope.license[property]

          $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': `undefined`}
          }).success (res)->
              $modalInstance.close res

        when 'update'
          fd = new FormData()
          if !!$scope.attachment
            fd.append 'attachment', $scope.attachment
          
          for property of $scope.license
            if !!$scope.license[property] && $.inArray(property, attr) >=0
              fd.append property, $scope.license[property]

          $http.put(url+'/'+$scope.license.id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': `undefined`}
          }).success (res)->
            $modalInstance.close res
    
    $scope.delete = ()->
      if confirm('Are you sure you want proceed on this action?')
        $http.delete(url+'/'+$scope.license.id).success (res)->
          if res.status.toLocaleLowerCase()=='ok'
            $modalInstance.close {action: 'delete'}

    $scope.cancel = ()->
      $modalInstance.dismiss('cancel')
]