# Preload Background Images
bg_url = '/img'
bgs = ['watermark_sample_bg_1.jpg', 'watermark_sample_bg_2.jpg', 'watermark_sample_bg_3.jpg', 'watermark_sample_bg_4.jpg']
$(document).ready ()->
	angular.forEach bgs, (bg)->
		img=new Image()
		img.src=bg_url + '/'+bg


url = '/api/watermarks'
angular.module 'common'
.controller 'watermarkModalInstanceCtrl',[
	'$scope'
	'$modalInstance'
	'watermark'
	'$http'
	'$modal'
	'localStorageService'
	'cfpLoadingBar'
	'notify_success'
	'$timeout'
	($scope, $modalInstance, watermark, $http, $modal, localStorageService, cfpLoadingBar, notify_success, $timeout)->
		_untouched_watermark = angular.copy(watermark)
		$scope.watermark = angular.copy(watermark)
		$scope.default_modal_width = $scope.default_modal_width||$('.modal-dialog').width()
		$scope.update_styles = ()->
			$('#custom-styles').text('/**/')
			if ($scope.watermark||{}).watermark_image
				switch $scope.watermark.display_type
					when 'tiled'
						styles = '.tiled-display:before{position: absolute;
						width: 200%;
						height: 200%;
						top: -50%;
						left: -50%;
						content: "";
						background-repeat: space;
						-webkit-transform: rotate('+$scope.watermark.rotation+'deg);
						-moz-transform: rotate('+$scope.watermark.rotation+'deg);
						-ms-transform: rotate('+$scope.watermark.rotation+'deg);
						-o-transform: rotate('+$scope.watermark.rotation+'deg);
						transform: rotate('+$scope.watermark.rotation+'deg);
						background-image: url('+(($scope.watermark||{}).watermark_image||{}).previewUrl+');
						background-size: '+$scope.watermark.tiled_size+'px;
						overflow: hidden;}
						'
						$('#custom-styles').text(styles)
					
					when 'positioned'
						$scope.update_positioned_styles()

			$scope.$digest() if !$scope.$$phase
			return

		setTimeout ->
			$scope.update_styles()
		, 
			100
			
		$scope.update_positioned_styles = ->
			$('#custom-styles').text("")
			return false unless $scope.watermark.position
			vertical_align = $scope.watermark.position.split(" ")[0]
			if vertical_align == 'center'	
				img = new Image()
				img.src = (($scope.watermark||{}).watermark_image||{}).previewUrl
				img.onload = ->
					wh = $('.positioned-display img').height()
					bgh = $('.positioned-display').height()

					if vertical_align == 'center'
						styles = ".preview.positioned-display img{
									top: "+(bgh/2-(wh/2))+"px !important;
								}
							"
						$('#custom-styles').text(styles)
				false

		calculate_image_size = ()->
			setTimeout ()->
				$scope.watermark.base_image_width = $('.preview').width()
				$scope.watermark.base_image_height = $('.preview').height()
			,	
				1000
		
		if !!!($scope.watermark||{}).watermark_image && ($scope.watermark||{}).name !='none'
			$http.get(url+'/last_used_watermark', {notify: false}).success (data)->
				loadImage(
					data.photo_url,
					(img)->
						$scope.watermark.display_type = data.display_type
						$scope.watermark.tiled_size = data.tiled_size
						$scope.watermark.position = data.position
						$scope.watermark.positioned_size = data.positioned_size
						$scope.watermark.rotation = data.rotation
						$scope.watermark.stretched_size = data.stretched_size
						$scope.watermark.opacity = data.opacity
						$scope.watermark.watermark_image = {previewUrl: img.src}
						$scope.watermark.image_asset_id = data.image_asset_id
						$('#watermarked-image-preview').click()
					,
					{maxWidth: 900, noRevoke: true, canvas: false}
			  )
		$scope.update_styles()

		$scope.$watch 'watermark.tiled_size', (w)->
			$scope.update_styles()
			return

		$scope.$watch 'watermark.display_type', (w)->
			$scope.update_styles()
			return

		$scope.$watch 'watermark.watermark_image', (w)->
			$scope.update_styles()
			return	

		$scope.$watch 'watermark.position', ->
			$scope.update_positioned_styles()
			return

		$scope.$watch 'watermark.positioned_size', ->
			$scope.update_positioned_styles()
			return

		$scope.bgI = 0

		$scope.watermark.background_sample_image = {previewUrl:  localStorageService.get('sample_bg') || (bg_url + '/' + bgs[$scope.bgI])}	

		$scope.$watch 'watermark.tile_size', (ts)->

		calculate_image_size()
		$scope.changeDefaultBackground = ()->
			calculate_image_size()
			$scope.bgI = $scope.bgI + 1
			if $scope.bgI > 3
					$scope.bgI = 0

			path = bg_url + '/' + bgs[$scope.bgI]
			$scope.watermark.background_sample_image = {previewUrl:  path}
			localStorageService.set 'sample_bg', path

		$scope.$watch 'watermark.background_sample_image', (obj)->
			# return false
			$timeout ->
				$('.modal-dialog').width($scope.default_modal_width)

				$('#watermarked-image-preview').css({'padding-left': 0, 'padding-right': 0})
				path = obj.previewUrl
				img = new Image()
				img.src = path
				img.onload = ->
					i = 0
					exec_loop1 = true
					exec_loop2 = true
					while $('#image-background-sample').height() > $(window).height()-200 && exec_loop1
						exec_loop2 = false

						$('#watermarked-image-preview').css({'padding-left': i, 'padding-right': i})
						i += 10

						unless $('#image-background-sample').height() > $(window).height()-200
							exec_loop1 = false
							console.log 'exec loop1'
							while $('#image-background-sample').height() < $(window).height()-200
								console.log 'exec loop21'
								h = $('.modal-dialog').width()
								$('.modal-dialog').width(h+10)
					
					while exec_loop2 && $('#image-background-sample').height() < $(window).height()-200
						console.log 'exec loop22'
						h = $('.modal-dialog').width()
						$('.modal-dialog').width(h+10)

					$scope.update_styles()
			, 10
			return

		$scope.preview = ->
			return false
			cfpLoadingBar.start()
			html2canvas $('#watermarked-image-preview center'), onrendered: (canvas) ->
				# canvas is the final rendered <canvas> element
				myImage = canvas.toDataURL('image/png')
				newwindow=window.open(myImage, $scope.watermark.name, 'toolbar=1,statusbar=0,menubar=1,type=fullWindow,fullscreen="yes",scrollbars=1,width='+canvas.width+',height='+canvas.height)
				newwindow.focus() if (window.focus)
				cfpLoadingBar.complete()
				false
			false

		$scope.rotateLeft = ()->
			$scope.setAsNew()	if $scope.watermark.user_id == null
			$scope.watermark.rotation = parseInt($scope.watermark.rotation) - 3
			$scope.update_styles()
			return

		$scope.rotateRight = ()->
			$scope.setAsNew()	if $scope.watermark.user_id == null
			$scope.watermark.rotation = parseInt($scope.watermark.rotation) + 3
			$scope.update_styles()
			return

		$(document).on 'focus', '[name="watermark_form"] input', ->
			$scope.setAsNew()	if $scope.watermark.user_id == null

		$scope.setAsNew = ->
			$scope.watermark.id = null
			$scope.watermark.name = null
			$scope.watermark.user_id = user_id

		$scope.ok = (type)->
			switch type
			  when "create"
			  	fd = new FormData()
			  	fd.append 'file', $scope.watermark.watermark_image.file
			  	fd.append 'data', JSON.stringify($scope.watermark)
				  $http.post(url, fd, {notify_success: notify_success||false, transformRequest: angular.identity, headers: {'Content-Type': `undefined`} })
				  .success (res)->
				  	if res.status == 'ok'
				  		data = JSON.parse(res.data)
				  		data.type = type
				  		$modalInstance.close(data)
			  
			  when 'update'
			  	fd = new FormData()
			  	fd.append 'file', $scope.watermark.watermark_image.file
			  	fd.append 'data', JSON.stringify($scope.watermark)
			  	$http.put(url+'/'+$scope.watermark.id, fd, {notify_success: notify_success||false, transformRequest: angular.identity, headers: {'Content-Type': `undefined`} })
			  	.success (res)->
			  		if res.status == 'ok'
			  			data = JSON.parse(res.data)
			  			watermark.name = data.name
							watermark.photo_file_name = data.photo_file_name
							watermark.photo_content_type = data.photo_content_type
							watermark.photo_file_size = data.photo_file_size
							watermark.photo_updated_at = data.photo_updated_at
							watermark.created_at = data.created_at
							watermark.updated_at = data.updated_at
							watermark.user_id = data.user_id
							watermark.display_type = data.display_type
							watermark.position = data.position
							watermark.image_asset_id = data.image_asset_id
							watermark.stretched_size = data.stretched_size
							watermark.positioned_size = data.positioned_size
							watermark.base_image_height = data.base_image_height
							watermark.base_image_width = data.base_image_width
							watermark.tiled_size = data.tiled_size
							watermark.rotation = data.rotation
							watermark.opacity = data.opacity
							watermark.photo_url = data.photo_url
							$modalInstance.close data

		$scope.cancel = ()->
			$modalInstance.dismiss('cancel')

		$scope.delete = ()->
			if confirm('Are you sure you want proceed on this action?')
				$http.delete(url+'/'+$scope.watermark.id, {notify_success: notify_success||false}).success (res)->
					if res.status.toLocaleLowerCase()=='ok'
						$modalInstance.close {action: 'delete'}

		$scope.chooseWatermarkImage = ()->
			modalInstance = $modal.open
				size: 'sm'
				templateUrl: 'ng-modules/photographer/views/preferences/watermarks/_option.html'
				controller: 'chooseWatermarkModalInstanceCtrl'
				resolve:
					watermark: ()->
						$scope.watermark
			modalInstance.result.then ()->
]