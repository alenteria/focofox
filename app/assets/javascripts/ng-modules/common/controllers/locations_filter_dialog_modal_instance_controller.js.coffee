url = '/api/locations'
angular.module 'common'
.controller 'locationsFilterDialogModalInstanceCtrl',[
    '$rootScope'
    '$scope'
    '$modalInstance'
    '$http'
    'photos'
    'locations'
    'localStorageService'
    ($rootScope, $scope, $modalInstance, $http, photos, locations, localStorageService)->
      $scope.map_settings = localStorageService.get('map_settings')||{}
      $scope.map = {}
      $scope.locations = locations
      
      default_styles = $('#custom-styles').html()
      $('#custom-styles').text(default_styles+'#map{height: '+($(window).height()-170)+'px;}')

      $scope.ok = (type)->
        $('#custom-styles').text(default_styles)
        $modalInstance.close _.where($scope.locations, {selected: true})

      $scope.cancel = ()->
        $('#custom-styles').text(default_styles)
        $modalInstance.dismiss('cancel')
      
      $scope.$watchCollection '[center, zoom]', (settings)->
        if settings[0] && settings[1]
          $scope.map_settings = {}
          _center = {lat: settings[0].lat(), lng: settings[0].lng()}
          $scope.map_settings.zoom = settings[1]
          $scope.map_settings.center = _center#new google.maps.LatLng(parseFloat(settings[0].A), parseFloat(settings[0].F))
          localStorageService.set('map_settings', $scope.map_settings)
        return

      $scope.initMap = ()->
        mapElement = document.getElementById('map')
        
        $scope.map = new google.maps.Map(mapElement,
          mapTypeId: google.maps.MapTypeId.TERRAIN
          minZoom: 3
          zoom: ($scope.map_settings||{}).zoom||9
          zoomControl: true
          zoomControlOptions: style: google.maps.ZoomControlStyle.SMALL
        )
        
        $scope.oms = new OverlappingMarkerSpiderfier($scope.map, {circleSpiralSwitchover: 'Infinity', keepSpiderfied: true, markersWontMove: true})

        $scope.overlay = new google.maps.OverlayView()
        $scope.overlay.draw = ()->
        $scope.overlay.setMap($scope.map)

        $scope.geocoder = new (google.maps.Geocoder)

        google.maps.event.addListener $scope.map, 'zoom_changed', (e)->
          $scope.center = $scope.map.getCenter()
          $scope.zoom = $scope.map.getZoom()


        google.maps.event.addListener $scope.map, 'dragend', (e)->
          $scope.center = $scope.map.getCenter()
          $scope.zoom = $scope.map.getZoom()
        $scope.infoBox = new google.maps.InfoWindow(
          zIndex: 99999999
          disableAutoPan: true
        )
        $scope.createMarker = (data) ->
          marker = new (google.maps.Marker)(
            id: data.id
            map: $scope.map
            icon: if data.selected then '/map-marker-selected.png' else '/map-marker2.png'
            position: new (google.maps.LatLng)(data.latitude, data.longitude)
            title: data.title
            draggable: false
            zIndex: 9999999999
            animation: `null`
          )
          $scope.oms.addMarker(marker)

          marker
          
        $scope.showInfoBox = (marker, data) ->
          return false if ($('.marker-'+marker.id)||[]).length > 0
          $http.post('/api/photos/photos_count_by_location', {id: data.id}).success (photos_count)->
            content = '<div id="searchPlaceInfoBox"class="marker marker-'+marker.id+'" style="text-align: left;padding:10px;">
                <strong>Location:&nbsp;</strong><u>'+(data.name)+'</u><br/><br/>
                <strong>Photos:&nbsp;</strong><u>'+photos_count+'</u><br/><br/>
                <i><b>Click</b> on the marker to select/unselect</i><br/>
              </div>'

            $scope.infoBox.setContent(content)


            $scope.infoBox.open($scope.map, marker)

          return

        $scope.hideInfoBox = (marker) ->
          ($scope.infoBox||{close: ->}).close()
          return

        $scope.cleanupMarker = ->
          angular.forEach $scope.markers, (marker)->
            try
              marker.setMap null
              marker.infoBox.hide()
            catch e
              # 
          return

        $scope.fitToMarkers = (markers) ->
          bounds = new (google.maps.LatLngBounds)
          # Create bounds from markers
          angular.forEach markers, (marker)->
            if marker
              latlng = marker.getPosition()
              bounds.extend latlng
          # Don't zoom in too far on only one marker
          if bounds.getNorthEast().equals(bounds.getSouthWest())
            extendPoint1 = new (google.maps.LatLng)(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01)
            extendPoint2 = new (google.maps.LatLng)(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01)
            bounds.extend extendPoint1
            bounds.extend extendPoint2
          $scope.map.fitBounds bounds
          # Adjusting zoom here doesn't work :/
          return

        $scope.autocomplete = new google.maps.places.Autocomplete((document.getElementById('search_field')),{ types: ['geocode'] })
         
        google.maps.event.addListener $scope.autocomplete, 'place_changed', ->
          $scope.center = `null`
          place = $scope.autocomplete.getPlace()
          if place.geometry
            $scope.country_selection = place.types.indexOf('country') != -1
            $scope.map.panTo place.geometry.location
            $scope.map.setZoom if $scope.country_selection then 5 else 10

            $scope.center = $scope.map.getCenter()
            $scope.zoom = $scope.map.getZoom()

          return
        
        # Initial point
        $scope.markers = []
        #bounds = new google.maps.LatLngBounds()
        angular.forEach $scope.locations, (l)->
          marker = $scope.createMarker l
          $scope.markers.push marker
          data = l
          google.maps.event.addListener $scope.map, 'mousemove', (e)->
            angular.forEach $scope.markers, (m)->
              ($scope.infoBox||{close: ->}).close()
            $scope.$digest() if !$scope.$$phase

          google.maps.event.addListener marker, 'mouseout', (e)->
            (marker.infoBox||{close: ->}).close()
            $scope.$digest() if !$scope.$$phase

          google.maps.event.addListener marker, 'mouseover', (e)->
            angular.forEach $scope.markers, (m)->
              ($scope.infoBox||{close: ->}).close()
            $scope.showInfoBox marker, data
            $scope.$digest() if !$scope.$$phase

          google.maps.event.addListener marker, 'click', (e)->
            $scope.center = $scope.map.getCenter()
            $scope.zoom = $scope.map.getZoom()

            item = _.findWhere(locations, {id: data.id})
            map_item = _.findWhere($scope.locations, {id: data.id})

            if map_item && map_item.selected
              item.selected = false
              map_item.selected = false
              marker.setIcon('/map-marker2.png')
            else if map_item && !map_item.selected
              item.selected = true
              map_item.selected = true
              tmp = angular.copy $scope.locations
              tmp.push data
              $scope.locations = tmp
              marker.setIcon('/map-marker-selected.png')

            $scope.$digest() if !$scope.$$phase

          # bounds.extend(marker.getPosition())
        
        setTimeout ()->
          if $scope.map_settings.center && $scope.map_settings.zoom && (($scope.map_settings||{}).center||{}).lat && (($scope.map_settings||{}).center||{}).lng
            _center = $scope.map_settings.center
            center = new google.maps.LatLng(parseFloat(_center.lat), parseFloat(_center.lng))
            $scope.map.setCenter(center)
            $scope.map.setZoom($scope.map_settings.zoom)
          else if $scope.markers.length > 0
            $scope.fitToMarkers($scope.markers)
          else
            window.navigator.geolocation.getCurrentPosition (position) ->
              bounds = new (google.maps.LatLngBounds)
              $scope.map.fitBounds(bounds)
              LatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
              $scope.map.setCenter(LatLng)
              return
          return
        , 500
        
        setTimeout ()->
          google.maps.event.trigger($scope.map, 'resize')
          $scope.map.setZoom( $scope.map.getZoom()-1 )
          $scope.map.setZoom( $scope.map.getZoom()+1 )
        , 100

        if(!$scope.$$phase)
          $scope.$digest()
      
      $scope.focusOnPins = ->
        bounds = new google.maps.LatLngBounds();
        angular.forEach $scope.markers, (marker)->        
          bounds.extend marker.getPosition()

        $scope.map.fitBounds(bounds)

      $scope.delete= ()->
        $http.delete(url+'/'+$scope.location.id).success (res)->
          $modalInstance.close res

      $(document).on 'mouseenter', '#draggable-marker', (e) ->
        item = $(this)
        #check if the item is already draggable
        item.draggable
          helper: 'clone'
          stop: (e, ui)->
            mOffset=$($scope.map.getDiv()).offset()
            point=new google.maps.Point(
              ui.offset.left-mOffset.left+(ui.helper.width()/2),
              ui.offset.top-mOffset.top+(ui.helper.height())
            )
            ll=$scope.overlay.getProjection().fromContainerPixelToLatLng(point)
            $scope.marker.setPosition(ll)
            lat = $scope.marker.getPosition().lat()
            long = $scope.marker.getPosition().lng()
            $scope.location.gps = {latitude: lat, longitude: long}
            $scope.$digest()
        return
      return
]
