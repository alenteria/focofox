url = '/api/image_assets'
angular.module 'common'
.controller 'watermarkImagesModalInstanceCtrl',[
	'$scope'
	'$modalInstance'
	'watermark'
	'$http'
	'$modal'
	'cfpLoadingBar'
	($scope, $modalInstance, watermark, $http, $modal, cfpLoadingBar)->
		$scope.watermark = watermark
		$http.get(url).success (data)->
			$scope.images = data
		
		$scope.select = (m)->
			$modalInstance.close
			$modalInstance.dismiss('cancel')
			$scope.watermark.image_asset_id = m.id
			cfpLoadingBar.start()
			loadImage(
	      m.image_url,
	      (img)->
	      		cfpLoadingBar.complete()
	      		$scope.watermark.watermark_image = {previewUrl: img.src}
	      		$('#watermarked-image-preview').click()
	      		return
	      ,
	      {maxWidth: 900, noRevoke: true, canvas: false}
			)
			return
		$scope.delete = (img)->
			if confirm('Do you want to proceed on this action?')
				$http.delete(url+'/'+img.id).success (res)->
					$scope.images.splice($scope.images.indexOf(img), 1)

		$scope.cancel = ()->
			$modalInstance.dismiss('cancel')
]