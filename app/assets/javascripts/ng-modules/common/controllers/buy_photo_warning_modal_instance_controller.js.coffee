'use strict'
angular.module 'common'
.controller 'buyPhotoWarningModalInstanceCtrl', [
  "$scope"
  "$modalInstance"
  ($scope, $modalInstance)->
    $scope.buy = (photo)->
      $scope.$parent.add2cart(photo)
      $modalInstance.dismiss('cancel')

    $scope.cancel = ()->
      $modalInstance.dismiss('cancel')
]