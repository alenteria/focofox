angular.module 'common'
.controller 'filterWarningModalInstanceCtrl',[
  '$scope'
  '$modalInstance'
  '$modal'
  'ignored'
  '$cookieStore'
  ($scope, $modalInstance, $modal, ignored, $cookieStore)->
    $scope.ignore_filter_warning = ignored
    $scope.$watch 'ignore_filter_warning', (fw)->
      return false unless fw
      $cookieStore.put('ignore_filter_warning', $scope.ignore_filter_warning)

    $scope.add = ()->
      $modalInstance.dismiss('cancel')            
    $scope.cancel = ()->
      $modalInstance.close 'cancel'
]