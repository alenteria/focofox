angular.module 'common'
.controller 'dateTimeFilterDialogModalInstnceCtrl',[
  '$scope'
  '$modalInstance'
  '$modal'
  ($scope, $modalInstance, $modal)->
    $(document).on 'mouseover', 'input.datetime', ->
      $(this).datetimepicker({
        dateFormat: "m/d/yy"
        separator: ' '
        timeFormat: "h:mm tt"
        onSelect: (e, t) ->
          if !t.clicks
            t.clicks = 0
          if ++t.clicks == 2
            $('[data-handler="hide"]').click()
            t.inline = false
            t.clicks = 0
          setTimeout (->
            t.clicks = 0
            return
          ), 500
      })
    $scope.add = ->
      filter = {
        from: $scope.datetime_filter_from
        to: $scope.datetime_filter_to
      }
      $modalInstance.close(filter) 

    $scope.cancel = ->
      $modalInstance.dismiss 'cancel'
    return
]


