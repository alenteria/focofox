# HTML RESPONSE means redirect 
_is_not_logged_in = (response)->
  html_attr = ['<html', '<body', '<div', 'Sign in']
  data = response.data || response
  return false unless typeof(data) is 'string'
  verdict = true
  $.each html_attr, (i, h)->
    
    if (data.indexOf(h)<0)
      verdict = false

  return verdict

angular.module('notifier', [
  'ngAnimate'
  'toastr'
])
.config [
  'toastrConfig'
  (toastrConfig)->
    angular.extend(toastrConfig, {
      allowHtml: true,
      autoDismiss: true,
      closeButton: true,
    })
]
.provider $rootElement: ->

  @$get = ->
    angular.element '<div ng-app></div>'

  return
angular.module('common').factory 'MyHttpInterceptor', [
  '$q'
  ($q) ->
    {
      request: (config) ->
        url = (config.url||"").split('?')
        _params = if url[1] then '?'+url[1] else ''
        _url = (url||[])[0]
        if config.url && !!!config.cache
          if _url.indexOf('.json') < 0
            _url = _url.replace(' ','')
            if _url.substring(_url.length-1) == '/'
              _url = _url.slice(0, _url.length-1)

            config.url = _url+'.json'+_params

        $('html,body').css('cursor','progress') unless config.ignoreLoadingBar
        # console.log(config); // Contains the data about the request before it is sent.
        # Return the config or wrap it in a promise if blank.
        config or $q.when(config)
      requestError: (rejection) ->
        # console.log(rejection); // Contains the data about the error on the request.
        # Return the promise rejection.
        $q.reject rejection
      response: (response, p2, p3) ->
        $('html,body').css('cursor','default')
        
        data = response.data||{}
          
        if ((response.config||{}).data||{}).notify != false && (response.config||{}).notify != false
          $injector = angular.injector([ 'notifier' ])
          $injector.invoke [
            'toastr'
            '$location'
            (toastr, $location) ->
              if data.status == 'ok' && data.message && (response.config||{}).notify_success != false
                toastr.success(data.message, 'Success!')
              else if data.status=='fail' && data.message  && (response.config||{}).notify_error != false
                message = ''
                if typeof data.message == 'string'
                  message = data.message
                else if typeof data.message == 'object' && data.message.length > 0  && (response.config||{}).notify_error != false
                  angular.forEach data.message, (m)->
                    message += "<p><b>"+m.field+"</b>: &nbsp;<i>"+m.issue+"</i></p>"

                toastr.error(message, 'Fail!') 
              else if data.status == 'db_validation_error' && data.message  && (response.config||{}).notify_error != false
                message = ''
                angular.forEach data.message, (m, k)->
                  ms = ''
                  angular.forEach m, (mi)->
                    ms += mi+'<br/>'

                  message += "<p><b>"+k+"</b>: &nbsp;<i>"+ms+"</i></p>"

                  toastr.error(message, 'Fail!') 
              else if _.includes([401, 201], parseInt(response.status)) || _is_not_logged_in(response.data)  && (response.config||{}).notify_error != false
                toastr.warning('Please <a class="loggedin-popup" style="color:green;" href="javascript:void(0)" onclick="location.reload(true); return false;">sign in here.</a>', 'Session Expired!') unless $('.loggedin-popup').is(':visible')

          ]
        response or $q.when(response)
      responseError: (response, data) ->
        console.log response
        $('html,body').css('cursor','default')
        # Return the promise rejection.
        if (((response||{}).config||{}).data||{}).notify != false  && (response.config||{}).notify_error != false
          $injector = angular.injector([ 'notifier' ])
          $injector.invoke [
            'toastr'
            '$location'
            (toastr, $location) ->
              if _.includes([401, 201, 422], parseInt(response.status))
                toastr.warning('Please <a class="loggedin-popup" style="color:green;" href="javascript:void(0)" onclick="location.reload(true); return false;">sign in here.</a>', 'Session Expired!') unless $('.loggedin-popup').is(':visible')
              else if response.data
                data = response.data || ""
                message = _.first(_.last(data.split("::")).split('.'))
                toastr.error(message, 'Fail!')
          ]
        $q.reject response

    }
]

