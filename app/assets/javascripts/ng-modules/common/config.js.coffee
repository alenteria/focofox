angular.module 'common', [
  'angular-loading-bar'
  'ngCookies'
  'Scope.onReady'
  'cfp.loadingBar'
  'LocalStorageModule'
  'ui.bootstrap' 
  'ngAnimate'
  'angular-loading-bar'
  'toastr'
  'templates'
  'Devise'
]

.config [
  "$compileProvider"
  "$sceDelegateProvider"
  'localStorageServiceProvider'
  '$httpProvider'
  'toastrConfig'
  'AuthInterceptProvider'
  (
    $compileProvider,
    $sceDelegateProvider,
    localStorageServiceProvider
    $httpProvider
    toastrConfig
    AuthInterceptProvider
  ) ->
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|blob):/)
    $sceDelegateProvider.resourceUrlWhitelist(['self', /.*/])
    
    # AuthInterceptProvider.interceptAuth(true)

    $httpProvider.interceptors.push 'MyHttpInterceptor'
    angular.extend(toastrConfig, {
      allowHtml: true,
      autoDismiss: true,
      maxOpened: 4
      preventOpenDuplicates: true
      newestOnTop: true
    })

    localStorageServiceProvider.setPrefix('focofox_'+user_id)
] 
