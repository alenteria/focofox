img = new Image()
img.src = '/img/loading.gif'
angular.module('common').directive 'imgLoadingProgress', [
  '$animate'
  ($animate) ->
    (scope, element) ->
      setTimeout ->
        imgLoad = imagesLoaded( '.modal.preview-photo' )
        $('.modal.preview-photo img').parent().addClass('is-loading')
        imgLoad.on 'progress', (instance, image) ->
          $(image.img).parent().removeClass('is-loading')
          cl = if image.isLoaded then 'loaded' else 'broken'
          $(image.img).addClass(cl)
          return
      , 500
]
