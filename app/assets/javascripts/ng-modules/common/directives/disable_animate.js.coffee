angular.module('common').directive 'disableAnimate', [
  '$animate'
  ($animate) ->
    (scope, element) ->
      $animate.enabled false, element
]