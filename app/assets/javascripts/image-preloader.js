(function () {
    function preload (sources) {
        sources.forEach(function (url, index) {
            var img = new Image();
            img.src = url;
        });
    }

    var sources = [
        '/assets/new-front-page/learn-more-arrow.png',
        '/assets/new-front-page/learn-more-arrow-hover.png',
        '/assets/new-front-page/learn-more-arrow-active.png',
        '/assets/new-front-page/learn-more-arrow.png',
        '/assets/new-front-page/learn-more-arrow-hover.png',
        '/assets/new-front-page/learn-more-arrow-active.png',
        '/assets/new-front-page/big-arrow-right-hover.png',
        '/assets/new-front-page/big-arrow-right-active.png',
        '/assets/new-front-page/big-arrow-right.png',
        '/assets/new-front-page/big-arrow-hover.png',
        '/assets/new-front-page/big-arrow-active.png',
        '/assets/new-front-page/big-arrow-right.png',
        '/assets/select2-arrow.png',
        '/assets/select2-arrow-active.png',
        '/assets/pagination-next.png',
        '/assets/pagination-next-active.png',
        '/assets/pagination-prev.png',
        '/assets/pagination-prev-active.png',
        '/assets/refresh-photos.png',
        '/assets/refresh-photos-active.png',
        '/assets/photo-buy-active.png',
        '/assets/photo-buy-hover.png',
        '/assets/photo-buy.png',
        '/assets/photo-zoom-active.png',
        '/assets/photo-zoom-hover.png',
        '/assets/photo-zoom.png',
        '/assets/product-cart-delete.png',
        '/assets/product-cart-delete-hover.png',
        '/assets/date-filter-btn-down.png',
        '/assets/date-filter-btn-down-hover.png',
        '/assets/date-filter-btn-up.png',
        '/assets/date-filter-btn-up-hover.png',
    ];

    preload(sources);
}());
