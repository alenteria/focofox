window.initTranslateApp = ->
  if $('body').hasClass('editable')
    elements = $('[editable]')
    $.each elements, (i, el)->
      e = $(el)
      content = e.html()
      $.get '/api/translations?key='+e.data("key"), (res)->
        content = res.content

        btn = '<a href class="edit-content" style="display:inline;" ng-controller="mainController" ng-click="edit('+"'"+e.data("key")+"'"+','+"'"+i+"'"+')">
                <i class="fa fa-pencil">&nbsp;</i>
                Edit
              </a>'
        content = '<div ng-cloak="true" class="content content-index-'+i+'" data-content-key="'+e.data("key")+'" style="display:inline;">'+content+'</div>'+btn
        e.html(content)
        angular.bootstrap(e, ['translateApp'])
   
$(document).ready ()->
  initTranslateApp()

$(document).on 'click', '[info-editable][popover]', ->
  initTranslateApp()
