class HelpTooltipUploader
  @open: ( slug )->
    @setHelpTooltip( slug )

  @setHelpTooltip: (slug) ->
    $.getJSON(
      "/help_tooltips/#{slug}",
      {id: slug},
      (data) ->
        $( '#helpTooltipUploader h4' ).html( data.text_en )
        $( '#helpTooltipUploader #text_en' ).val( data.text_en )
        $( '#helpTooltipUploader #text_es' ).val( data.text_es )

        $( '#helpTooltipUploader' ).modal( 'show' )

    )

    $( '#helpTooltipUploader #slug' ).val( slug )

$ ->
  isAdmin = isAdmin||null
  return unless isAdmin

  $( document ).on 'click', '#helpTooltipUploader .modal-body .nav-tabs a', (e) ->
    e.preventDefault()
    $(this).tab('show')

  $( document ).on 'click', '#helpTooltipUploader textarea', (e) ->
    e.stopPropagation()

  $( 'a[popover] .fa-info-circle' ).on 'click', (e) ->
    slug = $($(this).parent().attr('popover')).attr('data-key')

    HelpTooltipUploader.open( slug )

