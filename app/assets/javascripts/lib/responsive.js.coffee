class FrontPageScreenManager
  constructor: () ->
    return unless $( '.front-page-welcome-screen' )

    enquire.register "screen and (min-width: 768px)",
      match:   @attachEvent
      unmatch: @dettachEvent

  attachEvent: () =>
    $( window ).on 'resize load', @manageScreenResize

  dettachEvent: () =>
    $( window ).off 'resize load', @manageScreenResize
    $( '.front-page-full' ).attr( 'style', '' )

  manageScreenResize: (e) ->
    window_height  = $( window ).height()
    top_bar_height = $( '.focofox-top-menu' ).height()
    bottom_heading = 110

    computed_height = window_height - top_bar_height
    computed_height = 400 if computed_height < 400
    $( '.front-page-full' ).css( 'height', computed_height )

$ () ->
  front_page_manager = new FrontPageScreenManager()


