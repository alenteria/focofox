window.shiftDown = false
window.ctrlDown = false
setShiftDown = (event) ->
	if event.keyCode == 16 or event.charCode == 16
	  window.shiftDown = true
	else if event.keyCode == 17 or event.charCode == 17
		window.ctrlDown = true
	return

setShiftUp = (event) ->
	if event.keyCode == 16 or event.charCode == 16
	  window.shiftDown = false
	else if event.keyCode == 17 or event.charCode == 17
		window.ctrlDown = false
	return

if window.addEventListener then document.addEventListener('keydown', setShiftDown) else document.attachEvent('keydown', setShiftDown)
if window.addEventListener then document.addEventListener('keyup', setShiftUp) else document.attachEvent('keyup', setShiftUp)
