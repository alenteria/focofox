window.isScrolledIntoView = (elem) ->
  $elem = $(elem)
  $window = $(window)
  docViewTop = $window.scrollTop()
  docViewBottom = docViewTop + $window.height()
  elemTop = $elem.offset().top
  elemBottom = elemTop + $elem.height()
  elemBottom <= docViewBottom and elemTop >= docViewTop

$(document).on "click", "a", ->
  conf = $(this).attr("confirm") || $(this).data('confirm')
  if $(this).attr("method")
    return false  unless confirm(conf)  if conf
    form = document.createElement("form")
    action = $(this).attr("href")||$(this).attr("data-href")
    form.action = action
    method = $(this).attr("method")
    form.method = method
    data = $(this).attr("data")||'{}'
    data = jQuery.parseJSON(data)  if data
    if method.toLowerCase() is "post" or method.toLowerCase() is "get"
      jQuery.each data, (key, value) ->
        $("<input>").attr("type", "hidden").attr("name", key).attr("value", value).appendTo form
        return
      $(form).css('visibility', 'hidden')
      $(form).appendTo('body')
      form.submit()
    else
      $.ajax
        type: method
        url: action
        data: data # serializes the form's elements.
        success: (data) ->
          if data.redirect # show response from the php script.
            window.location.href = data.redirect
          else
            window.location.reload()
          return

    false
  else
    true

# Helpers
Date::addHours = (h) ->
  @setHours @getHours() + h
  this

Date::minusHours = (h) ->
  @setHours @getHours() - h
  this

Array::clean = (deleteValue) ->
  i = 0

  while i < @length
    if this[i] is deleteValue
      @splice i, 1
      i--
    i++
  this

Array::allValuesSame = ->
  return false if this.length <=0
  arraysEqual = (arr1, arr2) ->
    if arr1.length != arr2.length
      return false
    i = arr1.length
    while i--
      if arr1[i] != arr2[i]
        return false
    true
  i = 1
  while i < @length
    if typeof this[i] isnt 'string' && typeof this[i] isnt 'number' && typeof this[i] isnt 'undefined' && this[i] isnt null && typeof this[0] isnt 'undefined' && this[0] isnt null
      unless arraysEqual(this[i], this[0])
        return false
    else
      unless this[i] is this[0]
        return false
    i++
  true

Array::sort_by = (sort1, sort2)->
  collection = @

  if sort1
    collection = _.sortBy collection, (obj)->
      +obj[sort2]

  if sort2
    collection = _.sortBy collection, (obj)->
      +obj[sort1]

  collection

# selectable
$.fn.focusWithoutScrolling = ->
  x = window.scrollX
  y = window.scrollY
  @focus()
  window.scrollTo x, y
  return

$(document).on 'mousedown', '.thumbnails', (event)->
  $(this).focusWithoutScrolling()

`window.whereKeyLike = function(obj, _key){
  val = null
  for(key in obj){
    if (key.toLowerCase().indexOf(_key.toLowerCase()) >= 0){
      val = obj[key]
      break
    }
  }
  return val
}`

window.getAddressNameFromLatLong = (latitude, longitude, fn)->
  geocoder = undefined
  add_name = ''
  geocoder = new (google.maps.Geocoder)
  latlng = new (google.maps.LatLng)(latitude, longitude)
  geocoder.geocode { 'latLng': latlng }, (results, status) ->
    #alert("Else loop1");
    if status == google.maps.GeocoderStatus.OK
      if (results||[])[0]
        add_name = results[0].formatted_address
      else
        add_name = '['+latitude+' ; '+longitude+']'
    else
      console.log("Geocoder failed due to: " + status)

    fn(add_name, (results||[])[0])
  return


$(document).on 'mousedown', '.modal-backdrop:not(:has(*))', ()->
  $(this).hide()

window.bytesToSize = (bytes) ->
  sizes = [
    'Bytes'
    'KB'
    'MB'
    'GB'
    'TB'
  ]
  if bytes == 0
    return 'n/a'
  i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
  (bytes / 1024 ** i).toFixed(1) + ' ' + sizes[i]
