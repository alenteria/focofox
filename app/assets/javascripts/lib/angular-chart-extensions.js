Chart.types.Line.extend({
    name: "LineWithLine",
    initialize: function () {
        Chart.types.Line.prototype.initialize.apply(this, arguments);
    },
    draw: function () {
        Chart.types.Line.prototype.draw.apply(this, arguments);

        var point = this.datasets[0].points[0]
        var point2 = this.datasets[0].points[this.options.lineAtIndex||0]
        var scale = this.scale

        // draw line
        this.chart.ctx.beginPath();
        this.chart.ctx.moveTo(point.x, point2.y);
        this.chart.ctx.strokeStyle = 'gray';
        this.chart.ctx.lineTo(point2.x, point2.y);
        this.chart.ctx.stroke();
        
        this.chart.ctx.moveTo(point2.x, scale.endPoint);
        this.chart.ctx.strokeStyle = 'gray';
        this.chart.ctx.lineTo(point2.x, point2.y);
        this.chart.ctx.stroke();
        
    }
});
angular.module('chart.js').directive('chartWithLine', ['ChartJsFactory', function (ChartJsFactory) { return new ChartJsFactory('LineWithLine'); }])