// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require image-preloader
//= require jquery
//= require jquery-ui
//= require angular
//= require lib/angular-ui-router.min
//= require angular-resource
//= require angular-sanitize
//= require angular-ui-bootstrap
//= require angular-ui-bootstrap-tpls
//= require angular-rails-templates
//= require lib/devise-min
//= require lib/moment
//= require lib/moment-range
//= require lib/angular-animate.min
//= require lib/angular-cookies.min
//= require lib/angular-local-storage.min
//  require lib/angular-toastr.min
//= require lib/angular-toastr.tpls.min
//= require lib/mousehold
//= require lib/loading-bar.min
//= require lib/ui-router-tabs
//= require lib/enquire
//= require lib/responsive
//= require lib/locales
//= require lib/datetime
//= require ng-modules/reset
//= require lib/scope.onready
//= require twitter/bootstrap
//= require lib/help_tooltip_uploader
//= require_tree ./ng-modules/translate/
//= require_tree ./ng-modules/common/
//= require jquery_ujs
//= require underscore
//= require lib/jquery.lazyload.min
//= require lib/custom-lib
//= require lib/imagesloaded.pkgd.min
//= require_tree ./lib/translations/
//= require lib/angular-tablesort
//= require lib/jquery.fullPage.min
//= require formatted_form
//= require lib/select2
//= require_self

$(function () {
    $('select.select2').select2();

    if ($('.dashboard-container').length) {
        $('#footer').appendTo('#results');
    }

   	try{
	    unknow_cart_data = JSON.parse(localStorage.getItem('focofox_.cart'))
	    // my_cart_data = JSON.parse(localStorage.getItem('focofox_'+user_id+'.cart'))||[]
	    if(unknow_cart_data && user_id){
	    	combine = _.uniq(unknow_cart_data)
		    localStorage.setItem('focofox_'+user_id+'.cart', JSON.stringify(combine))
		    localStorage.removeItem('focofox_.cart')
	    }
   	}
   	catch(err) {

   	}
});

