module ApplicationHelper
  def verify_photographer
    unless current_user.verified_photographer?
      redirect_to(root_path, {:flash => { :error => "You need to be a verified photographer to access this page" }})
    end
    true
  end

  def photographer_dashboard_page?
    curren_path = request.path
    bol = curren_path == photographer_photos_path || curren_path == "#{photographer_index_path}#/add-photos" || curren_path == photographer_preferences_path
    session[:photographer_dashboard_path] = curren_path || "#{photographer_index_path}#/add-photos" if bol
    bol
  end

  def search_page?
    begin
      curren_path = request.path
      curren_path == users_search_index_path
    rescue Exception => e
    end
  end

  def editable?
    current_user.try(:admin?)
  end

  def help_tooltip_icon slug, float_right = true
    text = HelpTooltip.get(slug).try(:text)

    link_to '',
      class: float_right ? 'pull-right' : '',
      :'info-editable' => true,
      popover: "<div editable data-key='#{slug}'>#{text}</div>",
      :'popover-append-to-body' => 'true',
      :'popover-trigger' => 'focus',
      :'popover-placement' => 'left',
      style: 'display:flex;color:#428bca;position:absolute;right:28px;' do
        content_tag :i, '', class: 'fa fa-info-circle', style: 'font-size: 20px;'
      end
  end
end


