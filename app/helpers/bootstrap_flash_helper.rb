BootstrapFlashHelper.class_eval do
	def extended_bootstrap_flash
		bootstrap_flash.gsub!(/confirm your email address/, link_to("confirm your email address", new_confirmation_path(resource_name))).try(:html_safe)
	end
end