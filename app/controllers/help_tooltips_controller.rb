class HelpTooltipsController < ApplicationController
  def show
    @help_tooltip = HelpTooltip.find_by(slug: params[:id])

    unless @help_tooltip
      @help_tooltip = HelpTooltip.find(params[:id])
    end

    json_response = {
      text_en: @help_tooltip.text_en,
      text_es: @help_tooltip.text_es
    }

    respond_to do |format|
      format.html
      format.json { render json: json_response.to_json }
    end
  end

  def manage
    HelpTooltip.find_by(slug: params[:slug]).update_attributes(
      text_en: params[:text_en],
      text_es: params[:text_es]
    )
  end
end
