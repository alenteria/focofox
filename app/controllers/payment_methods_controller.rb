class PaymentMethodsController < ApplicationController
  before_action :authenticate_user!, only: [:index, :update]
  before_action :load_data, only: [:index, :update]
  def index
    render
  end

  private
  def load_data
    session[:active_tab] ||= 'send'
    @account = current_user.account
    @send_method = @account.payment_methods.find_or_create_by(method_type: :send)
    @receive_method = @account.payment_methods.find_or_create_by(method_type: :receive)
    
    @send_method_credit_cards = @send_method.try(:credit_cards)||[]
    @send_method_paypal_accounts = @send_method.try(:paypal_accounts)||[]

    @receive_method_paypal_accounts = @receive_method.try(:paypal_accounts)||[]
  end
end