class Photographer::PhotosController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :load_photo, only: [:edit, :destroy, :show, :video_player, :generate_preview]
  before_action :verify_photographer, except: [:show]

  def index
  end

  def new
  end

  def show
    redirect_to @photo.preview_url
  end

  def video_player
    render 'users/video_player', layout: false
  end

  def generate_preview
    render json: {src: @photo.generate_preview}
  end

  def create
    data = params[:photo]
    params[:files].each_with_index do |f, i|
      photo = Photo.new
      photo.category_id = data[:"#{i}"][:category_id]
      photo.original_attachment = f
      photo.attachment = f
      photo.title = data[:"#{i}"][:name]
      photo.description = data[:"#{i}"][:description]
      photo.save

      photo.exif = data[:"#{i}"][:exif]
      photo.build_exif

      data[:"#{i}"][:tags].each do |t|
        tag = Tag.find_or_create_by(t)
        photo.tags << tag
      end
      photo.crop_attachment(data[:"#{i}"])
      photo.save
    end
  	
    redirect_to photos_path 
  end

  def edit

  end

  def update

  end

  def destroy
    @photo.destroy
    flash[:notice] = "photo successfully deleted"
    render :json => {status: :OK, redirect: request.headers["Referer"]}
  end

  private
  def load_photo
    @photo = Photo.find params[:id]
  end
end
