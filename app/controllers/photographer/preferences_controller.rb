class Photographer::PreferencesController < ApplicationController
	before_action :authenticate_user!
  before_action :verify_photographer

	def index
		render
	end
end