class CheckoutController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!, except: [:save_cart]
  before_action :load_data, only: [:review_items, :create] 
  before_action :load_payment, only: [:success, :cancelled]
  before_action :parse_purchase_details, only: [:success]
  after_action :distribute_money, only: [:success]

  def save_cart
    unless current_user.present?
      session[:intended_path] = review_items_checkout_index_path
    end

    Rails.cache.write "cart#{@visitor_id}", params[:cart]
    render json: {status: :ok, redirect_to: review_items_checkout_index_path}
  end

  def create
    errors = ''
    
    @payment = current_user.payments.new(
      to: @merchants,
      items: @cart.map{|c| c['id']},
      payment_method: @payment_method,
      customer_location_name: params[:location_name].strip,
      customer_latitude: params[:latitude],
      customer_longitude: params[:longitude],
    )

    payment_method = params[:method].split(',')
    type = payment_method[0]
    id = payment_method[1]
    case type
    when 'CreditCard'
      credit_card = @payment_method.credit_cards.where(id: id).first
      @payment.update_attribute(:credit_card_id, credit_card.id)
      begin
        setup = @payment.setup('credit_card') if @payment.save
      rescue Exception => e
        setup = nil
        @payment.errors.messages[:payment_error] = [e.message]
      end

      if setup
        redirect_to "#{success_checkout_index_path}/?id=#{@payment.id}"
      else
        flash[:id] = credit_card.id
        flash[:error] = "#{@payment.errors.messages[:payment_error].first}"
        @payment.delete
        redirect_to :back
      end
    when 'PaypalAccount'
      @payment.paypal_account_id = id
      begin
        @payment = @payment.setup if @payment.save
      rescue Exception => e
        @payment.errors.messages[:payment_error] = [e.message]
      end

      if @payment.redirect_to.present?
        redirect_to "#{@payment.redirect_to}&type=paypal"
      else
        flash[:error] = "#{@payment.errors.messages[:payment_error].first}"
        @payment.delete
        redirect_to :back
      end
    end
  end

  def review_items
    render
  end

  def addresses
  end

  def success
    @cart = Rails.cache.read("cart#{@visitor_id}")||[]
    if @cart.count <= 0 || @payment.blank?
      raise ActionController::RoutingError.new('Not Found')
    else
      @account = current_user.account
      @payment_method = @account.payment_methods.where(method_type: :send).first
      @credit_cards = @payment_method.try(:credit_cards)||[]
      @paypal_accounts = @payment_method.try(:paypal_accounts)||[]
      @default = (@credit_cards.try(:defaults).try(:first) || @paypal_accounts.try(:defaults).try(:first))
      @merchants = @cart.map{|c|c['photographer_id']}.uniq

      Rails.cache.write "cart#{@visitor_id}", []
      render :review_items
    end
  end

  def cancelled
    render json: {status: :fail, message: 'purchase was cancelled'}
  end

  private
  def load_data
    @cart = Rails.cache.read("cart#{@visitor_id}")||[]
    @account = current_user.account
    @payment_method = @account.payment_methods.where(method_type: :send).first
    @credit_cards = @payment_method.try(:credit_cards)||[]
    @paypal_accounts = @payment_method.try(:paypal_accounts)||[]
    @default = (@credit_cards.try(:defaults).try(:first) || @paypal_accounts.try(:defaults).try(:first))
    @merchants = @cart.map{|c|c['photographer_id']}.uniq
  end

  def load_payment
    begin
      @payment = Payment.find(params[:id])
    rescue Exception => e
      @payment = nil
    end
  end

  def credit_card_params
    data = {
      first_name: params[:ccFirstName],
      last_name: params[:ccLastName],
      number: params[:ccNumber],
      expire_month: params[:ccExpMonth],
      expire_year: params[:ccExpYear],
      type: params[:ccType],
    }
    
    data[:cvv2] = params[:ccCvc] || CreditCardValidator::Validator.card_type(data[:number].to_s)

    data

  end

  def parse_purchase_details
    if params[:type] = 'paypal'
      @payment.parse_paypal_purchase_details
    else
      @payment.parse_credit_card_purchase_details
    end
  end

  def distribute_money
    @payment.destribute_payment_to_photographers if @payment.verified?
  end
end