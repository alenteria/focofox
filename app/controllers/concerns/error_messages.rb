module Concerns::ErrorMessages
  extend ActiveSupport::Concern

protected
  def format_photo_upload_errors(obj)
    returns = []
    messages = obj.errors.messages
    messages.each do |k, m|
      if k == :attachment_fingerprint
        returns.push duplicate_photo(obj)
      else
        _returns = []
        m.each do |_m|
          _returns.push _m
        end
        returns.push _returns
      end
    end
    returns
  end

  def duplicate_photo(photo)
    existingPhoto = Photo.where(attachment_fingerprint: photo.attachment_fingerprint).first
    {type: 'duplicate', data: {owner: existingPhoto.user, photo_url: photographer_photo_path(id: existingPhoto.id)}}
  end
end