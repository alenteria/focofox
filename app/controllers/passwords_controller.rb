class PasswordsController < Devise::PasswordsController
  def create
    user = User.find_by_email(params[:user][:email])

    if user.nil?
      flash[:notice] = I18n.t(:'devise.confirmations.send_paranoid_instructions')
    	redirect_to new_user_session_path 
    else
    	super
    end
  end
end