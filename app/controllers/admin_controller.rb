class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin

  def index
  end

  def my_focofox
  	begin
  		session[:admin_acted_as] = params[:user_id]
  		redirect_to my_focofox_pages_path
  	rescue Exception => e
  		raise ActionController::RoutingError.new('Not Found')
  	end
  end

  protected
  def verify_admin
    render status: :unauthorized unless current_user.try(:admin?)
  end
end