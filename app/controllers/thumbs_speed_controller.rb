class ThumbsSpeedController < ApplicationController
  layout 'thumbs_speed'

  def photos
    @photos = Dir.entries(Rails.root.join('public', '.test-data'))
    @photos.select! { |file| ! File.directory? file }
    @photos.map! do |photo|
      "/.test-data/#{photo}"
    end
  end
end
