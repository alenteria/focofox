class Admin::Api::SalesController < AdminController
	def index
		date_time_q = if params[:from].present? && params[:to].present?
			df = DateTime.strptime(params[:from], "%m/%d/%Y %I:%M %P").in_time_zone
      dt = DateTime.strptime(params[:to], "%m/%d/%Y %I:%M %P").in_time_zone
			['payments.verified_at BETWEEN ? AND ?', df, dt]
		end
		
		payments = Payment.verifieds.where(date_time_q)
		sales = payments.map do |p|
			obj = {id: p.id, price: p.price, items: p.items, photos: p.items}
			obj['date'] = p.verified_at.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
		
		render json: sales
	end
end