class Admin::Api::ServerStatsController < AdminController
	def index
		filter = params[:filter] || {}
		date_time_q = if filter[:from].present? && filter[:to].present?
			df = DateTime.strptime(filter[:from], "%m/%d/%Y %I:%M %P").in_time_zone
      dt = DateTime.strptime(filter[:to], "%m/%d/%Y %I:%M %P").in_time_zone
			column_name = case params[:type]
				when 'photos_uploaded'
					'photos.created_at'
				else
					'server_stats.date_time'
				end 
			["#{column_name} BETWEEN ? AND ?", df, dt]
		end
		
		stats = case params[:type]
		when 'logins'
			load_logins(date_time_q)
		when 'new_users'
			load_signups(date_time_q)
		when 'photos_uploaded'
			load_photo_uploads(date_time_q)
		when 'storage_required'
			load_storage_stat(date_time_q)
		when 'droplets_provisioned'
			load_provisioned_droplets(date_time_q)
		when 'bandwidth'
			load_bandwidth_stats(date_time_q)
		end
		
		render json: stats || []
	end

	private
	def load_logins(date_time_q)
		ss = ServerStat.where(date_time_q).where(name: 'logins').distinct
		stats = ss.map do |s|
			obj = {id: s.id, stat: s.stat}
			obj['date'] = s.date_time.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end

	def load_signups(date_time_q)
		ss = ServerStat.where(date_time_q).where(name: 'new_users').distinct
		stats = ss.map do |s|
			obj = {id: s.id, stat: s.stat}
			obj['date'] = s.date_time.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end

	def load_photo_uploads(date_time_q)
		ss = Photo.where(date_time_q).where(verified: true).distinct
		stats = ss.map do |s|
			obj = {id: s.id, stat: 1}
			obj['date'] = s.created_at.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end

	def load_storage_stat(date_time_q)
		ss = ServerStat.where(date_time_q).where(name: 'storage_required').distinct
		stats = ss.map do |s|
			obj = {id: s.id, stat: s.stat}
			obj['date'] = s.date_time.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end

	def load_provisioned_droplets(date_time_q)
		ss = ServerStat.where(date_time_q).where(name: 'droplets_provisioned').distinct
		stats = ss.map do |s|
			obj = {id: s.id, stat: s.stat}
			obj['date'] = s.date_time.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end

	def load_bandwidth_stats(date_time_q)
		ss = ServerStat.where(date_time_q).where(name: 'bandwidth').distinct
		stats = ss.map do |s|
			obj = {id: s.id, rx: s.rx, tx: s.tx}
			obj['date'] = s.date_time.to_s(:classical_date)
			obj
		end.group_by{|s|s['date']}
	end
end