class Admin::Api::PhotographersController < AdminController
	before_action :load_photographer, only: [:locations]
	def index
		photographers = User.photographers.includes(:photos).includes(:sold_photos).includes(:photo_views).includes(:locations).where.not(:'users.id' => current_user.id).uniq
		data = photographers.map do |d|
			{
				joined_date: d.created_at.to_s(:classical_date),
				photographer_id: d.id, 
				photographer_name: d.display_name, 
				locations_count: d.locations.count,
				uploads_count: d.photos.uniq.count,
				space_in_mb: ((d.photos.pluck(:photo_file_size, :video_file_size).map{|p|p.sum{|e|e.to_i}}.sum{|e|e.to_i})/(1024.0 * 1024.0)).round(2),
				photo_solds_count: d.sold_photos.count,
				photo_views_count: d.photo_views.map{|p| p.view_type=="preview" ? p.count : 0}.sum,
				thumb_views_count: d.photo_views.map{|p| p.view_type=="thumb" ? p.count : 0}.sum
			}
		end
		render json: data
	end

	def funnel_report
		_ps = Photographer.select(:id, :created_at, :dynamic_attribs).distinct
		_ps = _ps.map do |d|
			{
				id: d.id,
				registered_at: d.registered_at.to_s(:classical_date),
				first_1hun_sale_at: (d.first_1hun_sale_at.present? ? DateTime.parse(d.first_1hun_sale_at) : nil).try(:to_s, :classical_date),
				first_1k_sale_at: (d.first_1k_sale_at.present? ?  DateTime.parse(d.first_1k_sale_at)  : nil).try(:to_s, :classical_date),
				first_add_photos_at: (d.first_add_photos_at.present? ?  DateTime.parse(d.first_add_photos_at)  : nil).try(:to_s, :classical_date),
			}
		end

		registrations = _ps.group_by{|s|s[:'registered_at']}
		first_1hun_sales = _ps.group_by{|s|s[:'first_1hun_sale_at']}
		first_1k_sales = _ps.group_by{|s|s[:'first_1k_sale_at']}
		first_add_photos = _ps.group_by{|s|s[:'first_add_photos_at']}

		keys = (registrations.keys + first_1hun_sales.keys + first_1k_sales.keys + first_add_photos.keys).uniq.reject { |c| c.blank? }
	
		data = {
			keys: keys,
			registrations: registrations,
			first_1hun_sales: first_1hun_sales, 
			first_1k_sales: first_1k_sales,
			first_add_photos: first_add_photos,
 		}

		render json: data
	end

	def locations
		locations = @photographer.locations.select(:id, :name, :latitude, :longitude).distinct
		data = locations.map do |l|
			{
				id: l.id,
				name: l.name,
				latitude: l.latitude,
				longitude: l.longitude,
				uploads_count: l.photos.select(:'photos.id').count
			}
		end
		render json: data
	end

	private
		def load_photographer
			@photographer = User.find(params[:id])
		end
end