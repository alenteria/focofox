class Admin::Api::ClientsController < AdminController
	before_action :parse_params, only: [:index]
	def index
		
		# debugger if @filter[:location].present? && @filter[:distance].present?

		location_q = if @filter[:location].present? && @filter[:distance].present?
			locs_id = Location.near([@filter[:location]['latitude'], @filter[:location]['longitude']], @filter[:distance].to_f*0.621371).map{|l| l.id}
			['location_views.location_id IN (?)', locs_id]
		end
		date_time_q = if @filter[:date_time].present?
			df = DateTime.strptime(@filter[:date_time]['from'], "%m/%d/%Y %I:%M %P").in_time_zone
      dt = DateTime.strptime(@filter[:date_time]['to'], "%m/%d/%Y %I:%M %P").in_time_zone
			['users.created_at BETWEEN ? AND ?', df, dt]
		end

		clients = User.customers.includes(:paid_photos).joins(:viewed_locations).where(location_q).where(date_time_q).where.not(:'users.id' => current_user.id).uniq
		data = clients.map do |c|
			{
				id: c.id,
				client_id: c.id,
				joined_date: c.created_at.to_s(:classical_date),
				email: c.email,
				locations_count: c.viewed_locations.select(:id).count,
				purchases_count: c.paid_photos.verifieds.select(:id).count,
				total_spent: c.paid_photos.verifieds.select(:price).pluck(:price).sum,
				viewed_photos_count: c.viewed_photos.map{|p| p.view_type=="preview" ? p.count : 0}.sum,
				viewed_thumbs_count: c.viewed_photos.map{|p| p.view_type=="thumb" ? p.count : 0}.sum,
			}
		end
		render json: data
	end

	def funnel_report
		_ps = Account.where(type: nil).select(:id, :created_at, :dynamic_attribs).distinct
		_ps = _ps.map do |d|
			{
				id: d.id,
				registered_at: d.registered_at.to_s(:classical_date),
				first_find_photos_at: (d.first_find_photos_at.present? ? DateTime.parse(d.first_find_photos_at) : nil).try(:to_s, :classical_date),
				first_purchase_at: (d.first_purchase_at.present? ?  DateTime.parse(d.first_purchase_at)  : nil).try(:to_s, :classical_date),
				fifth_purhase_at: (d.fifth_purhase_at.present? ?  DateTime.parse(d.fifth_purhase_at)  : nil).try(:to_s, :classical_date),
			}
		end

		registrations = _ps.group_by{|s|s[:'registered_at']}
		first_find_photos = _ps.group_by{|s|s[:'first_find_photos_at']}
		first_purchase = _ps.group_by{|s|s[:'first_purchase_at']}
		fifth_purhase = _ps.group_by{|s|s[:'fifth_purhase_at']}

		keys = (registrations.keys + first_find_photos.keys + first_purchase.keys + fifth_purhase.keys).uniq.reject { |c| c.blank? }
	
		data = {
			keys: keys,
			registrations: registrations,
			first_find_photos: first_find_photos, 
			first_purchase: first_purchase,
			fifth_purhase: fifth_purhase,
 		}

		render json: data
	end

	private
		def parse_params
			@filter ||= {}
			req = request.headers
	    if params.keys.include?('location')
	      @filter[:location] = params[:location].is_a?(String) ? JSON.parse(params[:location]) : params[:location]
	    elsif req[:HTTP_LOCATION].present?
	      @filter[:location] = req[:HTTP_LOCATION].is_a?(String) ? JSON.parse(req[:HTTP_LOCATION]) : req[:HTTP_LOCATION]
	    end

	    if params.keys.include?('date_time')
	      @filter[:date_time] = params[:date_time].is_a?(String) ? JSON.parse(params[:date_time]) : params[:date_time]
	    elsif req[:HTTP_DATE_TIME].present?
	      @filter[:date_time] = req[:HTTP_DATE_TIME].is_a?(String) ? JSON.parse(req[:HTTP_DATE_TIME]) : req[:HTTP_DATE_TIME]
	    end

	    if params.keys.include?('distance')
	      @filter[:distance] = params[:distance]
	    elsif req[:HTTP_DISTANCE].present?
	      @filter[:distance] = req[:HTTP_DISTANCE]
	    end
		end	
end
