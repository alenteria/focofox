class PagesController < ApplicationController
  before_action :authenticate_user!, only: [:my_photos, :my_focofox]
  before_action :load_account, except: [:run_server_stat_cron]

  before_action :load_fb, only: [:my_photos, :my_focofox]
  before_action :load_dropbox, only: [:my_photos, :my_focofox]

  def my_photos
  end

  def my_focofox
  end

  def download
    ids = JSON.parse params[:photos]
    photos = PaidPhoto.where(id: ids)||Photo.where(id: ids)
    if photos.present?
      #Attachment name
      filename = 'Focofox-photos-'+ DateTime.now.to_s(:short_date_with_time) +'.zip'
      temp_file = Tempfile.new(filename)
       
      begin
        #This is the tricky part
        #Initialize the temp file as a zip file
        Zip::OutputStream.open(temp_file) { |zos| }
       
        #Add files to the zip file as usual
        Zip::File.open(temp_file.path, Zip::File::CREATE) do |zip|
          photos.each do |p|
            if File.exists? p.attachment.path
              zip.add p.file_name, p.attachment.path
            end
          end
        end
       
        #Read the binary data from the file
        zip_data = File.read(temp_file.path)
       
        #Send the data to the browser as an attachment
        #We do not send the file directly because it will
        #get deleted before rails actually starts sending it
        send_data(zip_data, :type => 'application/zip', :filename => filename)
      ensure
        #Close and delete the temp file
        temp_file.close
        temp_file.unlink
      end
    else
      send_data nil
    end
  end

  def empty
    render
  end

  def block
  end

  def run_server_stat_cron
    ServerStat.fetch_provisioned_droplets
    ServerStat.calc_storage_required
    render json: {status: :ok, message: 'cron successfully executed.'}
  end

  private
  def load_account
    @account = current_user.account
  end

  def load_fb
    @has_valid_fb_token = @account.has_valid_fb_token?
    @albums = @has_valid_fb_token ? @account.facebook.try(:albums) : []
  end

  def load_dropbox
    begin
      @dropbox = current_user.dropbox_session if current_user.dropbox_session
      @dbsession = DropboxSession.deserialize(current_user.dropbox_session)
      @client = DropboxClient.new(@dbsession, DROPBOX_APP_MODE)

      dropbox_root_path = @client.metadata('/')
      @dropbox_folders = (dropbox_root_path.try(:[], 'contents')||[]).map do |d|
        if d['is_dir']
          path = d['path'].tr('/', '')
          {path: path, name: path}
        else
          nil
        end
      end.reject(&:empty?)

      @has_valid_dropbox_token = @dropbox_folders.present?
    rescue Exception => e
    end
  end
end