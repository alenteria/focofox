class RegistrationsController < Devise::RegistrationsController
  after_filter :add_account
  after_action :update_avatar

  def update_resource(resource, _params)
    if current_user.logged_in_with_social_media? || (params[:user][:current_password].blank? && params[:user][:password].blank? && params[:user][:password_confirmation].blank?)
      params.delete("current_password")
      resource.update(account_update_params)
    else
      super
    end
  end

  private
    def add_account
      if resource.persisted? && params[:user].try(:[], :role) == 'photographer' && resource.try(:photographer_account).blank?
        storage_space = 2
        storage_space = storage_space.to_i * 1000000000
        Photographer.create user: resource, storage_space: storage_space
      else
        Account.create user: resource, paypal_email: resource.email
      end

      log_signup
      
      resource
    end

    def sign_up_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :username, :telephone)
    end

    def account_update_params
      accessible = [ :first_name, :name, :last_name, :email, :username] # extend with your own params
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      accessible << [:current_password] unless params[:user][:current_password].blank?
      params.require(:user).permit(accessible)
    end
    def after_update_path_for(resource)
      edit_user_registration_path#(resource)
    end

    def update_avatar
      if params[:user].try(:[], :avatar).present?
        resource.update(avatar: params[:user][:avatar])
      end
    end

    def log_signup
      ss = ServerStat.find_or_create_by(name: 'new_users', date_time: Date.current)
      ss.stat = ss.try(:stat).to_f + 1
      ss.save
    end
end
