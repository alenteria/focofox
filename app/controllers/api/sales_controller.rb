class Api::SalesController < ApplicationController
  include CleanPagination

  before_action :authenticate_user!

  before_action :parse_filter_params, only: [:history, :sort_history]
  before_action :load_sales_histories, only: [:history, :sort_history]

  def history
  	max_per_page = 500

    begin
      if @sales.length > 0
        paginate @sales.uniq.count, max_per_page do |limit, offset|
          _sales = @sales

          if @filter[:sort] == 'date'
            _sales = _sales.sort_by do |obj|
              [obj[@filter[:sort].to_sym], obj[:location_name]]
            end
          elsif @filter[:sort].present?
            _sales = _sales.sort_by do |obj|
              [obj[@filter[:sort].to_sym], obj[:date]]
            end
          end
          
          _sales = _sales.reverse! if @filter[:sort_type] == 'desc'
          _sales = _sales.slice(offset, limit)
          render json: _sales.as_json(methods: [:photos_count, :total, :paid_at, :photos_by_location, :customer_location_name])
        end
      else
        render json: []
      end
    rescue Exception => e
      render json: []
    end

  end

  def fetch_sales_stat
    purchases = if params[:scope]=='system'
      PaidPhoto.select('price')
    else
      current_user.paid_photos.select('price')
    end
    render json: purchases
  end

  private
   def sort(obj, by, type='asc')
    by = by.to_sym
    type=='asc' ? obj[by] : -obj[by]
  end

  def load_sales_histories
  	date_times_filter =  if @filter[:date_times].present?
        qs = ''
        _or = ''
        qp = []
        @filter[:date_times].each do |fd|
          begin
            df = DateTime.strptime(fd['from'], "%m/%d/%Y %I:%M %P")
            dt = DateTime.strptime(fd['to'], "%m/%d/%Y %I:%M %P")
            if df==dt
              df -= 7
              dt +=7
            end
            qs += _or+"verified_at BETWEEN ? AND ?"
            _or = ' OR '
            qp << df
            qp << dt
          rescue Exception => e
          end
        end
        qp.unshift(qs)
      else
        [] 
      end

    location_ids = []
    locations_filter = if @filter[:locations].present?
	      f = @filter[:locations].map{|l| l['id']}
        location_ids = f
	      if f.present?
	        ["paid_photos.location_id = ?", f]
	      else
	        []
	      end
	    else
	      []
	    end
  	_sales =  Payment.where("'?'= ANY(payments.to)", current_user.id).joins(:paid_photos).where('paid_photos.photographer_id = ?', current_user.id)
      .where(date_times_filter)
    
    _sales = _sales.group_by{|s|s.paid_at}
    _sales.each_with_index do |s_, key|
      @sales ||= []
      _pp = s_[1].map{|p| p.paid_photos.where('paid_photos.photographer_id = ?', current_user.id).distinct}.flatten
      photos = []
      _pp.each do |_p|
        if location_ids.include?(_p.location_id)||location_ids.blank?
          photos << _p
        end
      end
      
      _group_pp = photos.uniq.group_by do |_p|
        _p.location_name
      end
      
      customer_locations = s_[1].map do |s|
        "#{s.customer_location_name} - #{s.photos.count} photos"
      end

      _group_pp.each do |_gp|
        @sales += [{customer_locations: customer_locations,location_name: _gp[0], photos: _gp[1].as_json(methods: [:url, :formatted_date_time]),photos_count: _gp[1].count, date: s_[0], total_price: _gp[1].sum{|p|p.price.to_f}}]
      end
    end
    @sales

  end

  def parse_filter_params
    @filter = {}
    req = request.headers
    if params.keys.include?('date_times')
      @filter[:date_times] = params[:date_times].is_a?(String) ? JSON.parse(params[:date_times]) : params[:date_times]
    elsif req[:HTTP_DATE_TIMES].present?
      @filter[:date_times] = req[:HTTP_DATE_TIMES].is_a?(String) ? JSON.parse(req[:HTTP_DATE_TIMES]) : req[:HTTP_DATE_TIMES]
    end

    if params.keys.include?('locations')
      @filter[:locations] = params[:locations].is_a?(String) ? JSON.parse(params[:locations]) : params[:locations]
    elsif req[:HTTP_LOCATIONS].present?
      @filter[:locations] = req[:HTTP_LOCATIONS].is_a?(String) ? JSON.parse(req[:HTTP_LOCATIONS]) : req[:HTTP_LOCATIONS]
    end

    if params.keys.include?('places')
      @filter[:places] = params[:places].is_a?(String) ? JSON.parse(params[:places]) : params[:places]
    elsif req[:HTTP_PLACES].present?
      @filter[:places] = req[:HTTP_PLACES].is_a?(String) ? JSON.parse(req[:HTTP_PLACES]) : req[:HTTP_PLACES]
    end

    if params.keys.include?('sort')
      @filter[:sort] = params[:sort]
    elsif req[:HTTP_SORT].present?
      @filter[:sort] = req[:HTTP_SORT]
    end

    if params.keys.include?('sort')
      @filter[:sort_type] = params[:sort_type]
    elsif req[:HTTP_SORT_TYPE].present?
      @filter[:sort_type] = req[:HTTP_SORT_TYPE]
    end
  end
end