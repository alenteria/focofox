class Api::GroupsController < ApplicationController
  before_action :authenticate_user!

  def create
    group = current_user.photo_groups.create(group_params)
    if members_params.present? && group.present?
    	members = Photo.where(id: members_params)
    	group.photos << members
    end

    unless group.errors.present?
    	render json: {status: 'ok', data: group}
    else
    	render json: {status: 'fail', errors: group.errors.messages}
    end
  end

  def search
    query = params[:q]
    result = if query.present?
      current_user.photo_groups.search(query).joins(:photos).where(:'photos.verified' => true).select(:'groups.id', :'groups.name').map{|g| {id: g.id, text: g.name, name: g.name}}
    else
      current_user.photo_groups.joins(:photos).where(:'photos.verified' => true).select(:'groups.id', :'groups.name').map{|g| {id: g.id, text: g.name, name: g.name}}
    end
    render :json => result.uniq.sort_by{|g|-g[:id]}
  end
  private
  def group_params
  	params.permit(:name)
  end
  def members_params
  	params.fetch(:members).collect{|m|m['id']}
  end
end
