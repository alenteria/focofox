class Api::PhotographerPreferencesController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_photographer

  def update_default_photo_price
    # validate for photo
    _p = Photo.new(price: params[:default_photo_price])
    _p.valid?
    errors = if _p.errors[:price].blank?
  	  photographer.update!(default_photo_price: params[:default_photo_price])
      nil
     else
      _p.errors[:price].first
     end 
     
    status = _p.errors[:price].present? ? :fail : :ok
    message = _p.errors[:price].present? ? nil : 'Default photo price updated!'
  	render json: {status: status, message: message, error: _p.errors[:price].first}
  end

  def update_thumbnail_size
    photographer.update(params.permit(:thumbnail_size))
  	render json: {status: :ok}
  end

  def update_per_page_display
    photographer.update(params.permit(:per_page))
    render json: {status: :ok}
  end

  def default_configs
    account = current_user.photographer_account || current_user.admin_account
    render json: {
      default_photo_price: (account.try(:default_photo_price)||0),
      license_lists: current_user.license_lists.as_json(methods: [:attachment_url]),
      default_license: account.default_license,
      locations: current_user.locations||[], 
      default_location: account.default_location,
      photos_count: current_user.photos.count,
      watermark_lists: current_user.watermark_lists.as_json(methods: [:photo_url]),
      default_watermark: account.default_watermark,
      groups: current_user.photo_groups.map{|g| {id: g.id, text: g.name, name: g.name}},
      is_resize_photos: account.is_resize_photos,
      max_width: account.max_width, 
      max_height: account.max_height,
    }
  end

  def disable_photo_resizing
    photographer.update(is_resize_photos: params[:value])
    render json: {status: :ok}
  end

  def update_photo_resizing
    photographer.update(is_resize_photos: true, max_height: params[:max_height], max_width: params[:max_width])
    render json: {status: :ok}
  end

  private
  def photographer
  	current_user.photographer_account || current_user.admin_account
  end
end