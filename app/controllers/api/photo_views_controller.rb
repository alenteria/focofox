class Api::PhotoViewsController < ApplicationController
	before_action :load_photo_view, only: [:add, :minus]
	before_action :authenticate_user!, only: [:stats_vs_purchased]
	before_action :verify_photographer, only: [:stats_vs_purchased]

	def add
		begin
			@photo_view.update_column(:count, @photo_view.count.to_i+1)
		rescue
		end
		render json: {status: 'ok'}
	end

	def minus
		begin
			count = @photo_view.count <= 0 ? 0 : (@photo_view.count||0)-1
			@photo_view.update_column(:count, count)
		rescue
		end
		render json: {status: 'ok'}
	end

	private
	def load_photo_view
		@photo_view = PhotoView.find_or_create_by(user_id: current_user.try(:id), photo_id: params[:photo_id], photographer_id: params[:photographer_id], latitude: params[:latitude].try(:to_f), longitude: params[:longitude].try(:to_f), view_type: params[:view_type].to_sym)
	end

end