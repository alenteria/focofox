class Api::PaypalAccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_payment_method
  before_action :load_paypal_account, only: [:update, :destroy, :mark_default]
  
  def create
    paypal_account = @payment_method.paypal_accounts.create(
      email: params[:email],
      is_default:( @payment_method.paypal_accounts.count <=0&&@payment_method.credit_cards.count <=0),
      )
    if paypal_account.errors.present?
      render json: {status: :db_validation_error, message: paypal_account.errors.messages}
    else
      render json: {status: :ok, message: 'Paypal Account added!', data: paypal_account}
    end
  end

  def update
    @paypal_account.update_attribute(:email, params[:email])
    if @paypal_account.errors.present?
      render json: {status: :db_validation_error, message: paypal_account.errors.messages}
    else
      render json: {status: :ok, message: 'Paypal Account email updated!', data: @paypal_account}
    end
  end

  def destroy
    is_default = @paypal_account.is_default?
    @paypal_account.delete
    next_default = nil
    if is_default
      next_default = (@payment_method.paypal_accounts.first||@payment_method.credit_cards.first)
      next_default.update_attribute(:is_default, true)
    end
    
    render json: {status: :ok, message: :'Paypal account deleted!', type: next_default.try(:class).try(:name), data: next_default}
  end

  def mark_default
    @payment_method.paypal_accounts.update_all(is_default: false) if @payment_method.paypal_accounts.present?
    @payment_method.credit_cards.update_all(is_default: false) if @payment_method.credit_cards.present?
    @paypal_account.update_attribute(:is_default, true)
    render json: {status: :ok, message: 'Paypal Account marked as default.'}
  end

  private
  def load_paypal_account
    @paypal_account = PaypalAccount.find(params[:id])
  end

  def load_payment_method
    session[:active_tab] = params[:method_type]
    @payment_method = PaymentMethod.find_or_create_by(method_type: (params[:method_type]||'send').to_sym, account_id: current_user.account.try(:id))
    @payment_method.save if @payment_method.id.nil?
  end
end