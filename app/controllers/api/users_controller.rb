class Api::UsersController < ApplicationController

  def verify_email
  	user = User.find_by_email(params[:email])
  	render json: user.present?
  end

  def verify_username
  	user = User.find_by_username(params[:username])
  	render json: user.present?
  end

  def verify_login_credentials
    user = User.find_by_email(params[:email])||User.find_by_username(params[:email])
    if user_signed_in?
      render json: {valid: true, signed_in: true, message: 'You are already signed in.'}
      return false
    end

    if user.blank?
      render json: {valid: false}
      return false
    end 

    is_valid = user.try(:valid_password?, params[:password])
    if is_valid && !user.try(:access_locked?)
      render json: {valid: true}
    elsif user.try(:access_locked?)
      render json: {valid: false, message: 'Your account was <a href="#locked_account_help">locked</a>! Unlock email was sent.'}
    else
      user.failed_attempts ||= 0
      user.failed_attempts += 1
      if user.try(:failed_attempts) >= user.try(:class).try(:maximum_attempts)
        user.lock_access! unless user.access_locked?
        render json: {valid: false, message: 'Your account was <a href="#locked_account_help">locked</a>! Unlock email was sent.'}
      else
        user.save(validate: false)
        render json: {valid: false, }
      end
    end
  end

  def verify_password
    strength = PasswordStrength.test(params[:username]||params[:email], params[:password])
    render json: {valid: strength.valid?(:strong), status: strength.status}
  end

  def verify_otp
    render json: {is_otp: (User.find_by_email(params[:email])||User.find_by_username(params[:email])).try(:'otp_required_for_login?')}
  end
end