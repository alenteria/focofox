class Api::WatermarksController < ApplicationController
  before_action :authenticate_user!
  before_action :load_watermark, only: [:update, :destroy]

  def index
  end

  def create
  	data = JSON.parse(params[:data])
  	watermark = current_user.watermarks.new
  	watermark.name = data['name']
    watermark.display_type = data['display_type']
    watermark.stretched_size = data['stretched_size']
    if params[:file].present? && params[:file] != 'undefined'
      image_asset = current_user.image_assets.create(image: params[:file])
    	watermark.image_asset = image_asset
    elsif data['image_asset_id'].present?
      watermark.image_asset_id = data['image_asset_id']
    end 
  	watermark.tiled_size = data['tiled_size']
  	watermark.rotation = data['rotation']
    watermark.opacity = data['opacity']
  	watermark.positioned_size = data['positioned_size']
    watermark.base_image_width = data['base_image_width']
    watermark.base_image_height = data['base_image_height']
    watermark.position = data['position']
  	watermark.save
    unless watermark.errors.present?
  	  render json: {status: :ok, message: 'Watermark successfully created.', data: watermark.to_json(methods: [:photo_url])}
    else
      render json: {status: :db_validation_error, message: watermark.errors.messages}
    end
  end
  def update
    data = JSON.parse(params[:data])
    @watermark.name = data['name']
    @watermark.display_type = data['display_type']
    @watermark.stretched_size = data['stretched_size']

    if params[:file].present? && params[:file] != 'undefined'
      image_asset = current_user.image_assets.create(image: params[:file])
      @watermark.image_asset = image_asset 
    elsif data['image_asset_id'].present?
      @watermark.image_asset_id = data['image_asset_id']
    end

    @watermark.tiled_size = data['tiled_size']
    @watermark.rotation = data['rotation']
    @watermark.opacity = data['opacity']
    @watermark.position = data['position']
    @watermark.base_image_width = data['base_image_width']
    @watermark.base_image_height = data['base_image_height']
    @watermark.positioned_size = data['positioned_size']
    @watermark.save
    unless @watermark.errors.present?
      render json: {status: :ok, message: 'Watermark successfully updated.', data: @watermark.to_json(methods: [:photo_url])}
    else
      render json: {status: :db_validation_error, message: @watermark.errors.messages}
    end
  end

  def destroy
    @watermark.delete
    render json: {status: :ok, message: 'Watermark successfully deleted.'}
  end

  def mark_default_watermark
    photographer = current_user.photographer_account || current_user.admin_account
    photographer.default_watermark_id = params[:id]
    photographer.save
    render json: {status: :ok, message: 'watermark mark as default'}
  end

  def last_used_watermark
    render json: current_user.watermark_lists.sort_by! {|w| w.updated_at}.last.to_json(methods: [:photo_url])
  end

  def verify_name
    exist = (current_user.watermarks.where(name: params[:name]).where.not(id: params[:id]).first || Watermark.where(name: params[:name], user_id: nil).where.not(id: params[:id]).first)
    render json: exist.present?
  end

  private 
    def load_watermark
      @watermark = Watermark.find(params[:id])
    end
end