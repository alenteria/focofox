class Api::LocationsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy, :mark_default_location]
  before_action :load_location, only: [:update, :destroy]

  def index
  end

  def create
    location = current_user.locations.near([location_params[:latitude], location_params[:longitude]], Location::UNIQUENESS_DISTANCE).first || current_user.locations.create(location_params)
    unless location.errors.present?
  	  render json: {status: :ok, data: location, :'message'=>'Location successfully created.'}
    else
      render :json => {status: :db_validation_error, message: location.errors.messages}
    end
  end

  def update
    @location.update(location_params)
    unless @location.errors.present?
      render json: {status: :ok, data: @location, message: 'Location successfully updated.'}
    else
      render :json => {status: :db_validation_error, message: @location.errors.messages}
    end
  end

  def destroy
    data = @location
    @location.delete
    render json: {status: :ok, action: :delete, data: data, message: 'Location was deleted!'}
  end

  def mark_default_location
    photographer = current_user.photographer_account || current_user.admin_account
    photographer.default_location_id = params[:id]
    photographer.save
    render json: {status: :ok, message: 'Location was marked as default.'}
  end

  def verify_name
    render json: current_user.locations.where(name: params[:name]).where.not(id: params[:id]).present?
  end

  def verify_gps
    if params[:gps].present?
      nearby = current_user.locations.where.not(id: params[:id]).near([params[:gps][:latitude], params[:gps][:longitude]], Location::UNIQUENESS_DISTANCE)
      render json: {verdict: nearby.present?, nearby: nearby}
    else
      render json: false
    end
  end

  def search
    if params[:q].present?
      locations = Location.near(params[:q])
    else
      locations = Location.all
    end
    render :json => locations.map{|t| {id: t.id, text: t.name, name: t.name}}.uniq
  end

  private
  def load_location
    begin
  	 @location = Location.find(params[:id])
    rescue
      @location = current_user.locations.new
    end
  end
  def location_params
  	data = params.permit(:name, :local_names, :city, :province, :country, :zoom, :latitude, :longitude)
    if params[:gps].present?
      data = data.merge params.fetch(:gps)
    end
    
    data[:local_names] = data[:local_names].try(:split, ',')
    data[:name] = data[:name].humanize
    data  
  end

end

