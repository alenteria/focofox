require 'socket'
require 'cgi'

class Api::PaymentsController < ApplicationController
  before_action :parse_cart_items, only: [:create]

  include CleanPagination

  def index
    render json: current_user.payments.verifieds.sort_by{
        |a|
        a.verified_at
      }.reverse!.as_json(methods: [:paid_at, :photos_count, :payment_method, :credit_card, :paypal_account])
  end

  def show
    photos = current_user.payments.verifieds.find(params[:id]).photos
    max_per_page = 200
    paginate photos.uniq.count, max_per_page do |limit, offset|
      render json: photos.limit(limit).offset(offset).as_json(methods: [:photos, :paid_at, :formatted_date_time])
    end
  end

  def photos
    return render json: [] if params[:id].to_i <= 0

    @filter = {
      sort_by: request.headers[:HTTP_SORT_BY]||:date_time,
      payment_id: request.headers[:HTTP_PAYMENT_ID]||params[:id],
    }

    max_per_page = 200
    photos = current_user.payments.verifieds.find(@filter[:payment_id]).photos
    paginate photos.uniq.count, max_per_page do |limit, offset|
      render json: photos.sort_by{
        |a|
        sort(a, @filter[:sort_by])
      }.slice!(offset, limit).uniq.as_json(methods: [:formatted_date_time, :attachment_url, :orientation_type, :attachment_content_type,  :attachment_absolute_url, :video_thumbnails, :preview_url, :url, :photographer_name])
    end
  end

  def create
    @payment = Payment.new({
      from: current_user.try(:id),
      to: @merchants,
      items: @items.pluck(:id),
      description: ''
    })

    if @payment.save
      res = @payment.setup
      if res.try(:[], 'payKey').present?
        render json: {status: :ok, redirect_to: res['redirect_to']}
      else
        render json: {status: :fail, message: res}
      end
    else
      render json: {status: :fail, message: @payment.errors}
    end
  end

  private
  def parse_cart_items
    photo_ids = params[:_json].map{|a|a['id']}.uniq
    @items = Photo.where(id: photo_ids)
    @merchants = @items.pluck(:user_id).uniq
  end

  def sort(obj, by)
    if by.to_sym == :date_time
      [obj.public_send(by), obj.location_name]
    else
      [obj.public_send(by), obj.date_time]
    end
  end
  
end
