class Api::LocationViewsController < ApplicationController
	before_action :authenticate_user!, only: [:my_stats]
	before_action :verify_photographer, only: [:my_stats]

	before_action :load_location_view, only: [:add, :minus]
	def add
		begin
			@location_view.update_column(:count, @location_view.count.to_i+1)
		rescue
		end
		render json: {status: 'ok'}
	end

	def minus
		begin
			count = @location_view.count <= 0 ? 0 : (@location_view.count||0)-1
			@location_view.update_column(:count, count)
		rescue
		end
		render json: {status: 'ok'}
	end

	def my_stats
		lv = current_user.locations_viewed.select(:created_at).group_by{|a|a.created_at.to_date.to_s(:classical_date)}
		render json: lv
	end

	private
	def load_location_view
		@location_view = LocationView.find_or_create_by(user_id: current_user.try(:id), location_id: params[:location_id]||params[:id])
	end

end