class Api::ImageAssetsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_image_asset, only: [:update, :destroy]

  def index
  	render json: current_user.image_asset_lists.to_json(methods: [:image_url])
  end 

  def destroy
    @image_asset.delete
    render json: {status: :ok}
  end

  private
  def load_image_asset
  	@image_asset = ImageAsset.find(params[:id])
  end
end