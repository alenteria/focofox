class Api::PhotographersController < ApplicationController

  def search
  	name = params[:q]
  	ids_q = if params[:ids].present? && params[:ids].length > 0
  		['users.id IN (?)', params[:ids].split(',')]
  	end

    result = User.photographers.where(ids_q)
	    .where('
	      lower(first_name) like ? OR 
	      lower(last_name) like ? OR
	      lower(email) like ? OR
	      lower(username) like ?
	      ',
	      "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%", "%#{name.downcase}%" 
	    ).select(:id, :first_name, :last_name, :email, :username)
	    .distinct.limit(5).map{|t| {id: t.id, text: t.display_name, name: t.display_name}}
    render :json => result
  end
end