class Api::TagsController < ApplicationController
  before_action :authenticate_user!

  def search
    query = params[:q]
    result = Tag.search(query).map{|t| {id: t.id, text: t.name, name: t.name}}
    render :json => result.uniq
  end
end
