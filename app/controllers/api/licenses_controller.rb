class Api::LicensesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_license, only: [:update, :destroy, :mark_default]

  def index
  end

  def create
  	license = current_user.licenses.create(license_params)
    unless license.errors.present?
  	 render json: {status: :ok, data: license.as_json(methods: [:attachment_name, :attachment_url]), message: 'License successfully created.'}
    else
      render json: {status: :db_validation_error, errors: license.errors.messages, message: license.errors.messages}
    end
  end

  def update
    @license.update(license_params)
    unless @license.errors.present?
      render json: {status: :ok, data: @license.as_json(methods: [:attachment_name, :attachment_url])}
    else
      render json: {status: :db_validation_error, message: @license.errors.messages, data: @license.as_json(methods: [:attachment_name, :attachment_url])}
    end
  end

  def destroy
    @license.delete
    render json: {status: :ok, message: 'License successfully deleted.'}
  end

  def mark_default
    photographer = current_user.photographer_account || current_user.admin_account
    photographer.default_license_id = @license.id
    photographer.save
    render json: {status: :ok, message: 'License marked as default.'}
  end
  def search
    query = params[:q]
    result = if query.present?
      License.search(query).map{|g| {id: g.id, text: g.title, name: g.title, title: g.title}}
    else
      License.all
    end
    render :json => result.uniq
  end
  private
  def load_license
  	@license = License.find(params[:id])
  end
  def license_params
  	params.permit(:title, :description, :attachment)
  end

end

