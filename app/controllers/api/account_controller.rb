class Api::AccountController < ApplicationController
  before_action :authenticate_user!
  before_action :load_account

  def fb_albums
    if params[:accessToken].present?
      @account.fb_access_token = params[:accessToken]
      @account.save
    end
    render json: (@account.has_valid_fb_token? ? (@account.facebook.try(:albums)||[]).to_json(methods: [:raw_attributes]) : [])
  end

  private
  def load_account
    @account = current_user.account
  end
end