class Api::PhotosController < ApplicationController
  before_action :authenticate_user!, except: [:search, :load_thumbs_base64, :search_count, :photos_by_location, :bounds, :all_locations_by_filter, :photo_info, :all_searched_photos, :my_photo_ids]
  before_action :load_photo, only: [:update, :destroy]
  before_action :parse_search_params, only: [:bounds, :all_locations_by_filter, :search, :search_count, :photos_count_by_location, :all_searched_photos]
  before_action :parse_filtered_photos_params, only: [:index]
  before_action :load_filtered_photos, only: [:index]
  before_action :load_searched_photos, only: [:search, :search_count]

  after_action :set_search_stat, only: [:search]
  
  include Concerns::ErrorMessages
  include CleanPagination

  VIDEO_FORMATS = [ 'video/x-ms-wmv', 'application/x-mp4', 'video/mpeg', 'video/avi', 'video/quicktime', 'video/x-la-asf', 'video/x-ms-asf', 'video/x-msvideo','video/mov', 'video/x-sgi-movie', 'video/x-flv', 'flv-application/octet-stream','application/octet-stream', 'video/3gp', 'video/3gpp', 'video/3gpp2', 'video/3gpp-tt', 'video/BMPEG', 'video/BT656', 'video/CelB', 'video/DV', 'video/H261', 'video/H263', 'video/H263-1998', 'video/H263-2000', 'video/H264', 'video/JPEG', 'video/MJ2', 'video/MP1S', 'video/MP2P', 'video/MP2T', 'video/mp4', 'video/MP4V-ES', 'video/MPV', 'video/mpeg4', 'video/mpeg4-generic', 'video/nv', 'video/parityfec', 'video/pointer', 'video/raw', 'video/rtx', 'video/ogg', 'video/flv', 'video/mkv','video/divx', 'video/wmv', 'video/webm', 'video/ogv' ]
  PHOTO_FORMATS = ['image/jpg', 'image/png', 'image/gif', 'image/jpeg']

  def index
    @filter[:sort_by] ||= :date_time
    max_per_page = 500
    respond_to do |format|
      format.json do
        response.headers['Content-Disposition'] = 'attachment; filename="my_photos.txt"'
        begin
          paginate @photos.uniq.count, max_per_page do |limit, offset|
            render json: {
              photos: @photos.limit(limit).offset(offset).order(sort_string_secondary).order(sort_string_primary)
                .as_json(methods: [
                  :attachment_url, 
                  :orientation_type, 
                  :attachment_content_type, 
                  :attachment_absolute_url, 
                  :video_thumbnails, 
                  :preview_url, 
                  :formatted_date_time, 
                  # :location_name,
                  # :photographer_name,
                  # :group_name,
                  # :license_name,
                ],except: [:thumb_base64]),
              total: @photos.uniq.count,
              length: @photos.uniq.count,
              all_photo_ids: @photos.pluck(:id),
            }
          end
        rescue Exception => e
          render json: {photos: [], total: 0, length: 0}
        end
      end
    end
  end

  def photos_by_location
    respond_to do |format|
      format.json do
        location = Location.find(params[:location_id])
        photos_filter = if params[:photo_ids].present?
            ids = params[:photo_ids].try(:split, ',')
            ids.count > 0 ? ['photos.id IN (?)', ids] : []
          else
            []
          end
        begin
          render json: {
            count: location.photos.where(photos_filter).select(:id).distinct.count,
            photographer: location.user.try(:display_name), 
          }
        rescue Exception => e
          render json: {count: 0, photographer: []}
        end
      end
    end
  end

  def photos_count_by_location
    _sqnp = search_query_no_places_filter
    @photos = Photo.where(_sqnp[:media_type_filter])
      .where(_sqnp[:date_time_filters])
      .where('photos.verified = true')
      .includes(:user).where(_sqnp[:photographer_id_filters])
      .where(location_id: params[:id])
    render json: @photos.distinct.count
  end

  def search
    @filter[:sort_by] ||= :date_time
    max_per_page = 200
    respond_to do |format|
      format.json do
        response.headers['Content-Disposition'] = 'attachment; filename="search.txt"'
        begin
          paginate @photos.select(:id).count, max_per_page do |limit, offset|
            render json: {
              # photos: @photos.limit(limit).offset(offset).order(sort_string_secondary, sort_string_primary)
              photos: @photos.limit(limit).offset(offset).order(sort_string_primary)
              .as_json(methods: [
                :attachment_url, 
                :orientation_type, 
                :attachment_content_type, 
                :attachment_absolute_url, 
                :video_thumbnails, 
                :preview_url, 
                :formatted_date_time, 
              ], except: [:thumb_base64]),
              length: @photos.select(:id).count,
              all_searched_photos: @photos.select(:id).pluck(:id).join(','),
            }
          end
        rescue Exception => e
          render json: {photos: [], nearby_places: @nearby_places, all_searched_photos:[]}.as_json
        end  
      end
    end
  end

  def all_searched_photos
    respond_to do |format|
      format.json do
        response.headers['Content-Disposition'] = 'attachment; filename="all_searched_photos.txt"'

          load_searched_photos_no_places_filter
          render json: @photos_no_places_filter.as_json(methods: [
                    :attachment_url, 
                    :orientation_type, 
                    :attachment_content_type, 
                    :attachment_absolute_url, 
                    :video_thumbnails, 
                    :preview_url, 
                    :formatted_date_time, 
                  ],
                  except: [:thumb_base64]
                )
      end
    end
  end

  def search_count
    render json: {total: @photos.distinct.count}
  end

  def my_photos
    user = if current_user.admin? && session[:admin_acted_as].present?
      User.find(session[:admin_acted_as])
    else
      current_user
    end

    max_per_page = 200
    @filter ||= {}
    @filter[:sort_by] = request.headers[:HTTP_SORT_BY]||:date_time

    if @filter[:sort_by] == 'order_date'
      @photos = user.paid_photos.joins(:payment).where(payments: {verified: true}).order(:created_at)
    else
      @photos = user.paid_photos.joins(:payment).where(payments: {verified: true})
      .sort_by{
      |a|
        sort(a, @filter[:sort_by])
      }
    end

    session.delete('admin_acted_as') if session[:admin_acted_as].present?
    photos_count = @photos.count
    all_ids = @photos.map(&:id)
    paginate photos_count, max_per_page do |limit, offset|
      render json: {
        photos: @photos.slice!(offset, limit).as_json(methods: [
          :attachment_url, 
          :orientation_type, 
          :attachment_content_type,  
          :attachment_absolute_url, 
          :video_thumbnails, 
          :url, 
          :formatted_date_time, 
          :location_name,
          :photographer_name,
          :group_name,
          :license_name,
        ]),
        total: photos_count,
        length: photos_count,
        all_photo_ids: all_ids,
      }
    end
  end

  def my_photo_ids
    user = if current_user.try(:admin?) && session[:admin_acted_as].present?
      User.find(session[:admin_acted_as])
    else
      current_user
    end
    
    @photo_ids = if user.present?
      user.paid_photos.joins(:payment).where(payments: {verified: true}).distinct.pluck(:source_id) + user.photos.pluck(:id)
    else
      []
    end
    render json: @photo_ids
  end

  def share_to_fb
    succeed = false
    status = :fail
    album = nil
    message = :'Something went wrong. Please try again.'

    photos = PaidPhoto.where(id: params[:ids])

    me = FbGraph::User.me(current_user.fb_access_token)
    privacy = {
      value: params[:fb_privacy],
    }
    
    if params[:is_new_album]
      begin
        album = me.album!(
          :name => params[:fb_album_name],
          :message => 'focofox-created album',
          :privacy => privacy,
        )
      rescue Exception => e
        status = :fail
        message = e.message
      end
    end
    begin
      album ||= me.albums.select{|a| a.identifier==params[:fb_album_id]}.first
    rescue Exception => e
      status = :fail
      message = e.message
    end

    if album.present?
      begin  
        (photos||[]).each do |p|
          album.photo!(
            :access_token => current_user.fb_access_token,
            :source => open(p.attachment.path),
            :message => "Focofox #{p.file_name}",
            :privacy => privacy,
          )
        end
        succeed = true
        status = :ok
        message = :'Photo successfully shared to fb.'
      rescue Exception => e
        status = :fail
        message = [message, e.message].join(" ")
      end
    end
      
    render json: {status: status, message: message||'Photo successfully shared to fb.', new_fb_album_id: album.try(:identifier)}
  end

  def upload_to_dropbox
    dropbox = current_user.dropbox_session if current_user.dropbox_session
    dbsession = DropboxSession.deserialize(current_user.dropbox_session)
    client = DropboxClient.new(dbsession, DROPBOX_APP_MODE)

    ActiveRecord::Base.transaction do
      photos = PaidPhoto.where(id: params[:ids])
      photos.each do |p|
        resp = client.put_file("#{params[:path]}/#{p.file_name}", File.open(p.attachment.path).read)
        dropbox_status = client.metadata(params[:path])
      end
    end
    
    render json: {status: :ok, message: 'Upload to dropbox queued ...'}
  end

  def create
    groups = params[:groups].split(',') unless params[:groups].blank?
    location = params[:location]

    photo = current_user.photos.new
    # photo.category_id = category.try(:[], "id")
    content_type =  params[:files][0].content_type
    if Photo.photo_formats.include?(content_type)
      photo.photo = params[:files][0]
    else
      photo.video = params[:files][0]
    end

    photo.local_name = params[:local_name]
    photo.price = params[:price] if params[:price].present?
    photo.license_id = params[:license_id]

    dateTime = DateTime.now
    begin
      dateTime = DateTime.strptime(params[:date_time], '%m/%d/%Y %I:%M %p')
    rescue Exception => e
      
    end
    photo.date_time = dateTime
    if params[:thumbnail_size].present? && params[:thumbnail_size] != 'undefined'
      photo.thumb_width = params[:thumb_width]
      photo.thumb_height = params[:thumb_height]
    end
    photo.watermark_id = params[:watermark_id]
    
    if params[:location_id].present? && params[:location_id].to_i > 0
      photo.location_id = params[:location_id]
    elsif location.present? && location!='null'
      data = JSON.parse location
      if data['id']
        photo.location_id = data['id']
      else
        photo.location_id = current_user.locations.near([data['latitude'], data['longitude']], Location::UNIQUENESS_DISTANCE).first.try(:id)
      end
    else
      photo.location_id = current_user.default_location_id||current_user.locations.first.try(:id)
    end
    
    photo.session_names = groups if groups.present?
    photo.save
    if photo.errors.blank?
      render :json => {status: :ok, message: 'Photo successfully uploaded.', hash_key: params[:hash_key]}
    else
      render :json => {status: :db_validation_error, message: photo.errors.messages, errors: format_photo_upload_errors(photo), hash_key: params[:hash_key]}
    end
  end
 
  def update
    render :json => {status: :fail, message: 'Photo not found'} if @photo.blank?
    @photo.update_attributes!(photo_params)
    @photo.update_preview(true)
    render :json => {status: :ok, message: 'Photo successfully updated.'}
  end

  def photo_info
    begin
      @photo = Photo.includes(:location).includes(:license).includes(:user).includes(:groups).find(params[:id])
    rescue Exception => e
      @photo = PaidPhoto.includes(:location).includes(:license).includes(:photographer).includes(:groups).find(params[:id])
    end
    render json: {
      location_name: @photo.location_name,
      license_name: @photo.license_name,
      photographer_name: @photo.photographer_name,
      group_name: @photo.group_name,
      license_name: @photo.license_name,
    }
  end

  def bulk_update
    key = params[:key]
    val = case key
    when 'location'
      key = 'location_id'
      params[:val].try(:[], 'id')
    when 'price'
      params[:val] = params[:val].blank? ? 0 : params[:val]
    when 'date_time'
      DateTime.strptime(params[:val], Date::DATE_FORMATS[:classical_date_with_time])
    else
      params[:val]
    end

    photos = Photo.where(id: params[:photos])

    updates = begin
      ActiveRecord::Base.transaction do
        photos.find_each do |p| 
          p.update_attributes!({key=> val})
        end
      end

      if key == 'watermark_id'
        Thread.new do
          photos.find_each do |p|
            p.update_preview(true)
          end
          ActiveRecord::Base.connection.close
        end
      end
    rescue Exception => e
      e
    end
    status = updates.try(:message).present? ? :fail : :ok
    success_message = updates.try(:message).present? ? nil : "Photos\' <b>#{params[:key].humanize}</b> successfully updated."
    render json: {status: status, key: key, message: success_message, error: updates.try(:message)}
  end

  def bulk_delete
    Photo.bulk_delete(params[:photos], current_user.try(:id))
    if (params[:photos]||[]).count > 0
      render json: {status: :ok, message: 'Photos successfully deleted.'}
    else
      render json: {status: :ok}
    end
  end

  def bulk_delete_paid_photos
    PaidPhoto.bulk_delete(params[:photos], current_user.try(:id))
    if (params[:photos]||[]).count > 0
      render json: {status: :ok, message: 'Photos successfully deleted.'}
    else
      render json: {status: :ok}
    end
  end

  def destroy
    
  	@photo.destroy
    render :json => {status: :ok, id: params[:id], message: 'Photo successfully deleted.'}
  end

  def last_updated_data
    photo = current_user.photos.order('updated_at DESC').first
    render json: photo.as_json(methods: [:time, :date, :location])
  end

  def all_locations_by_filter
    _sqnp = search_query_no_places_filter
    locations = Location.joins(:photos)
      .where(_sqnp[:media_type_filter])
      .where(_sqnp[:date_time_filters])
      .where(_sqnp[:photographer_id_filters])
      .distinct
      .select(:id, :latitude, :longitude, :name, :user_id)
      .map do |l|
        {
          latitude: l.latitude, 
          longitude: l.longitude, 
          location_name: l.name, 
          id: l.id, 
          name: l.name, 
          text: l.name,
          user_id: l.user_id,
          photographer_id: l.user_id,
        }
      end
    render json: {nearby_places: locations}
  end

  def bounds
    _sqnp = search_query_no_places_filter
    bounds = params[:bounds]
    locations = Location.in_bounds([params[:south_west_point], params[:north_east_point]])
      .joins(:photos)
      .where(_sqnp[:media_type_filter])
      .where(_sqnp[:date_time_filters])
      .where(_sqnp[:photographer_id_filters])
      .distinct
      .select(:id, :latitude, :longitude, :name, :user_id)
      .map do |l|
        {
          latitude: l.latitude, 
          longitude: l.longitude, 
          location_name: l.name, 
          id: l.id, 
          name: l.name, 
          text: l.name,
          user_id: l.user_id,
          photographer_id: l.user_id,
        }
      end
    render json: {nearby_places: locations}
  end

  def additional_info
  end

  def load_thumbs_base64
    PhotoView.delay.bulk_thumbs_view({photo_ids: params[:ids], user_location: {latitude: params[:latitude], longitude: params[:longitude]}, current_user_id: current_user.try(:id)})
    ids = params[:ids].try(:split, ',')
    send_data Photo.where(id: ids).select(:id, :thumb_base64).map{|p| "#{p.id}-->#{p.thumb_base64}"}.join('|,|'), :type => 'text/plain',:disposition => 'inline'
  end

  private

  def set_search_stat
    account = current_user.try(:account)
    return false if account.blank?
    return false if account.try(:first_find_photos_at).present?

    account.first_find_photos_at = DateTime.current
    account.save
  end

  def load_photo
    @photo = Photo.find params[:id]
  end

  def search_query_no_places_filter
    date_time_filters = if @filter[:date_times].present?
      qs = ''
      _or = ''
      qp = []
      @filter[:date_times].each do |fd|
        begin
          df = DateTime.strptime(fd['from'], "%m/%d/%Y %I:%M %P").in_time_zone
          dt = DateTime.strptime(fd['to'], "%m/%d/%Y %I:%M %P").in_time_zone
          if df==dt
            df -=120
            dt +=120
          end
          qs += _or+"date_time BETWEEN ? AND ?"
          _or = ' OR '
          qp << df
          qp << dt
        rescue Exception => e
        end
      end
      qp.unshift(qs)
    else
      []   
    end

    photographer_id_filters = if @filter[:photographers].present?
      filter = @filter[:photographers]
      blank_ids = (filter||[]).map{|a|a['id']}
      return [] if filter.blank? || blank_ids.include?(0) || blank_ids.include?('0')

      ids = (if filter.is_a? Array
        filter.map{|p| p['id'].to_i}
      else
        [filter['id'].to_i]
      end || []).compact

      if ids.present?
        ['photos.user_id IN (?)', ids]
      else
        []
      end
    else
      []
    end

    media_type_filter = if @filter[:media_type].present?
      if @filter[:media_type] == 'videos'
        ['photos.video_content_type IN (?)', VIDEO_FORMATS]
      elsif @filter[:media_type] == 'photos'
        ['photos.photo_content_type IN (?)', PHOTO_FORMATS]
      else
        []
      end
    end

    {media_type_filter: media_type_filter, date_time_filters: date_time_filters, photographer_id_filters: photographer_id_filters}

  end

  def search_query
    _sqnp = search_query_no_places_filter
    f = (@filter[:places] || @filter[:locations] ||[]).compact
    _sqnp[:places_filter] = if f.present?    
      ids = (if f.is_a? Array
        f.map{|p| (p||{})['id'].to_i}
      else
        [(f||{})['id'].to_i]
      end || []).compact
      if ids.present?
        ['photos.location_id IN (?)', ids]
      else
        []
      end
    else
      []
    end
    _sqnp
  end

  def load_filtered_photos
    prices_filter = if @filter[:prices].present?
      qs = ''
      _or = ''
      qp = []
      @filter[:prices].each do |fp|
        qs += _or+"price BETWEEN ? AND ?"
        _or = ' OR '
        qp << fp['from'].to_i
        qp << fp['to'].to_i
      end
      
      qp.unshift(qs)
      else
        []
      end

    date_times_filter =  if @filter[:date_times].present?
        qs = ''
        _or = ''
        qp = []
        @filter[:date_times].each do |fd|
          begin
            df = DateTime.strptime(fd['from'], "%m/%d/%Y %I:%M %P")
            dt = DateTime.strptime(fd['to'], "%m/%d/%Y %I:%M %P")
            if df==dt
              df -= 7
              dt +=7
            end
            qs += _or+"date_time BETWEEN ? AND ?"
            _or = ' OR '
            qp << df
            qp << dt
          rescue Exception => e
          end
        end
        qp.unshift(qs)
      else
        [] 
      end

    locations_filter = if @filter[:locations].present?
      f = @filter[:locations].map{|l| l['id']}
      if f.present?
        ["location_id IN (?)", f]
      else
        []
      end
    else
      []
    end

    licenses_filter = if @filter[:licenses].present?
      f = @filter[:licenses].map{|l| l['id']}
      if f.present?
        ["license_id IN (?)", f]
      else
        []
      end
    else
      []
    end

    groups_filter = if @filter[:groups].present?
      qs = ''
      _or = ''
      qp = []
      @filter[:groups].each do |fl|
        qs += _or+"groups_photos.group_id = ?"
        _or = ' OR '
        qp << fl['id']
      end
      qp.unshift(qs)
    else
      []
    end

    media_type_filter = if @filter[:media_type].present?
      if @filter[:media_type] == 'videos'
        ['photos.video_content_type IN (?)', VIDEO_FORMATS]
      elsif @filter[:media_type] == 'photos'
        ['photos.photo_content_type IN (?)', PHOTO_FORMATS]
      else
        []
      end
    end

    # {media_type_filter: media_type_filter, prices_filter: prices_filter, date_times_filter: date_times_filter, licenses_filter: licenses_filter, locations_filter: locations_filter, groups_filter: groups_filter}
    @photos = current_user.photos(:thumb)
    .where(media_type_filter)
    .where(prices_filter)
    .where(date_times_filter)
    .includes(:license).where(licenses_filter)
    .includes(:location).where(locations_filter)
    .where('photos.verified = true')
    .joins(:groups).where(groups_filter)
  end
  
  def photo_params
    param = params.permit(
      :price, :license_id, :gps_location, :watermark_id, :location_id
    )

    begin
      dateTime = DateTime.strptime(params[:date_time], "%m/%d/%Y %I:%M %P")
    rescue Exception => e
    end

    {date_time: dateTime}.merge param
  end

  def load_searched_photos
    _sq = search_query
    @photos = Photo.where(_sq[:media_type_filter])
      .where(_sq[:date_time_filters])
      .includes(:user).where(_sq[:photographer_id_filters])
      .includes(:location).where(_sq[:places_filter])
      .where('photos.verified = true')
      .distinct
  end

  def load_searched_photos_no_places_filter
    _sqnp = search_query_no_places_filter
    @photos = Photo.where(_sqnp[:media_type_filter])
      .where(_sqnp[:date_time_filters])
      .where('photos.verified = true')
      .includes(:user).where(_sqnp[:photographer_id_filters])
  end

  def load_nearby_places
    @nearby_places = []
    _sqnp = search_query_no_places_filter
    location_ids = Photo.where(_sqnp[:media_type_filter])
      .where(_sqnp[:date_time_filters])
      .where('photos.verified = true')
      .includes(:user).where(_sqnp[:photographer_id_filters])
      .try(:pluck, 'location_id').uniq.compact
    
    return [] if location_ids.blank?

    searched_locations = (
      locs = nil
      i = -1
      while (locs.blank? && i < location_ids.length) do
        i += 1
        begin
          l = Location.find(location_ids[i])
        rescue Exception=>e
          next
        end 
        locs = l.nearbys(1000).where(id: location_ids).limit(50).uniq
      end || Location.where(id: location_ids).limit(50).uniq
    ).includes(:user)
    .includes(:photos)
    # .select('count(photos.id) as photos_count')

    locations = searched_locations.map do |l|
      {
        latitude: l['latitude'], 
        longitude: l['longitude'], 
        photographer: l.user.try(:display_name), 
        location_name: l.name, 
        id: l.id, 
        name: l.name, 
        text: l.name,
      }
    end
    @nearby_places = locations
  end

  def sort(obj, by)
    if by.to_sym == :date_time
      [obj.public_send(by), obj.location_name]
    else
      [obj.public_send(by), obj.date_time]
    end
  end

  def sort_string_primary
    @filter[:sort_by]
  end

  def sort_string_secondary
    if @filter[:sort_by] == :date_time
      'locations.name ASC'
    else
      'date_time ASC'
    end
  end

  def parse_filtered_photos_params
    @filter = {}
    req = request.headers
    @filter[:date_times] = params.keys.include?('date_times') ? params[:date_times] : (JSON.parse(req[:HTTP_DATE_TIMES]) if req[:HTTP_DATE_TIMES].present?)
    @filter[:groups] = params.keys.include?('groups') ? params[:groups] : (JSON.parse(req[:HTTP_GROUPS]) if req[:HTTP_GROUPS].present?)
    @filter[:licenses] = params.keys.include?('licenses') ? params[:licenses] : (JSON.parse(req[:HTTP_LICENSES]) if req[:HTTP_LICENSES].present?)
    @filter[:locations] = params.keys.include?('locations') ? params[:locations] : (JSON.parse(req[:HTTP_LOCATIONS]) if req[:HTTP_LOCATIONS].present?)
    @filter[:prices] = params.keys.include?('prices') ? params[:prices] : (JSON.parse(req[:HTTP_PRICES]) if req[:HTTP_PRICES].present?)
  
    if params.keys.include?('date_times')
      @filter[:date_times] = params[:date_times].is_a?(String) ? JSON.parse(params[:date_times]) : params[:date_times]
    elsif req[:HTTP_DATE_TIMES].present?
      @filter[:date_times] = req[:HTTP_DATE_TIMES].is_a?(String) ? JSON.parse(req[:HTTP_DATE_TIMES]) : req[:HTTP_DATE_TIMES]
    end

    if params.keys.include?('locations')
      @filter[:locations] = params[:locations].is_a?(String) ? JSON.parse(params[:locations]) : params[:locations]
    elsif req[:HTTP_LOCATIONS].present?
      @filter[:locations] = req[:HTTP_LOCATIONS].is_a?(String) ? JSON.parse(req[:HTTP_LOCATIONS]) : req[:HTTP_LOCATIONS]
    end

    if params.keys.include?('groups')
      @filter[:groups] = params[:groups].is_a?(String) ? JSON.parse(params[:groups]) : params[:groups]
    elsif req[:HTTP_GROUPS].present?
      @filter[:groups] = req[:HTTP_GROUPS].is_a?(String) ? JSON.parse(req[:HTTP_GROUPS]) : req[:HTTP_GROUPS]
    end

    if params.keys.include?('licenses')
      @filter[:licenses] = params[:licenses].is_a?(String) ? JSON.parse(params[:licenses]) : params[:licenses]
    elsif req[:HTTP_LICENSES].present?
      @filter[:licenses] = req[:HTTP_LICENSES].is_a?(String) ? JSON.parse(req[:HTTP_LICENSES]) : req[:HTTP_LICENSES]
    end

    if params.keys.include?('prices')
      @filter[:prices] = params[:prices].is_a?(String) ? JSON.parse(params[:prices]) : params[:prices]
    elsif req[:HTTP_PRICES].present?
      @filter[:prices] = req[:HTTP_PRICES].is_a?(String) ? JSON.parse(req[:HTTP_PRICES]) : req[:HTTP_PRICES]
    end

    if params.keys.include?('sort_by')
      @filter[:sort_by] = params[:sort_by]
    elsif req[:HTTP_SORT_BY].present?
      @filter[:sort_by] = req[:HTTP_SORT_BY]
    end

    if params.keys.include?('media_type')
      @filter[:media_type] = params[:media_type]
    elsif req[:HTTP_MEDIA_TYPE].present?
      @filter[:media_type] = req[:HTTP_MEDIA_TYPE]
    end
  end

  def parse_search_params
    @filter = {}
    req = request.headers
    if params.keys.include?('date_times')
      @filter[:date_times] = params[:date_times].is_a?(String) ? JSON.parse(params[:date_times]) : params[:date_times]
    elsif req[:HTTP_DATE_TIMES].present?
      @filter[:date_times] = req[:HTTP_DATE_TIMES].is_a?(String) ? JSON.parse(req[:HTTP_DATE_TIMES]) : req[:HTTP_DATE_TIMES]
    end

    if params.keys.include?('locations')
      @filter[:locations] = params[:locations].is_a?(String) ? JSON.parse(params[:locations]) : params[:locations]
    elsif req[:HTTP_LOCATIONS].present?
      @filter[:locations] = req[:HTTP_LOCATIONS].is_a?(String) ? JSON.parse(req[:HTTP_LOCATIONS]) : req[:HTTP_LOCATIONS]
    end

    if params.keys.include?('places')
      @filter[:places] = params[:places].is_a?(String) ? JSON.parse(params[:places]) : params[:places]
    elsif req[:HTTP_PLACES].present?
      @filter[:places] = req[:HTTP_PLACES].is_a?(String) ? JSON.parse(req[:HTTP_PLACES]) : req[:HTTP_PLACES]
    end

    if params.keys.include?('photographers')
      @filter[:photographers] = params[:photographers].is_a?(String) ? JSON.parse(params[:photographers]) : params[:photographers]
    elsif req[:HTTP_PHOTOGRAPHERS].present?
      @filter[:photographers] = req[:HTTP_PHOTOGRAPHERS].is_a?(String) ? JSON.parse(req[:HTTP_PHOTOGRAPHERS]) : req[:HTTP_PHOTOGRAPHERS]
    end

    if params.keys.include?('groups')
      @filter[:groups] = params[:groups].is_a?(String) ? JSON.parse(params[:groups]) : params[:groups]
    elsif req[:HTTP_GROUPS].present?
      @filter[:groups] = req[:HTTP_GROUPS].is_a?(String) ? JSON.parse(req[:HTTP_GROUPS]) : req[:HTTP_GROUPS]
    end

    if params.keys.include?('licenses')
      @filter[:licenses] = params[:licenses].is_a?(String) ? JSON.parse(params[:licenses]) : params[:licenses]
    elsif req[:HTTP_LICENSES].present?
      @filter[:licenses] = req[:HTTP_LICENSES].is_a?(String) ? JSON.parse(req[:HTTP_LICENSES]) : req[:HTTP_LICENSES]
    end

    if params.keys.include?('prices')
      @filter[:prices] = params[:prices].is_a?(String) ? JSON.parse(params[:prices]) : params[:prices]
    elsif req[:HTTP_PRICES].present?
      @filter[:prices] = req[:HTTP_PRICES].is_a?(String) ? JSON.parse(req[:HTTP_PRICES]) : req[:HTTP_PRICES]
    end

    if params.keys.include?('sort_by')
      @filter[:sort_by] = params[:sort_by]
    elsif req[:HTTP_SORT_BY].present?
      @filter[:sort_by] = req[:HTTP_SORT_BY]
    end

    if params.keys.include?('media_type')
      @filter[:media_type] = params[:media_type]
    elsif req[:HTTP_MEDIA_TYPE].present?
      @filter[:media_type] = req[:HTTP_MEDIA_TYPE]
    end
  end
end

