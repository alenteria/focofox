class Api::StatisticsController < ApplicationController
	before_action :authenticate_user!
	before_action :verify_photographer, only: [:photos_viewed_vs_purchased]

	def photos_viewed_vs_purchased
		photos_previewed = if params[:system_wide].present?
			PhotoView.select(:count, :created_at).group_by{|s|s.created_at.to_date.to_s(:classical_date)}
		else
			current_user.photo_views.select(:count, :created_at).group_by{|s|s.created_at.to_date.to_s(:classical_date)}
		end

		photos_purchased = if params[:system_wide].present?
			PaidPhoto.select(:created_at).group_by{|s|s.created_at.to_s(:classical_date)}
		else
			current_user.sold_photos.select(:created_at).group_by{|s|s.created_at.to_s(:classical_date)}
		end
		render json: {photos_previewed: photos_previewed, photos_purchased: photos_purchased, dates: (photos_previewed.keys+photos_purchased.keys).uniq}
	end

	private
end