class Api::EventsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy]

  before_action :parse_filter_params, only: [:index]
  before_action :load_events, only: [:index]

  include CleanPagination
  
  def index
  	max_per_page = 500
    if @events.length > 0
      paginate @events.uniq.count, max_per_page do |limit, offset|
        render json: @events.limit(limit).offset(offset).as_json(methods: [:location_name])
      end
    else
    	render json: []
    end
  end

 	def create
 		event = Event.create(
 			user_id: current_user.id,
 			location_id: params[:location].try(:[], 'id'),
 			date: Date.strptime(params[:date], "%m/%d/%y"),
 			time_from: Time.strptime(params[:time_from], "%I:%M %P"),
 			time_to: Time.strptime(params[:time_to], "%I:%M %P"),
 			description: params[:description]
 		)
 		render json: {status: :ok, message: 'Event successfully created.', data: event}
 	end

  private
  def load_events
    date_times_filter =  if @filter[:date_times].present?
        qs = ''
        _or = ''
        qp = []
        @filter[:date_times].each do |fd|
          begin
            df = DateTime.strptime(fd['from'], "%m/%d/%Y %I:%M %P")
            dt = DateTime.strptime(fd['to'], "%m/%d/%Y %I:%M %P")
            if df==dt
              df -= 7
              dt +=7
            end
            qs += _or+"date BETWEEN ? AND ?"
            _or = ' OR '
            qp << df
            qp << dt
          rescue Exception => e
          end
        end
        qp.unshift(qs)
      else
        [] 
      end

    locations_filter = if @filter[:locations].present?
        f = @filter[:locations].map{|l| l['id']}
        if f.present?
          ["location_id = ?", f]
        else
          []
        end
      else
        []
      end

    @events = current_user.events
      .where(date_times_filter)
      .where(locations_filter)
  end

  def parse_filter_params
    @filter = {}
    req = request.headers
    if params.keys.include?('date_times')
      @filter[:date_times] = params[:date_times].is_a?(String) ? JSON.parse(params[:date_times]) : params[:date_times]
    elsif req[:HTTP_DATE_TIMES].present?
      @filter[:date_times] = req[:HTTP_DATE_TIMES].is_a?(String) ? JSON.parse(req[:HTTP_DATE_TIMES]) : req[:HTTP_DATE_TIMES]
    end

    if params.keys.include?('locations')
      @filter[:locations] = params[:locations].is_a?(String) ? JSON.parse(params[:locations]) : params[:locations]
    elsif req[:HTTP_LOCATIONS].present?
      @filter[:locations] = req[:HTTP_LOCATIONS].is_a?(String) ? JSON.parse(req[:HTTP_LOCATIONS]) : req[:HTTP_LOCATIONS]
    end

    if params.keys.include?('places')
      @filter[:places] = params[:places].is_a?(String) ? JSON.parse(params[:places]) : params[:places]
    elsif req[:HTTP_PLACES].present?
      @filter[:places] = req[:HTTP_PLACES].is_a?(String) ? JSON.parse(req[:HTTP_PLACES]) : req[:HTTP_PLACES]
    end

    if params.keys.include?('sort')
      @filter[:sort] = params[:sort]
    elsif req[:HTTP_SORT].present?
      @filter[:sort] = req[:HTTP_SORT]
    end

    if params.keys.include?('sort')
      @filter[:sort_type] = params[:sort_type]
    elsif req[:HTTP_SORT_TYPE].present?
      @filter[:sort_type] = req[:HTTP_SORT_TYPE]
    end
  end
 end