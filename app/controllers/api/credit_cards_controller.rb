class Api::CreditCardsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_credit_card, only: [:update, :destroy, :mark_default]
  before_action :load_payment_method, only: [:create, :mark_default, :update, :destroy]

  def create
    card_attrs = card_params.to_h
    card_attrs['is_default'] = @payment_method.credit_cards.count <=0 && @payment_method.paypal_accounts.count <=0

    card = @payment_method.credit_cards.new(card_attrs)
    vault = card.set_cc_attributes({
      type: params[:card_type],
      number: params[:number],
      expire_month: params[:expire_month],
      expire_year: params[:expire_year],
      first_name: params[:first_name],
      last_name: params[:last_name],
      cvv2: params[:ccv],
      :billing_address => {
        :line1 => params[:address1],
        :line2 => params[:address2],
        :city => params[:city],
        :state => params[:address_state],
        :postal_code => params[:postal_code],
        :country_code => params[:country_code],
      }
    })

    if vault[:status] == :ok
      if card.save
        render json: {status: :ok, message: 'Card added successfully!', card: card.as_json(methods: [:address1, :address2, :city, :address_state, :postal_code, :expired?])}
      else
        render json: {status: :db_validation_error, message: card.errors.messages}
      end
    else
      message = vault[:message] || "Can't add card!"
      render json: {status: :fail, message: message}
    end
  end

  def update
    @credit_card.update_attributes(updateable_card_attr)
    # @credit_card.update_paypal_card({
    #   expire_month: params[:expire_month].to_i,
    #   expire_year: params[:expire_year].to_i,
    #   first_name: params[:first_name],
    #   last_name: params[:last_name],
    #   line1: params[:address1],
    #   line2: params[:address2],
    #   city: params[:city],
    #   postal_code: params[:postal_code],
    #   state: params[:address_state],
    # })
    render json: {status: :ok, message: :'Credit card updated!'}
  end

  def destroy
    is_default = @credit_card.is_default?
    @credit_card.destroy
    next_default = nil
    if is_default
      next_default = (@payment_method.credit_cards.first || @payment_method.paypal_accounts.first)
      next_default.update_attribute(:is_default, true)
    end
    render json: {status: :ok, message: :'Credit card deleted!', type: next_default.try(:class).try(:name), data: next_default}
  end

  def mark_default
    @payment_method.credit_cards.update_all(is_default: false) if @payment_method.credit_cards.present?
    @payment_method.paypal_accounts.update_all(is_default: false) if @payment_method.paypal_accounts.present?
    @credit_card.update_attribute(:is_default, true)
    render json: {status: :ok, message: 'Card marked as default.'}
  end

  private
  def card_params
    params.permit(:number, :expire_month, :expire_year, :card_type, :first_name, :last_name, :address1, :address2, :city, :address_state, :postal_code, :country_code)
  end

  def updateable_card_attr
    params.permit(:first_name, :last_name, :address1, :address2, :city, :address_state, :postal_code)
  end

  def load_credit_card
    session[:active_tab] = params[:method_type]
    @credit_card = CreditCard.find(params[:id])
  end

  def load_payment_method
    @payment_method = PaymentMethod.find_or_create_by(method_type: (params[:method_type]||'send').to_sym, account_id: current_user.account.try(:id))
  end
end
