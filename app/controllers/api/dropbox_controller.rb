class Api::DropboxController < ApplicationController
  require 'dropbox_sdk'
    
  before_filter :load_dropbox_data, only: [:callback]

  def authorize
    dbsession = DropboxSession.new(DROPBOX_APP_KEY, DROPBOX_APP_KEY_SECRET)
    #serialize and save this DropboxSession
    session[:dropbox_session] = dbsession.serialize
    #pass to get_authorize_url a callback url that will return the user here
    redirect_to dbsession.get_authorize_url url_for(:action => 'callback', domain: request.host)
  end
   
  # @Params : None
  # @Return : None
  # @Purpose : To callback for dropbox authorization
  def callback
    render :inline => "<%= javascript_tag 'window.opener.dropbox_callback(#{@folders.to_json});window.close();'%>"
  end # end of dropbox_callback action

  def unauthorize
    session[:dropbox_session] = nil
    current_user.dropbox_session = nil
    current_user.save!
  end

  protected
  def load_dropbox_data
    dbsession = DropboxSession.deserialize(session[:dropbox_session])
    dbsession.get_access_token #we've been authorized, so now request an access_token
    session[:dropbox_session] = dbsession.serialize
    current_user.update_attributes(:dropbox_session => session[:dropbox_session])
    session.delete :dropbox_session

    @client = DropboxClient.new(dbsession, DROPBOX_APP_MODE)
    dropbox_root_path = @client.metadata('/')
    @folders = (dropbox_root_path.try(:[], 'contents')||[]).map do |d|
      if d['is_dir']
        path = d['path'].tr('/', '')
        {path: path, name: path}
      else
        nil
      end
    end.reject{|f|f.try(:empty?)}
  end
end