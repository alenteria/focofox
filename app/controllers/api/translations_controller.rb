class Api::TranslationsController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin

  def index
    data = {
      locale: I18n.locale,
      key: params[:key],
      content: I18n.t(params[:key])
    }
    render json: data
  end

  def update
    I18n.backend.store_translations(params[:locale], {params[:key] => params[:content]}, :escape => false)
    render json: {status: :ok}
  end

  protected
  def verify_admin
    unless current_user.admin?
      render json: "not authorized!", status: 401
    end
  end
end