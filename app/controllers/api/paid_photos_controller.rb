class Api::PaidPhotosController < ApplicationController
  before_action :authenticate_user!
  before_action :load_photo, only: [:photo_info]

  def photo_info
    render json: {
      location_name: @paid_photo.location_name,
      license_name: @paid_photo.license_name,
      photographer_name: @paid_photo.photographer_name,
      license_name: @paid_photo.license_name,
    }
  end

  private
  def load_photo
    @paid_photo = PaidPhoto.find params[:id]
  end
end