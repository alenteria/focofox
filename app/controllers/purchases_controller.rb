class PurchasesController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @payments = current_user.payments
  end

  def show
    current_user.payments.find(params[:id])
  end
end

