class PhotographerController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :load_photo, only: [:edit, :destroy, :show, :video_player, :generate_preview]
  before_action :verify_photographer, except: [:show]

  def index
  end
end