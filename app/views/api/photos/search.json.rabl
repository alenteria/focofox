object false
child @photos => :photos do
  attributes :all
end

child @nearby_places => :nearby_places do
  attributes :all
end