       _____                 _           
      |  ___|__    __ __   / _|  _ _    _
      | |_ / _ \ / __/ _ \| |_ / _ \ \/ /
      |  _| (_) | (_| (_) |  _| (_) >  < 
      |_|  \___/ \___\___/|_|  \___/_/\_\
                                        
               O  [/]                 O /
              /\ /                 ---|       
             |\ `                    /\       
             | \,                   |  \      
     ^^^^^^^^`^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

Ruby version: ruby 2.1.3p242

Rails version: Rails 4.1.6

Provision and Start the Box

Now move to the project directory where the Vagrantfile and your application code will live. Then:
```
vagrant init focofox_box
vagrant up --provision
```

Possible problems

https://github.com/mitchellh/vagrant/issues/5199
``
Shared folders that Chef requires are missing on the virtual machine.
This is usually due to configuration changing after already booting the
machine. The fix is to run a `vagrant reload` so that the proper shared
folders will be prepared and mounted on the VM.
``
you need to do
```
remove .vagrant/machines/default/virtualbox/synced_folders
vagrant reload --provision
```

To run standard rails commands (like migrating the database, bundle install, etc)
you'll need to ssh into the instance:

```
vagrant ssh
```

On a Windows environment you may need to setup an ssh environment and point to the
Vagrant ssh key (you can find the location of this key by running "vagrant ssh-config"
and using the path specified for IdentityFile)

After connecting by ssh, switch to the /vagrant directory:

```
cd /vagrant
```

<!-- Due to how paths are setup in Ubuntu, the easiest way to run Rails command line commands
is to prefix wtih bin/:

```
bin/rake db:migrate
bin/rails s
etc ...
``` -->


To get started, run the command in ssh to setup the app, database, and start the
Rails server:

```
cd /vagrant
bundle install
rake db:create
rake db:migrate
rake db:seed
rails s
```

You should then be able to access the site via http://localhost:3000 (in the host)
