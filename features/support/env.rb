require 'cucumber/rails'

ActionController::Base.allow_rescue = false

require 'capybara'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara-webkit'
require_relative 'fixture_access'

Capybara.default_driver = :rack_test
Capybara.javascript_driver = :webkit
Capybara.default_selector = :css
Capybara.always_include_port = true
Capybara.server_port = 4000

World(FixtureAccess)
World(Capybara)

ActiveRecord::FixtureSet.reset_cache
fixtures_folder = File.join(Rails.root, 'spec', 'fixtures')
fixtures = Dir[File.join(fixtures_folder, '*.yml')].map {|f| File.basename(f, '.yml') }
fixtures += Dir[File.join(fixtures_folder, '*.csv')].map {|f| File.basename(f, '.csv') }
ActiveRecord::FixtureSet.create_fixtures(fixtures_folder, fixtures)    # This will populate the test database tables

begin
  DatabaseCleaner.strategy = :transaction
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end
Cucumber::Rails::Database.javascript_strategy = :truncation

