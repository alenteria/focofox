# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "3024", "--natdnshostresolver1", "on"]
  end

  config.vm.network :forwarded_port, guest: 3000, host: 3000

  config.berkshelf.enabled = true

  config.berkshelf.berksfile_path = 'Berksfile'

  config.vm.provision :shell, path: "bootstrap.sh"

  # Use Chef Solo to provision our virtual machine  
  config.vm.provision :chef_solo do |chef|

    chef.add_recipe "apt"
    chef.add_recipe "nodejs"
    chef.add_recipe "ruby_build"
    chef.add_recipe "rbenv::user"
    chef.add_recipe "rbenv::vagrant"
    chef.add_recipe "vim"
    chef.add_recipe "postgresql::client"
    chef.add_recipe "postgresql::server"
    chef.add_recipe "imagemagick"

    chef.json = {
      rbenv: {
        user_installs: [{
          user: "vagrant",
          rubies: ["2.1.3"],
          global: "2.1.3",
          gems: {
            "2.1.3" => [
              { name: "bundler" }
            ]
          }
        }]
      },
      postgresql: {
        version: '9.3',
        password: {postgres: "vagrant"}
        # pg_hba: [{
        #   type: 'host',
        #   db: 'all',
        #   user: 'postgres',
        #   addr: nil,
        #   method: 'password'
        # }]
      }
    }
  end

end
