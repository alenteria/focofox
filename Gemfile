source 'https://rubygems.org'

gem 'rails',                                        '4.1.6'
gem 'unicorn',                                      '4.6.3'
gem 'pg',                                           '0.18.2'
gem 'sass-rails',                                   '4.0.3'
gem 'less-rails'
gem 'uglifier',                                     '1.3.0'
gem 'coffee-rails',                                 '4.0.0'
gem 'therubyracer',                                 '0.12.2',         platforms: :ruby
gem 'jbuilder',                                     '2.0'
gem 'sdoc',                                         '0.4.0',          group: :doc
gem 'spring',                                       '1.3.6',          group: :development
gem "font-awesome-rails"
gem "devise",                                       '3.5.1'
gem 'devise-bootstrap-views'
gem "cancan"
gem 'angularjs-rails',                              '1.3.8'
gem 'angular-ui-bootstrap-rails'
gem 'omniauth'
gem 'omniauth-twitter'
gem 'omniauth-facebook'
gem 'omniauth-linkedin'
gem "omniauth-google-oauth2"
gem "paperclip",                                    '4.2.1'
gem "rmagick",                                      '2.15.0'
gem "ImageResize",                                  '0.0.5'
gem 'haml-rails'
gem 'formatted_form'
gem 'underscore-rails',                             '1.8.2'
gem 'exifr'
gem 'mini_exiftool',                                 '2.0.0'
gem 'papercrop',                                    '0.2.0'
gem 'dynattribs'
gem 'clean_pagination'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem "twitter-bootstrap-rails",                      '3.2.0'
gem 'geocoder'
gem 'active_link_to'
gem "cocaine"
gem 'paperclip-av-transcoder',                      '0.6.4'
gem 'utf8-cleaner',                                 '0.0.9'
gem "airbrake"
gem 'headless'
gem 'paperclip-meta',                               '1.2.0'
gem 'whenever',                                     '0.9.4',         :require => false
gem 'curb'
gem 'credit_card_validator'
gem 'angular-rails-templates'
gem 'htmlcompressor'
gem 'fb_graph'
gem 'rubyzip',                                      '1.1.7',         :require => 'zip'
gem 'redis',                                        '3.2.1'
gem 'i18n-active_record',                           '0.0.2',
      :git => 'git://github.com/svenfuchs/i18n-active_record.git',
      :require => 'i18n/active_record'
gem 'globalize'
gem 'globalize-accessors'
gem 'subdomain_locale'
gem 'attr_encrypted'
gem "dropbox-sdk"
gem "password_strength"
gem 'angular_rails_csrf',                            '1.0.4'
gem 'randexp'
gem 'faker'
gem 'geokit-rails'
gem 'delayed_job_active_record'
gem "daemons"
gem "delayed_paperclip"
gem 'devise-two-factor'
gem 'rqrcode-rails3'
gem 'mini_magick'

group :development, :staging do
  gem 'ruby-prof'
  gem "better_errors"
end
  

group :development do
  # gem 'flamegraph'
  # gem 'rack-mini-profiler'
  gem 'rails-footnotes', '~> 4.0'
  gem 'i18n-tasks', '~> 0.8.7'
end

group :development, :test do
  gem 'jazz_hands', github: 'nixme/jazz_hands', branch: 'bring-your-own-debugger'
  gem 'pry-byebug',                                  '1.3.2'  
  gem 'launchy'
  gem 'rb-readline',                                 '0.5.2'
  gem 'awesome_print'
  gem 'cucumber-rails', require: false
  gem "capybara-webkit", require: false
  gem 'capybara'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'konacha'
  gem 'poltergeist',                                 '1.6.0'
  gem "teaspoon",                                    '1.0.2',         require: false
  gem 'phantomjs',                                   '1.9.8.0'
  gem "mocha"
  gem "selenium-webdriver",                          '2.45.0'
  gem 'rb-fsevent',                                  '0.9.5'
  gem 'thin'
  gem 'formatted_rails_logger'
  gem "git"
end

group :staging do
  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rails', '~> 1.1.1'
  gem 'capistrano-rbenv', github: "capistrano/rbenv"
end

group :development, :test, :staging do
  gem 'rspec-rails',                                 '3.0.0'
end