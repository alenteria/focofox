#!/usr/bin/env bash

apt-get update 
apt-get install ImageMagick -y
apt-get install libmagickwand-dev -y
apt-get install libssl-dev -y
apt-get install g++ -y
apt-get install libimage-exiftool-perl -y
apt-get install libqt4-dev -y
apt-get install libav-tools -y
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install nodejs -y
sudo npm install -g phantomjs
sudo apt-get install -y libreadline-dev
sudo apt-get install libcurl3 libcurl3-gnutls libcurl4-openssl-dev -y
sudo apt-get install --reinstall wamerican -y
sudo apt-get install trimage -y
sudo npm install -g qrcode-terminal
sudo npm install -g bower
sudo apt-get install vnstat vnstati -y
apt-get autoremove -y

sudo cp -f /vagrant/webrick_config.rb /usr/lib/ruby/1.9.1/webrick/config.rb