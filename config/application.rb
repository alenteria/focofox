require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsApp
  class Application < Rails::Application
    config.middleware.use Rack::Deflater
    config.time_zone = 'UTC'
    config.threadsafe = true
    config.angular_templates.htmlcompressor = true
    config.assets.register_engine '.haml', Tilt::HamlTemplate
    config.to_prepare do
        Devise::SessionsController.layout proc{ |controller| action_name == 'new' ? "application"   : "application" }
    end

    # I18n library now recommends you to enforce available locales.
    config.i18n.enforce_available_locales = true
    config.i18n.available_locales = :en, :es

    # See "Configuration" for difference between these:
    config.default_locale = :en
    config.i18n.default_locale = :en
  end
end
