# require 'socket'
# ip_address = Socket.ip_address_list.find { |ai| ai.ipv4? && !ai.ipv4_loopback? }.ip_address

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.allow_concurrency = true
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.compile = true
  config.assets.debug = false
  config.assets.compress = true
  # config.action_controller.asset_host = "http://#{ip_address}:3000"

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  config.app_domain = 'focofox.com'

  # Email
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    :user_name => "brice@focofox.com",
    :password  => "IwkSUu_ztFjJgCmopZ14UQ", # SMTP password is any valid API key
    :authentication => 'login', # Mandrill supports 'plain' or 'login'
    :domain => 'focofox.com', # your domain to identify your server when connecting
  }

  config.action_dispatch.perform_deep_munge = true
end

Devise.setup do |config|
  config.omniauth :facebook, Rails.application.secrets.fb_app_id, Rails.application.secrets.fb_app_secret, {:scope => 'email, publish_actions,user_photos,user_videos'}
  config.omniauth :twitter, "IJ2vRvOsdQLiHDEGE8VRQMD8j", "Td4i7AHkSbaLLdCpF0QzSmBeLZ568hFRayUrc8jWQULo33BWTi"
  config.omniauth :linkedin, "759sv8qextn51y", "UwOND2c2Wypp9yxj"
  config.omniauth :google_oauth2, "214296151219-s7mp8gkgiq96lmt4n4maqhebrhs9lr42.apps.googleusercontent.com", "dIlm0zbBE3V_tKLGyRZPa5Iv"
end


# Multithreaded webrick
# Remove Rack::Lock so WEBrick can be fully multi-threaded.
require 'rails/commands/server'

class Rails::Server
  def middleware
    middlewares = []
    middlewares << [Rails::Rack::Debugger] if options[:debugger]
    middlewares << [::Rack::ContentLength]

    Hash.new middlewares
  end
end
