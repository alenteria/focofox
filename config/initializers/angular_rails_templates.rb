# config/initializers/angular_rails_templates.rb

module CustomHamlEngine
  class HamlTemplate < Tilt::HamlTemplate
    def evaluate(scope, locals, &block)
      scope.class_eval do
        include Rails.application.routes.url_helpers
        include Rails.application.routes.mounted_helpers
        include ActionView::Helpers
        include ApplicationHelper
        include ActionView::Context
      end

      super
    end
  end
end

Tilt.register CustomHamlEngine::HamlTemplate, '.haml'
