Delayed::Worker.delay_jobs = !(Rails.env.test? || Rails.env.development?)
Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'dj.log'))
Delayed::Worker.destroy_failed_jobs = true

# if Rails.env.production?
# 	system("RAILS_ENV=production bin/delayed_job start") unless system("RAILS_ENV=production bin/delayed_job status")
# end
