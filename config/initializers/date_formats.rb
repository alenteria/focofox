CUSTOM_DATETIME_FORMATS = {
  :classical_date           => "%m/%d/%Y",
  :classical_date_with_time => "%m/%d/%Y %I:%M %p",
  :short_date               => "%b %d, %Y",
  :short_date_without_comma => "%b %d %Y",
  :short_date_with_time          => '%b %d, %Y %I:%M %p',
  :standard_time            => "%I:%M %p",
  :short_with_time_no_year  => '%b %d %I:%M %p'
}

[ Date::DATE_FORMATS,
  Time::DATE_FORMATS
].each do |h|
  h.merge!(CUSTOM_DATETIME_FORMATS)
end
