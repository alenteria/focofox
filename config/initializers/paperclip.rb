require 'paperclip/media_type_spoof_detector'
Paperclip.interpolates :uuid do |attachment, style| 
  attachment.instance.uuid 
end

Paperclip.register_processor(:bulk, Paperclip::Bulk)

Paperclip.options[:command_path] = "/usr/bin"

module Paperclip
  class MediaTypeSpoofDetector
    def spoofed?
      false
    end
  end
end
