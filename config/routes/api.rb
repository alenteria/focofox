namespace :api, path: '/api' do
  resources :photos do
    member do
      get :photo_info
    end
    collection do
      get :index
      get :last_updated_data
      post :filtered_count
      get :search
      post :all_searched_photos
      post :search
      get :search_count
      post :search_count
      post :bounds
      post :all_locations_by_filter
      post :photos_by_location
      post :photos_count_by_location
      put :bulk_update
      post :bulk_delete
      post :bulk_delete_paid_photos
      get :my_photos
      post :share_to_fb
      post :upload_to_dropbox
      post :load_thumbs_base64
      get :my_photo_ids
    end
  end
  resources :paid_photos do
    member do
      get :photo_info
    end
  end
  resources :watermarks do
    collection do
      get :index
      get :last_used_watermark
      post :verify_name
    end
    post :mark_default_watermark
  end

  resources :credit_cards, only: [:create, :index, :update, :destroy] do
    member do
      put :mark_default
    end
    collection do
    end
  end

  resources :photographers do
    collection do
      get :search
    end
  end

  resources :locations do
    collection do
      get :index
      post :verify_gps
      post :verify_name
      get :search
    end
    post :mark_default_location
  end

  resources :image_assets do
  end

  resources :photographer_preferences, only: [:update_default_photo_price] do
    collection do
      put :update_default_photo_price
      put :update_thumbnail_size
      put :update_per_page_display
      get :default_configs

      post :disable_photo_resizing
      put :update_photo_resizing

    end
  end

  resources :licenses do
    put :mark_default
    collection do
      get :search
    end
  end

  resources :users do
    collection do
      post :verify_login_credentials
      post :verify_email
      post :verify_username
      post :verify_password
      post :verify_otp
    end
  end

  resources :groups, only: [:create, :index] do
    collection do
      get :search
    end
  end

  resources :payments, only: [:create, :show] do
    collection do
      get :index
    end

    member do
      get :photos
      get :success
      get :cancelled
    end
  end

  resources :account do
    collection do
      post :fb_albums
      get :fb_albums
      get :dropbox_authorize_url
    end
  end

  resources :translations do
    collection do
      post :update
    end
  end

  resources :paypal_accounts do
    member do
      post :mark_default
    end
  end

  resources :sales do
    member do
      get :details
    end
    collection do
      get :history
      post :sort_history
      get :fetch_sales_stat
    end
  end

  resources :events do
  end

  resources :photo_views do
    collection do
      post :add
      get :stat_vs_purchased
    end
  end

  resources :location_views do
    collection do
      post :add
      get :my_stats
    end
  end

  resources :statistics do
    collection do
      get :photos_viewed_vs_purchased
    end
  end

  get '/dropbox_authorize' => 'dropbox#authorize', as: 'dropbox_authorize'
  get '/dropbox_unauthorize' => 'dropbox#unauthorize', as: 'dropbox_unauthorize'
  get '/dropbox_path_change' => 'dropbox#path_change', as: 'dropbox_path_change'
  get '/dropbox_callback' => 'dropbox#callback', as: 'dropbox_callback'
  get '/dropbox_download' => 'dropbox#download', as: 'dropbox_download'
  post '/dropbox_upload' => 'dropbox#upload', as: 'upload'
  post '/dropbox_search' => 'dropbox#search', as: 'search'

end