namespace :users, path: '/u' do
	resources :search, only: [:index] do
		collection do
			get :index
		end
	end
end

resources :payment_methods, only: [:index, :destroy] do
  collection do
    post :update
  end
end

resources :checkout do
  collection do
    post :save_cart
    post :create
    post :review_items
    get :review_items
    get :addresses
    get :success
  end
end

resources :purchases do
  collection do
    get :index
  end
end


resources :pages, path: '/', only: [:index] do
  collection do
    get :my_photos
    get :download
    get :'my-focofox', to: :my_focofox
    get :empty
    get :run_server_stat_cron
  end
end


resources :dropbox do
  collection do
    get :callback
  end
end