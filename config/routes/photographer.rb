resources :photographer do
  member do
    get :index
  end
end

namespace :photographer, path: '/p' do
	resources :photos, only: [:index, :edit, :create, :update, :show, :destroy] do
    member do
      get :video_player
      get :generate_preview
    end
	end
	resources :preferences, only: [:index] do
		collection do
			get :index
		end
	end
end