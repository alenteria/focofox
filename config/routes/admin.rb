resources :admin do
  collection do
    get :index
  	get :my_focofox
   end
  member do
  end
end

namespace :admin, path: '/admin' do
	namespace :api, path: '/api' do
		resources :photographers do
			collection do
				get :index
				get :funnel_report
				post :funnel_report
			end
			member do
				get :locations
			end
		end
		resources :clients do
			collection do
				get :index
				get :funnel_report
				post :funnel_report
			end
		end
		resources :sales do
			collection do
				post :index
				get :index
			end
		end

		resources :server_stats do
			collection do
				post :index
			end
		end
	end
end