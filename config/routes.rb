Rails.application.routes.draw do
  root 'welcome#index'

  devise_scope :user do
    devise_for :users, :controllers => {
      registrations: 'registrations',
      omniauth_callbacks: 'omniauth_callbacks',
      :sessions => "users/sessions",
      :passwords => "passwords"
    }
  end

  match '/users/:id/finish_signup' => 'users#finish_signup',
    via: [:get, :patch], :as => :finish_signup

  resources :users, :only => [:update_avatar] do
    collection do
      post :update_avatar
    end
  end

  draw :user
  draw :admin
  draw :photographer
  draw :api

  match  '/system/photos/*' => 'pages#block', via: [:get, :post]
  match  '/system/videos/*' => 'pages#block', via: [:get, :post]

  match '/thumbs-speed' => 'thumbs_speed#photos', via: [:get]

  resources :help_tooltips do
    post :manage, on: :collection
  end
  resources :translations do
  end

end
