require_relative '../../spec_helper' 

describe Api::WatermarksController, :type => :controller do
  before :each do
    create_photographer_user_session
  end
  describe "#Api Api::WatermarksController" do
    it 'should create watermark successfully' do
      create_watermark
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
    end

    it 'should update watermark successfully' do
      create_watermark
      data = {
        "display_type"=> "positioned",
        "tiled_size"=> 200,
        "position"=> "center center",
        "positioned_size"=> 50,
        "rotation"=> 0,
        "stretched_size"=> 50,
        "opacity"=> 0.5,
        "base_image_width"=> 800,
        "base_image_height"=> 600,
        "name"=> "my sample watermark edited"
      }
      put :update, {data: data.to_json, id: Watermark.last.id}
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
      watermark = Watermark.last
      expect(watermark.name).to eq('my sample watermark edited')
      expect(watermark.base_image_width).to eq(800)
      expect(watermark.base_image_height).to eq(600)
    end

    # it 'should mark default watermark' do
    #   create_watermark
    #   watermark = Watermark.last
    #   debugger
    #   post :mark_default_watermark, {id: watermark.id}
    #   expect(response.status).to eq(200)
    #   res = JSON.parse(response.body)
    #   expect(res["status"]).to eq("ok")
    #   expect(@current_user.default_watermark_id).to eq(watermark.id)
    # end
  end

  def create_watermark
    data = {
      "display_type"=> "positioned",
      "tiled_size"=> 200,
      "position"=> "center center",
      "positioned_size"=> 50,
      "rotation"=> 0,
      "stretched_size"=> 50,
      "opacity"=> 1,
      "base_image_width"=> 898,
      "base_image_height"=> 599,
      "name"=> "my sample watermark"
    }
    file = Rack::Test::UploadedFile.new(Rails.root.join("spec", "fixtures", "watermarks", "1_medium_sample_watermark_1.png"), 'image/png')
    post :create, {file: file, data: data.to_json}
  end
end
