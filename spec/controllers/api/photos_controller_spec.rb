require_relative '../../spec_helper' 

describe Api::PhotosController, :type => :controller do
	before :each do
		create_photographer_user_session
	  request.env["HTTP_ACCEPT"] = 'application/json'
  end
	describe "#Api PhotosController" do
    it "should load index successfully" do
      get :index
    end

    it "should create photos successfully" do
      upload_photo

      expect(response.status).to eq(200)
    end

    it "should create video successfully" do
      upload_video

      expect(response.status).to eq(200)
    end

    it 'should update photo successfully' do
      upload_photo

      params = {"id"=>Photo.first.id}
      put :update, params
      expect(response.status).to eq(200)
    end

    it 'should delete photo successfully' do
      upload_photo
      photo = Photo.first
      params = {"id"=>photo.id}
      get :destroy, params
      expect(response.status).to eq(200)
    end

    it 'should return filtered photos' do
      upload_photo
      @request.env['HTTP_PRICES'] = [from: 100, to: 100].to_json
      @request.env['HTTP_RANGE'] = '1-100'
      @request.env['HTTP_SORT_BY'] = 'date_time'
      get :index
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res.length>0) 
    end

    it 'should return searched photos' do
      upload_photo
      @request.env['HTTP_DATE_TIMES'] = [{from: '03/28/1994', to: '03/28/1994'}].to_json
      get :search
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res.length>0) 
    end
  end

  def upload_photo
    params = {price: 100, date_time: '03/28/1994'}
    params[:files] = [Rack::Test::UploadedFile.new(Rails.root.join("spec", "fixtures", "attachments", "img_0001.jpg"), 'image/jpg')]
    post :create, params
  end

  def upload_video
    params = {price: 100, date_time: '03/28/1994'}
    params[:files] = [Rack::Test::UploadedFile.new(Rails.root.join("spec", "fixtures", "attachments", "videoplayback.mp4"), 'video/mp4')]
    post :create, params
  end
end
