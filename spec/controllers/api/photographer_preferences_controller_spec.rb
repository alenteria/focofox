require_relative '../../spec_helper'

describe Api::PhotographerPreferencesController, :type => :controller do
  before :each do
    create_photographer_user_session
  end
  describe "#Api PhotographerPreferencesController" do
    it 'should update default photo price' do
      put :update_default_photo_price, {default_photo_price: 100}
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
      expect(@current_user.account.default_photo_price.to_f).to eq(100)
    end

    it 'should update thumbnail size' do
      put :update_thumbnail_size, {thumbnail_size: 100}
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
      expect(@current_user.account.thumbnail_size).to eq("100")
    end

    it 'should update per page photos count' do
      put :update_per_page_display, {per_page: 100}
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
      expect(@current_user.account.per_page).to eq("100")
    end
  end
end
