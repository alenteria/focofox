require_relative '../../spec_helper' 

describe Api::LocationsController, :type => :controller do
  before :each do
    create_photographer_user_session
  end
  describe "#Api Api::LocationsController" do
    it 'should create location successfully' do
      create_location
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
    end

    it 'should update location successfully' do
      create_location
      data = {
        id: Location.last.id,
        city: "Carmen",
        country: "Philippines",
        gps: {latitude: 10, longitude: 123},
        name: "Location edited",
        province: "Bohol",
        zoom: 11
      }
      put :update, data
      expect(response.status).to eq(200)
      res = JSON.parse(response.body)
      expect(res["status"]).to eq("ok")
      location = Location.last
      expect(location.name).to eq('Location edited')
      expect(location.latitude).to eq(10)
      expect(location.longitude).to eq(123)
      expect(location.zoom).to eq(11)
    end

  end

  def create_location
    data = {
      city: "Carmen",
      country: "Philippines",
      gps: {latitude: 9.849991099999999, longitude: 124.14354270000001},
      name: "Carmen, Bohol, Philippines",
      province: "Bohol",
      zoom: 10
    }
    post :create, data

  end
end

