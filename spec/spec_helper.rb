require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require "capybara/rails"
require "capybara/rspec"

ENV["RAILS_ENV"] ||= 'test'
Dir["./spec/support/**/*.rb"].sort.each {|f| require f} #load spec helpers
ActionView::TestCase::TestController.instance_eval do
  helper Rails.application.routes.url_helpers#, (append other helpers you need)
end
ActionView::TestCase::TestController.class_eval do
  def _routes
    Rails.application.routes
  end
end

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include Capybara::DSL
  config.include OmniauthModule

  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = false
  config.infer_spec_type_from_file_location!
end