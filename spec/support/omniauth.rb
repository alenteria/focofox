module OmniauthModule
  def valid_login_setup(_for, opts = {})
    default = {:provider => _for,
             :uuid     => "1234",
             _for => {
                            :email => "foobar@example.com",
                            :gender => "Male",
                            :first_name => "foo",
                            :last_name => "bar"
                          }
            }

    credentials = default.merge(opts)
    provider = credentials[:provider]
    user_hash = credentials[provider]

    OmniAuth.config.test_mode = true

    OmniAuth.config.mock_auth[provider] = OmniAuth::AuthHash.new({
      :uid => credentials[:uuid],
      :provider => provider,
      :info => OmniAuth::AuthHash.new({
        :email => user_hash[:email],
        :first_name => user_hash[:first_name],
        :last_name => user_hash[:last_name],
        :gender => user_hash[:gender]
        }),
      :extra => OmniAuth::AuthHash.new({
        :raw_info => OmniAuth::AuthHash.new({
          :email => user_hash[:email],
          :first_name => user_hash[:first_name],
          :last_name => user_hash[:last_name],
          :gender => user_hash[:gender]
        }),
      })
    })
  end

  def create_photographer_user_session
    return false if @current_user.present?
    valid_login_setup(:facebook)
    visit "/users/auth/facebook/callback"
    @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
    user = User.last
    Photographer.create user: user, storage_space: (9 * 1000000000)
    sign_in user
    @current_user = user
  end
end