require_relative '../spec_helper'

describe "GET '/auth/twitter/callback'" do

  before(:each) do
    valid_login_setup(:twitter)
    get "/users/auth/twitter/callback"
    request.env["devise.mapping"] = Devise.mappings[:user]
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
  end

  it "should set user_id" do
    expect(session.to_hash["warden.user.user.key"][0][0]).to eq(User.last.id)
  end

  it "should redirect to finish signup" do
    expect(response).to redirect_to root_path
  end
end
