require_relative '../spec_helper'

describe "GET '/auth/facebook/callback'" do

  before(:each) do
    valid_login_setup(:facebook)
    get "/users/auth/facebook/callback"
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
  end

  it "should set user_id" do
    expect(session.to_hash["warden.user.user.key"][0][0]).to eq(User.last.id)
  end

  it "should redirect to finish signup" do
    expect(response).to redirect_to root_path
  end
end
