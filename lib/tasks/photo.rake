namespace :photos do
	task :reset => :environment do
		require 'fileutils'
		FileUtils.rm_rf "#{Rails.root}/public/system/photos"
		FileUtils.rm_rf "#{Rails.root}/public/system/videos"
		FileUtils.rm_rf "#{Rails.root}/public/system/.tmp"
	end

	namespace :generate do
		task :preview => :environment do
			Photo.find_each{|p| p.generate_preview}				
		end
	end

	namespace :sort do
		task :width => :environment do
			Photo.all.each do |p|
				begin
					original = Magick::Image.read(p.attachment.path(:original)).first
					thumb = Magick::Image.read(p.attachment.path(:thumb)).first
	        p.width = original.columns
	        p.height = original.rows
	        p.thumb_width = thumb.columns
	        p.thumb_height = thumb.rows
	        p.save
	      rescue
	      end
        print '.'
				$stdout.flush
			end
			puts "\nphotos successfully sorted by width!"
		end
	end
end