require 'fileutils'
namespace :watermark do
	task :reset => :environment do
		Watermark.delete_all
		
		FileUtils.rm_rf "#{Rails.root}/public/system/image_assets"

		public_assets_path = "#{Rails.root}/public/img"
		bgs = %w(watermark_sample_bg_1.jpg watermark_sample_bg_2.jpg watermark_sample_bg_3.jpg)
		watermark_path = "#{public_assets_path}/sample_watermark_1.png" 
		positioned_tiled_watermark_path = "#{public_assets_path}/Focofox-Default.png" 
		stretched_watermark_path = "#{public_assets_path}/Focofox-Stretched.png" 
		
		default_watermark = Watermark.create(
			[
				{
					name: :none
				},
				{
					name: 'Focofox Stretched', 
					image_asset: ImageAsset.create(image: File.open(stretched_watermark_path)), 
					display_type: :stretched, 
					rotation: 0, 
					opacity: 0.75,
					stretched_size: 50
				},
				{
					name: 'Focofox Tiled',
					image_asset: ImageAsset.create(image: File.open(positioned_tiled_watermark_path)), 
					display_type: :tiled, 
					rotation: 0, 
					opacity: 0.75,
					tiled_size: 200,
					position: 'center center',
					positioned_size: 50,
					stretched_size: 50,
					:base_image_height => 263.0,
					:base_image_width => 395.0,
					:tiled_size => 154.666666666667,

				},
				{
					name: 'Focofox Positioned',
					image_asset: ImageAsset.create(image: File.open(positioned_tiled_watermark_path)), 
					display_type: :positioned, 
					rotation: 0, 
					opacity: 0.75,
					positioned_size: 50,
					position: 'center center',
					stretched_size: 50
				}
			]
		)
		puts "watermark successfully reseted!"
	end
end