namespace :license do
	task :reset => :environment do
		License.delete_all
		
		default_license = License.create(
			[
				{
					title: 'Attribution'
				},
				{
					title: 'Attribution-NoDerivs'
				},
				{
					title: 'Attribution-NonCommercial-ShareAlike'
				},
				{
					title: 'Attribution-ShareAlike'
				},
				{
					title: 'Attribution-NonCommercial'
				},
				{
					title: 'Attribution-NonCommercial-NoDervis'
				}
			]
		)
		puts "Licenses successfully reseted!"
	end
end

