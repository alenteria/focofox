password = '!p@$$w0rd'
namespace :initializedb do
  task :all => :environment do
    Rake::Task['initializedb:users'].invoke
    Rake::Task['initializedb:photographer:account'].invoke
    Rake::Task['initializedb:user:account'].invoke
    Rake::Task['initializedb:photos:seed'].invoke
    Rake::Task['initializedb:help_tooltips'].invoke
  end

  task :help_tooltips => :environment do
    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_add_side_button'
    ).update_attributes(
      text_en: 'Add photos help. This is supposedly filtered for the user type e.g admin only.',
      text_es: 'Add photos help. This is supposedly filtered for the user type e.g admin only.'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_sidebar_add_photo'
    ).update_attributes(
      text_en: 'Add Photo',
      text_es: 'Add Photo'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_sidebar_refresh',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_help',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_event_help',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_event_create',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'devise_sessions_new',
    ).update_attributes(
      text_en: 'Instruction for photographer signup. This can be editable by adding "editable" class on body via the browser console before clicking on this info button. This is supposedly filtered according to the user type e.g admin.',
      text_es: 'Instruction for photographer signup. This can be editable by adding "editable" class on body via the browser console before clicking on this info button. This is supposedly filtered according to the user type e.g admin.'
    )

    HelpTooltip.find_or_create_by(
      slug: 'devise_registrations_new',
    ).update_attributes(
      text_en: 'Insert Password',
      text_es: 'Insert Password'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_index_help',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_new_help',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'user_search_narrow_results',
    ).update_attributes(
      text_en: 'User Search Narrow result. This is supposedly filtered for the user type e.g admin only.',
      text_es: 'User Search Narrow result. This is supposedly filtered for the user type e.g admin only.'
    )

  end

  task :users => :environment do
    user = User.new(:email => 'p1@focofox.com', :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
    user.save

    user = User.new(:email => 'p2@focofox.com', :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
    user.save

    user = User.new(:email => 'p3@focofox.com', :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
    user.save

    user = User.new(:email => 'u1@focofox.com', :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
    user.save

    puts "Users successfully created"
  end

  namespace :user do
    task :account => :environment do
      Account.create user: User.where(email: 'u1@focofox.com').first
    end
  end
  namespace :photographer do
    task :account => :environment do
      storage_space = 2
      storage_space = storage_space.to_i * 1000000000

      user = User.where(email: 'p1@focofox.com').first
      Photographer.create user: user, storage_space: storage_space, paypal_email: 'photographer1@focofox.com'

      user = User.where(email: 'p2@focofox.com').first
      Photographer.create user: user, storage_space: storage_space, paypal_email: 'photographer1@focofox.com'

      user = User.where(email: 'p3@focofox.com').first
      Photographer.create user: user, storage_space: storage_space, paypal_email: 'photographer1@focofox.com'

      puts "Three users converted to Photographers"
    end
  end

  namespace :photos do
    task :seed => :environment do 
      puts 'PLEASE INPUT THE DIRECTORY NAME:'
      puts 'NOTE: By convention, each sub-directory should be the name of a valid photographer.
      Inside this "Photographer" sub-directory, there should be one or more sub-directories 
      that have photos in them. The sub-directory name should be modeled as follows:
      `price.date.time.location.watermark`
      For example: 15.2015-01-01.23-32.Dominical.MyWatermark'

      directory_name = $stdin.gets.chomp || '.seed-data'
    

      directory_name = "#{Rails.root}/#{directory_name}"
      
      next unless File.directory?(directory_name)

      date = Date.current

      Dir["#{directory_name}/**"].each do |email|
        email = email.split('/').last
        photographer = User.where(email: email).first

        if photographer.blank?
          user = User.create(email: email, password: password, password_confirmation: password, confirmed_at: Date.current)
          Photographer.create user: user, storage_space: 2000000000
          photographer = user
        end
        
        photographer_dir = "#{directory_name}/#{email}"

        Dir["#{photographer_dir}/**"].each do |data|

           upload_date = "#{date.year}/#{date.month}/#{date.day+1} at #{Time.now.strftime("%I:%M %p")}"
           @group = Group.create({
            name: 'photos uploaded on '+upload_date,
            user: photographer
          })
          data = data.split('/').last

          inf = data.split('.')
          price = inf[0]
          date = inf[1]
          begin
            date = Date.parse date
          rescue Exception => e
            date = Date.current
          end

          time = inf[2]
          begin
            time = Time.parse time.sub!("-", ":")
          rescue Exception => e
            time = Time.now
          end
          date_time = (date.to_s + ' ' + time.to_s).to_datetime

          location_name = inf[3]
          geometry = Geocoder.search(location_name)[0].try(:geometry)
          if geometry.present? && geometry['location'].present?
            loc = geometry['location']
            @location = photographer.locations.near([loc['lat'], loc['lng']], 1).first||
              Location.create({
                user: photographer,
                name: location_name,
                latitude: loc['lat'],
                longitude: loc['lng']
              })
          else
            lat = 0
            long = 0
            @location = photographer.locations.first ||
              Location.create({
                user: photographer,
                name: location_name,
                latitude: lat,
                longitude: long
              })
          end

          watermark_name = inf[4]
          @watermark = photographer.watermarks.where("lower(name) = ?", watermark_name.downcase).first

          if @watermark.blank?
            file_name = "Focofox"

            w_image = Magick::Image.new(300, 100){ self.background_color = "none" }
            image_text = Magick::Draw.new
            image_text.annotate(w_image, 0,0,0,0, file_name) do
              image_text.fill = '#1F98B9'
              self.fill = '#1F98B9'
              image_text.gravity = CenterGravity
              self.pointsize = 50
              self.font_family = "Arial"
              self.font_weight = BoldWeight
              self.stroke = "none"
            end
            tempfile = Tempfile.new([file_name, '.png'])
            w_image.write(tempfile.path)
            image_asset = ImageAsset.create(user: photographer, image: tempfile)
            tempfile.unlink
            @watermark = photographer.watermarks.create(name: watermark_name, image_asset: image_asset, display_type: 'positioned', positioned_size: '100', rotation: '0', opacity: '1', tiled_size: '200', position: 'center center', stretched_size: 50)
          end

          photos_dir = "#{photographer_dir}/#{data}"
          puts "photos_dir: " + photos_dir
          puts "@date_time: " + date_time.to_s
          puts Dir["#{photos_dir}/*.{png,jpg,jpeg}"].length.to_s
          puts "-------------file list------------------"

          Dir["#{photos_dir}/*.*"].each do |photo|
            photo_name = photo.split('/').last
            content_type =  MIME::Types.type_for(photo).first.try(:content_type)
            next if content_type.blank?

            file = File.open(photo)

            begin
              photo = Photo.create({
                user: photographer,
                location: @location,
                watermark: @watermark,
                date_time: date_time,
                "#{Photo.photo_formats.include?(content_type) ? 'photo' : 'video'}"=> file
              })
              @group.members << photo
              puts "Photo `#{photo_name}` seeded successfully!"
            rescue Exception=>e
              puts  "Photo `#{photo_name}` #{e.to_s}"
            end            
          end

          puts "------------End file list---------------"
          Photo.all.each { |p| p.update_attributes(verified: true) }
        end
      end
      #require 'fileutils'
      #FileUtils.remove_dir "#{directory_name}", true
    end
  end
end
