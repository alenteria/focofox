namespace :deploy do
	task :staging => :environment do
		system('git remote add staging root@staging.focofox.com')
		system('ssh root@staging.focofox.com "cd /var/deploy/focofox_staging/web_head/current ; git checkout deploy"')
		system('git push staging develop')
		system('ssh root@staging.focofox.com "cd /var/deploy/focofox_staging/web_head/current ; git merge develop"')
		system('ssh root@staging.focofox.com "cd /var/deploy/focofox_staging/web_head/current ; touch tmp/restart.txt"')
	end

	task :production do
		system("curl -X POST -d '' https://hooks.cloud66.com/stacks/redeploy/9ac77ee14ec30a5605e761b8f729cb4c/823cca5f7f72f4db8eb960c987bb6ee3")
	end
end