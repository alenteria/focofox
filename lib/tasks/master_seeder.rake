require 'json'

# Constants {{{ #

PHOTOGRAPHERS_COUNT = 25
PASSWORD = "Gn8fH7MZAkPpqd!"
STORAGE_SPACE       = 200
LOCATIONS = [
  [28.490364, -81.703403],
  [32.579919, -83.362922],
  [33.785114, -80.895022],
  [35.664297, -79.111778],
  [37.573458, -78.522875],
  [39.483678, -77.106500],
  [40.063647, -74.438772],
  [42.493725, -74.387444],
  [41.728192, -72.636433],
  [41.682614, -71.576083],
  [42.338103, -71.976889],
  [43.431703, -71.583547],
  [44.800597, -69.735586],
  [44.175444, -72.615053],
  [40.847728, -78.605114],
  [38.884347, -80.629694],
  [32.170369, -86.334547],
  [35.776519, -85.959639],
  [37.349317, -84.611775],
  [40.201853, -83.081739],
  [42.776131, -84.691225],
  [40.387903, -86.084828],
  [40.748194, -89.009411],
  [43.751775, -89.715500],
  [38.442622, -92.161742]
]


class MultiIO
  def initialize(*targets)
     @targets = targets
  end

  def write(*args)
    @targets.each {|t| t.write(*args)}
  end

  def close
    @targets.each(&:close)
  end
end

log_file = File.open(Rails.root.join('log', 'master_seeder.log'), "a")
LOGGER ||= Logger.new MultiIO.new(STDOUT, log_file)

# }}} Constants #

namespace :master_seeder do

  # Photographers {{{ #

  desc "Create #{PHOTOGRAPHERS_COUNT} photographers"
  task :photographers => :environment do
    (1..PHOTOGRAPHERS_COUNT).each do |photographer_index|
      email = "p#{photographer_index}@focofox.com"

      user = User.create(
        email: email,
        first_name: Randgen.first_name,
        last_name: Randgen.last_name,
        password: PASSWORD,
        password_confirmation: PASSWORD,
        confirmed_at: Date.current
      )

      Photographer.create user_id: user.id, storage_space: STORAGE_SPACE
      LOGGER.info("Photographer created: #{email}/#{PASSWORD}")
    end
  end

  # }}} Photographers #

  # Locations {{{ #

  desc "Create locations"
  task :locations => :environment do
    LOCATIONS.each_with_index do |position, location_index|

      user = User.find_by(email: "p#{location_index + 1}@focofox.com")

      locus = Geocoder.search(position).first
      location_name = locus.try(:city) || locus.try(:region_name) || locus.try(:country_name) || position.join(', ')

      location = Location.create(
        name: location_name,
        user_id: user.id,
        latitude: locus.try(:latitude) || position[0],
        longitude: locus.try(:longitude) || position[1]
      )

      LOGGER.info("Location created: #{location_name}")

      9.times do 
        dy = rand(1..100000).to_f
        dx = rand(1..100000).to_f
        lat = locus.latitude + (180/Math::PI)*(dy/6378137)
        lng = locus.longitude + (180/Math::PI)*(dx/6378137)/Math.cos(locus.latitude)
        random_location = Geocoder.search([lat, lng].join(',')).first

        location_name = random_location.try(:city) || random_location.try(:region_name) || random_location.try(:country_name) || [lat, lng].join(', ')

        location = Location.create(
          name: location_name,
          user_id: user.id,
          latitude: random_location.try(:latitude) || locus.latitude,
          longitude: random_location.try(:longitude) || locus.longitude
        )

        LOGGER.info("Location created: #{location_name}")
      end
    end


  end

  # }}} Locations #

  # Clients {{{ #

  desc 'Upload clients'
  task :clients => :environment do
    LOGGER.info "Start uploading clients"

    ActiveRecord::Base.transaction do
      1000.times do |client_index|
        username = "c#{'0' * (4 - (client_index + 1).to_s.length)}#{client_index + 1}"
        email = "#{username}@focofox.com"
        password = "KsztTQzwZHJ89w!!"

        user = User.create(
          username: username,
          email: email,
          first_name: Randgen.first_name,
          last_name: Randgen.last_name,
          password: password,
          password_confirmation: password,
          confirmed_at: Date.current
        )

        user.build_account.save(validate: false)
        payment_method = PaymentMethod.new(
          method_type: :send,
          account_id: user.account.try(:id)
        )
        payment_method.save(validate: false)
        payment_method.paypal_accounts.build(
          email: 'merchant@focofox.com',
          is_default: true
        ).save(validate: false)

        LOGGER.info "Client uploaded #{email}/#{password}"
      end
    end

    LOGGER.info "Finish uploading clients"
  end

  # }}} Clients #

  # Sales {{{ #
  desc 'Create sales for each photographer'
  task :sales => :environment do
    (1..PHOTOGRAPHERS_COUNT).each do |photographer_index|
      photographer = User.find_by(email: "p#{photographer_index}@focofox.com")
      client = User.find_by(email: pick_random_client_email)

      paypal_account = PaypalAccount.create(
        user_id: client.id,
        email: 'photographer1@focofox.com',
        is_default: true,
        payment_method_id: client.account.payment_methods.first.id,
      )

      items = photographer.photos.limit(rand(10)).order('RANDOM()').pluck(:id)

      payment = Payment.new(
        from: client.account.id,
        to: [photographer.id],
        items: items,
        description: '',
        paypal_account: paypal_account,
        verified: true,
        verified_at: DateTime.current,
      )

      payment.save
      payment.setup

      LOGGER.info "Finish creating one payment"
    end
  end
  # }}} Sales #

  def pick_random_client_email
    client_index = rand(1000)
    username = "c#{'0' * (4 - (client_index + 1).to_s.length)}#{client_index + 1}"
    "#{username}@focofox.com"
  end

  # Photos {{{ #

  desc 'Upload photos'
  task :photos => :environment do
    print 'Seed directory name: '
    directory_name = $stdin.gets.chomp || '.seed-data-source'

    directory_name = Rails.root.join directory_name

    Dir["#{directory_name}/**"].each do |photographer_directory|
      email = "p#{File.basename(photographer_directory)[1..-1].to_i}@focofox.com"
      photographer = User.find_by(email: email)

      Dir["#{photographer_directory}/**"].each do |photos_directory|
        config = JSON.parse(
          File.read(File.join(photos_directory, 'add-files.json')),
          symbolize_names: true
        )

        price = config[:price]
        date = Date.parse config[:date]
        time = Time.parse config[:time].sub('-', ':')
        date_time = ("#{date.to_s} #{time.to_s}").to_datetime

        group = Group.create({
          name: "photos uploaded on #{date_time.strftime("%m/%d/%Y %I:%M %p")}",
          user: photographer
        })

        location = photographer.locations
                    .offset(rand(photographer.locations.count))
                    .limit(1).first

        watermark = photographer.watermarks.first

        # Create watermark {{{ #

        if watermark.blank?
          w_image = Magick::Image.new(300, 100){ self.background_color = "none" }

          image_text = Magick::Draw.new

          image_text.annotate(w_image, 0,0,0,0, "Focofox") do
            image_text.fill = '#1F98B9'
            self.fill = '#1F98B9'
            image_text.gravity = CenterGravity
            self.pointsize = 50
            self.font_family = "Arial"
            self.font_weight = BoldWeight
            self.stroke = "none"
          end

          tempfile = Tempfile.new(["Focofox", '.png'])
          w_image.write(tempfile.path)
          image_asset = ImageAsset.create(user: photographer, image: tempfile)
          tempfile.unlink

          watermark = photographer.watermarks.create(
            name: 'Focofox',
            image_asset: image_asset,
            display_type: 'positioned',
            positioned_size: '100',
            rotation: '0',
            opacity: '1',
            tiled_size: '200',
            position: 'center center',
            stretched_size: 50
          )

        end

        # }}} Create watermark #

        LOGGER.info "Starting to look up photos in #{photos_directory}"

        Dir["#{photos_directory}/*.{png,jpg,jpeg}"].each do |photo|
          photo_name = File.basename photo
          content_type = MIME::Types.type_for(photo).first.try(:content_type)

          next if content_type.blank?

          file = File.open(photo)

          begin
            photo = Photo.create({
              user: photographer,
              location: location,
              watermark: watermark,
              date_time: date_time,
              price: price,
              verified: true,
              "#{Photo.photo_formats.include?(content_type) ? 'photo' : 'video'}" => file
            })

            group.members << photo
            LOGGER.info "Photo `#{photo_name}` seeded successfully!"
          rescue Exception=>e
            LOGGER.info  "Photo `#{photo_name}` #{e.to_s}"
          end
        end

        LOGGER.info "Ended to look up photos in #{photos_directory}"
      end
    end
  end

  # }}} Photos #

  # Help Tooltips {{{ #

  task :help_tooltips => :environment do
    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_add_side_button'
    ).update_attributes(
      text_en: 'Add photos help. This is supposedly filtered for the user type e.g admin only.',
      text_es: 'Add photos help. This is supposedly filtered for the user type e.g admin only.'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_sidebar_add_photo'
    ).update_attributes(
      text_en: 'Add Photo',
      text_es: 'Add Photo'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_sidebar_refresh',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_help',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_event_help',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_store_sidebar_event_create',
    ).update_attributes(
      text_en: 'Event Help',
      text_es: 'Event Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'devise_sessions_new',
    ).update_attributes(
      text_en: 'Instruction for photographer signup. This can be editable by adding "editable" class on body via the browser console before clicking on this info button. This is supposedly filtered according to the user type e.g admin.',
      text_es: 'Instruction for photographer signup. This can be editable by adding "editable" class on body via the browser console before clicking on this info button. This is supposedly filtered according to the user type e.g admin.'
    )

    HelpTooltip.find_or_create_by(
      slug: 'devise_registrations_new',
    ).update_attributes(
      text_en: 'Insert Password',
      text_es: 'Insert Password'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_index_help',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'photographer_photos_new_help',
    ).update_attributes(
      text_en: 'Photos Help',
      text_es: 'Photos Help'
    )

    HelpTooltip.find_or_create_by(
      slug: 'user_search_narrow_results',
    ).update_attributes(
      text_en: 'User Search Narrow result. This is supposedly filtered for the user type e.g admin only.',
      text_es: 'User Search Narrow result. This is supposedly filtered for the user type e.g admin only.'
    )

  end

  # }}} Help Tooltips #


  task all: [:photographers, :locations, :clients, :photos, :sales, :help_tooltips]

end

