require 'open-uri'
require "net/http"
require 'RMagick'
#require 'ruby-prof'
include Magick

password = '!p@$$w0rd'
storage_space = 200#GB
storage_space = storage_space.to_i * 1000000000
max_file_size = 500000
locus_list = [
        [28.490364, -81.703403],
        [32.579919, -83.362922],
        [33.785114, -80.895022],
        [35.664297, -79.111778],
        [37.573458, -78.522875],
        [39.483678, -77.106500],
        [40.063647, -74.438772],
        [42.493725, -74.387444],
        [41.728192, -72.636433],
        [41.682614, -71.576083],
        [42.338103, -71.976889],
        [43.431703, -71.583547],
        [44.800597, -69.735586],
        [44.175444, -72.615053],
        [40.847728, -78.605114],
        [38.884347, -80.629694],
        [32.170369, -86.334547],
        [35.776519, -85.959639],
        [37.349317, -84.611775],
        [40.201853, -83.081739],
        [42.776131, -84.691225],
        [40.387903, -86.084828],
        [40.748194, -89.009411],
        [43.751775, -89.715500],
        [38.442622, -92.161742],
      ]

namespace :seed do
  task :photos ,[:photographer_count] => :environment do |t, args|
    default_photo = Photo.last
    path = default_photo.try(:photo).try(:path)||"/var/deploy/focofox_staging/web_head/shared/seed_photo.png"
    res_code = ''
    photographer_ids = []
    file_size = default_photo.try(:photo).try(:size)

    _i = 0
    while !((File.exists?(path||"") || (res_code=='200'&& path.present?) && file_size <= max_file_size))
      begin
        puts "PLEASE #{_i<=0 ? "INPUT" : "REINPUT"} THE PHOTO PATH OR URL(url should not be secure & photo should have maximum size of 500kb):"
        url = $stdin.gets.chomp
        _url = URI.parse(url)
        req = Net::HTTP.new(_url.host, _url.port)
        res = req.request_head(_url.path)
        res_code = res.code
        _f = open(url)
        path = _f.try(:path)
        file_size = _f.size
      rescue Exception => e
        res_code = nil
      end
      _i +=1
    end


    image = Magick::Image.read(path).first

    @logger ||= Logger.new("#{Rails.root}/public/system/seeded_photographers.log")
    
    photographer_count = (args[:photographer_count] || 25).to_i
    
    all_photos_count = 0
    
    main_locus = Geocoder.search([39.517375, -101.766776]).first

    # Create photographers
    (1..photographer_count).each do |photographer_i|
      user = User.create(email: Randgen.email, first_name: Randgen.first_name, last_name: Randgen.last_name, :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
      next unless user.try(:id).present?

      photographer = Photographer.create user: user, storage_space: storage_space
      photographer_ids = photographer_ids||[]
      photographer_ids << photographer.id if photographer.try(:id).present?

      next unless photographer.try(:id).present?
      
      puts "\nPhotograhper: #{user.display_name}"

      @logger.info("Photographer: #{user.email}/#{password} \n")

      license_ids = License.pluck(:id)

      #create watermarks
      number_of_watermarks = rand(1..10)
      watermark_ids = []
      (1..number_of_watermarks).each do |wi|
        file_name = "Focofox#{user.id}-#{wi}"

        w_image = Magick::Image.new(300, 100){ self.background_color = "none" }
        image_text = Magick::Draw.new
        image_text.annotate(w_image, 0,0,0,0, file_name) do
          image_text.fill = 'black'
          self.fill = 'black'
          image_text.gravity = CenterGravity
          self.pointsize = 50
          self.font_family = "Arial"
          self.font_weight = BoldWeight
          self.stroke = "none"
        end
        tempfile = Tempfile.new([file_name, '.png'])
        w_image.write(tempfile.path)

        image_asset = ImageAsset.create(user: user, image: tempfile)
        watermark = Watermark.create(user: user, image_asset: image_asset, display_type: 'positioned', positioned_size: '100', rotation: '0', opacity: '1', tiled_size: '200', position: 'center center', stretched_size: 50)
        watermark_ids.push(watermark.id) if watermark.try(:id)
        tempfile.unlink
      end

      lat = locus_list[photographer_i-1][0]
      lng = locus_list[photographer_i-1][1]

      locus = Geocoder.search([lat, lng]).first

      next if locus.blank?
      
      number_of_locations = rand(1..10)
      (1..number_of_locations).each do |li|

        location = {}
        fi = 0
        while location.blank? && fi <= 100
          # Create locations
          dy = rand(1..100000).to_f
          dx = rand(1..100000).to_f
          lat = locus.latitude + (180/Math::PI)*(dy/6378137)
          lng = locus.longitude + (180/Math::PI)*(dx/6378137)/Math.cos(locus.latitude)
          random_location = Geocoder.search([lat, lng].join(',')).first
          
          fi +=1
          next if random_location.try(:latitude).blank? || random_location.try(:longitude).blank?
          begin
            location_name = random_location.try(:city).present? ? random_location.city : (random_location.try(:region_name).present? ? random_location.region_name : (random_location.try(:country_name).present? ? random_location.country_name : [random_location.latitude, random_location.longitude].join(',')))
            location = Location.create(name: location_name, user_id: user.id, latitude: random_location.latitude, longitude: random_location.longitude)
          rescue Exception => e
            location = {}
            next
          end 
        end
        next unless location.try(:id).present?  
        
        puts "\nLocation: #{location.name}\n"
        # Upload sessions

        photos_by_location_count = 0
        upload_sessions_count = rand(1..10)

        (1..upload_sessions_count).each do |gi|
          session_name = "photos uploaded on #{DateTime.new(2015, rand(3..12), rand(1..28)).strftime("%m/%d/%Y %I:%M %p")}"
          
          puts "\nSession #{gi+1}: #{session_name}\n\n"
          photos_per_session = rand(10..50)

          portrait_photos_count = (photos_per_session * 0.10).to_i
          landscape_photos_count = (photos_per_session * 0.80).to_i

          begin
            ActiveRecord::Base.transaction do
              (1..portrait_photos_count).each do |pi|
                date_time = DateTime.new(2015, rand(1..12), rand(1..28))
                tempfile = Tempfile.new(["Photo-#{user.id}-#{pi}", '.png'])
                _img = image.copy


                if _img.columns > _img.rows || _img.columns == _img.rows
                  _img.resize_to_fill!(_img.columns, _img.columns*1.5)
                end

                _img.write(tempfile.path)

                photo = Photo.create(photo: tempfile, user_id: user.id, location_id: location.id, license_id: license_ids.sample, date_time: date_time, watermark_id: watermark_ids.sample, price: rand(2..20), skip_photo_fingerprint_validation: true, session_names: [session_name])
                
                all_photos_count += 1
                photos_by_location_count +=1
                print "."
                tempfile.unlink
              end
            end          
          rescue Exception => e
            puts "\n"
            puts e.try(:message)
            puts "\n"
          end

          begin
            ActiveRecord::Base.transaction do
              (1..landscape_photos_count).each do |pi|
                date_time = DateTime.new(2015, rand(1..12), rand(1..28))
                tempfile = Tempfile.new(["Photo-#{user.display_name}-#{pi}", '.png'])
                _img = image.copy


                if _img.columns < _img.rows || _img.columns == _img.rows
                  _img.resize_to_fill!(_img.columns, _img.columns/1.5)
                end

                _img.write(tempfile.path)

                photo = Photo.create(photo: tempfile, user_id: user.id, location_id: location.id, license_id: license_ids.sample, date_time: date_time, watermark_id: watermark_ids.sample, price: rand(2..20), skip_photo_fingerprint_validation: true, session_names: [session_name])

                all_photos_count += 1
                photos_by_location_count +=1
                print "."
                tempfile.unlink
              end
            end
          rescue Exception => e
            puts "\n"
            puts e.try(:message)
            puts "\n"
          end
          
        end

        puts "\nPhotographer: #{user.display_name}; Location: #{location.name}; Photos: #{photos_by_location_count}"
      end
    end
    puts "\nSuccessfully seeded #{all_photos_count} photos"
  end
  namespace :photos do 
    task :profile => :environment do |t, args|
      photo = Photo.where("photo_file_size IS NOT NULL").last

      if photo.blank? || photo.try(:user).blank?
        puts 'run the seed:photos first!!!'
        exit 0
      end

      user = photo.user

      if user.blank?
        puts 'No user found!'
        exit 0
      end

      if photo.try(:photo).try(:path).blank?
        puts 'No photo found!'
        exit 0
      end

      image = File.open(photo.photo.path)

      license = License.first
      location = user.locations.limit(1).first
      watermark = user.watermarks.limit(1).first
      license_id = license.try(:id)
      location_id = location.try(:id)
      watermark_id = watermark.try(:id)
      user_id = user.try(:id)
      session_names = ["photos uploaded on #{DateTime.new(2015, rand(3..12), rand(1..28)).strftime("%m/%d/%Y %I:%M %p")}"]
      total_time = 0.0
      photos_count = 1000
      date_time = DateTime.new(2015, rand(1..12), rand(1..28))
      (1..photos_count).each do |pi|
        start_time = Time.now
        photo = Photo.create(photo: image, user_id: user_id, location_id: location_id, license_id: license_id, date_time: date_time, watermark_id: watermark_id, price: rand(2..20), skip_photo_fingerprint_validation: true, session_names: session_names)
        end_time = Time.now
        total_time += end_time - start_time
        print '.'
      end
      image.close
      puts "\n#{photos_count/total_time} photos per second"
      puts "Seeded #{photos_count} in #{total_time} seconds\n"
    end
  end

  task :videos ,[:photographer_count] => :environment do |t, args|
    photographer_count = (args[:photographer_count] || 10).to_i

  end

  task :users => :environment do
    users_count = 1000
    last_user_name = User.last.try(:username).try(:downcase)||""
    last_user_name = last_user_name.downcase.include?('user000') ? last_user_name : 'user0000' 
    index = last_user_name.tr('user000', '').to_i
    total_time = 0.0
    locus = locus_list.sample
    
    ActiveRecord::Base.transaction do
      (1..users_count).each do |ui|
        index += 1
        name = "user000#{index}"
        first_name = name.humanize
        email = "#{name}@test.com"
        username = "#{name.downcase}_"
        dy = rand(1..100000).to_f
        dx = rand(1..100000).to_f
        lat = locus[0] + (180/Math::PI)*(dy/6378137)
        lng = locus[1] + (180/Math::PI)*(dx/6378137)/Math.cos(locus[0])
        location_name = [lat, lng].join(',')
        start_time = Time.now
        user = User.create(username: username, email: email, first_name: first_name, last_name: nil, :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
        # user = User.connection.execute "INSERT INTO users (username, email, first_name, last_name, encrypted_password, confirmed_at) values ('#{username}', '#{email}', '#{first_name}', '#{nil}', '#{"$2a$10$ulWoXwJCeitnPxYJIFyCu.5roYOFgU3bZay0TKHscywA7c7Xi3HzC"}', '#{Date.current}')"
        end_time = Time.now
        total_time += end_time - start_time
        print '.'
        next if user.try(:id).blank?
        user.build_location(name: location_name, latitude: lat, longitude: lng).save(validate: false)
        user.build_account.save(validate: false)

        payment_method = PaymentMethod.new(method_type: :send, account_id: user.account.try(:id))
        payment_method.save(validate: false)
        payment_method.paypal_accounts.build(email: email,is_default: true).save(validate: false)
      end
    end
    puts "\n#{users_count/total_time} users per second"
    puts "Seeded #{users_count} in #{total_time} seconds\n"

  end

  task :sales => :environment do
    sales_count = 3000
    ActiveRecord::Base.transaction do
      (1..sales_count).each do |si|
        user = User.customers.select(:id, :location_id).includes(:location, :account).sample
        next if user.blank?
        location = begin
          user.location.nearbys(62.1371).joins(:photos).first
        rescue
          nil
        end
        
        if location.blank?
          location = Location.includes(:photos).joins(:photos).first
        end

        next if location.blank?
        photographer_id = location.user_id
        photo_ids = location.photos.limit(rand(1..15)).distinct.select(:id).pluck(:id)

        next if photo_ids.blank?
        payment_method = user.account.payment_methods.where(method_type: :send).first
        payment = Payment.create({
          from: user.id,
          to: [photographer_id],
          items: photo_ids,
          payment_method: payment_method,
          customer_location_name: location.name,
          customer_latitude: location.latitude,
          customer_longitude: location.longitude,
          verified: true,
          verified_at: rand(1.month).ago
        })
        print '.'

      end
    end
    puts "\n Seeded #{sales_count} sales"
  end

  task :all => :environment do
    Rake::Task['seed:photos'].invoke
    Rake::Task['seed:videos'].invoke
    Rake::Task['seed:profile'].invoke
  end
end
