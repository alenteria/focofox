namespace :create do
	task :test_user => :environment do
    	puts "Creating your new account ...\n"
    	puts 'Enter your email (Enter with empty value to use generated dummy email):'
        @email = $stdin.gets.chomp

        puts 'Password (Enter with empty value to use "!p@$$w0rd"):'
        password = $stdin.gets.chomp

        @email = @email.blank? ? 'test@example.com' : @email
        password = password.blank? ? "!p@$$w0rd" : password
        user  = User.where(email: @email).first
        if user.blank?
            user = User.new(:email => @email, :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
        end
        user.save
        puts "User successfully created"
        puts "email: #{@email}"
        puts "password: #{password}"

        puts "\n"
        puts "Create photographer account for this user? (Y/N)"
        answer = $stdin.gets.chomp
        if answer.downcase != 'n'
            Rake::Task['create:photographer:account'].invoke
        else
            Rake::Task['create:user:account'].invoke
        end
    end

    task :admin_user => :environment do
        puts "Creating your new admin account ...\n"
        puts 'Enter your email (Enter with empty value to use generated dummy email):'
        @email = $stdin.gets.chomp

        puts 'Password (Enter with empty value to use "!p@$$w0rd"):'
        password = $stdin.gets.chomp

        @email = @email.blank? ? 'admin@focofox.com' : @email
        password = password.blank? ? "!p@$$w0rd" : password
        user  = User.where(email: @email).first
        if user.blank?
            user = User.new(:email => @email, :password => password, :password_confirmation => password, :confirmed_at=>Date.current)
        end
        user.otp_required_for_login = false
        user.otp_secret = User.generate_otp_secret
        user.save
        Admin.create user: user
        Rake::Task['create:photographer:account'].invoke
        puts "User successfully created"
        puts "email: #{@email}"
        puts "password: #{password}"
        # puts "Scan this qr code on your Authenticator:\n"
        # system("qrcode-terminal #{user.otp_provisioning_uri(user.email, issuer: 'Focofox-Two-Factor-Auth')}")
        puts 'User successfully created!'
    end

    namespace :photographer do
        task :account => :environment do
            @email = @email.blank? ? 'test@example.com' : @email
            storage_space = 2
            storage_space = storage_space.to_i * 1000000000
            user = User.where(email: @email).first||User.last

            Photographer.create user: user, storage_space: storage_space
            puts "\nPhotographer account created successfully!"
        end
    end

    namespace :user do
        task :account => :environment do
            user = User.where(email: @email).first||User.last
            Account.create user: user
            puts 'User account successfully created!'
        end
    end
end
