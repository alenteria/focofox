namespace :clean do
  task :previews => :environment do
    Dir.glob("public/.[a-z0-9]**").each do |dir|
      next unless File.directory? dir
      FileUtils.rm_rf(dir)

    end
    puts "previews successfully deleted!"
  end
end

